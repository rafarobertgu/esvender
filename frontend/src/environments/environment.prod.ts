export const environment = {
  production: true,
  backend: {
    server: {
      webpath: 'http://localhost:8001'
    }
  },
  frontend: {
    template: {
      canvas:'/assets/canvas',
      inspinia:'/assets/inspinia',
      vender:'/assets/vender',
      es:'/assets/es'
    },
    sql:false,
    server: {
      webpath: 'http://localhost:4200'
    }
  }
};
