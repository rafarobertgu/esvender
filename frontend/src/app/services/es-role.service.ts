import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsRoles} from "../models/esRoles";

@Injectable({
  providedIn: 'root'
})
export class EsRoleService {
  
  basePath: string = `${environment.backend.server.webpath}/es-roles/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getRoles() {
    return this.http.get(this.basePath);
  }
  createRole(esRole:EsRoles) {
    return this.http.post(this.basePath, esRole);
  }
  getRole(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateRole(id:any, esRole:EsRoles) {
    return this.http.post(this.basePath + id, esRole);
  }
  deleteRole(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
