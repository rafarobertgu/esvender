import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsAttributes} from "../models/esAttributes";

@Injectable({
  providedIn: 'root'
})
export class EsAttributeService {
  
  basePath: string = `${environment.backend.server.webpath}/es-attributes/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getAttributes() {
    return this.http.get(this.basePath);
  }
  createAttribute(esAttribute:EsAttributes) {
    return this.http.post(this.basePath, esAttribute);
  }
  getAttribute(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateAttribute(id:any, esAttribute:EsAttributes) {
    return this.http.post(this.basePath + id, esAttribute);
  }
  deleteAttribute(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
