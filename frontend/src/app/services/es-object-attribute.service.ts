import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsObjectAttributes} from "../models/esObjectAttributes";

@Injectable({
  providedIn: 'root'
})
export class EsObjectAttributeService {
  
  basePath: string = `${environment.backend.server.webpath}/es-object-attributes/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getObjectAttributes() {
    return this.http.get(this.basePath);
  }
  createObjectAttribute(esObjectAttribute:EsObjectAttributes) {
    return this.http.post(this.basePath, esObjectAttribute);
  }
  getObjectAttribute(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateObjectAttribute(id:any, esObjectAttribute:EsObjectAttributes) {
    return this.http.post(this.basePath + id, esObjectAttribute);
  }
  deleteObjectAttribute(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
