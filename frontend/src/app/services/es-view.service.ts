import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsViews} from "../models/esViews";
import {ActivatedRoute, Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class EsViewService {
  
  basePath: string = `${environment.backend.server.webpath}/es-views/api-esvender/`;
  view:EsViews;
  views:EsViews[] = [];
  
  constructor(
    private router: Router,
    private http: HttpClient,
    private route: ActivatedRoute,
  ) {
    //this.getViews();
  }
  
  async go(idView:number, params:object = {}){
    await this.findView(idView);
    if(this.view) {
      if(this.route.snapshot.queryParams['returnUrl']) {
        this.view.vie_route = this.route.snapshot.queryParams['returnUrl'];
      }
      if(this.view.vie_route == null) {
        this.view.vie_route = '/'
      }
      if(Object.keys(params).length) {
        this.router.navigate([this.view.vie_route, params]);
      } else {
        this.router.navigate([this.view.vie_route]);
      }
    }
  }
  
  findView(idView: any) {
    if (environment.frontend.sql) {
      this.view = this.views.find(param => param.id === idView);
    } else {
      this.view = this.views.find(param => param.uid === idView);
    }
    // this.views.forEach(element => {
    //   if (element.items) {
    //     this.buscaritems(element.items, id);
    //   }
    // });
  }
  
  getViews() {
    return this.http.get(this.basePath);
  }
  createView(esView:EsViews) {
    return this.http.post(this.basePath, esView);
  }
  findViewsByModuleId(id:any) {
    return this.http.get(this.basePath + `findViewsByModuleId/${id}`);
  }
  getView(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateView(id:any, esView:EsViews) {
    return this.http.post(this.basePath + id, esView);
  }
  deleteView(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
