import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsProfileModules} from "../models/esProfileModules";

@Injectable({
  providedIn: 'root'
})
export class EsProfileModuleService {
  
  basePath: string = `${environment.backend.server.webpath}/es-profile-modules/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getProfileModules() {
    return this.http.get(this.basePath);
  }
  createProfileModule(esProfileModule:EsProfileModules) {
    return this.http.post(this.basePath, esProfileModule);
  }
  getProfileModule(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateProfileModule(id:any, esProfileModule:EsProfileModules) {
    return this.http.post(this.basePath + id, esProfileModule);
  }
  deleteProfileModule(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
