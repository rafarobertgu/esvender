import { TestBed } from '@angular/core/testing';

import { EsPersonAttributeService } from './es-person-attribute.service';

describe('EsPersonAttributeService', () => {
  let service: EsPersonAttributeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsPersonAttributeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
