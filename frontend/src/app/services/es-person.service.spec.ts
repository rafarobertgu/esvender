import { TestBed } from '@angular/core/testing';

import { EsPersonService } from './es-person.service';

describe('EsPersonService', () => {
  let service: EsPersonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsPersonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
