import { TestBed } from '@angular/core/testing';

import { EsObjectService } from './es-object.service';

describe('EsObjectService', () => {
  let service: EsObjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsObjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
