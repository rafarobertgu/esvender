import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsFileAttributes} from "../models/esFileAttributes";

@Injectable({
  providedIn: 'root'
})
export class EsFileAttributeService {
  
  basePath: string = `${environment.backend.server.webpath}/es-file-attributes/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getFileAttributes() {
    return this.http.get(this.basePath);
  }
  createFileAttribute(esFileAttribute:EsFileAttributes) {
    return this.http.post(this.basePath, esFileAttribute);
  }
  getFileAttribute(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateFileAttribute(id:any, esFileAttribute:EsFileAttributes) {
    return this.http.post(this.basePath + id, esFileAttribute);
  }
  deleteFileAttribute(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
