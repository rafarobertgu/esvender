import { TestBed } from '@angular/core/testing';

import { EsModuleService } from './es-module.service';

describe('EsModuleService', () => {
  let service: EsModuleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsModuleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
