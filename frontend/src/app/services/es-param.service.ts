import { Injectable } from '@angular/core';
import {EsParams} from "../models/esParams";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {SelectItem} from "../models/template/select-item";

@Injectable({
  providedIn: 'root'
})
export class EsParamService {
  
  basePath: string = `${environment.backend.server.webpath}/es-params/api-esvender/`;
  
  esParam:EsParams;
  esParams:EsParams[];
  
  paramRestSuccess:EsParams;
  paramRestError:EsParams;
  
  ParamsRest:EsParams[];
  
  SelectParamsRest: SelectItem[];
  
  constructor(private http: HttpClient) { }
  
  setParamsByDictionaryIds(ids:any[], callback:Function = null) {
    if(ids.length) {
      this.findParamsByDictionaryIds(ids).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsParams[] };
        if (response.data.length) {
          this.esParams = response.data;
          await this.switchParamsByIds();
        }
        if(typeof callback == 'function') {
          callback();
        }
      });
    }
  }
  
  async switchParamsByIds() {
    this.ParamsRest = [];
    
    this.esParams.forEach(async (esParam: EsParams) => {
      switch (esParam.par_dictionry_id.toString()) {
        case "5":
          this.ParamsRest.push(esParam);
          switch (esParam.id.toString()) {
            case "10":
              this.paramRestSuccess = this.esParam;
              break;
            case "11":
              this.paramRestError = this.esParam;
              break;
          }
          break;
      }
    });
  }
  
  getParams() {
    return this.http.get(this.basePath);
  }
  createParam(esParam:EsParams) {
    return this.http.post(this.basePath, esParam);
  }
  getParam(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateParam(id:any, esParam:EsParams) {
    return this.http.post(this.basePath + id, esParam);
  }
  deleteParam(id:any) {
    return this.http.delete(this.basePath + id);
  }
  findParamsByDictionaryIds(ids:any) {
    return this.http.get(this.basePath + `findParamsByDictionaryIds/${ids}`);
  }
}
