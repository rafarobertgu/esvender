import { TestBed } from '@angular/core/testing';

import { EsDictionaryService } from './es-dictionary.service';

describe('EsDictionaryService', () => {
  let service: EsDictionaryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsDictionaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
