import { TestBed } from '@angular/core/testing';

import { EsProfileComponentService } from './es-profile-component.service';

describe('EsProfileComponentService', () => {
  let service: EsProfileComponentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsProfileComponentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
