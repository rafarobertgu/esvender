import { TestBed } from '@angular/core/testing';

import { EsRoleService } from './es-role.service';

describe('EsRoleService', () => {
  let service: EsRoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsRoleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
