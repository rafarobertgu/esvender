import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsObjects} from "../models/esObjects";

@Injectable({
  providedIn: 'root'
})
export class EsObjectService {
  
  basePath: string = `${environment.backend.server.webpath}/es-objects/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getObjects() {
    return this.http.get(this.basePath);
  }
  createObject(esObject:EsObjects) {
    return this.http.post(this.basePath, esObject);
  }
  getObject(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateObject(id:any, esObject:EsObjects) {
    return this.http.post(this.basePath + id, esObject);
  }
  deleteObject(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
