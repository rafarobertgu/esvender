import {Inject, Injectable, NgZone} from '@angular/core';
import {
  AdminScriptStore,
  AdminStyleStore, EsScriptStore, EsStyleStore,
  FrontScriptStore,
  FrontStyleStore,
  VenderScriptStore,
  VenderStyleStore
} from "../file-store.component";

declare var document: any;

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  
  private scripts: any = {};
  private styles: any = {};
  private scriptsByUrl: any = {};
  private stylesByUrl: any = {};
  
  constructor(
    private ngZone:NgZone,
    @Inject('module') public module: String
  ) {
    // this.ngZone.onStable.subscribe(this.onZoneStable);
    // this.ngZone.onUnstable.subscribe(this.onZoneUnstable);
    // this.ngZone.onError.subscribe(this.onZoneError);
    
    switch (module) {
      case 'front':
        FrontStyleStore.forEach(async (style: any) => {
          this.styles[style.name] = {
            loaded: false,
            style: style
          };
          this.stylesByUrl[style.href] = {
            loaded: false,
            style: style
          };
        });
        FrontScriptStore.forEach(async (script: any) => {
          this.scripts[script.name] = {
            loaded: false,
            script: script
          };
          this.scriptsByUrl[script.src] = {
            loaded: false,
            script: script
          };
        });
        break;
      case 'vender':
        VenderStyleStore.forEach(async (style: any) => {
          this.styles[style.name] = {
            loaded: false,
            style: style
          };
          this.stylesByUrl[style.href] = {
            loaded: false,
            style: style
          };
        });
        VenderScriptStore.forEach(async (script: any) => {
          this.scripts[script.name] = {
            loaded: false,
            script: script
          };
          this.scriptsByUrl[script.src] = {
            loaded: false,
            script: script
          };
        });
        break;
      case 'admin':
        AdminStyleStore.forEach(async (style: any) => {
          this.styles[style.name] = {
            loaded: false,
            style: style
          };
          this.stylesByUrl[style.href] = {
            loaded: false,
            style: style
          };
        });
        AdminScriptStore.forEach(async (script: any) => {
          this.scripts[script.name] = {
            loaded: false,
            script: script
          };
          this.scriptsByUrl[script.src] = {
            loaded: false,
            script: script
          };
        });
        break;
      case 'es':
        EsStyleStore.forEach(async (style: any) => {
          this.styles[style.name] = {
            loaded: false,
            style: style
          };
          this.stylesByUrl[style.href] = {
            loaded: false,
            style: style
          };
        });
        EsScriptStore.forEach(async (script: any) => {
          this.scripts[script.name] = {
            loaded: false,
            script: script
          };
          this.scriptsByUrl[script.src] = {
            loaded: false,
            script: script
          };
        });
        break;
    }
  }
  
  async load(styleNames: string[] = null, scriptNames: string[] = null, callback:Function = null) {
    var promises: any[] = [];
    if(styleNames.length) {
      await this.asyncForEach(styleNames, async (styleName) => {
        await promises.push(await this.loadStyle(styleName))
      });
    }
    if(scriptNames.length) {
      await this.asyncForEach(scriptNames, async (scriptName) => {
        await promises.push(await this.loadScript(scriptName))
      });
    }
    if(typeof callback == 'function') await callback();
    return Promise.all(promises);
  }
  
  async loadByUrl(styleUrls: string[] = null, scriptUrls: string[] = null, callback:Function = null) {
    var promises: any[] = [];
    if(styleUrls.length) {
      await this.asyncForEach(styleUrls, async (styleUrl) => {
        await promises.push(await this.loadStyleByUrl(styleUrl))
      });
    }
    if(scriptUrls.length) {
      await this.asyncForEach(scriptUrls, async (scriptUrl) => {
        await promises.push(await this.loadScriptByUrl(scriptUrl))
      });
    }
    if(typeof callback == 'function') await callback();
    return Promise.all(promises);
  }
  
  async loadScript(name: string, callback: Function = null) {
    return await new Promise(async (resolve, reject) => {
      //resolve if already loaded
      try {
        if (this.scripts[name].loaded) {
          await resolve({script: name, loaded: true, status: 'Already Loaded'});
        }
        else {
          //load script
          let script = this.scripts[name].script;
          let scriptElemnt = document.createElement('script');
          scriptElemnt.type = 'text/javascript';
          scriptElemnt.src = script.src;
          if(script.extras != undefined && script.extras.length){
            this.asyncForEach(script.extras,async (extra) => {
              if(Object.keys(extra).length) {
                let name = Object.keys(extra)[0];
                let value = Object.values(extra)[0];
                scriptElemnt[name] = value;
              }
            });
          }
          if (scriptElemnt.readyState) {  //IE
            scriptElemnt.onreadystatechange = async () => {
              if (scriptElemnt.readyState === "loaded" || scriptElemnt.readyState === "complete") {
                scriptElemnt.onreadystatechange = null;
                this.scripts[name].loaded = true;
                await resolve({script: name, loaded: true, status: 'Loaded'});
              }
            };
          } else {  //Others
            scriptElemnt.onload = async () => {
              this.scripts[name].loaded = true;
              await resolve({script: name, loaded: true, status: 'Loaded'});
            };
          }
          scriptElemnt.onerror = (error: any) => resolve({script: name, loaded: false, status: 'Loaded'});
          await document.getElementsByTagName('head')[0].appendChild(scriptElemnt);
        }
      } catch (e) {
        console.log(e);
      }
    });
    if(typeof callback == 'function') {
      callback(true);
    }
  }
  
  async loadScriptByUrl(src: string, callback: Function = null) {
    return await new Promise(async (resolve, reject) => {
      //resolve if already loaded
      try {
        if (this.scriptsByUrl[src].loaded) {
          await resolve({script: src, loaded: true, status: 'Already Loaded'});
        }
        else {
          //load script
          let script = this.scriptsByUrl[src].script;
          let scriptElemnt = document.createElement('script');
          scriptElemnt.type = 'text/javascript';
          scriptElemnt.src = script.src;
          if(script.extras != undefined && script.extras.length){
            this.asyncForEach(script.extras,async (extra) => {
              if(Object.keys(extra).length) {
                let name = Object.keys(extra)[0];
                let value = Object.values(extra)[0];
                scriptElemnt[name] = value;
              }
            });
          }
          if (scriptElemnt.readyState) {  //IE
            scriptElemnt.onreadystatechange = async () => {
              if (scriptElemnt.readyState === "loaded" || scriptElemnt.readyState === "complete") {
                scriptElemnt.onreadystatechange = null;
                this.scriptsByUrl[src].loaded = true;
                await resolve({script: src, loaded: true, status: 'Loaded'});
              }
            };
          } else {  //Others
            scriptElemnt.onload = async () => {
              this.scriptsByUrl[src].loaded = true;
              await resolve({script: src, loaded: true, status: 'Loaded'});
            };
          }
          scriptElemnt.onerror = (error: any) => resolve({script: src, loaded: false, status: 'Loaded'});
          await document.getElementsByTagName('head')[0].appendChild(scriptElemnt);
        }
      } catch (e) {
        console.log(e);
      }
    });
    if(typeof callback == 'function') {
      callback(true);
    }
  }
  
  async loadStyle(name: string) {
    return new Promise(async (resolve, reject) => {
      //resolve if already loaded
      try {
        if (this.styles[name].loaded) {
          await resolve({style: name, loaded: true, status: 'Already Loaded'});
        }
        else {
          //load style
          let style = this.styles[name].style;
          let styleElemnt = document.createElement('link');
          styleElemnt.type = 'text/css';
          styleElemnt.rel = 'stylesheet';
          styleElemnt.href = style.href;
          if(style.extras != undefined && style.extras.length){
            this.asyncForEach(style.extras,async (extra) => {
              if(Object.keys(extra).length) {
                let name = Object.keys(extra)[0];
                let value = Object.values(extra)[0];
                styleElemnt[name] = value;
              }
            });
          }
          if (styleElemnt.readyState) {  //IE
            styleElemnt.onreadystatechange = async () => {
              if (styleElemnt.readyState === "loaded" || styleElemnt.readyState === "complete") {
                styleElemnt.onreadystatechange = null;
                this.styles[name].loaded = true;
                await resolve({style: name, loaded: true, status: 'Loaded'});
              }
            };
          } else {  //Others
            styleElemnt.onload = async () => {
              this.styles[name].loaded = true;
              await resolve({style: name, loaded: true, status: 'Loaded'});
            };
          }
          styleElemnt.onerror = (error: any) => resolve({style: name, loaded: false, status: 'Loaded'});
          await document.getElementsByTagName('head')[0].appendChild(styleElemnt);
        }
      } catch (e) {
        console.log(e)
      }
    });
  }
  
  async loadStyleByUrl(href: string) {
    return new Promise(async (resolve, reject) => {
      //resolve if already loaded
      try {
        if (this.stylesByUrl[href].loaded) {
          await resolve({style: href, loaded: true, status: 'Already Loaded'});
        }
        else {
          //load style
          let style = this.stylesByUrl[href].style;
          let styleElemt = document.createElement('link');
          styleElemt.type = 'text/css';
          styleElemt.rel = 'stylesheet';
          styleElemt.href = style.href;
          if(style.extras != undefined && style.extras.length){
            this.asyncForEach(style.extras,async (extra) => {
              if(Object.keys(extra).length) {
                let name = Object.keys(extra)[0];
                let value = Object.values(extra)[0];
                styleElemt[name] = value;
              }
            });
          }
          if (styleElemt.readyState) {  //IE
            styleElemt.onreadystatechange = async () => {
              if (styleElemt.readyState === "loaded" || styleElemt.readyState === "complete") {
                styleElemt.onreadystatechange = null;
                this.stylesByUrl[href].loaded = true;
                await resolve({style: href, loaded: true, status: 'Loaded'});
              }
            };
          } else {  //Others
            styleElemt.onload = async () => {
              this.stylesByUrl[href].loaded = true;
              resolve({style: href, loaded: true, status: 'Loaded'});
            };
          }
          styleElemt.onerror = (error: any) => resolve({style: href, loaded: false, status: 'Loaded'});
          await document.getElementsByTagName('head')[0].appendChild(styleElemt);
        }
      } catch (e) {
        console.log(e)
      }
    });
  }
  
  async asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }
  
  onZoneStable() {
    console.log('We are stable');
  }
  
  onZoneUnstable() {
    console.log('We are unstable');
  }
  
  onZoneError(error) {
    console.error('Error', error instanceof Error ? error.message : error.toString());
  }
  
}
