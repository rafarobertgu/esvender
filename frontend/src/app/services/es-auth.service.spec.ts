import { TestBed } from '@angular/core/testing';

import { EsAuthService } from './es-auth.service';

describe('EsAuthService', () => {
  let service: EsAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
