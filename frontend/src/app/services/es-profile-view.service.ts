import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsProfileViews} from "../models/esProfileViews";

@Injectable({
  providedIn: 'root'
})
export class EsProfileViewService {
  
  basePath: string = `${environment.backend.server.webpath}/es-profile-views/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getProfileViews() {
    return this.http.get(this.basePath);
  }
  createProfileView(esProfileView:EsProfileViews) {
    return this.http.post(this.basePath, esProfileView);
  }
  getProfileView(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateProfileView(id:any, esProfileView:EsProfileViews) {
    return this.http.post(this.basePath + id, esProfileView);
  }
  deleteProfileView(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
