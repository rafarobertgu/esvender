import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsUsers} from "../models/esUsers";

@Injectable({
  providedIn: 'root'
})
export class EsUserService {
  
  basePath: string = `${environment.backend.server.webpath}/es-users/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getUsers() {
    return this.http.get(this.basePath);
  }
  createUser(esUser:EsUsers) {
    return this.http.post(this.basePath, esUser);
  }
  getUser(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateUser(id:any, esUser:EsUsers) {
    return this.http.post(this.basePath + id, esUser);
  }
  deleteUser(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
