import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsPersonAttributes} from "../models/esPersonAttributes";

@Injectable({
  providedIn: 'root'
})
export class EsPersonAttributeService {
  
  basePath: string = `${environment.backend.server.webpath}/es-person-attributes/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getPersonAttributes() {
    return this.http.get(this.basePath);
  }
  createPersonAttribute(esPersonAttribute:EsPersonAttributes) {
    return this.http.post(this.basePath, esPersonAttribute);
  }
  getPersonAttribute(id:any) {
    return this.http.get(this.basePath + id);
  }
  updatePersonAttribute(id:any, esPersonAttribute:EsPersonAttributes) {
    return this.http.post(this.basePath + id, esPersonAttribute);
  }
  deletePersonAttribute(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
