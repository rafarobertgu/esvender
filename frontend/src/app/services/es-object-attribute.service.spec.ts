import { TestBed } from '@angular/core/testing';

import { EsObjectAttributeService } from './es-object-attribute.service';

describe('EsObjectAttributeService', () => {
  let service: EsObjectAttributeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsObjectAttributeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
