import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsProfiles} from "../models/esProfiles";

@Injectable({
  providedIn: 'root'
})
export class EsProfileService {
  
  basePath: string = `${environment.backend.server.webpath}/es-profiles/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getProfiles() {
    return this.http.get(this.basePath);
  }
  createProfile(esProfile:EsProfiles) {
    return this.http.post(this.basePath, esProfile);
  }
  getProfile(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateProfile(id:any, esProfile:EsProfiles) {
    return this.http.post(this.basePath + id, esProfile);
  }
  deleteProfile(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
