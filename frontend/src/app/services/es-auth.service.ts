import {Inject, Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsUserService} from "./es-user.service";
import {EsAuths} from "../models/esAuths";
import {EsUsers} from "../models/esUsers";
import {BehaviorSubject, Observable} from "rxjs";
import {map} from "rxjs/operators";
import {EsViewService} from "./es-view.service";
import {EsViews} from "../models/esViews";
import {FormGroup} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class EsAuthService {
  
  basePath:string = `${environment.backend.server.webpath}/es-auths`;
  esAuth:EsAuths;
  viewRedirect:EsViews;
  currentUser:EsUsers;
  views:EsViews[] = [];
  viewCover:EsViews;
  viewLogin:EsViews;
  isLoading: boolean = false;
  isSubmitted: boolean = false;
  
  constructor(
    private http: HttpClient,
    private esUserService:EsUserService,
    private esViewService:EsViewService,
    private translateService: TranslateService,
    @Inject('module') public module: String,
    @Inject('moduleId') public moduleId: String
  ) {}
  
  // signup
  // login
  // facebook
  // logout
  
  setData() {
    sessionStorage.setItem('aut_data', JSON.stringify(this.esAuth.aut_data));
  }
  
  setLang() {
    this.translateService.addLangs(['en','es','fr']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      this.translateService.use(browserLang.match(/en|fr|es/) ? browserLang : 'en');
    } else {
      localStorage.setItem('locale', 'en');
      this.translateService.setDefaultLang('en');
    }
  }
  
  changeLang(language:string) {
    localStorage.setItem('locale', language);
    this.translateService.use(language);
  }
  
  getLang() {
    return localStorage.getItem('locale');
  }
  
  async login(loginForm:FormGroup) {
    this.isSubmitted = true;
    if (loginForm.invalid) {
      return;
    }
    this.isLoading = true;
    this.http.post(this.basePath + '/login', loginForm.value).subscribe(async res => {
      let responseLogin = res as { status: string, message: string, data: EsUsers };
      await this.esViewService.findViewsByModuleId(this.moduleId).subscribe(async res => {
        let responseView = res as { status: string, message: string, data: EsViews[] };
        this.views = responseView.data;
        this.esViewService.views = this.views;
        this.views.forEach((view: EsViews) => {
          switch (view.id.toString()) {
            case "1":
              this.viewCover = view;
              this.viewRedirect = view;
              break;
            case "2":
              this.viewLogin = view;
              break;
            case "3":
              this.viewCover = view;
              this.viewRedirect = view;
              break;
            case "4":
              this.viewLogin = view;
              break;
          }
        });
        if (responseLogin.data) {
          this.setUserInfo(this.module + '_user', responseLogin.data);
          this.setViewInfo(this.module + '_cover', this.viewCover);
          this.setViewInfo(this.module + '_login', this.viewLogin);
          this.currentUser = responseLogin.data;
          this.isLoading = false;
          this.esViewService.go(this.viewCover.id);
        }
      });
    })
  }
  
  async signup(signupForm:FormGroup) {
    this.isSubmitted = true;
    if (signupForm.invalid) {
      return;
    }
    this.isLoading = true;
    this.http.post(this.basePath + '/signup', signupForm.value).subscribe(async res => {
      let responseSignup = res as { status: string, message: string, data: EsUsers };
      await this.esViewService.findViewsByModuleId(this.moduleId).subscribe(async res => {
        let responseView = res as { status: string, message: string, data: EsViews[] };
        this.views = responseView.data;
        this.esViewService.views = this.views;
        this.views.forEach((view: EsViews) => {
          switch (view.id.toString()) {
            case "1":
              this.viewCover = view;
              this.viewRedirect = view;
              break;
            case "2":
              this.viewLogin = view;
              break;
            case "3":
              this.viewCover = view;
              this.viewRedirect = view;
              break;
            case "4":
              this.viewLogin = view;
              break;
          }
        });
        if (responseSignup.data) {
          this.setUserInfo(this.module + '_user', responseSignup.data);
          this.setViewInfo(this.module + '_cover', this.viewCover);
          this.setViewInfo(this.module + '_login', this.viewLogin);
          this.currentUser = responseSignup.data;
          this.isLoading = false;
          this.esViewService.go(this.viewCover.id);
        }
      });
    })
  }
  
  logout() {
    return this.http.get(this.basePath + '/logout', {}).subscribe(res => {
      let response = res as { status: string, message: string, data: EsUsers };
      // this.updateAuth()
      this.viewCover = JSON.parse(localStorage.getItem(this.module + '_cover'));
      localStorage.removeItem(this.module + '_user');
      localStorage.removeItem(this.module + '_login');
      localStorage.removeItem(this.module + '_cover');
      this.currentUser = null;
      this.esViewService.go(this.viewCover.id);
    });
  }
  canActivate(){
    let isAuthenteticated:Boolean = false;
    if(localStorage.length) {
      isAuthenteticated = this.isAuthenticated();
    }
    if(isAuthenteticated){
      return true;
    }
    this.esViewService.go(this.viewRedirect.id);
    return false;
  }
  setUserInfo(key, user){
    localStorage.setItem(key, JSON.stringify(user));
  }
  setViewInfo(key, view){
    localStorage.setItem(key, JSON.stringify(view));
  }
  isAuthenticated() : Boolean {
    this.currentUser = JSON.parse(localStorage.getItem(this.module + '_user'));
    this.viewLogin = JSON.parse(localStorage.getItem(this.module + '_login'));
    this.viewCover = JSON.parse(localStorage.getItem(this.module + '_cover'));
    if(this.currentUser){
      return true;
    }
    return false;
  }
  getCurrentUser(): EsUsers {
    let userData = localStorage.getItem(this.module + '_user');
    return JSON.parse(userData);
  }
  
  getAuths() {
    return this.http.get(this.basePath);
  }
  createAuth(esAuth:EsAuths) {
    return this.http.post(this.basePath, esAuth);
  }
  getAuth(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateAuth(id:any, esAuth:EsAuths) {
    return this.http.post(this.basePath + id, esAuth);
  }
  deleteAuth(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
