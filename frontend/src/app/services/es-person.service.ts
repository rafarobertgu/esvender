import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsPeople} from "../models/esPeople";

@Injectable({
  providedIn: 'root'
})
export class EsPersonService {
  
  basePath: string = `${environment.backend.server.webpath}/es-people/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getPeople() {
    return this.http.get(this.basePath);
  }
  createPerson(esPerson:EsPeople) {
    return this.http.post(this.basePath, esPerson);
  }
  getPerson(id:any) {
    return this.http.get(this.basePath + id);
  }
  updatePerson(id:any, esPerson:EsPeople) {
    return this.http.post(this.basePath + id, esPerson);
  }
  deletePerson(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
