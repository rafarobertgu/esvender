import { TestBed } from '@angular/core/testing';

import { EsRoleProfileService } from './es-role-profile.service';

describe('EsRoleProfileService', () => {
  let service: EsRoleProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsRoleProfileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
