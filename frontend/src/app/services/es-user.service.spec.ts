import { TestBed } from '@angular/core/testing';

import { EsUserService } from './es-user.service';

describe('EsUserService', () => {
  let service: EsUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsUserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
