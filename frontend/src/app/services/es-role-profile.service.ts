import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsRoleProfiles} from "../models/esRoleProfiles";

@Injectable({
  providedIn: 'root'
})
export class EsRoleProfileService {
  
  basePath: string = `${environment.backend.server.webpath}/es-role-profiles/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getRoleProfiles() {
    return this.http.get(this.basePath);
  }
  createRoleProfile(esRoleProfile:EsRoleProfiles) {
    return this.http.post(this.basePath, esRoleProfile);
  }
  getRoleProfile(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateRoleProfile(id:any, esRoleProfile:EsRoleProfiles) {
    return this.http.post(this.basePath + id, esRoleProfile);
  }
  deleteRoleProfile(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
