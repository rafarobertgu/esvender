import { TestBed } from '@angular/core/testing';

import { EsProfileViewService } from './es-profile-view.service';

describe('EsProfileViewService', () => {
  let service: EsProfileViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsProfileViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
