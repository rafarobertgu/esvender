import { TestBed } from '@angular/core/testing';

import { EsLogService } from './es-log.service';

describe('EsLogService', () => {
  let service: EsLogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsLogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
