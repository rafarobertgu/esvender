import { TestBed } from '@angular/core/testing';

import { EsAttributeService } from './es-attribute.service';

describe('EsAttributeService', () => {
  let service: EsAttributeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsAttributeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
