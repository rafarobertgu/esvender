import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsModules} from "../models/esModules";

@Injectable({
  providedIn: 'root'
})
export class EsModuleService {
  
  basePath: string = `${environment.backend.server.webpath}/es-modules/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getModules() {
    return this.http.get(this.basePath);
  }
  createModule(esModule:EsModules) {
    return this.http.post(this.basePath, esModule);
  }
  getModule(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateModule(id:any, esModule:EsModules) {
    return this.http.post(this.basePath + id, esModule);
  }
  deleteModule(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
