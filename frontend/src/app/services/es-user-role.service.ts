import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsUserRoles} from "../models/esUserRoles";

@Injectable({
  providedIn: 'root'
})
export class EsUserRoleService {
  
  basePath: string = `${environment.backend.server.webpath}/es-user-roles/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getUserRoles() {
    return this.http.get(this.basePath);
  }
  createUserRole(esUserRole:EsUserRoles) {
    return this.http.post(this.basePath, esUserRole);
  }
  getUserRole(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateUserRole(id:any, esUserRole:EsUserRoles) {
    return this.http.post(this.basePath + id, esUserRole);
  }
  deleteUserRole(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
