import { TestBed } from '@angular/core/testing';

import { EsComponentsService } from './es-components.service';

describe('EsComponentsService', () => {
  let service: EsComponentsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsComponentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
