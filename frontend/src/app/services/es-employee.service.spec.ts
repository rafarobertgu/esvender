import { TestBed } from '@angular/core/testing';

import { EsEmployeeService } from './es-employee.service';

describe('EsEmployeeService', () => {
  let service: EsEmployeeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsEmployeeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
