import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsDictionaries} from "../models/esDictionaries";

@Injectable({
  providedIn: 'root'
})
export class EsDictionaryService {
  
  basePath: string = `${environment.backend.server.webpath}/es-dictionaries/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getDictionaries() {
    return this.http.get(this.basePath);
  }
  createDictionary(esDictionary:EsDictionaries) {
    return this.http.post(this.basePath, esDictionary);
  }
  getDictionary(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateDictionary(id:any, esDictionary:EsDictionaries) {
    return this.http.post(this.basePath + id, esDictionary);
  }
  deleteDictionary(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
