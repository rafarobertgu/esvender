import { Injectable,Output,EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import {environment} from "../../environments/environment";
import {EsComponents} from "../models/esComponents";

@Injectable({
  providedIn: 'root'
})
export class EsComponentsService {
  
  basePath: string = `${environment.backend.server.webpath}/es-components/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getComponents() {
    return this.http.get(this.basePath);
  }
  createComponent(esComponent:EsComponents) {
    return this.http.post(this.basePath, esComponent);
  }
  getComponent(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateComponent(id:any, esComponent:EsComponents) {
    return this.http.post(this.basePath + id, esComponent);
  }
  deleteComponent(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
