import { TestBed } from '@angular/core/testing';

import { EsViewService } from './es-view.service';

describe('EsViewService', () => {
  let service: EsViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
