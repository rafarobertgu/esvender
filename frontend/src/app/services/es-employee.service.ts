import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsEmployees} from "../models/esEmployees";

@Injectable({
  providedIn: 'root'
})
export class EsEmployeeService {
  
  basePath: string = `${environment.backend.server.webpath}/es-employees/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getEmployees() {
    return this.http.get(this.basePath);
  }
  createEmployee(esEmployee:EsEmployees) {
    return this.http.post(this.basePath, esEmployee);
  }
  getEmployee(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateEmployee(id:any, esEmployee:EsEmployees) {
    return this.http.post(this.basePath + id, esEmployee);
  }
  deleteEmployee(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
