import { TestBed } from '@angular/core/testing';

import { EsProfileService } from './es-profile.service';

describe('EsProfileService', () => {
  let service: EsProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsProfileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
