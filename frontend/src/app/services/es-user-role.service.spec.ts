import { TestBed } from '@angular/core/testing';

import { EsUserRoleService } from './es-user-role.service';

describe('EsUserRoleService', () => {
  let service: EsUserRoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsUserRoleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
