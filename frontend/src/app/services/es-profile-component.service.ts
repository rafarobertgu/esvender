import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsProfileComponents} from "../models/esProfileComponents";

@Injectable({
  providedIn: 'root'
})
export class EsProfileComponentService {
  
  basePath: string = `${environment.backend.server.webpath}/es-profile-components/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getProfileComponents() {
    return this.http.get(this.basePath);
  }
  createProfileComponent(esProfileComponent:EsProfileComponents) {
    return this.http.post(this.basePath, esProfileComponent);
  }
  getProfileComponent(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateProfileComponent(id:any, esProfileComponent:EsProfileComponents) {
    return this.http.post(this.basePath + id, esProfileComponent);
  }
  deleteProfileComponent(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
