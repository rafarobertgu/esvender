import { TestBed } from '@angular/core/testing';

import { EsFileService } from './es-file.service';

describe('EsFileService', () => {
  let service: EsFileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsFileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
