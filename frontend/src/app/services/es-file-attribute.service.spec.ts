import { TestBed } from '@angular/core/testing';

import { EsFileAttributeService } from './es-file-attribute.service';

describe('EsFileAttributeService', () => {
  let service: EsFileAttributeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsFileAttributeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
