import { TestBed } from '@angular/core/testing';

import { EsProfileModuleService } from './es-profile-module.service';

describe('EsProfileModuleService', () => {
  let service: EsProfileModuleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsProfileModuleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
