import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsLogs} from "../models/esLogs";

@Injectable({
  providedIn: 'root'
})
export class EsLogService {
  
  basePath: string = `${environment.backend.server.webpath}/es-logs/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getLogs() {
    return this.http.get(this.basePath);
  }
  createLog(esLog:EsLogs) {
    return this.http.post(this.basePath, esLog);
  }
  getLog(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateLog(id:any, esLog:EsLogs) {
    return this.http.post(this.basePath + id, esLog);
  }
  deleteLog(id:any) {
    return this.http.delete(this.basePath + id);
  }
}
