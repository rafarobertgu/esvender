import { TestBed } from '@angular/core/testing';

import { EsParamService } from './es-param.service';

describe('EsParamService', () => {
  let service: EsParamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EsParamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
