import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EsFiles} from "../models/esFiles";

@Injectable({
  providedIn: 'root'
})
export class EsFileService {
  
  basePath: string = `${environment.backend.server.webpath}/es-files/api-esvender/`;
  
  constructor(private http: HttpClient) { }
  
  getFiles() {
    return this.http.get(this.basePath);
  }
  createFile(esFile:EsFiles) {
    return this.http.post(this.basePath, esFile);
  }
  getFile(id:any) {
    return this.http.get(this.basePath + id);
  }
  updateFile(id:any, esFile:EsFiles) {
    return this.http.post(this.basePath + id, esFile);
  }
  deleteFile(id:any) {
    return this.http.delete(this.basePath + id);
  }}
