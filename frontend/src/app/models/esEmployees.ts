import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";

export class EsEmployees {
  id:number;
  uid:string;
  emp_name:string;
  emp_position:string;
  emp_office:string;
  emp_salary:string;
  emp_par_status_id:number;
  emp_par_status_uid:string;
  createdById:number;
  createdByUid:string;
  updatedById:number;
  updatedByUid:string;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  EmpParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


