import {Image} from "./image";
import {Rating} from "./rating";
import {Slide} from "./slide";
import {Coin} from "./coin";

export class ShopItem {
  id:string = '';
  images:Image[] = [];
  saleFlash:string = '';
  addToCart:boolean = true;
  fSlider:Slide;
  url:string = '';
  productTitle:string = '';
  productPrice:number = 0.0;
  productPriceBefore:number = 0.0;
  productCoin:Coin;
  productRating:Rating[] = [];
}


