import {Layer} from "./layer";
import {ShopItem} from "./shop-item";

export class TabShopItem {
  id:string = '';
  title:string = '';
  items:ShopItem[] = [];
}


