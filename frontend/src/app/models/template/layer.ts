export class Layer {
  data_x: string = '';
  data_y: string = '';
  data_customin: string = '';
  data_speed: string = '';
  data_start: string = '';
  data_easing: string = '';
  data_splitin: string = '';
  data_splitout: string = '';
  data_elementdelay: string = '';
  data_endelementdelay: string = '';
  data_endspeed: string = '';
  data_endeasing: string = '';
  classList: string = '';
  text: string = '';
  style: {} = {};
}


