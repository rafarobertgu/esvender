import {Layer} from "./layer";

export class Slide {
  layers: Layer[] = [];
  data_transition: string = '';
  data_slotamount: string = '';
  data_masterspeed: string = '';
  data_delay: string = '';
  data_saveperformance: string = '';
  data_title: string = '';
  data_arrows: string = '';
  data_thumbs: string = '';
  data_animation: string = '';
  data_speed: string = '';
  data_pause: string = '';
  data_pagi: string = '';
  data_lightbox: string = '';
  style: {} = {};
}


