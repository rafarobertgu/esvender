import {EsProfiles} from "./esProfiles";
import {EsViews} from "./esViews";
import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";

export class EsProfileViews {
  id:number;
  uid:string;
  pro_id:number;
  pro_uid:string;
  vie_id:number;
  vie_uid:string;
  pro_vie_par_status_id:number;
  pro_vie_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  Pro:EsProfiles;
  Vie:EsViews;
  ProVieParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


