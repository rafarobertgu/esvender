import {EsUsers} from "./esUsers";
import {EsParams} from "./esParams";
import {EsAttributes} from "./esAttributes";
import {EsFiles} from "./esFiles";

export class EsFileAttributes {
  id:number;
  uid:string;
  fil_id:number;
  fil_uid:string;
  att_id:number;
  att_uid:string;
  fil_att_value:string;
  fil_att_par_status_id:number;
  fil_att_par_status_uid:string;
  createdById:number;
  createdByUid:string;
  updatedById:number;
  updatedByUid:string;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  Fil:EsFiles;
  Att:EsAttributes;
  FilAttParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


