import {EsRoles} from "./esRoles";
import {EsPeople} from "./esPeople";
import {EsFiles} from "./esFiles";
import {EsParams} from "./esParams";
import {EsUserRoles} from "./esUserRoles";
import {EsModules} from "./esModules";

export class EsUsers {
  id:number;
  uid:string;
  usr_id:number;
  usr_username:string;
  usr_password:string;
  usr_mail:string;
  usr_token:string;
  usr_name:string;
  usr_lastname:string;
  usr_par_auth_strategy_id:number;
  usr_par_auth_strategy_uid:string;
  usr_accept_terms:boolean;
  usr_rol_id:number;
  usr_rol_uid:string;
  usr_mod_id:number;
  usr_mod_uid:string;
  usr_person_id:number;
  usr_person_uid:string;
  usr_fil_img_id:number;
  usr_fil_img_uid:string;
  usr_last_login:Date;
  usr_pwd_change:number;
  usr_par_status_uid:string;
  usr_par_status_id:number;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  UsrRol:EsRoles;
  UsrMod:EsModules;
  UsrPerson:EsPeople;
  UsrFilImg:EsFiles;
  UsrParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
  
  UserRoles:EsUserRoles[];
}


