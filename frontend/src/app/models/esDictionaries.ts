export class EsDictionaries {
  id:number;
  uid:string;
  dic_code:string;
  dic_description:string;
  dic_par_status_id:number;
  dic_par_status_uid:string;
  createdById:number;
  createdByUid:string;
  updatedById:number;
  updatedByUid:string;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
}


