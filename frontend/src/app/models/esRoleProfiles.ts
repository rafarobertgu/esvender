import {EsParams} from "./esParams";
import {EsProfiles} from "./esProfiles";
import {EsRoles} from "./esRoles";
import {EsUsers} from "./esUsers";

export class EsRoleProfiles {
  id:number;
  uid:string;
  rol_id:number;
  rol_uid:string;
  pro_id:number;
  pro_uid:string;
  rol_pro_par_status_id:number;
  rol_pro_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  Rol:EsRoles;
  Pro:EsProfiles;
  RolProParStatus:EsParams;
  CreatedById:EsUsers;
  UpdatedByUid:EsUsers;
}


