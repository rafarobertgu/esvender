import {EsUsers} from "./esUsers";
import {EsParams} from "./esParams";
import {EsComponents} from "./esComponents";

export class EsFields {
  id:number;
  uid:string;
  fie_com_id:number;
  fie_com_uid:string;
  fie_code:string;
  fie_description:string;
  fie_par_status_id:number;
  fie_par_status_uid:string;
  fie_par_severity_id:number;
  fie_par_severity_uid:string;
  fie_par_validator_id:number;
  fie_par_validator_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  FieCom:EsComponents;
  FieParStatus:EsParams;
  FieParSeverity:EsParams;
  FieParValidator:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


