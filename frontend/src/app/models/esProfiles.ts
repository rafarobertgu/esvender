import {EsUsers} from "./esUsers";
import {EsParams} from "./esParams";
import {EsProfileComponents} from "./esProfileComponents";
import {EsProfileModules} from "./esProfileModules";
import {EsProfileViews} from "./esProfileViews";

export class EsProfiles {
  id:number;
  uid:string;
  pro_code:string;
  pro_description:string;
  pro_abbr:string;
  pro_par_status_id:number;
  pro_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  ProParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
  
  ProfileComponents:EsProfileComponents[] = [];
  ProfileModules:EsProfileModules[] = [];
  ProfileViews:EsProfileViews[] = [];
}


