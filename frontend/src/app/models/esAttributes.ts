import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";

export class EsAttributes {
  id:number;
  uid:string;
  
  att_par_type_id:number;
  att_par_type_uid:string;
  att_code:string;
  att_description:string;
  att_par_status_id:number;
  att_par_status_uid:string;
  createdById:number;
  createdByUid:string;
  updatedById:number;
  updatedByUid:string;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  AttParType:EsParams;
  AttParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


