import {EsParams} from "./esParams";
import {EsFileAttributes} from "./esFileAttributes";

export class EsFiles {
  id:number;
  uid:string;
  fil_rute:string;
  fil_par_type_id:number;
  fil_par_type_uid:string;
  fil_par_status_id:number;
  fil_par_status_uid:string;
  createdById:number;
  createdByUid:string;
  updatedById:number;
  updatedByUid:string;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  FilParType:EsParams;
  FilParStatus:EsParams;
  
  FileAttributes:EsFileAttributes[] = [];
}


