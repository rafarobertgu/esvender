import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";

export class EsModules {
  id:number;
  uid:string;
  mod_parent_id:number;
  mod_parent_uid:string;
  mod_code:string;
  mod_description:string;
  mod_abbr:string;
  mod_icon:string;
  mod_par_status_id:number;
  mod_par_status_uid:string;
  createdById:string;
  createdByUid:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  ModParent:EsParams;
  ModParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


