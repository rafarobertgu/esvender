import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";
import {EsObjectAttributes} from "./esObjectAttributes";

export class EsObjects {
  id:number;
  uid:string;
  obj_id:number;
  obj_uid:string;
  att_id:number;
  att_uid:string;
  obj_att_default_value:string;
  obj_att_par_editable_id:number;
  obj_att_par_editable_uid:string;
  obj_att_par_interface_behavior_id:number;
  obj_att_par_interface_behavior_uid:string;
  obj_att_par_status_id:number;
  obj_att_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  ObjParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
  
  ObjectAttributes:EsObjectAttributes[] =  [];
}


