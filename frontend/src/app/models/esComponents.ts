import {EsViews} from "./esViews";
import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";

export class EsComponents {
  id:number;
  uid:string;
  vie_id:number;
  vie_uid:string;
  com_code:string;
  com_tag:string;
  com_description:string;
  com_par_type_id:number;
  com_par_type_uid:string;
  com_par_status_id:number;
  com_par_status_uid:string;
  com_parent_id:number;
  com_parent_uid:string;
  createdById:number;
  createdByUid:string;
  updatedById:number;
  updatedByUid:string;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  Vie:EsViews;
  ComParType:EsParams;
  ComParSeverity:EsParams;
  ComParStatus:EsParams;
  ComParent:EsComponents;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


