import {EsParams} from "./esParams";
import {EsPersonAttributes} from "./esPersonAttributes";

export class EsPeople {
  id:number;
  uid:string;
  per_first_name:string;
  per_second_name:string;
  per_first_lastname:string;
  per_second_lastname:string;
  per_license:string;
  per_license_comp:string;
  per_home_address:string;
  per_mail:string;
  per_home_phone:string;
  per_cellphone:string;
  per_birthday:string;
  per_parent_id:number;
  per_parent_uid:string;
  per_par_type_doc_id:number;
  per_par_type_doc_uid:string;
  per_par_city_id:number;
  per_par_city_uid:string;
  per_par_sex_id:number;
  per_par_sex_uid:string;
  per_par_country_id:number;
  per_par_country_uid:string;
  per_par_nacionality_id:number;
  per_par_nacionality_uid:string;
  per_par_status_uid:string;
  per_par_status_id:number;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  PerParent:EsPeople;
  PerParTypeDoc:EsParams;
  PerParCity:EsParams;
  PerParSex:EsParams;
  PerParCountry:EsParams;
  PerParNacionality:EsParams;
  PerParStatus:EsParams;
  
  PersonAttributes:EsPersonAttributes[] = [];
  
}


