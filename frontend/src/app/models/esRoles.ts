import {EsParams} from "./esParams";
import {EsRoleProfiles} from "./esRoleProfiles";

export class EsRoles {
  id:number;
  uid:string;
  rol_code:string;
  rol_description:string;
  rol_abbr:string;
  rol_par_status_id:number;
  rol_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  RolParStatus:EsParams;
  
  RoleProfiles:EsRoleProfiles[] = [];
}


