import {EsUsers} from "./esUsers";
import {EsParams} from "./esParams";
import {EsComponents} from "./esComponents";
import {EsFields} from "./esFields";

export class EsFieldParams {
  id:number;
  uid:string;
  fie_par_status_id:number;
  fie_par_status_uid:string;
  fie_id:number;
  fie_uid:string;
  par_id:number;
  par_uid:string;
  fie_par_severity_id:number;
  fie_par_severity_uid:string;
  fie_par_languaje_id:number;
  fie_par_languaje_uid:string;
  fie_par_description:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  FieParStatus:EsParams;
  Fie:EsFields;
  Par:EsParams;
  FieParSeverity:EsParams;
  FieParLanguaje:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


