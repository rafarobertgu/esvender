import {EsUsers} from "./esUsers";
import {EsParams} from "./esParams";
import {EsComponents} from "./esComponents";
import {EsFields} from "./esFields";

export class EsComponentFields {
  id:number;
  uid:string;
  com_fie_par_status_id:number;
  com_fie_par_status_uid:string;
  com_fie_par_severity_id:number;
  com_fie_par_severity_uid:string;
  com_fie_severity_description:string;
  com_id:number;
  com_uid:string;
  fie_id:number;
  fie_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  ComFieParStatus:EsParams;
  ComFieParSeverity:EsParams;
  Com:EsComponents;
  Fie:EsFields;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


