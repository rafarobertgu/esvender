import {EsUsers} from "./esUsers";
import {EsParams} from "./esParams";
import {EsRoles} from "./esRoles";

export class EsUserRoles {
  id:string;
  uid:string;
  usr_id:string;
  usr_uid:string;
  rol_id:string;
  rol_uid:string;
  usr_rol_par_status_id:string;
  usr_rol_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  Usr:EsUsers;
  Rol:EsRoles;
  UsrRolParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


