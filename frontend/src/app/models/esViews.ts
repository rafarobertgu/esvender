import {EsUsers} from "./esUsers";
import {EsModules} from "./esModules";
import {EsParams} from "./esParams";

export class EsViews {
  id:number;
  uid:string;
  vie_module_id:number;
  vie_module_uid:string;
  vie_code:string;
  vie_description:string;
  vie_route:string;
  vie_params:string;
  vie_return_id:string;
  vie_return_uid:string;
  vie_icon:string;
  vie_par_status_id:number;
  vie_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  VieModule:EsModules;
  VieReturn:EsViews;
  VieParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


