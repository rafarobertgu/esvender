import {EsUsers} from "./esUsers";
import {EsAttributes} from "./esAttributes";
import {EsPeople} from "./esPeople";
import {EsParams} from "./esParams";

export class EsPersonAttributes {
  id:number;
  uid:string;
  per_id:number;
  per_uid:string;
  att_id:number;
  att_uid:string;
  per_att_value:string;
  per_att_par_status_id:number;
  per_att_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  Per:EsPeople;
  Att:EsAttributes;
  PerAttParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;

}


