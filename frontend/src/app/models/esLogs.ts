import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";

export class EsLogs {
  id:number;
  uid:string;
  log_description:string;
  log_par_severity_id:number;
  log_par_severity_uid:string;
  log_par_status_id:number;
  log_par_status_uid:string;
  createdById:number;
  createdByUid:string;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  LogParSeverity:EsParams;
  LogParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


