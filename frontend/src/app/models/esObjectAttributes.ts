import {EsObjects} from "./esObjects";
import {EsAttributes} from "./esAttributes";
import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";

export class EsObjectAttributes {
  id:number;
  uid:string;
  obj_id:number;
  obj_uid:string;
  att_id:number;
  att_uid:string;
  obj_att_default_value:string;
  obj_att_par_editable_id:number;
  obj_att_par_editable_uid:string;
  obj_att_par_interface_behavior_id:number;
  obj_att_par_interface_behavior_uid:string;
  obj_att_par_status_id:number;
  obj_att_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  Obj:EsObjects;
  Att:EsAttributes;
  ObjAttParEditable:EsParams;
  ObjAttParInterfaceBehavior:EsParams;
  ObjAttParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


