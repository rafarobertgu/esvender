import {EsProfiles} from "./esProfiles";
import {EsComponents} from "./esComponents";
import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";

export class EsProfileComponents {
  id:number;
  uid:string;
  pro_id:number;
  pro_uid:string;
  com_id:number;
  com_uid:string;
  pro_com_par_status_id:number;
  pro_com_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  Pro:EsProfiles;
  Com:EsComponents;
  ProComParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


