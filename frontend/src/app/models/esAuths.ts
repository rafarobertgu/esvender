import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";

export class EsAuths {
  id:number;
  uid:string;
  aut_par_status_id:number;
  aut_par_status_uid:string;
  usr_id:number;
  usr_uid:string;
  aut_data:string;
  createdById:number;
  createdByUid:string;
  updatedById:number;
  updatedByUid:string;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  AutParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


