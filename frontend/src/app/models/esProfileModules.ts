import {EsProfiles} from "./esProfiles";
import {EsModules} from "./esModules";
import {EsParams} from "./esParams";
import {EsUsers} from "./esUsers";

export class EsProfileModules {
  id:number;
  uid:string;
  pro_id:number;
  pro_uid:string;
  mod_id:number;
  mod_uid:string;
  pro_mod_par_status_id:number;
  pro_mod_par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  Pro:EsProfiles;
  Mod:EsModules;
  ProModParStatus:EsParams;
  CreatedBy:EsUsers;
  UpdatedBy:EsUsers;
}


