export class EsParams {
  id:number;
  uid:string;
  par_cod:string;
  par_description:string;
  par_abbr:string;
  par_dictionry_id:number;
  par_dictionry_uid:string;
  par_order:string;
  par_status_id:number;
  par_status_uid:string;
  createdByUid:string;
  createdById:number;
  updatedByUid:string;
  updatedById:number;
  dueAt:Date;
  createdAt:Date;
  updatedAt:Date;
  
  ParDictionry:EsParams;
  ParStatus:EsParams;
}


