import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../services/common.service";
import {environment} from "../../../../environments/environment";
declare var oCanvas:any;

@Component({
  selector: 'app-header-index',
  templateUrl: './header-index.component.html',
  styleUrls: ['./header-index.component.scss']
})
export class HeaderIndexComponent implements OnInit {
  
  @ViewChild('IndexVenderHeaderComponent', {static: true}) IndexVenderHeaderComponent;
  
  templatePath:string = `${environment.frontend.template.vender}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([],[
      this.templatePath + '/js/jquery.js',
    ]);
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.IndexVenderHeaderComponent);
  }

}
