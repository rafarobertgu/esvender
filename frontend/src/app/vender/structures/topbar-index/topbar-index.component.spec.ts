import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbarIndexComponent } from './topbar-index.component';

describe('TopbarIndexComponent', () => {
  let component: TopbarIndexComponent;
  let fixture: ComponentFixture<TopbarIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopbarIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbarIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
