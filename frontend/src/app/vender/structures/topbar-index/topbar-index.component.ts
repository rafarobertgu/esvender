import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {EsViewService} from "../../../services/es-view.service";
import {EsViews} from "../../../models/esViews";
import {EsParams} from "../../../models/esParams";
import {EsAuthService} from "../../../services/es-auth.service";
import {EsUsers} from "../../../models/esUsers";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-topbar-index',
  templateUrl: './topbar-index.component.html',
  styleUrls: ['./topbar-index.component.scss']
})
export class TopbarIndexComponent implements OnInit {
  
  @ViewChild('TopbarIndexComponent', {static: true}) TopbarIndexComponent;
  templatePath:string = `${environment.frontend.template.vender}`;
  venderViewLogin:EsViews;
  venderLoginForm: FormGroup;
  venderUser:EsUsers;
  isLoading:boolean = false;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    public esViewService: EsViewService,
    public esAuthService: EsAuthService,
    private fb: FormBuilder,
    public translate: TranslateService
  ) {
    this.venderUser = new EsUsers();
    this.venderLoginForm = this.fb.group({
      usr_username: new FormControl('', Validators.required),
      usr_password: new FormControl('', Validators.required),
    });
  }
  
  get f() { return this.venderLoginForm.controls; }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.TopbarIndexComponent);
  }

}
