import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VenderComponent} from "./vender.component";
import {LoginComponent} from "./pages/login/login.component";
import {SignupComponent} from "./pages/signup/signup.component";


const routes: Routes = [
  { path: '', component: VenderComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent, pathMatch: 'full'},
  { path: 'signup', component: SignupComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class VenderRoutingModule { }
