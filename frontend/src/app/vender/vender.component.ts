import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Slide} from "../models/template/slide";
import {environment} from "../../environments/environment";
import {CommonService} from "../services/common.service";
import {EsViewService} from "../services/es-view.service";
import {EsAuthService} from "../services/es-auth.service";
import {EsUsers} from "../models/esUsers";
import {TranslateService} from "@ngx-translate/core";
declare var oCanvas:any;

declare var $:any;
@Component({
  selector: 'app-vender',
  templateUrl: './vender.component.html',
  styleUrls: ['./vender.component.scss']
})
export class VenderComponent implements OnInit, AfterViewInit {
  
  slides: Slide[] = [];
  templatePath:string = `${environment.frontend.template.vender}`;
  cssPath:string = `${environment.frontend.template.venderCSS}`;
  jsPath:string = `${environment.frontend.template.venderJS}`;
  venderUser:EsUsers;
  
  constructor(
    private commonService: CommonService,
    public esAuthService: EsAuthService
  ) {
    this.commonService.loadByUrl([
      this.cssPath + "/css/bootstrap.css",
      this.cssPath + "/style.css",
      this.cssPath + "/css/dark.css",
      this.cssPath + "/css/font-icons.css",
      this.cssPath + "/css/animate.css",
      this.cssPath + "/css/magnific-popup.css",
      this.cssPath + "/css/responsive.css",
    ],[
      this.jsPath + '/js/jquery.js',
      this.jsPath + '/js/crisdomson.js',
      this.jsPath + '/js/plugins.js',
      this.jsPath + '/js/functions.js',
    ], () => {
      oCanvas.reload();
    });
    this.esAuthService.setLang();
  }
  
  ngOnInit(): void {
    if (this.esAuthService.isAuthenticated()) {
      this.venderUser = this.esAuthService.getCurrentUser();
    }
  }

  ngAfterViewInit(): void {
  
  }
}
