import {AfterViewInit, Component, OnInit} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {CommonService} from "../../../services/common.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {EsUsers} from "../../../models/esUsers";
import {EsViewService} from "../../../services/es-view.service";
import {EsAuthService} from "../../../services/es-auth.service";
import {AlertService} from "../../../services/alert.service";
import {EsParamService} from "../../../services/es-param.service";
declare var oCanvas:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  
  templatePath:string = `${environment.frontend.template.vender}`;
  cssPath:string = `${environment.frontend.template.venderCSS}`;
  jsPath:string = `${environment.frontend.template.venderJS}`;
  venderLoginForm: FormGroup;
  venderUser:EsUsers;
  isLoading:boolean = false;
  
  constructor(
    private commonService: CommonService,
    private esParamService: EsParamService,
    public esAuthService: EsAuthService,
    private fb: FormBuilder,
    private alertService: AlertService,
    private esViewService: EsViewService
  ) {
    
    this.commonService.loadByUrl([
      this.cssPath + "/css/bootstrap.css",
      this.cssPath + "/style.css",
      this.cssPath + "/css/dark.css",
      this.cssPath + "/css/font-icons.css",
      this.cssPath + "/css/animate.css",
      this.cssPath + "/css/magnific-popup.css",
      this.cssPath + "/css/responsive.css",
    ],[
      this.jsPath + '/js/jquery.js',
      this.jsPath + '/js/crisdomson.js',
      this.jsPath + '/js/plugins.js',
      this.jsPath + '/js/functions.js'
    ], () => {
      oCanvas.reload();
    });
    this.esAuthService.setLang();
    this.venderUser = new EsUsers();
    this.venderLoginForm = this.fb.group({
      usr_username: new FormControl('', Validators.required),
      usr_password: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
    if (this.esAuthService.isAuthenticated()) {
      this.esViewService.go(this.esAuthService.viewCover.id);
    }
  }
  
  get f() { return this.venderLoginForm.controls; }
  
  ngAfterViewInit(): void {

  }
}
