import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../services/common.service";
import {EsParamService} from "../../../services/es-param.service";
import {EsAuthService} from "../../../services/es-auth.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AlertService} from "../../../services/alert.service";
import {EsViewService} from "../../../services/es-view.service";
import {EsUsers} from "../../../models/esUsers";
import {environment} from "../../../../environments/environment";
import {MustMatch} from "../../../helpers/must-match.validator";
import {TranslateService} from "@ngx-translate/core";

declare var oCanvas:any;
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  
  templatePath:string = `${environment.frontend.template.vender}`;
  cssPath:string = `${environment.frontend.template.venderCSS}`;
  jsPath:string = `${environment.frontend.template.venderJS}`;
  venderSignupForm: FormGroup;
  venderUser:EsUsers;
  isLoading:boolean = false;
  
  constructor(
    private commonService: CommonService,
    private esParamService: EsParamService,
    public esAuthService: EsAuthService,
    private fb: FormBuilder,
    private alertService: AlertService,
    private esViewService: EsViewService,
  ) {
    
    this.commonService.loadByUrl([
      this.cssPath + "/css/bootstrap.css",
      this.cssPath + "/style.css",
      this.cssPath + "/css/dark.css",
      this.cssPath + "/css/font-icons.css",
      this.cssPath + "/css/animate.css",
      this.cssPath + "/css/magnific-popup.css",
      this.cssPath + "/css/responsive.css",
    ],[
      this.jsPath + '/js/jquery.js',
      this.jsPath + '/js/crisdomson.js',
      this.jsPath + '/js/plugins.js',
      this.jsPath + '/js/functions.js'
    ], () => {
      oCanvas.reload();
    });
    this.esAuthService.setLang();
    this.venderUser = new EsUsers();
    this.venderSignupForm = this.fb.group({
      usr_name: new FormControl('', Validators.required),
      usr_username: new FormControl('', Validators.required),
      usr_password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      usr_confirm_password: new FormControl('', Validators.required),
      usr_accept_terms: new FormControl('', Validators.required),
    }, {
      validator: MustMatch('usr_password','usr_confirm_password')
    });
  }
  
  get f() { return this.venderSignupForm.controls; }
  
  ngOnInit(): void {
    if (this.esAuthService.isAuthenticated()) {
      this.esViewService.go(this.esAuthService.viewCover.id);
    }
  }
}
