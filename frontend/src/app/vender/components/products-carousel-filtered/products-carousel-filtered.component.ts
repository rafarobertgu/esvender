import {AfterViewChecked, AfterViewInit, Component, NgZone, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {TabShopItem} from "../../../models/template/tab-shop-item";
import {CommonService} from "../../../services/common.service";
import {Slide} from "../../../models/template/slide";
import {Coin} from "../../../models/template/coin";
import {Rating} from "../../../models/template/rating";
import {ShopItem} from "../../../models/template/shop-item";
import {Image} from "../../../models/template/image";
@Component({
  selector: 'app-products-carousel-filtered',
  templateUrl: './products-carousel-filtered.component.html',
  styleUrls: ['./products-carousel-filtered.component.scss']
})
export class ProductsCarouselFilteredComponent implements OnInit, AfterViewChecked {
  
  @ViewChild('ProductsCarouselFilteredComponent', {static: true}) ProductsCarouselFilteredComponent;
  
  templatePath: string = `${environment.frontend.template.vender}`;
  tabs:TabShopItem[] = [];
  
  constructor(
    private ngZone:NgZone,
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([], [
      this.templatePath + '/js/jquery.js',
      this.templatePath + '/js/libs/products-carousel-filtered.js'
    ]);
    
    let tab1:TabShopItem = new TabShopItem();
    let tab2:TabShopItem = new TabShopItem();
    let tab3:TabShopItem = new TabShopItem();
    let fSlider:Slide = new Slide();
    let coin = new Coin();
    
    fSlider.data_arrows = 'false';
    
    let rating_star3 = new Rating();
    rating_star3.class_icon = 'icon-star3';
    let rating_star_half_full = new Rating();
    rating_star_half_full.class_icon = 'icon-star-half-full';
    let rating_star_empty = new Rating();
    rating_star_empty.class_icon = 'icon-star-empty';
    
    coin.name = 'Dollar';
    coin.symbol = '$';
    
    tab1.id = 'tabs-9';
    tab1.title = 'New Arrivals';
    tab2.id = 'tabs-10';
    tab2.title = 'Best Sellers';
    tab3.id = 'tabs-11';
    tab3.title = 'You may like';
    
    let shopItem1:ShopItem;
    let shopItem2:ShopItem;
    let shopItem3:ShopItem;
    let shopItem4:ShopItem;
    
    let image1:Image;
    let image2:Image;
    let image3:Image;
    
    // tab 1
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/dress/1.jpg";
    image1.alt = 'Checked Short Dress';
    image2.src = this.templatePath + "/images/shop/dress/1-1.jpg";
    image2.alt = 'Checked Short Dress';
    shopItem1 = new ShopItem();
    shopItem1.addToCart = true;
    shopItem1.saleFlash = '50% Off*';
    shopItem1.fSlider = fSlider;
    shopItem1.url = '/assets/include/ajax/shop-item.html';
    shopItem1.productTitle = 'Checked Short Dress';
    shopItem1.productPrice = 24.99;
    shopItem1.productCoin = coin;
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star_half_full);
    shopItem1.images.push(image1);
    shopItem1.images.push(image2);
    tab1.items.push(shopItem1);
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/dress/1.jpg";
    image1.alt = 'Checked Short Dress';
    image2.src = this.templatePath + "/images/shop/dress/1-1.jpg";
    image2.alt = 'Checked Short Dress';
    shopItem1 = new ShopItem();
    shopItem1.addToCart = true;
    shopItem1.saleFlash = '50% Off*';
    shopItem1.fSlider = fSlider;
    shopItem1.url = '/assets/include/ajax/shop-item.html';
    shopItem1.productTitle = 'Checked Short Dress';
    shopItem1.productPrice = 24.99;
    shopItem1.productCoin = coin;
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star_half_full);
    shopItem1.images.push(image1);
    shopItem1.images.push(image2);
    tab1.items.push(shopItem1);
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/pants/1-1.jpg";
    image1.alt = 'Slim Fit Chinos';
    image2.src = this.templatePath + "/images/shop/pants/1.jpg";
    image2.alt = 'Slim Fit Chinos';
    shopItem2 = new ShopItem();
    shopItem2.addToCart = true;
    shopItem2.fSlider = fSlider;
    shopItem2.url = '/assets/include/ajax/shop-item.html';
    shopItem2.productTitle = 'Slim Fit Chinos';
    shopItem2.productPrice = 39.99;
    shopItem2.productCoin = coin;
    shopItem2.productRating.push(rating_star3);
    shopItem2.productRating.push(rating_star3);
    shopItem2.productRating.push(rating_star3);
    shopItem2.productRating.push(rating_star_half_full);
    shopItem2.productRating.push(rating_star_empty);
    shopItem2.images.push(image1);
    shopItem2.images.push(image2);
    tab1.items.push(shopItem2);
    
    image1 = new Image();
    image2 = new Image();
    image3 = new Image();
    image1.src = this.templatePath + "/images/shop/shoes/1.jpg";
    image1.alt = 'Dark Brown Boots';
    image2.src = this.templatePath + "/images/shop/shoes/1-1.jpg";
    image2.alt = 'Dark Brown Boots';
    image3.src = this.templatePath + "/images/shop/shoes/1-2.jpg";
    image3.alt = 'Dark Brown Boots';
    shopItem3 = new ShopItem();
    shopItem3.addToCart = true;
    shopItem3.fSlider = fSlider;
    shopItem3.url = '/assets/include/ajax/shop-item.html';
    shopItem3.productTitle = 'Dark Brown Boots';
    shopItem3.productPrice = 49;
    shopItem3.productCoin = coin;
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star_empty);
    shopItem3.productRating.push(rating_star_empty);
    shopItem3.images.push(image1);
    shopItem3.images.push(image2);
    // shopItem3.images.push(image3);
    tab1.items.push(shopItem3);
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/dress/2.jpg";
    image1.alt = 'Light Blue Denim Dress';
    image2.src = this.templatePath + "/images/shop/dress/2-2.jpg";
    image2.alt = 'Light Blue Denim Dress';
    shopItem4 = new ShopItem();
    shopItem4.addToCart = true;
    shopItem4.fSlider = fSlider;
    shopItem4.url = '/assets/include/ajax/shop-item.html';
    shopItem4.productTitle = 'Light Blue Denim Dress';
    shopItem4.productPrice = 19.95;
    shopItem4.productCoin = coin;
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star_empty);
    shopItem4.images.push(image1);
    shopItem4.images.push(image2);
    tab1.items.push(shopItem4);
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/dress/2.jpg";
    image1.alt = 'Light Blue Denim Dress';
    image2.src = this.templatePath + "/images/shop/dress/2-2.jpg";
    image2.alt = 'Light Blue Denim Dress';
    shopItem4 = new ShopItem();
    shopItem4.addToCart = true;
    shopItem4.fSlider = fSlider;
    shopItem4.url = '/assets/include/ajax/shop-item.html';
    shopItem4.productTitle = 'Light Blue Denim Dress';
    shopItem4.productPrice = 19.95;
    shopItem4.productCoin = coin;
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star_empty);
    shopItem4.images.push(image1);
    shopItem4.images.push(image2);
    tab1.items.push(shopItem4);
    
    // tab 2
    
    // item 1
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/sunglasses/1.jpg";
    image1.alt = 'Unisex Sunglasses';
    image2.src = this.templatePath + "/images/shop/sunglasses/1-1.jpg";
    image2.alt = 'Unisex Sunglasses';
    shopItem1 = new ShopItem();
    shopItem1.addToCart = true;
    shopItem1.saleFlash = 'Sale!';
    shopItem1.fSlider = fSlider;
    shopItem1.url = '/assets/include/ajax/shop-item.html';
    shopItem1.productTitle = 'Unisex Sunglasses';
    shopItem1.productPrice = 19.99;
    shopItem1.productPriceBefore = 11.99;
    shopItem1.productCoin = coin;
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star_empty);
    shopItem1.productRating.push(rating_star_empty);
    shopItem1.images.push(image1);
    shopItem1.images.push(image2);
    tab2.items.push(shopItem1);
    
    // item 2
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/tshirts/1.jpg";
    image1.alt = 'Blue Round-Neck Tshirt';
    image2.src = this.templatePath + "/images/shop/tshirts/1-1.jpg";
    image2.alt = 'Blue Round-Neck Tshirt';
    shopItem2 = new ShopItem();
    shopItem2.addToCart = true;
    shopItem2.fSlider = fSlider;
    shopItem2.url = '/assets/include/ajax/shop-item.html';
    shopItem2.productTitle = 'Blue Round-Neck Tshirt';
    shopItem2.productPrice = 9.99;
    shopItem2.productCoin = coin;
    shopItem2.productRating.push(rating_star3);
    shopItem2.productRating.push(rating_star3);
    shopItem2.productRating.push(rating_star3);
    shopItem2.productRating.push(rating_star_half_full);
    shopItem2.productRating.push(rating_star_empty);
    shopItem2.images.push(image1);
    shopItem2.images.push(image2);
    tab2.items.push(shopItem2);
    
    // item 3
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/watches/1.jpg";
    image1.alt = 'Silver Chrome Watch';
    image2.src = this.templatePath + "/images/shop/watches/1-1.jpg";
    image2.alt = 'Silver Chrome Watch';
    shopItem3 = new ShopItem();
    shopItem3.addToCart = true;
    shopItem3.fSlider = fSlider;
    shopItem3.url = '/assets/include/ajax/shop-item.html';
    shopItem3.productTitle = 'Silver Chrome Watch';
    shopItem3.productPrice = 129.99;
    shopItem3.productCoin = coin;
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star_empty);
    shopItem3.images.push(image1);
    shopItem3.images.push(image2);
    shopItem3.images.push(image3);
    tab2.items.push(shopItem3);
    
    // item 4
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/shoes/2.jpg";
    image1.alt = 'Men Grey Casual Shoes';
    image2.src = this.templatePath + "/images/shop/shoes/2-1.jpg";
    image2.alt = 'Men Grey Casual Shoes';
    shopItem4 = new ShopItem();
    shopItem4.addToCart = true;
    shopItem4.saleFlash = 'Sale!';
    shopItem4.fSlider = fSlider;
    shopItem4.url = '/assets/include/ajax/shop-item.html';
    shopItem4.productTitle = 'Men Grey Casual Shoes';
    shopItem4.productPrice = 39.49;
    shopItem4.productPriceBefore = 45.99;
    shopItem4.productCoin = coin;
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star_half_full);
    shopItem4.productRating.push(rating_star_empty);
    shopItem4.productRating.push(rating_star_empty);
    shopItem4.images.push(image1);
    shopItem4.images.push(image2);
    tab2.items.push(shopItem4);
    
    // tab 3
    
    // item 1
    
    image1 = new Image();
    image2 = new Image();
    image3 = new Image();
    image1.src = this.templatePath + "/images/shop/dress/3.jpg";
    image1.alt = 'Pink Printed Dress';
    image2.src = this.templatePath + "/images/shop/dress/3-1.jpg";
    image2.alt = 'Pink Printed Dress';
    image3.src = this.templatePath + "/images/shop/dress/3-2.jpg";
    image3.alt = 'Pink Printed Dress';
    shopItem1 = new ShopItem();
    shopItem1.fSlider = fSlider;
    shopItem1.addToCart = true;
    shopItem1.url = '/assets/include/ajax/shop-item.html';
    shopItem1.productTitle = 'Pink Printed Dress';
    shopItem1.productPrice = 39.49;
    shopItem1.productCoin = coin;
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star3);
    shopItem1.productRating.push(rating_star_empty);
    shopItem1.productRating.push(rating_star_empty);
    shopItem1.images.push(image1);
    shopItem1.images.push(image2);
    shopItem1.images.push(image3);
    tab3.items.push(shopItem1);
    
    // item 2
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/pants/5.jpg";
    image1.alt = 'Green Trousers';
    image2.src = this.templatePath + "/images/shop/pants/5-1.jpg";
    image2.alt = 'Green Trousers';
    shopItem2 = new ShopItem();
    shopItem2.addToCart = true;
    shopItem2.saleFlash = 'Sale!';
    shopItem2.fSlider = fSlider;
    shopItem2.url = '/assets/include/ajax/shop-item.html';
    shopItem2.productTitle = 'Blue Round-Neck Tshirt';
    shopItem2.productPrice = 9.99;
    shopItem2.productCoin = coin;
    shopItem2.productRating.push(rating_star3);
    shopItem2.productRating.push(rating_star3);
    shopItem2.productRating.push(rating_star3);
    shopItem2.productRating.push(rating_star_half_full);
    shopItem2.productRating.push(rating_star_empty);
    shopItem2.images.push(image1);
    shopItem2.images.push(image2);
    tab3.items.push(shopItem2);
    
    // item 3
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/sunglasses/2.jpg";
    image1.alt = 'Men Aviator Sunglasses';
    image2.src = this.templatePath + "/images/shop/sunglasses/2-1.jpg";
    image2.alt = 'Men Aviator Sunglasses';
    shopItem3 = new ShopItem();
    shopItem3.addToCart = true;
    shopItem3.fSlider = fSlider;
    shopItem3.url = '/assets/include/ajax/shop-item.html';
    shopItem3.productTitle = 'Men Aviator Sunglasses';
    shopItem3.productPrice = 13.49;
    shopItem3.productCoin = coin;
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star3);
    shopItem3.productRating.push(rating_star_empty);
    shopItem3.images.push(image1);
    shopItem3.images.push(image2);
    shopItem3.images.push(image3);
    tab3.items.push(shopItem3);
    
    // item 4
    
    image1 = new Image();
    image2 = new Image();
    image1.src = this.templatePath + "/images/shop/tshirts/4.jpg";
    image1.alt = 'Black Polo Tshirt';
    image2.src = this.templatePath + "/images/shop/tshirts/4-1.jpg";
    image2.alt = 'Black Polo Tshirt';
    shopItem4 = new ShopItem();
    shopItem4.addToCart = true;
    shopItem4.fSlider = fSlider;
    shopItem4.url = '/assets/include/ajax/shop-item.html';
    shopItem4.productTitle = 'Black Polo Tshirt';
    shopItem4.productPrice = 11.49;
    shopItem4.productCoin = coin;
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.productRating.push(rating_star3);
    shopItem4.images.push(image1);
    shopItem4.images.push(image2);
    tab3.items.push(shopItem4);
    
    this.tabs.push(tab1);
    this.tabs.push(tab2);
    this.tabs.push(tab3);
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.ProductsCarouselFilteredComponent);
  }
  
  ngAfterViewChecked(): void {
  }
  
}
