import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsCarouselFilteredComponent } from './products-carousel-filtered.component';

describe('ProductsCarouselFilteredComponent', () => {
  let component: ProductsCarouselFilteredComponent;
  let fixture: ComponentFixture<ProductsCarouselFilteredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsCarouselFilteredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsCarouselFilteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
