import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrivalsSellersLikeComponent } from './arrivals-sellers-like.component';

describe('ArrivalsSellersLikeComponent', () => {
  let component: ArrivalsSellersLikeComponent;
  let fixture: ComponentFixture<ArrivalsSellersLikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArrivalsSellersLikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrivalsSellersLikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
