import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerProductsPromotionComponent } from './banner-products-promotion.component';

describe('BannerProductsPromotionComponent', () => {
  let component: BannerProductsPromotionComponent;
  let fixture: ComponentFixture<BannerProductsPromotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerProductsPromotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerProductsPromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
