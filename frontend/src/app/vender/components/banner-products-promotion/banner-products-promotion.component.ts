import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-banner-products-promotion',
  templateUrl: './banner-products-promotion.component.html',
  styleUrls: ['./banner-products-promotion.component.scss']
})
export class BannerProductsPromotionComponent implements OnInit {
  
  @ViewChild('BannerProductsPromotionComponent', {static: true}) BannerProductsPromotionComponent;
  
  templatePath: string = `${environment.frontend.template.vender}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.BannerProductsPromotionComponent);
  }

}
