import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-top-cart-index',
  templateUrl: './top-cart-index.component.html',
  styleUrls: ['./top-cart-index.component.scss']
})
export class TopCartIndexComponent implements OnInit {
  
  @ViewChild('TopCartIndexComponent', {static: true}) TopCartIndexComponent;
  
  templatePath:string = `${environment.frontend.template.vender}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.TopCartIndexComponent);
  }

}
