import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopCartIndexComponent } from './top-cart-index.component';

describe('TopCartIndexComponent', () => {
  let component: TopCartIndexComponent;
  let fixture: ComponentFixture<TopCartIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopCartIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopCartIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
