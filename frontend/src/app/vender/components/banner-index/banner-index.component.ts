import {Component, Input, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {Slide} from "../../../models/template/slide";
import {environment} from "../../../../environments/environment";
import {CommonService} from "../../../services/common.service";
import {Layer} from "../../../models/template/layer";

@Component({
  selector: 'app-banner-index',
  templateUrl: './banner-index.component.html',
  styleUrls: ['./banner-index.component.scss']
})
export class BannerIndexComponent implements OnInit {
  
  @ViewChild('BannerIndexComponent', {static: true}) BannerIndexComponent;
  @Input() slides:Slide[] = [];
  templatePath:string = `${environment.frontend.template.vender}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      this.templatePath + '/include/rs-plugin/css/settings.css'
    ], [
      this.templatePath + '/js/jquery.js',
      this.templatePath + '/include/rs-plugin/js/jquery.themepunch.revolution.min.js',
      this.templatePath + '/include/rs-plugin/js/jquery.themepunch.tools.min.js',
      this.templatePath + '/js/libs/banners.js',
    ]);
    
    let slide1:Slide = new Slide();
    let slide2:Slide = new Slide();
    
    let layer1 = new Layer();
    let layer2 = new Layer();
    let layer3 = new Layer();
    let layer4 = new Layer();
    let layer5 = new Layer();
    let layer6 = new Layer();
    let layer7 = new Layer();
    let layer8 = new Layer();
    let layer9 = new Layer();
    let layer10 = new Layer();
    let layer11 = new Layer();
    
    // Slide 1
    
    layer1.data_x = "100";
    layer1.data_y = "50";
    layer1.data_customin = "x:-200;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer1.data_speed = "400";
    layer1.data_start = "1000";
    layer1.data_easing = "easeOutQuad";
    layer1.data_splitin = "none";
    layer1.data_splitout = "none";
    layer1.data_elementdelay = "0.01";
    layer1.data_endelementdelay = "0.1";
    layer1.data_endspeed = "1000";
    layer1.data_endeasing = "Power4.easeIn";
    layer1.classList = 'revo-slider-caps-text uppercase';
    layer1.text = '<img src="/assets/canvas/images/slider/rev/shop/girl1.jpg" alt="Girl">';
    
    layer2.data_x = "570";
    layer2.data_y = "75";
    layer2.data_customin = "x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer2.data_speed = "700";
    layer2.data_start = "1000";
    layer2.data_easing = "easeOutQuad";
    layer2.data_splitin = "none";
    layer2.data_splitout = "none";
    layer2.data_elementdelay = "0.01";
    layer2.data_endelementdelay = "0.1";
    layer2.data_endspeed = "1000";
    layer2.data_endeasing = "Power4.easeIn";
    layer2.style = {'color':'#333'};
    layer2.classList = 'revo-slider-caps-text uppercase';
    layer2.text = "Get your Shopping Bags Ready";
    
    layer3.data_x = "570";
    layer3.data_y = "105";
    layer3.data_customin = "x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer3.data_speed = "700";
    layer3.data_start = "1200";
    layer3.data_easing = "easeOutQuad";
    layer3.data_splitin = "none";
    layer3.data_splitout = "none";
    layer3.data_elementdelay = "0.01";
    layer3.data_endelementdelay = "0.1";
    layer3.data_endspeed = "1000";
    layer3.data_endeasing = "Power4.easeIn";
    layer3.style = {'color':'#333', 'max-width':'430px', 'white-space':'normal', 'line-height':'1.15'};
    layer3.classList = 'revo-slider-emphasis-text nopadding noborder';
    layer3.text = "Latest Fashion Collections";
    
    layer4.data_x = "570";
    layer4.data_y = "275";
    layer4.data_customin = "x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer4.data_speed = "700";
    layer4.data_start = "1400";
    layer4.data_easing = "easeOutQuad";
    layer4.data_splitin = "none";
    layer4.data_splitout = "none";
    layer4.data_elementdelay = "0.01";
    layer4.data_endelementdelay = "0.1";
    layer4.data_endspeed = "1000";
    layer4.data_endeasing = "Power4.easeIn";
    layer4.style = {'color':'#333', 'max-width':'550px', 'white-space':'normal'};
    layer4.classList = 'revo-slider-desc-text tleft';
    layer4.text = "We have created a Design that looks Awesome, performs Brilliantly &amp; senses Orientations.";
    
    layer5.data_x = "570";
    layer5.data_y = "375";
    layer5.data_customin = "x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer5.data_speed = "700";
    layer5.data_start = "1550";
    layer5.data_easing = "easeOutQuad";
    layer5.data_splitin = "none";
    layer5.data_splitout = "none";
    layer5.data_elementdelay = "0.01";
    layer5.data_endelementdelay = "0.1";
    layer5.data_endspeed = "1000";
    layer5.data_endeasing = "Power4.easeIn";
    layer5.classList = 'revo-slider-desc-text tleft';
    layer5.text = '<a href="#" class="button button-border button-large button-rounded tright nomargin"><span>Start Shopping</span> <i class="icon-angle-right"></i></a>';
    
    // Slide 2
    
    layer6.data_x = "630";
    layer6.data_y = "78";
    layer6.data_customin = "x:250;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer6.data_speed = "400";
    layer6.data_start = "1000";
    layer6.data_easing = "easeOutQuad";
    layer6.data_splitin = "none";
    layer6.data_splitout = "none";
    layer6.data_elementdelay = "0.01";
    layer6.data_endelementdelay = "0.1";
    layer6.data_endspeed = "1000";
    layer6.data_endeasing = "Power4.easeIn";
    layer6.classList = 'revo-slider-caps-text uppercase';
    layer6.text = '<img src="/assets/canvas/images/slider/rev/shop/bag.png" alt="Bag">';
    
    layer7.data_x = "0";
    layer7.data_y = "110";
    layer7.data_customin = "x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer7.data_speed = "700";
    layer7.data_start = "1000";
    layer7.data_easing = "easeOutQuad";
    layer7.data_splitin = "none";
    layer7.data_splitout = "none";
    layer7.data_elementdelay = "0.01";
    layer7.data_endelementdelay = "0.1";
    layer7.data_endspeed = "1000";
    layer7.data_endeasing = "Power4.easeIn";
    layer7.style = {'color':'#333'};
    layer7.classList = 'revo-slider-caps-text uppercase';
    layer7.text = 'Buy Stylish Bags at Discounted Prices';
    
    layer8.data_x = "0";
    layer8.data_y = "140";
    layer8.data_customin = "x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer8.data_speed = "700";
    layer8.data_start = "1200";
    layer8.data_easing = "easeOutQuad";
    layer8.data_splitin = "none";
    layer8.data_splitout = "none";
    layer8.data_elementdelay = "0.01";
    layer8.data_endelementdelay = "0.1";
    layer8.data_endspeed = "1000";
    layer8.data_endeasing = "Power4.easeIn";
    layer8.style = {'color':'#333', 'white-space':'normal', 'line-height':'1.15'};
    layer8.classList = 'revo-slider-emphasis-text nopadding noborder';
    layer8.text = 'Messenger Bags';
    
    layer9.data_x = "0";
    layer9.data_y = "240";
    layer9.data_customin = "x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer9.data_speed = "700";
    layer9.data_start = "1400";
    layer9.data_easing = "easeOutQuad";
    layer9.data_splitin = "none";
    layer9.data_splitout = "none";
    layer9.data_elementdelay = "0.01";
    layer9.data_endelementdelay = "0.1";
    layer9.data_endspeed = "1000";
    layer9.data_endeasing = "Power4.easeIn";
    layer9.style = {'color':'#333', 'max-width':'550px', 'white-space':'normal'};
    layer9.classList = 'revo-slider-desc-text tleft';
    layer9.text = "Grantees insurmountable challenges invest protect, growth improving quality social entrepreneurship.";
    
    layer10.data_x = "0";
    layer10.data_y = "340";
    layer10.data_customin = "x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer10.data_speed = "700";
    layer10.data_start = "1550";
    layer10.data_easing = "easeOutQuad";
    layer10.data_splitin = "none";
    layer10.data_splitout = "none";
    layer10.data_elementdelay = "0.01";
    layer10.data_endelementdelay = "0.1";
    layer10.data_endspeed = "1000";
    layer10.data_endeasing = "Power4.easeIn";
    layer10.classList = 'revo-slider-desc-text tleft';
    layer10.text ='<a href="#" class="button button-border button-large button-rounded tright nomargin"><span>Start Shopping</span> <i class="icon-angle-right"></i></a>';
    
    layer11.data_x = "510";
    layer11.data_y = "0";
    layer11.data_customin = "x:0;y:-236;z:0;rotationZ:0;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;";
    layer11.data_speed = "600";
    layer11.data_start = "2100";
    layer11.data_easing = "easeOutQuad";
    layer11.data_splitin = "none";
    layer11.data_splitout = "none";
    layer11.data_elementdelay = "0.01";
    layer11.data_endelementdelay = "0.1";
    layer11.data_endspeed = "1000";
    layer11.data_endeasing = "Power4.easeIn";
    layer11.classList = 'revo-slider-caps-text uppercase';
    layer11.text = '<img src="/assets/canvas/images/slider/rev/shop/tag.png" alt="Bag">';
    
    slide1.data_transition = "fade";
    slide1.data_slotamount = "1";
    slide1.data_masterspeed = "1500";
    slide1.data_delay = "10000";
    slide1.data_saveperformance = "off";
    slide1.data_title = "Latest Collections";
    slide1.style = {'background-color':'#F6F6F6'};
    
    slide2.data_transition = "slideup";
    slide2.data_slotamount = "1";
    slide2.data_masterspeed = "1500";
    slide2.data_delay = "10000";
    slide2.data_saveperformance = "off";
    slide2.data_title = "Messenger bags";
    slide2.style = {'background-color':'#F6F6F6'};
    
    slide1.layers = [];
    slide1.layers.push(layer1);
    slide1.layers.push(layer2);
    slide1.layers.push(layer3);
    slide1.layers.push(layer4);
    slide1.layers.push(layer5);
    
    slide2.layers = [];
    slide2.layers.push(layer6);
    slide2.layers.push(layer7);
    slide2.layers.push(layer8);
    slide2.layers.push(layer9);
    slide2.layers.push(layer10);
    slide2.layers.push(layer11);
    
    this.slides.push(slide1);
    this.slides.push(slide2);
    
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.BannerIndexComponent);
  }

}
