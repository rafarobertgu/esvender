import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {CommonService} from "../../../services/common.service";

@Component({
  selector: 'app-carousel-brands-index',
  templateUrl: './carousel-brands-index.component.html',
  styleUrls: ['./carousel-brands-index.component.scss']
})
export class CarouselBrandsIndexComponent implements OnInit {
  
  @ViewChild('CarouselBrandsIndexComponent', {static: true}) CarouselBrandsIndexComponent;
  
  templatePath:string = `${environment.frontend.template.vender}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([],[
      this.templatePath + '/js/jquery.js',
      this.templatePath + '/js/libs/carousel-brands-index.js'
    ]);
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.CarouselBrandsIndexComponent);
    
  }

}
