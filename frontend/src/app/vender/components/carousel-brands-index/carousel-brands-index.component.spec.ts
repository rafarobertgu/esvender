import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselBrandsIndexComponent } from './carousel-brands-index.component';

describe('CarouselBrandsIndexComponent', () => {
  let component: CarouselBrandsIndexComponent;
  let fixture: ComponentFixture<CarouselBrandsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselBrandsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselBrandsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
