import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopWarrantiesComponent } from './shop-warranties.component';

describe('ShopWarrantiesComponent', () => {
  let component: ShopWarrantiesComponent;
  let fixture: ComponentFixture<ShopWarrantiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopWarrantiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopWarrantiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
