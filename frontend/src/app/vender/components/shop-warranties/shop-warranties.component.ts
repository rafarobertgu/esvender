import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-shop-warranties',
  templateUrl: './shop-warranties.component.html',
  styleUrls: ['./shop-warranties.component.scss']
})
export class ShopWarrantiesComponent implements OnInit {
  
  @ViewChild('ShopWarrantiesComponent', {static: true}) ShopWarrantiesComponent;
  
  templatePath: string = `${environment.frontend.template.vender}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) {
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.ShopWarrantiesComponent);
    
  }

}
