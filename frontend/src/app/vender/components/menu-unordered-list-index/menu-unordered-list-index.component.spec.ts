import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuUnorderedListIndexComponent } from './menu-unordered-list-index.component';

describe('MenuUnorderedListIndexComponent', () => {
  let component: MenuUnorderedListIndexComponent;
  let fixture: ComponentFixture<MenuUnorderedListIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuUnorderedListIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuUnorderedListIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
