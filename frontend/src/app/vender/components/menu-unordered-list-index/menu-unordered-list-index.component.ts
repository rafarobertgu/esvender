import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-menu-unordered-list-index',
  templateUrl: './menu-unordered-list-index.component.html',
  styleUrls: ['./menu-unordered-list-index.component.scss']
})
export class MenuUnorderedListIndexComponent implements OnInit {
  
  @ViewChild('MenuUnorderedListIndexComponent', {static: true}) MenuUnorderedListIndexComponent;
  
  templatePath:string = `${environment.frontend.template.vender}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.MenuUnorderedListIndexComponent);
    
  }

}
