import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-vender-logo',
  templateUrl: './vender-logo.component.html',
  styleUrls: ['./vender-logo.component.scss']
})
export class VenderLogoComponent implements OnInit {
  
  @ViewChild('VenderLogoComponent', {static: true}) VenderLogoComponent;
  
  templatePath:string = `${environment.frontend.template.vender}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.VenderLogoComponent);
  }

}
