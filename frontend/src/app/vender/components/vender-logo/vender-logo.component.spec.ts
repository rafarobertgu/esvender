import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VenderLogoComponent } from './vender-logo.component';

describe('VenderLogoComponent', () => {
  let component: VenderLogoComponent;
  let fixture: ComponentFixture<VenderLogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VenderLogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VenderLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
