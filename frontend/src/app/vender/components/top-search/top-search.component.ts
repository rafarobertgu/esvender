import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-top-search',
  templateUrl: './top-search.component.html',
  styleUrls: ['./top-search.component.scss']
})
export class TopSearchComponent implements OnInit {
  
  @ViewChild('TopSearchComponent', {static: true}) TopSearchComponent;
  
  templatePath:string = `${environment.frontend.template.vender}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.TopSearchComponent);
  }
  
}
