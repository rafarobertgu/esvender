import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {CommonService} from "../../../services/common.service";

@Component({
  selector: 'app-index-search-form',
  templateUrl: './index-search-form.component.html',
  styleUrls: ['./index-search-form.component.scss']
})
export class IndexSearchFormComponent implements OnInit {
  
  @ViewChild('IndexSearchFormComponent', {static: true}) IndexSearchFormComponent;
  
  templatePath:string = `${environment.frontend.template.vender}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([],[
      this.templatePath + '/js/jquery.js',
      this.templatePath + '/js/plugins.js',
      this.templatePath + '/js/libs/index-search-form.js'
    ]);
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.IndexSearchFormComponent);
    
  }
}
