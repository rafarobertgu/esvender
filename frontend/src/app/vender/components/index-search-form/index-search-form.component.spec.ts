import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexSearchFormComponent } from './index-search-form.component';

describe('IndexSearchFormComponent', () => {
  let component: IndexSearchFormComponent;
  let fixture: ComponentFixture<IndexSearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexSearchFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
