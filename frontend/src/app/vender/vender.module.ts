import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {VenderRoutingModule} from './vender-routing.module';
import {VenderComponent} from './vender.component';
import {VenderLogoComponent} from './components/vender-logo/vender-logo.component';
import {HeaderIndexComponent} from './structures/header-index/header-index.component';
import {BannerIndexComponent} from './components/banner-index/banner-index.component';
import {FooterIndexComponent} from './structures/footer-index/footer-index.component';
import {MenuUnorderedListIndexComponent} from './components/menu-unordered-list-index/menu-unordered-list-index.component';
import {TopCartIndexComponent} from './components/top-cart-index/top-cart-index.component';
import {TopSearchComponent} from './components/top-search/top-search.component';
import {IndexSearchFormComponent} from "./components/index-search-form/index-search-form.component";
import {CommonService} from "../services/common.service";
import { CarouselBrandsIndexComponent } from './components/carousel-brands-index/carousel-brands-index.component';
import { TopbarIndexComponent } from './structures/topbar-index/topbar-index.component';
import { ShopWarrantiesComponent } from './components/shop-warranties/shop-warranties.component';
import { ArrivalsSellersLikeComponent } from './components/arrivals-sellers-like/arrivals-sellers-like.component';
import { BannerProductsPromotionComponent } from './components/banner-products-promotion/banner-products-promotion.component';
import { ProductsCarouselFilteredComponent } from './components/products-carousel-filtered/products-carousel-filtered.component';
import {ErrorHandler} from "protractor/built/exitCodes";
import {GlobalErrorHandlerService} from "../services/global-error-handler.service";
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {EsAuthService} from "../services/es-auth.service";
import {EsViewService} from "../services/es-view.service";
import {TranslateLoader, TranslateModule, TranslatePipe} from "@ngx-translate/core";
import {createTranslateLoader} from "../app.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";


@NgModule({
  declarations: [
    VenderComponent,
    VenderLogoComponent,
    HeaderIndexComponent,
    BannerIndexComponent,
    FooterIndexComponent,
    MenuUnorderedListIndexComponent,
    TopCartIndexComponent,
    TopSearchComponent,
    IndexSearchFormComponent,
    CarouselBrandsIndexComponent,
    TopbarIndexComponent,
    ShopWarrantiesComponent,
    ArrivalsSellersLikeComponent,
    BannerProductsPromotionComponent,
    ProductsCarouselFilteredComponent,
    LoginComponent,
    SignupComponent
  ],
  imports: [
    CommonModule,
    VenderRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      
      }
    })
  ],
  providers: [
    CommonService,
    EsAuthService,
    EsViewService,
    {provide: 'module', useValue:'vender'},
    {provide: 'moduleId', useValue:'3'},
    {provide:ErrorHandler, useClass: GlobalErrorHandlerService},
  ],
  bootstrap: [
    VenderComponent,
    
  ]
  
})
export class VenderModule {
}
