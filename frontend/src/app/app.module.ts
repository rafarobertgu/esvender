import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Object } from 'core-js';
import {ErrorHandler} from "protractor/built/exitCodes";
import {GlobalErrorHandlerService} from "./services/global-error-handler.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";

import {TranslateHttpLoader} from "@ngx-translate/http-loader";

export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, `./assets/lang/`, '.json');
};


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
  ],
  providers: [
    {
      provide:ErrorHandler,
      useClass: GlobalErrorHandlerService
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
