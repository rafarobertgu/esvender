import {Component, NgZone} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {EsAuthService} from "./services/es-auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {
  title = 'app';
  
  constructor(
    protected zone: NgZone,
  ) {
  }
}
