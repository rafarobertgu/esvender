import {Component, OnInit} from '@angular/core';
import {CommonService} from "../../services/common.service";
import {Slide} from "../../models/template/slide";
declare var $:any;

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-front',
  templateUrl: './front.component.html',
  styleUrls: ['./front.component.scss']
})
export class FrontComponent implements OnInit {
  
  slides: Slide[] = [];
  templatePath:string = `${environment.frontend.template.canvas}`;
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadScriptByUrl(this.templatePath + '/js/jquery.js');
    this.commonService.loadByUrl([
      'http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic',
      this.templatePath + "/css/bootstrap.css",
      this.templatePath + "/style.css",
      this.templatePath + "/css/dark.css",
      this.templatePath + "/css/font-icons.css",
      this.templatePath + "/css/animate.css",
      this.templatePath + "/css/magnific-popup.css",
      this.templatePath + "/css/responsive.css",
    ],[
      this.templatePath + '/js/plugins.js',
      this.templatePath + '/js/functions.js',
      this.templatePath + '/js/libs/submit-map-form.js'
    ], () => {
      $('#wrapper').css('background-color','rgb(246, 246, 246)');
    });
  }
  
  ngOnInit(): void {
  
  }

}
