import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbarOneComponent } from './topbar-one.component';

describe('TopbarOneComponent', () => {
  let component: TopbarOneComponent;
  let fixture: ComponentFixture<TopbarOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopbarOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbarOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
