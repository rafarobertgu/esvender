import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-sidebar-shop-right',
  templateUrl: './sidebar-shop-right.component.html',
  styleUrls: ['./sidebar-shop-right.component.scss']
})
export class SidebarShopRightComponent implements OnInit {
  
  @ViewChild('SidebarShopRightComponent', {static: true}) SidebarShopRightComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.SidebarShopRightComponent);
  }
}
