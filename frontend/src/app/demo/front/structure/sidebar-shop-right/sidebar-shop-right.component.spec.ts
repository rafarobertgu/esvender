import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarShopRightComponent } from './sidebar-shop-right.component';

describe('SidebarShopRightComponent', () => {
  let component: SidebarShopRightComponent;
  let fixture: ComponentFixture<SidebarShopRightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarShopRightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarShopRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
