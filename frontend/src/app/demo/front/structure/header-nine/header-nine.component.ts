import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-header-nine',
  templateUrl: './header-nine.component.html',
  styleUrls: ['./header-nine.component.scss']
})
export class HeaderNineComponent implements OnInit {
  
  @ViewChild('HeaderNineComponent', {static: true}) HeaderNineComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.HeaderNineComponent);
  }
  
}
