import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-menu-four',
  templateUrl: './menu-four.component.html',
  styleUrls: ['./menu-four.component.scss']
})
export class MenuFourComponent implements OnInit {
  
  @ViewChild('MenuFourComponent', {static: true}) MenuFourComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.MenuFourComponent);
  }
}
