import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbarFourComponent } from './topbar-four.component';

describe('TopbarFourComponent', () => {
  let component: TopbarFourComponent;
  let fixture: ComponentFixture<TopbarFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopbarFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbarFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
