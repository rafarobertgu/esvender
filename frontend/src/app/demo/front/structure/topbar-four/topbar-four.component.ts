import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-topbar-four',
  templateUrl: './topbar-four.component.html',
  styleUrls: ['./topbar-four.component.scss']
})
export class TopbarFourComponent implements OnInit {
  
  @ViewChild('TopbarFourComponent', {static: true}) TopbarFourComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.TopbarFourComponent);
  }
}
