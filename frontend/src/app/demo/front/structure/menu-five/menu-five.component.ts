import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-menu-five',
  templateUrl: './menu-five.component.html',
  styleUrls: ['./menu-five.component.scss']
})
export class MenuFiveComponent implements OnInit {
  
  @ViewChild('MenuFiveComponent', {static: true}) MenuFiveComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.MenuFiveComponent);
  }
}
