import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-header-seven',
  templateUrl: './header-seven.component.html',
  styleUrls: ['./header-seven.component.scss']
})
export class HeaderSevenComponent implements OnInit {
  
  @ViewChild('HeaderSevenComponent', {static: true}) HeaderSevenComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.HeaderSevenComponent);
  }
  
}
