import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderTwoShopComponent } from './header-two-shop.component';

describe('HeaderTwoShopComponent', () => {
  let component: HeaderTwoShopComponent;
  let fixture: ComponentFixture<HeaderTwoShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderTwoShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderTwoShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
