import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-header-two-shop',
  templateUrl: './header-two-shop.component.html',
  styleUrls: ['./header-two-shop.component.scss']
})
export class HeaderTwoShopComponent implements OnInit {
  
  @ViewChild('HeaderTwoShopComponent', {static: true}) HeaderTwoShopComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.HeaderTwoShopComponent);
  }
  
}
