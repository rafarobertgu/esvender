import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderThreeShopComponent } from './header-three-shop.component';

describe('HeaderThreeShopComponent', () => {
  let component: HeaderThreeShopComponent;
  let fixture: ComponentFixture<HeaderThreeShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderThreeShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderThreeShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
