import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-header-three-shop',
  templateUrl: './header-three-shop.component.html',
  styleUrls: ['./header-three-shop.component.scss']
})
export class HeaderThreeShopComponent implements OnInit {
  
  @ViewChild('HeaderThreeShopComponent', {static: true}) HeaderThreeShopComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.HeaderThreeShopComponent);
  }
}
