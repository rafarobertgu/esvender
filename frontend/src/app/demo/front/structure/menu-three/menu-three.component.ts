import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-menu-three',
  templateUrl: './menu-three.component.html',
  styleUrls: ['./menu-three.component.scss']
})
export class MenuThreeComponent implements OnInit {
  
  @ViewChild('MenuThreeComponent', {static: true}) MenuThreeComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.MenuThreeComponent);
  }
  
}
