import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-header-ten',
  templateUrl: './header-ten.component.html',
  styleUrls: ['./header-ten.component.scss']
})
export class HeaderTenComponent implements OnInit {
  
  @ViewChild('HeaderTenComponent', {static: true}) HeaderTenComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.HeaderTenComponent);
  }
  
}
