import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-topbar-three',
  templateUrl: './topbar-three.component.html',
  styleUrls: ['./topbar-three.component.scss']
})
export class TopbarThreeComponent implements OnInit {
  
  @ViewChild('TopbarThreeComponent', {static: true}) TopbarThreeComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.TopbarThreeComponent);
  }
}
