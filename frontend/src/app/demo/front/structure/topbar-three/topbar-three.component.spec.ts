import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbarThreeComponent } from './topbar-three.component';

describe('TopbarThreeComponent', () => {
  let component: TopbarThreeComponent;
  let fixture: ComponentFixture<TopbarThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopbarThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbarThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
