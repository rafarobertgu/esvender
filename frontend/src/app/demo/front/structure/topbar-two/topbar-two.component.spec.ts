import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbarTwoComponent } from './topbar-two.component';

describe('TopbarTwoComponent', () => {
  let component: TopbarTwoComponent;
  let fixture: ComponentFixture<TopbarTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopbarTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbarTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
