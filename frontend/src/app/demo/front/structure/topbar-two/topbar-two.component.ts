import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-topbar-two',
  templateUrl: './topbar-two.component.html',
  styleUrls: ['./topbar-two.component.scss']
})
export class TopbarTwoComponent implements OnInit {
  
  @ViewChild('TopbarTwoComponent', {static: true}) TopbarTwoComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.TopbarTwoComponent);
  }
  
}
