import {AfterViewChecked, Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
declare var $:any;

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-header-four',
  templateUrl: './header-four.component.html',
  styleUrls: ['./header-four.component.scss']
})
export class HeaderFourComponent implements OnInit, AfterViewChecked {
  
  @ViewChild('HeaderFourComponent', {static: true}) HeaderFourComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.HeaderFourComponent);
  }
  
  ngAfterViewChecked(): void {
  
  }
  
}
