import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-header-eleven',
  templateUrl: './header-eleven.component.html',
  styleUrls: ['./header-eleven.component.scss']
})
export class HeaderElevenComponent implements OnInit {
  
  @ViewChild('HeaderElevenComponent', {static: true}) HeaderElevenComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.HeaderElevenComponent);
  }
  
}
