import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-header-eight',
  templateUrl: './header-eight.component.html',
  styleUrls: ['./header-eight.component.scss']
})
export class HeaderEightComponent implements OnInit {
  
  @ViewChild('HeaderEightComponent', {static: true}) HeaderEightComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.HeaderEightComponent);
  }
  
}
