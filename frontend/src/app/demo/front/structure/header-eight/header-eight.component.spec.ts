import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderEightComponent } from './header-eight.component';

describe('HeaderEightComponent', () => {
  let component: HeaderEightComponent;
  let fixture: ComponentFixture<HeaderEightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderEightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderEightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
