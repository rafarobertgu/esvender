import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-sidebar-shop-left',
  templateUrl: './sidebar-shop-left.component.html',
  styleUrls: ['./sidebar-shop-left.component.scss']
})
export class SidebarShopLeftComponent implements OnInit {
  
  @ViewChild('SidebarShopLeftComponent', {static: true}) SidebarShopLeftComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.SidebarShopLeftComponent);
  }
}
