import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarShopLeftComponent } from './sidebar-shop-left.component';

describe('SidebarShopLeftComponent', () => {
  let component: SidebarShopLeftComponent;
  let fixture: ComponentFixture<SidebarShopLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarShopLeftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarShopLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
