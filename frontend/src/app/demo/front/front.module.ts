import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontRoutingModule } from './front-routing.module';
import { FrontComponent } from './front.component';

import {FooterComponent} from "./structure/footer/footer.component";
import {HeaderComponent} from "./structure/header/header.component";
import {ShopOneLeftSidebarComponent} from "./views/shop-menu/shop-one-left-sidebar/shop-one-left-sidebar.component";
import {ShopFourComponent} from "./views/shop-menu/shop-four/shop-four.component";
import {ShopOneComponent} from "./views/shop-menu/shop-one/shop-one.component";
import {ShopOneBothSidebarComponent} from "./views/shop-menu/shop-one-both-sidebar/shop-one-both-sidebar.component";
import {ShopOneRightSidebarComponent} from "./views/shop-menu/shop-one-right-sidebar/shop-one-right-sidebar.component";
import {ShopTwoBothSidebarComponent} from "./views/shop-menu/shop-two-both-sidebar/shop-two-both-sidebar.component";
import {ShopTwoLeftSidebarComponent} from "./views/shop-menu/shop-two-left-sidebar/shop-two-left-sidebar.component";
import {ShopTwoRightSidebarComponent} from "./views/shop-menu/shop-two-right-sidebar/shop-two-right-sidebar.component";
import {ShopThreeComponent} from "./views/shop-menu/shop-three/shop-three.component";
import {ShopThreeLeftSidebarComponent} from "./views/shop-menu/shop-three-left-sidebar/shop-three-left-sidebar.component";
import {ShopThreeRightSidebarComponent} from "./views/shop-menu/shop-three-right-sidebar/shop-three-right-sidebar.component";
import {ShopCategoryParallaxComponent} from "./views/shop-category-parallax/shop-category-parallax.component";
import {ShopSingleComponent} from "./views/shop-products/shop-single/shop-single.component";
import {ShopSingleBothSidebarComponent} from "./views/shop-products/shop-single-both-sidebar/shop-single-both-sidebar.component";
import {ShopSingleCustomLinkingComponent} from "./views/shop-products/shop-single-custom-linking/shop-single-custom-linking.component";
import {ShopSingleLeftSidebarComponent} from "./views/shop-products/shop-single-left-sidebar/shop-single-left-sidebar.component";
import {ShopSingleRightSidebarComponent} from "./views/shop-products/shop-single-right-sidebar/shop-single-right-sidebar.component";
import {HeaderOneComponent} from "./structure/header-one/header-one.component";
import {ShopIndexTwoComponent} from "./views/shop-index/shop-index-two/shop-index-two.component";
import {ShopIndexOneComponent} from "./views/shop-index/shop-index-one/shop-index-one.component";
import {IndexBannersComponent} from "./components/index-banners/index-banners.component";
import {TopbarTwoComponent} from "./structure/topbar-two/topbar-two.component";
import {TopbarOneComponent} from "./structure/topbar-one/topbar-one.component";
import {NotFoundOneComponent} from "./views/not-found/not-found-one/not-found-one.component";
import {NotFoundTwoComponent} from "./views/not-found/not-found-two/not-found-two.component";
import {HeaderFourComponent} from "./structure/header-four/header-four.component";
import {NotFoundThreeComponent} from "./views/not-found/not-found-three/not-found-three.component";
import {AboutOneComponent} from "./views/about/about-one/about-one.component";
import {HeaderFiveComponent} from "./structure/header-five/header-five.component";
import {AboutTwoComponent} from "./views/about/about-two/about-two.component";
import {CartComponent} from "./views/cart/cart.component";
import {LoginOneComponent} from "./views/login/login-one/login-one.component";
import {LoginTwoComponent} from "./views/login/login-two/login-two.component";
import {LoginRegisterOneComponent} from "./views/login-register/login-register-one/login-register-one.component";
import {TopbarThreeComponent} from "./structure/topbar-three/topbar-three.component";
import {LoginRegisterTwoComponent} from "./views/login-register/login-register-two/login-register-two.component";
import {LoginRegisterThreeComponent} from "./views/login-register/login-register-three/login-register-three.component";
import {MaintenanceComponent} from "./views/maintenance/maintenance.component";
import {MenuOneComponent} from "./structure/menu-one/menu-one.component";
import {MenuTwoComponent} from "./structure/menu-two/menu-two.component";
import {MenuThreeComponent} from "./structure/menu-three/menu-three.component";
import {HeaderThreeShopComponent} from "./structure/header-three-shop/header-three-shop.component";
import {HeaderTwoShopComponent} from "./structure/header-two-shop/header-two-shop.component";
import {UnorderedListShopComponent} from "./components/unordered-list-shop/unordered-list-shop.component";
import {UnorderedListComponent} from "./components/unordered-list/unordered-list.component";
import {TopCartComponent} from "./components/top-cart/top-cart.component";
import {TopSearchComponent} from "./components/top-search/top-search.component";
import {LogoComponent} from "./components/logo/logo.component";
import {MenuFourComponent} from "./structure/menu-four/menu-four.component";
import {MenuFiveComponent} from "./structure/menu-five/menu-five.component";
import {TopbarFourComponent} from "./structure/topbar-four/topbar-four.component";
import {HeaderSixComponent} from "./structure/header-six/header-six.component";
import {HeaderSevenComponent} from "./structure/header-seven/header-seven.component";
import {HeaderEightComponent} from "./structure/header-eight/header-eight.component";
import {NavbarOneComponent} from "./structure/navbar-one/navbar-one.component";
import {HeaderNineComponent} from "./structure/header-nine/header-nine.component";
import {HeaderTenComponent} from "./structure/header-ten/header-ten.component";
import {HeaderElevenComponent} from "./structure/header-eleven/header-eleven.component";
import {UnorderedListSimpleComponent} from "./components/unordered-list-simple/unordered-list-simple.component";
import {ContactOneComponent} from "./views/contact/contact-one/contact-one.component";
import {ContactTwoComponent} from "./views/contact/contact-two/contact-two.component";
import {ContactThreeComponent} from "./views/contact/contact-three/contact-three.component";
import {GoogleMapOneComponent} from "./components/google-map-one/google-map-one.component";
import {GoogleMapTwoComponent} from "./components/google-map-two/google-map-two.component";
import {WidgetLinksComponent} from "./components/widget-links/widget-links.component";
import {WidgetRecentItemsComponent} from "./components/widget-recent-items/widget-recent-items.component";
import {WidgetLastViewedItemsComponent} from "./components/widget-last-viewed-items/widget-last-viewed-items.component";
import {WidgetPopularItemsComponent} from "./components/widget-popular-items/widget-popular-items.component";
import {WidgetFacebookLikeboxComponent} from "./components/widget-facebook-likebox/widget-facebook-likebox.component";
import {WidgetSubscribeComponent} from "./components/widget-subscribe/widget-subscribe.component";
import {WidgetImageCarouselComponent} from "./components/widget-image-carousel/widget-image-carousel.component";
import {WidgetTwitterFeedComponent} from "./components/widget-twitter-feed/widget-twitter-feed.component";
import {WidgetFlickrComponent} from "./components/widget-flickr/widget-flickr.component";
import {WidgetTabPopularRecentComponent} from "./components/widget-tab-popular-recent/widget-tab-popular-recent.component";
import {WidgetTagCloudComponent} from "./components/widget-tag-cloud/widget-tag-cloud.component";
import {WidgetFsliderComponent} from "./components/widget-fslider/widget-fslider.component";
import {WidgetPortfolioSliderComponent} from "./components/widget-portfolio-slider/widget-portfolio-slider.component";
import {WidgetSocialIconsComponent} from "./components/widget-social-icons/widget-social-icons.component";
import {WidgetShortcodesComponent} from "./components/widget-shortcodes/widget-shortcodes.component";
import {WidgetDribbleShotsComponent} from "./components/widget-dribble-shots/widget-dribble-shots.component";
import {AddressInfoComponent} from "./components/address-info/address-info.component";
import {ContactInfoComponent} from "./components/contact-info/contact-info.component";
import {ContactInfoSimpleComponent} from "./components/contact-info-simple/contact-info-simple.component";
import {SidebarShopRightComponent} from "./structure/sidebar-shop-right/sidebar-shop-right.component";
import {SidebarShopLeftComponent} from "./structure/sidebar-shop-left/sidebar-shop-left.component";
import {CommonService} from "../../services/common.service";
import { PostcontentComponent } from './components/postcontent/postcontent.component';
import { WidgetLogoComponent } from './components/widget-logo/widget-logo.component';
import { WidgetRecentPostComponent } from './components/widget-recent-post/widget-recent-post.component';
import { WidgetCounterComponent } from './components/widget-counter/widget-counter.component';
import { WidgetSocialIconsBgComponent } from './components/widget-social-icons-bg/widget-social-icons-bg.component';
import { WidgetSocialIconsDarkComponent } from './components/widget-social-icons-dark/widget-social-icons-dark.component';
import { CarouselProductComponent } from './components/carousel-product/carousel-product.component';
import { CarouselImageComponent } from './components/carousel-image/carousel-image.component';
import { CarouselSingleProductComponent } from './components/carousel-single-product/carousel-single-product.component';
import { HeaderExtrasPromotionComponent } from './components/header-extras-promotion/header-extras-promotion.component';
import { HeaderExtrasEmailComponent } from './components/header-extras-email/header-extras-email.component';
import { RelatedProductsComponent } from './components/related-products/related-products.component';
import { WidgetSubscribeLightComponent } from './components/widget-subscribe-light/widget-subscribe-light.component';
import { PortafolioThreeLeftSideComponent } from './views/portafolio/portafolio-three-left-side/portafolio-three-left-side.component';
import { WarrantiesComponent } from './components/warranties/warranties.component';
import { GoogleMapIndexComponent } from './components/google-map-index/google-map-index.component';
import { LinksComponent } from './views/links.component';
import { ContactSevenComponent } from './views/contact/contact-seven/contact-seven.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { PortafolioThreeComponent } from './views/portafolio/portafolio-three/portafolio-three.component';
import { PortafolioFilterComponent } from './components/portafolio-filter/portafolio-filter.component';
import { CarouselClientsComponent } from './components/carousel-clients/carousel-clients.component';
import { CarouselProjectsComponent } from './components/carousel-projects/carousel-projects.component';
import { ClientsTestimonialsComponent } from './components/clients-testimonials/clients-testimonials.component';
import { SpecialFeaturesComponent } from './components/special-features/special-features.component';
import { SlideParallaxCorporateComponent } from './components/slide-parallax-corporate/slide-parallax-corporate.component';
import { CorporateIndexThreeComponent } from './views/corporate/corporate-index-three/corporate-index-three.component';

@NgModule({
  declarations: [
    FrontComponent,
    FooterComponent,
    HeaderComponent,
    ShopOneLeftSidebarComponent,
    ShopFourComponent,
    ShopOneComponent,
    ShopOneBothSidebarComponent,
    ShopOneRightSidebarComponent,
    ShopTwoBothSidebarComponent,
    ShopTwoLeftSidebarComponent,
    ShopTwoRightSidebarComponent,
    ShopThreeComponent,
    ShopThreeLeftSidebarComponent,
    ShopThreeRightSidebarComponent,
    ShopCategoryParallaxComponent,
    ShopSingleComponent,
    ShopSingleBothSidebarComponent,
    ShopSingleCustomLinkingComponent,
    ShopSingleLeftSidebarComponent,
    ShopSingleRightSidebarComponent,
    HeaderOneComponent,
    ShopIndexTwoComponent,
    ShopIndexOneComponent,
    IndexBannersComponent,
    TopbarTwoComponent,
    TopbarOneComponent,
    NotFoundOneComponent,
    NotFoundTwoComponent,
    HeaderFourComponent,
    NotFoundThreeComponent,
    AboutOneComponent,
    HeaderFiveComponent,
    AboutTwoComponent,
    CartComponent,
    LoginOneComponent,
    LoginTwoComponent,
    LoginRegisterOneComponent,
    TopbarThreeComponent,
    LoginRegisterTwoComponent,
    LoginRegisterThreeComponent,
    MaintenanceComponent,
    MenuOneComponent,
    MenuTwoComponent,
    MenuThreeComponent,
    HeaderThreeShopComponent,
    HeaderTwoShopComponent,
    UnorderedListShopComponent,
    UnorderedListComponent,
    TopCartComponent,
    TopSearchComponent,
    LogoComponent,
    MenuFourComponent,
    MenuFiveComponent,
    TopbarFourComponent,
    HeaderSixComponent,
    HeaderSevenComponent,
    HeaderEightComponent,
    NavbarOneComponent,
    HeaderNineComponent,
    HeaderTenComponent,
    HeaderElevenComponent,
    UnorderedListSimpleComponent,
    ContactOneComponent,
    ContactTwoComponent,
    ContactThreeComponent,
    GoogleMapOneComponent,
    GoogleMapTwoComponent,
    WidgetLinksComponent,
    WidgetRecentItemsComponent,
    WidgetLastViewedItemsComponent,
    WidgetPopularItemsComponent,
    WidgetFacebookLikeboxComponent,
    WidgetSubscribeComponent,
    WidgetImageCarouselComponent,
    WidgetTwitterFeedComponent,
    WidgetFlickrComponent,
    WidgetTabPopularRecentComponent,
    WidgetTagCloudComponent,
    WidgetFsliderComponent,
    WidgetPortfolioSliderComponent,
    WidgetSocialIconsComponent,
    WidgetShortcodesComponent,
    WidgetDribbleShotsComponent,
    AddressInfoComponent,
    ContactInfoComponent,
    ContactInfoSimpleComponent,
    SidebarShopRightComponent,
    SidebarShopLeftComponent,
    PostcontentComponent,
    WidgetLogoComponent,
    WidgetRecentPostComponent,
    WidgetCounterComponent,
    WidgetSocialIconsBgComponent,
    WidgetSocialIconsDarkComponent,
    CarouselProductComponent,
    CarouselImageComponent,
    CarouselSingleProductComponent,
    HeaderExtrasPromotionComponent,
    HeaderExtrasEmailComponent,
    RelatedProductsComponent,
    WidgetSubscribeLightComponent,
    PortafolioThreeLeftSideComponent,
    WarrantiesComponent,
    GoogleMapIndexComponent,
    LinksComponent,
    ContactSevenComponent,
    ContactFormComponent,
    PortafolioThreeComponent,
    PortafolioFilterComponent,
    CarouselClientsComponent,
    CarouselProjectsComponent,
    ClientsTestimonialsComponent,
    SpecialFeaturesComponent,
    SlideParallaxCorporateComponent,
    CorporateIndexThreeComponent,
  ],
  imports: [
    CommonModule,
    FrontRoutingModule
  ],
  providers: [
    CommonService,
    {provide: 'module', useValue:'front'}
  ],
  bootstrap: [FrontComponent]
})
export class FrontModule { }
