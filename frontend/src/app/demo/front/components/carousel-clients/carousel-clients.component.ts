import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-carousel-clients',
  templateUrl: './carousel-clients.component.html',
  styleUrls: ['./carousel-clients.component.scss']
})
export class CarouselClientsComponent implements OnInit {
  
  @ViewChild('CarouselClientsComponent', {static: true}) CarouselClientsComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([],[
      this.templatePath + '/js/jquery.js',
      this.templatePath + '/js/libs/carousel-clients-corporate.js',
    ]);
    
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.CarouselClientsComponent);
    
  }
}
