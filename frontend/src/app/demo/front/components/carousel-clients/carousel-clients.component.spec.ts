import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselClientsComponent } from './carousel-clients.component';

describe('CarouselClientsComponent', () => {
  let component: CarouselClientsComponent;
  let fixture: ComponentFixture<CarouselClientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselClientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
