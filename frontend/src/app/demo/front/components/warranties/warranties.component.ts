import { Component, OnInit } from '@angular/core';
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-warranties',
  templateUrl: './warranties.component.html',
  styleUrls: ['./warranties.component.scss']
})
export class WarrantiesComponent implements OnInit {

  templatePath:string = `${environment.frontend.template.canvas}`;

constructor() { }

  ngOnInit(): void {
  }

}
