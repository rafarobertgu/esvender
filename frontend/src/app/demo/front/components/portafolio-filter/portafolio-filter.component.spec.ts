import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortafolioFilterComponent } from './portafolio-filter.component';

describe('PortafolioFilterComponent', () => {
  let component: PortafolioFilterComponent;
  let fixture: ComponentFixture<PortafolioFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortafolioFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortafolioFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
