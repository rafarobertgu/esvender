import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-portafolio-filter',
  templateUrl: './portafolio-filter.component.html',
  styleUrls: ['./portafolio-filter.component.scss']
})
export class PortafolioFilterComponent implements OnInit {
  
  @ViewChild('PortafolioFilterComponent', {static: true}) PortafolioFilterComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([],[
      this.templatePath + '/js/libs/portafolio-filter-items.js'
    ]);
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.PortafolioFilterComponent);
  }

}
