import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  
  @ViewChild('ContactFormComponent', {static: true}) ContactFormComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([],[
      this.templatePath + '/js/jquery.js',
      this.templatePath + '/js/plugins.js',
      this.templatePath + '/js/libs/contact-form.js'
    ]);
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.ContactFormComponent);
    
  }
}
