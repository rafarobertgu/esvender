import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-google-map-index',
  templateUrl: './google-map-index.component.html',
  styleUrls: ['./google-map-index.component.scss']
})
export class GoogleMapIndexComponent implements OnInit {
  
  @ViewChild('GoogleMapIndexComponent', {static: true}) GoogleMapIndexComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/gmap.js');
    this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/google-map.js');
    this.commonService.loadScriptByUrl('/assets/canvas/js/libs/markerclusterer.js');
    this.commonService.loadScriptByUrl('/assets/canvas/js/jquery.gmap.js');
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.GoogleMapIndexComponent);
    
  }
}
