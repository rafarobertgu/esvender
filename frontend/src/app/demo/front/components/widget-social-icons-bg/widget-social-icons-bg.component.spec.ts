import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetSocialIconsBgComponent } from './widget-social-icons-bg.component';

describe('WidgetSocialIconsBgComponent', () => {
  let component: WidgetSocialIconsBgComponent;
  let fixture: ComponentFixture<WidgetSocialIconsBgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetSocialIconsBgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetSocialIconsBgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
