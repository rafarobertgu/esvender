import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-social-icons-bg',
  templateUrl: './widget-social-icons-bg.component.html',
  styleUrls: ['./widget-social-icons-bg.component.scss']
})
export class WidgetSocialIconsBgComponent implements OnInit {
  
  @ViewChild('WidgetSocialIconsBgComponent', {static: true}) WidgetSocialIconsBgComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
  
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetSocialIconsBgComponent);
    
  }
}
