import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexBannersComponent } from './index-banners.component';

describe('IndexBannersComponent', () => {
  let component: IndexBannersComponent;
  let fixture: ComponentFixture<IndexBannersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexBannersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexBannersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
