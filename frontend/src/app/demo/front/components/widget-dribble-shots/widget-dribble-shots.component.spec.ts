import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetDribbleShotsComponent } from './widget-dribble-shots.component';

describe('WidgetDribbleShotsComponent', () => {
  let component: WidgetDribbleShotsComponent;
  let fixture: ComponentFixture<WidgetDribbleShotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetDribbleShotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetDribbleShotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
