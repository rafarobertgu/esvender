import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-dribble-shots',
  templateUrl: './widget-dribble-shots.component.html',
  styleUrls: ['./widget-dribble-shots.component.scss']
})
export class WidgetDribbleShotsComponent implements OnInit {
  
  @ViewChild('WidgetDribbleShotsComponent', {static: true}) WidgetDribbleShotsComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetDribbleShotsComponent);
  
  }

}
