import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-widget-portfolio-slider',
  templateUrl: './widget-portfolio-slider.component.html',
  styleUrls: ['./widget-portfolio-slider.component.scss']
})
export class WidgetPortfolioSliderComponent implements OnInit {
  
  @ViewChild('WidgetPortfolioSliderComponent', {static: true}) WidgetPortfolioSliderComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/carousel-portafolio.js');
    
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetPortfolioSliderComponent);
  
  }

}
