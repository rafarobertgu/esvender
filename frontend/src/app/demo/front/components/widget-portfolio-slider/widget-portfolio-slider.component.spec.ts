import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetPortfolioSliderComponent } from './widget-portfolio-slider.component';

describe('WidgetPortfolioSliderComponent', () => {
  let component: WidgetPortfolioSliderComponent;
  let fixture: ComponentFixture<WidgetPortfolioSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetPortfolioSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetPortfolioSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
