import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-related-products',
  templateUrl: './related-products.component.html',
  styleUrls: ['./related-products.component.scss']
})
export class RelatedProductsComponent implements OnInit {
  
  @ViewChild('RelatedProductsComponent', {static: true}) RelatedProductsComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    setTimeout(() => this.commonService.loadScriptByUrl('/assets/canvas/js/libs/carousel-product.js'),500);
    
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.RelatedProductsComponent);
    
  }

}
