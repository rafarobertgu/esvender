import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-contact-info-simple',
  templateUrl: './contact-info-simple.component.html',
  styleUrls: ['./contact-info-simple.component.scss']
})
export class ContactInfoSimpleComponent implements OnInit {
  
  @ViewChild('ContactInfoSimpleComponent', {static: true}) ContactInfoSimpleComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }

  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.ContactInfoSimpleComponent);
  
  }

}
