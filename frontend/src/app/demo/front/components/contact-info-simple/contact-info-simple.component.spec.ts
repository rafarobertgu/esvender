import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactInfoSimpleComponent } from './contact-info-simple.component';

describe('ContactInfoSimpleComponent', () => {
  let component: ContactInfoSimpleComponent;
  let fixture: ComponentFixture<ContactInfoSimpleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactInfoSimpleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactInfoSimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
