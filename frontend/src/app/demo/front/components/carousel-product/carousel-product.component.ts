import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-carousel-product',
  templateUrl: './carousel-product.component.html',
  styleUrls: ['./carousel-product.component.scss']
})
export class CarouselProductComponent implements OnInit {
  
  @ViewChild('CarouselProductComponent', {static: true}) CarouselProductComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/carousel-product.js');
    
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.CarouselProductComponent);
    
  }

}
