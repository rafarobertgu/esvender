import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetFlickrComponent } from './widget-flickr.component';

describe('WidgetFlickrComponent', () => {
  let component: WidgetFlickrComponent;
  let fixture: ComponentFixture<WidgetFlickrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetFlickrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetFlickrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
