import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-flickr',
  templateUrl: './widget-flickr.component.html',
  styleUrls: ['./widget-flickr.component.scss']
})
export class WidgetFlickrComponent implements OnInit {
  
  @ViewChild('WidgetFlickrComponent', {static: true}) WidgetFlickrComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetFlickrComponent);
  
  }

}
