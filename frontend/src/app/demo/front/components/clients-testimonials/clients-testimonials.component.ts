import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-clients-testimonials',
  templateUrl: './clients-testimonials.component.html',
  styleUrls: ['./clients-testimonials.component.scss']
})
export class ClientsTestimonialsComponent implements OnInit {
  
  @ViewChild('ClientsTestimonialsComponent', {static: true}) ClientsTestimonialsComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.ClientsTestimonialsComponent);
    
  }
}
