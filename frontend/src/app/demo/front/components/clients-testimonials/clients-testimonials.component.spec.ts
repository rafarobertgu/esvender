import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsTestimonialsComponent } from './clients-testimonials.component';

describe('ClientsTestimonialsComponent', () => {
  let component: ClientsTestimonialsComponent;
  let fixture: ComponentFixture<ClientsTestimonialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsTestimonialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsTestimonialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
