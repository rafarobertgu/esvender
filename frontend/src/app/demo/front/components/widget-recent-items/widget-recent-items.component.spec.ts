import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetRecentItemsComponent } from './widget-recent-items.component';

describe('WidgetRecentItemsComponent', () => {
  let component: WidgetRecentItemsComponent;
  let fixture: ComponentFixture<WidgetRecentItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetRecentItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetRecentItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
