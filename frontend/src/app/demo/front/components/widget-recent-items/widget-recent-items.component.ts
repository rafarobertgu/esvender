import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-recent-items',
  templateUrl: './widget-recent-items.component.html',
  styleUrls: ['./widget-recent-items.component.scss']
})
export class WidgetRecentItemsComponent implements OnInit {
  
  @ViewChild('WidgetRecentItemsComponent', {static: true}) WidgetRecentItemsComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetRecentItemsComponent);
  
  }

}
