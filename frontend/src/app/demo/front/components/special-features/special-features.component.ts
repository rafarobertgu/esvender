import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-special-features',
  templateUrl: './special-features.component.html',
  styleUrls: ['./special-features.component.scss']
})
export class SpecialFeaturesComponent implements OnInit {
  @ViewChild('SpecialFeaturesComponent', {static: true}) SpecialFeaturesComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.SpecialFeaturesComponent);
    
  }

}
