import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-tag-cloud',
  templateUrl: './widget-tag-cloud.component.html',
  styleUrls: ['./widget-tag-cloud.component.scss']
})
export class WidgetTagCloudComponent implements OnInit {
  
  @ViewChild('WidgetTagCloudComponent', {static: true}) WidgetTagCloudComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetTagCloudComponent);
  
  }

}
