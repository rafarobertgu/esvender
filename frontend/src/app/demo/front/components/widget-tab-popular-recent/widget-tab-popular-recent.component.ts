import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-tab-popular-recent',
  templateUrl: './widget-tab-popular-recent.component.html',
  styleUrls: ['./widget-tab-popular-recent.component.scss']
})
export class WidgetTabPopularRecentComponent implements OnInit {
  
  @ViewChild('WidgetTabPopularRecentComponent', {static: true}) WidgetTabPopularRecentComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetTabPopularRecentComponent);
  
  }

}
