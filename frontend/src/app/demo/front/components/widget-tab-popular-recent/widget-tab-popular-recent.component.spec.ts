import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetTabPopularRecentComponent } from './widget-tab-popular-recent.component';

describe('WidgetTabPopularRecentComponent', () => {
  let component: WidgetTabPopularRecentComponent;
  let fixture: ComponentFixture<WidgetTabPopularRecentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetTabPopularRecentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetTabPopularRecentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
