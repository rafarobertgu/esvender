import {Component, OnInit, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-unordered-list-shop',
  templateUrl: './unordered-list-shop.component.html',
  styleUrls: ['./unordered-list-shop.component.scss']
})
export class UnorderedListShopComponent implements OnInit {
  
  @ViewChild('UnorderedListShopComponent', {static: true}) UnorderedListShopComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }

  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.UnorderedListShopComponent);
  }

}
