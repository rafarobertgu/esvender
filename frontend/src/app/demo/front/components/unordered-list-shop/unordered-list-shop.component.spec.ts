import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnorderedListShopComponent } from './unordered-list-shop.component';

describe('UnorderedListShopComponent', () => {
  let component: UnorderedListShopComponent;
  let fixture: ComponentFixture<UnorderedListShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnorderedListShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnorderedListShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
