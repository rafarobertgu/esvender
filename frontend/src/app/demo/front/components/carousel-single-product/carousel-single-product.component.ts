import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-carousel-single-product',
  templateUrl: './carousel-single-product.component.html',
  styleUrls: ['./carousel-single-product.component.scss']
})
export class CarouselSingleProductComponent implements OnInit {
  
  @ViewChild('CarouselSingleProductComponent', {static: true}) CarouselSingleProductComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/carousel-single-product.js');
    
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.CarouselSingleProductComponent);
    
  }
  
}
