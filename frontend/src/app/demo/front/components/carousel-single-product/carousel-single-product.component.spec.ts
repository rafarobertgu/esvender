import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselSingleProductComponent } from './carousel-single-product.component';

describe('CarouselSingleProductComponent', () => {
  let component: CarouselSingleProductComponent;
  let fixture: ComponentFixture<CarouselSingleProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselSingleProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselSingleProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
