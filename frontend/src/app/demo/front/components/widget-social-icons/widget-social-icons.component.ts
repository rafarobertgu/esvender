import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-social-icons',
  templateUrl: './widget-social-icons.component.html',
  styleUrls: ['./widget-social-icons.component.scss']
})
export class WidgetSocialIconsComponent implements OnInit {
  
  @ViewChild('WidgetSocialIconsComponent', {static: true}) WidgetSocialIconsComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetSocialIconsComponent);
  
  }

}
