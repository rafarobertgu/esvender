import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetSocialIconsComponent } from './widget-social-icons.component';

describe('WidgetSocialIconsComponent', () => {
  let component: WidgetSocialIconsComponent;
  let fixture: ComponentFixture<WidgetSocialIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetSocialIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetSocialIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
