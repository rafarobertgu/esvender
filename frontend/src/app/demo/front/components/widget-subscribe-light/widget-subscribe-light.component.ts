import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-widget-subscribe-light',
  templateUrl: './widget-subscribe-light.component.html',
  styleUrls: ['./widget-subscribe-light.component.scss']
})
export class WidgetSubscribeLightComponent implements OnInit {
  
  @ViewChild('WidgetSubscribeLightComponent', {static: true}) WidgetSubscribeLightComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([],[
        this.templatePath + '/js/jquery.js',
        this.templatePath + '/js/libs/subscribe-light.js',
      ])
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetSubscribeLightComponent);
    
  }

}
