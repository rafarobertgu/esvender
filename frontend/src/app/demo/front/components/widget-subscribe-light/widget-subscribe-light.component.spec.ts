import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetSubscribeLightComponent } from './widget-subscribe-light.component';

describe('WidgetSubscribeLightComponent', () => {
  let component: WidgetSubscribeLightComponent;
  let fixture: ComponentFixture<WidgetSubscribeLightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetSubscribeLightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetSubscribeLightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
