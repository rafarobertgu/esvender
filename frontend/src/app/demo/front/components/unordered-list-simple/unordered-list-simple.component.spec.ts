import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnorderedListSimpleComponent } from './unordered-list-simple.component';

describe('UnorderedListSimpleComponent', () => {
  let component: UnorderedListSimpleComponent;
  let fixture: ComponentFixture<UnorderedListSimpleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnorderedListSimpleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnorderedListSimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
