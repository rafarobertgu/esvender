import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-unordered-list-simple',
  templateUrl: './unordered-list-simple.component.html',
  styleUrls: ['./unordered-list-simple.component.scss']
})
export class UnorderedListSimpleComponent implements OnInit {
  
  @ViewChild('UnorderedListSimpleComponent', {static: true}) UnorderedListSimpleComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.UnorderedListSimpleComponent);
  
  }

}
