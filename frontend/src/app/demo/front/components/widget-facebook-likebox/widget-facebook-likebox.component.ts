import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-facebook-likebox',
  templateUrl: './widget-facebook-likebox.component.html',
  styleUrls: ['./widget-facebook-likebox.component.scss']
})
export class WidgetFacebookLikeboxComponent implements OnInit {
  
  @ViewChild('WidgetFacebookLikeboxComponent', {static: true}) WidgetFacebookLikeboxComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetFacebookLikeboxComponent);
  
  }

}
