import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetFacebookLikeboxComponent } from './widget-facebook-likebox.component';

describe('WidgetFacebookLikeboxComponent', () => {
  let component: WidgetFacebookLikeboxComponent;
  let fixture: ComponentFixture<WidgetFacebookLikeboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetFacebookLikeboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetFacebookLikeboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
