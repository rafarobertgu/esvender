import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-counter',
  templateUrl: './widget-counter.component.html',
  styleUrls: ['./widget-counter.component.scss']
})
export class WidgetCounterComponent implements OnInit {
  
  @ViewChild('WidgetCounterComponent', {static: true}) WidgetCounterComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetCounterComponent);
    
  }

}
