import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetCounterComponent } from './widget-counter.component';

describe('WidgetCounterComponent', () => {
  let component: WidgetCounterComponent;
  let fixture: ComponentFixture<WidgetCounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetCounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
