import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-carousel-projects',
  templateUrl: './carousel-projects.component.html',
  styleUrls: ['./carousel-projects.component.scss']
})
export class CarouselProjectsComponent implements OnInit {
  
  @ViewChild('CarouselProjectsComponent', {static: true}) CarouselProjectsComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([],[
      this.templatePath + '/js/jquery.js',
      this.templatePath + '/js/libs/carousel-projects.js',
    ]);
    
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.CarouselProjectsComponent);
    
  }
}
