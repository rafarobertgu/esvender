import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselProjectsComponent } from './carousel-projects.component';

describe('CarouselProjectsComponent', () => {
  let component: CarouselProjectsComponent;
  let fixture: ComponentFixture<CarouselProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
