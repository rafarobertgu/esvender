import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-image-carousel',
  templateUrl: './widget-image-carousel.component.html',
  styleUrls: ['./widget-image-carousel.component.scss']
})
export class WidgetImageCarouselComponent implements OnInit {
  
  @ViewChild('WidgetImageCarouselComponent', {static: true}) WidgetImageCarouselComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/carousel-images.js');
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetImageCarouselComponent);
  
  }

}
