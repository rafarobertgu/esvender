import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetImageCarouselComponent } from './widget-image-carousel.component';

describe('WidgetImageCarouselComponent', () => {
  let component: WidgetImageCarouselComponent;
  let fixture: ComponentFixture<WidgetImageCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetImageCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetImageCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
