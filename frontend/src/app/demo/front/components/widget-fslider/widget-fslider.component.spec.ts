import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetFsliderComponent } from './widget-fslider.component';

describe('WidgetFsliderComponent', () => {
  let component: WidgetFsliderComponent;
  let fixture: ComponentFixture<WidgetFsliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetFsliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetFsliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
