import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-fslider',
  templateUrl: './widget-fslider.component.html',
  styleUrls: ['./widget-fslider.component.scss']
})
export class WidgetFsliderComponent implements OnInit {
  
  @ViewChild('WidgetFsliderComponent', {static: true}) WidgetFsliderComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetFsliderComponent);
  
  }

}
