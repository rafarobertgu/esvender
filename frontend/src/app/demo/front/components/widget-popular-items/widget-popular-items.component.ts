import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-popular-items',
  templateUrl: './widget-popular-items.component.html',
  styleUrls: ['./widget-popular-items.component.scss']
})
export class WidgetPopularItemsComponent implements OnInit {
  
  @ViewChild('WidgetPopularItemsComponent', {static: true}) WidgetPopularItemsComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetPopularItemsComponent);
  
  }

}
