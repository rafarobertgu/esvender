import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetPopularItemsComponent } from './widget-popular-items.component';

describe('WidgetPopularItemsComponent', () => {
  let component: WidgetPopularItemsComponent;
  let fixture: ComponentFixture<WidgetPopularItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetPopularItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetPopularItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
