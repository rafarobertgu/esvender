import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetLastViewedItemsComponent } from './widget-last-viewed-items.component';

describe('WidgetLastViewedItemsComponent', () => {
  let component: WidgetLastViewedItemsComponent;
  let fixture: ComponentFixture<WidgetLastViewedItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetLastViewedItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetLastViewedItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
