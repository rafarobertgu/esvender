import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-last-viewed-items',
  templateUrl: './widget-last-viewed-items.component.html',
  styleUrls: ['./widget-last-viewed-items.component.scss']
})
export class WidgetLastViewedItemsComponent implements OnInit {
  
  @ViewChild('WidgetLastViewedItemsComponent', {static: true}) WidgetLastViewedItemsComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetLastViewedItemsComponent);
  
  }

}
