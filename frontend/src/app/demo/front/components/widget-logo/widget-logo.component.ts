import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-logo',
  templateUrl: './widget-logo.component.html',
  styleUrls: ['./widget-logo.component.scss']
})
export class WidgetLogoComponent implements OnInit {
  
  @ViewChild('WidgetLogoComponent', {static: true}) WidgetLogoComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetLogoComponent);
    
  }

}
