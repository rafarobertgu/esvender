import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetLogoComponent } from './widget-logo.component';

describe('WidgetLogoComponent', () => {
  let component: WidgetLogoComponent;
  let fixture: ComponentFixture<WidgetLogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetLogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
