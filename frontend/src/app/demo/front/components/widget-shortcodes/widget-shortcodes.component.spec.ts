import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetShortcodesComponent } from './widget-shortcodes.component';

describe('WidgetShortcodesComponent', () => {
  let component: WidgetShortcodesComponent;
  let fixture: ComponentFixture<WidgetShortcodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetShortcodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetShortcodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
