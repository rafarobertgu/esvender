import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-shortcodes',
  templateUrl: './widget-shortcodes.component.html',
  styleUrls: ['./widget-shortcodes.component.scss']
})
export class WidgetShortcodesComponent implements OnInit {
  
  @ViewChild('WidgetShortcodesComponent', {static: true}) WidgetShortcodesComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetShortcodesComponent);
  
  }

}
