import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-slide-parallax-corporate',
  templateUrl: './slide-parallax-corporate.component.html',
  styleUrls: ['./slide-parallax-corporate.component.scss']
})
export class SlideParallaxCorporateComponent implements OnInit {
  
  @ViewChild('SlideParallaxCorporateComponent', {static: true}) SlideParallaxCorporateComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([],[
      this.templatePath + '/js/jquery.js',
      this.templatePath + '/js/libs/slide-parallax-corporate.js',
    ])
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.SlideParallaxCorporateComponent);
    
  }

}
