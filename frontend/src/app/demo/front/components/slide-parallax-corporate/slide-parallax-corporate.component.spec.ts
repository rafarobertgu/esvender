import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideParallaxCorporateComponent } from './slide-parallax-corporate.component';

describe('SlideParallaxCorporateComponent', () => {
  let component: SlideParallaxCorporateComponent;
  let fixture: ComponentFixture<SlideParallaxCorporateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideParallaxCorporateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideParallaxCorporateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
