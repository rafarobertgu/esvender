import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderExtrasEmailComponent } from './header-extras-email.component';

describe('HeaderExtrasEmailComponent', () => {
  let component: HeaderExtrasEmailComponent;
  let fixture: ComponentFixture<HeaderExtrasEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderExtrasEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderExtrasEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
