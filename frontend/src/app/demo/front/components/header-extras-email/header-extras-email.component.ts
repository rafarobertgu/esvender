import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-header-extras-email',
  templateUrl: './header-extras-email.component.html',
  styleUrls: ['./header-extras-email.component.scss']
})
export class HeaderExtrasEmailComponent implements OnInit {
  
  @ViewChild('HeaderExtrasEmailComponent', {static: true}) HeaderExtrasEmailComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.HeaderExtrasEmailComponent);
    
  }

}
