import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-recent-post',
  templateUrl: './widget-recent-post.component.html',
  styleUrls: ['./widget-recent-post.component.scss']
})
export class WidgetRecentPostComponent implements OnInit {
  
  @ViewChild('WidgetRecentPostComponent', {static: true}) WidgetRecentPostComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetRecentPostComponent);
    
  }

}
