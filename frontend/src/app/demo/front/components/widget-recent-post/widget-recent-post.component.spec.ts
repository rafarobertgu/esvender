import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetRecentPostComponent } from './widget-recent-post.component';

describe('WidgetRecentPostComponent', () => {
  let component: WidgetRecentPostComponent;
  let fixture: ComponentFixture<WidgetRecentPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetRecentPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetRecentPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
