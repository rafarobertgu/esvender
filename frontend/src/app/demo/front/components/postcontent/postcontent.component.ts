import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-postcontent',
  templateUrl: './postcontent.component.html',
  styleUrls: ['./postcontent.component.scss']
})
export class PostcontentComponent implements OnInit {
  
  @ViewChild('PostcontentComponent', {static: true}) PostcontentComponent;
  templatePath:string = `${environment.frontend.template.canvas}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/submit-handler.js');
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.PostcontentComponent);
    
  }
}
