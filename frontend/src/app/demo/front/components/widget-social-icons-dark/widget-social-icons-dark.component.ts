import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-social-icons-dark',
  templateUrl: './widget-social-icons-dark.component.html',
  styleUrls: ['./widget-social-icons-dark.component.scss']
})
export class WidgetSocialIconsDarkComponent implements OnInit {
  
  @ViewChild('WidgetSocialIconsDarkComponent', {static: true}) WidgetSocialIconsDarkComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetSocialIconsDarkComponent);
    
  }
}
