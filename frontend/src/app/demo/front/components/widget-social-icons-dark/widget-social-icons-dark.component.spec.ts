import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetSocialIconsDarkComponent } from './widget-social-icons-dark.component';

describe('WidgetSocialIconsDarkComponent', () => {
  let component: WidgetSocialIconsDarkComponent;
  let fixture: ComponentFixture<WidgetSocialIconsDarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetSocialIconsDarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetSocialIconsDarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
