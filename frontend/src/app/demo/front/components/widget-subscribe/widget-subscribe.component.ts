import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-subscribe',
  templateUrl: './widget-subscribe.component.html',
  styleUrls: ['./widget-subscribe.component.scss']
})
export class WidgetSubscribeComponent implements OnInit {
  
  @ViewChild('WidgetSubscribeComponent', {static: true}) WidgetSubscribeComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    setTimeout(()=>{
      this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/subscribe.js')
    }, 1000);
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetSubscribeComponent);
  
  }

}
