import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetSubscribeComponent } from './widget-subscribe.component';

describe('WidgetSubscribeComponent', () => {
  let component: WidgetSubscribeComponent;
  let fixture: ComponentFixture<WidgetSubscribeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetSubscribeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetSubscribeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
