import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleMapTwoComponent } from './google-map-two.component';

describe('GoogleMapTwoComponent', () => {
  let component: GoogleMapTwoComponent;
  let fixture: ComponentFixture<GoogleMapTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleMapTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleMapTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
