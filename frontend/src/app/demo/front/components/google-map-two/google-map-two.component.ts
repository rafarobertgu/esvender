import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CommonService} from "../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-google-map-two',
  templateUrl: './google-map-two.component.html',
  styleUrls: ['./google-map-two.component.scss']
})
export class GoogleMapTwoComponent implements OnInit {
  
  @ViewChild('GoogleMapTwoComponent', {static: true}) GoogleMapTwoComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef,
    private commonService: CommonService
  ) {
    this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/gmap.js');
    this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/google-map.js');
    this.commonService.loadScriptByUrl(this.templatePath + '/js/libs/markerclusterer.js');
    this.commonService.loadScriptByUrl(this.templatePath + '/js/jquery.gmap.js');
  }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.GoogleMapTwoComponent);
  
  }

}
