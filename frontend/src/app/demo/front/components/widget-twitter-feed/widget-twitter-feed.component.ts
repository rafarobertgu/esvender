import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-twitter-feed',
  templateUrl: './widget-twitter-feed.component.html',
  styleUrls: ['./widget-twitter-feed.component.scss']
})
export class WidgetTwitterFeedComponent implements OnInit {
  
  @ViewChild('WidgetTwitterFeedComponent', {static: true}) WidgetTwitterFeedComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetTwitterFeedComponent);
  
  }

}
