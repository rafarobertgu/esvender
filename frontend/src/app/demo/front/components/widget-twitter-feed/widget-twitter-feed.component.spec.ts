import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetTwitterFeedComponent } from './widget-twitter-feed.component';

describe('WidgetTwitterFeedComponent', () => {
  let component: WidgetTwitterFeedComponent;
  let fixture: ComponentFixture<WidgetTwitterFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetTwitterFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetTwitterFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
