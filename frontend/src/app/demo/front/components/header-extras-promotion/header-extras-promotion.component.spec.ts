import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderExtrasPromotionComponent } from './header-extras-promotion.component';

describe('HeaderExtrasPromotionComponent', () => {
  let component: HeaderExtrasPromotionComponent;
  let fixture: ComponentFixture<HeaderExtrasPromotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderExtrasPromotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderExtrasPromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
