import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-header-extras-promotion',
  templateUrl: './header-extras-promotion.component.html',
  styleUrls: ['./header-extras-promotion.component.scss']
})
export class HeaderExtrasPromotionComponent implements OnInit {
  
  @ViewChild('HeaderExtrasPromotionComponent', {static: true}) HeaderExtrasPromotionComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.HeaderExtrasPromotionComponent);
    
  }
  
  
}
