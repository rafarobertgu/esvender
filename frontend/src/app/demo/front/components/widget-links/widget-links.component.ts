import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-widget-links',
  templateUrl: './widget-links.component.html',
  styleUrls: ['./widget-links.component.scss']
})
export class WidgetLinksComponent implements OnInit {
  
  @ViewChild('WidgetLinksComponent', {static: true}) WidgetLinksComponent;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.WidgetLinksComponent);
  
  }

}
