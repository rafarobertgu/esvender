import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetLinksComponent } from './widget-links.component';

describe('WidgetLinksComponent', () => {
  let component: WidgetLinksComponent;
  let fixture: ComponentFixture<WidgetLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
