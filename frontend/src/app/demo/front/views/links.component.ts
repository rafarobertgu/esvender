import { Component, OnInit } from '@angular/core';
import {Router, Routes} from "@angular/router";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {
  
  basePath:string = `${environment.frontend.server.webpath}`;
  routes:Routes;
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(private router:Router) {}

  ngOnInit(): void {
    this.routes = this.router.config;
  }

}
