import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-contact-seven',
  templateUrl: './contact-seven.component.html',
  styleUrls: ['./contact-seven.component.scss']
})
export class ContactSevenComponent implements OnInit {
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadScript('jquery');
    this.commonService.loadByUrl([
      'http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic',
      "/assets/canvas/css/bootstrap.css",
      "/assets/canvas/style.css",
      "/assets/canvas/css/dark.css",
      "/assets/canvas/css/font-icons.css",
      "/assets/canvas/css/animate.css",
      "/assets/canvas/css/magnific-popup.css",
      "/assets/canvas/css/responsive.css",
    ],[
      'http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js',
      this.templatePath + '/js/plugins.js',
      this.templatePath + '/js/functions.js'
    ]);
  }
  
  ngOnInit(): void {
  }

}
