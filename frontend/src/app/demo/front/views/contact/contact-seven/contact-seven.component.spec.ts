import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactSevenComponent } from './contact-seven.component';

describe('ContactSevenComponent', () => {
  let component: ContactSevenComponent;
  let fixture: ComponentFixture<ContactSevenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactSevenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactSevenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
