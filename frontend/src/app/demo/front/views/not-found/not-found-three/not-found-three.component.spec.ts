import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotFoundThreeComponent } from './not-found-three.component';

describe('NotFoundThreeComponent', () => {
  let component: NotFoundThreeComponent;
  let fixture: ComponentFixture<NotFoundThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotFoundThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotFoundThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
