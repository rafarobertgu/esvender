import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotFoundOneComponent } from './not-found-one.component';

describe('NotFoundOneComponent', () => {
  let component: NotFoundOneComponent;
  let fixture: ComponentFixture<NotFoundOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotFoundOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotFoundOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
