import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotFoundTwoComponent } from './not-found-two.component';

describe('NotFoundTwoComponent', () => {
  let component: NotFoundTwoComponent;
  let fixture: ComponentFixture<NotFoundTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotFoundTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotFoundTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
