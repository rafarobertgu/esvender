import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopOneRightSidebarComponent } from './shop-one-right-sidebar.component';

describe('ShopOneRightSidebarComponent', () => {
  let component: ShopOneRightSidebarComponent;
  let fixture: ComponentFixture<ShopOneRightSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopOneRightSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopOneRightSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
