import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopThreeRightSidebarComponent } from './shop-three-right-sidebar.component';

describe('ShopThreeRightSidebarComponent', () => {
  let component: ShopThreeRightSidebarComponent;
  let fixture: ComponentFixture<ShopThreeRightSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopThreeRightSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopThreeRightSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
