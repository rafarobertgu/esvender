import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopTwoRightSidebarComponent } from './shop-two-right-sidebar.component';

describe('ShopTwoRightSidebarComponent', () => {
  let component: ShopTwoRightSidebarComponent;
  let fixture: ComponentFixture<ShopTwoRightSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopTwoRightSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopTwoRightSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
