import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopOneBothSidebarComponent } from './shop-one-both-sidebar.component';

describe('ShopOneBothSidebarComponent', () => {
  let component: ShopOneBothSidebarComponent;
  let fixture: ComponentFixture<ShopOneBothSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopOneBothSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopOneBothSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
