import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopThreeLeftSidebarComponent } from './shop-three-left-sidebar.component';

describe('ShopThreeLeftSidebarComponent', () => {
  let component: ShopThreeLeftSidebarComponent;
  let fixture: ComponentFixture<ShopThreeLeftSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopThreeLeftSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopThreeLeftSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
