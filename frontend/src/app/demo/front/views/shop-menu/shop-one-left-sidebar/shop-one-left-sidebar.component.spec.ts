import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopOneLeftSidebarComponent } from './shop-one-left-sidebar.component';

describe('ShopOneLeftSidebarComponent', () => {
  let component: ShopOneLeftSidebarComponent;
  let fixture: ComponentFixture<ShopOneLeftSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopOneLeftSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopOneLeftSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
