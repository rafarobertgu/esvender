import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopTwoBothSidebarComponent } from './shop-two-both-sidebar.component';

describe('ShopTwoBothSidebarComponent', () => {
  let component: ShopTwoBothSidebarComponent;
  let fixture: ComponentFixture<ShopTwoBothSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopTwoBothSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopTwoBothSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
