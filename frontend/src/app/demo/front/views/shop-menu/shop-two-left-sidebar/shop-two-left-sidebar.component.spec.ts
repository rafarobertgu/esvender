import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopTwoLeftSidebarComponent } from './shop-two-left-sidebar.component';

describe('ShopTwoLeftSidebarComponent', () => {
  let component: ShopTwoLeftSidebarComponent;
  let fixture: ComponentFixture<ShopTwoLeftSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopTwoLeftSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopTwoLeftSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
