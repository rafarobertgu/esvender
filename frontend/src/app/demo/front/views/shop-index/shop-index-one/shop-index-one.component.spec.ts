import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopIndexOneComponent } from './shop-index-one.component';

describe('ShopIndexOneComponent', () => {
  let component: ShopIndexOneComponent;
  let fixture: ComponentFixture<ShopIndexOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopIndexOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopIndexOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
