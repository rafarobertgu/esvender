import {AfterViewInit, Component, OnInit} from '@angular/core';
import {CommonService} from "../../../../../services/common.service";

import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-shop-index-one',
  templateUrl: './shop-index-one.component.html',
  styleUrls: ['./shop-index-one.component.scss'],
})
export class ShopIndexOneComponent implements OnInit, AfterViewInit {
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      'http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic',
      this.templatePath + "/css/bootstrap.css",
      this.templatePath + "/style.css",
      this.templatePath + "/css/dark.css",
      this.templatePath + "/css/font-icons.css",
      this.templatePath + "/css/animate.css",
      this.templatePath + "/css/magnific-popup.css",
      this.templatePath + "/css/responsive.css",
    ],[
      this.templatePath + '/js/jquery.js',
      this.templatePath + '/js/plugins.js',
      this.templatePath + '/js/functions.js',
    ]);
  }

  ngOnInit(): void {
  
  }
  
  ngAfterViewInit(): void {
  }
}
