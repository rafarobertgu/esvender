import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopIndexTwoComponent } from './shop-index-two.component';

describe('ShopIndexTwoComponent', () => {
  let component: ShopIndexTwoComponent;
  let fixture: ComponentFixture<ShopIndexTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopIndexTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopIndexTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
