import {AfterViewChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import {CommonService} from "../../../../../services/common.service";
import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-shop-index-two',
  templateUrl: './shop-index-two.component.html',
  styleUrls: ['./shop-index-two.component.scss'],
  providers: [
    {provide: 'module', useValue:'front'}
  ]
})
export class ShopIndexTwoComponent implements OnInit, AfterViewInit {
  
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadScript('jquery');
    this.commonService.loadByUrl([
      'http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic',
      this.templatePath + "/css/bootstrap.css",
      this.templatePath + "/style.css",
      this.templatePath + "/css/dark.css",
      this.templatePath + "/css/font-icons.css",
      this.templatePath + "/css/animate.css",
      this.templatePath + "/css/magnific-popup.css",
      this.templatePath + "/css/responsive.css",
    ],[
      'http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js',
      this.templatePath + '/js/plugins.js',
      this.templatePath + '/js/functions.js'
    ]);
  }

  ngOnInit(): void {
  }
  
  ngAfterViewInit() {
  }

}
