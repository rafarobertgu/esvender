import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopSingleRightSidebarComponent } from './shop-single-right-sidebar.component';

describe('ShopSingleRightSidebarComponent', () => {
  let component: ShopSingleRightSidebarComponent;
  let fixture: ComponentFixture<ShopSingleRightSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopSingleRightSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopSingleRightSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
