import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopSingleLeftSidebarComponent } from './shop-single-left-sidebar.component';

describe('ShopSingleLeftSidebarComponent', () => {
  let component: ShopSingleLeftSidebarComponent;
  let fixture: ComponentFixture<ShopSingleLeftSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopSingleLeftSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopSingleLeftSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
