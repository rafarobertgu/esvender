import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopSingleCustomLinkingComponent } from './shop-single-custom-linking.component';

describe('ShopSingleCustomLinkingComponent', () => {
  let component: ShopSingleCustomLinkingComponent;
  let fixture: ComponentFixture<ShopSingleCustomLinkingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopSingleCustomLinkingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopSingleCustomLinkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
