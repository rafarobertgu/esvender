import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopSingleBothSidebarComponent } from './shop-single-both-sidebar.component';

describe('ShopSingleBothSidebarComponent', () => {
  let component: ShopSingleBothSidebarComponent;
  let fixture: ComponentFixture<ShopSingleBothSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopSingleBothSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopSingleBothSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
