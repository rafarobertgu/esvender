import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateIndexThreeComponent } from './corporate-index-three.component';

describe('CorporateIndexThreeComponent', () => {
  let component: CorporateIndexThreeComponent;
  let fixture: ComponentFixture<CorporateIndexThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateIndexThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateIndexThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
