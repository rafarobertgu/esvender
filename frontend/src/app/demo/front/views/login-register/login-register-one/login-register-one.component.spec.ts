import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginRegisterOneComponent } from './login-register-one.component';

describe('LoginRegisterOneComponent', () => {
  let component: LoginRegisterOneComponent;
  let fixture: ComponentFixture<LoginRegisterOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginRegisterOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginRegisterOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
