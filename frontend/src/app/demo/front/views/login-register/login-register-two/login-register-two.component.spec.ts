import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginRegisterTwoComponent } from './login-register-two.component';

describe('LoginRegisterTwoComponent', () => {
  let component: LoginRegisterTwoComponent;
  let fixture: ComponentFixture<LoginRegisterTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginRegisterTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginRegisterTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
