import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginRegisterThreeComponent } from './login-register-three.component';

describe('LoginRegisterThreeComponent', () => {
  let component: LoginRegisterThreeComponent;
  let fixture: ComponentFixture<LoginRegisterThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginRegisterThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginRegisterThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
