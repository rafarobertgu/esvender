import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortafolioThreeLeftSideComponent } from './portafolio-three-left-side.component';

describe('PortafolioThreeLeftSideComponent', () => {
  let component: PortafolioThreeLeftSideComponent;
  let fixture: ComponentFixture<PortafolioThreeLeftSideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortafolioThreeLeftSideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortafolioThreeLeftSideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
