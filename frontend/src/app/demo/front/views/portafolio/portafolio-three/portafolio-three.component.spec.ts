import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortafolioThreeComponent } from './portafolio-three.component';

describe('PortafolioThreeComponent', () => {
  let component: PortafolioThreeComponent;
  let fixture: ComponentFixture<PortafolioThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortafolioThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortafolioThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
