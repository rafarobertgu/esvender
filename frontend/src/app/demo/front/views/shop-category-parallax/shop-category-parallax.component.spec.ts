import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopCategoryParallaxComponent } from './shop-category-parallax.component';

describe('ShopCategoryParallaxComponent', () => {
  let component: ShopCategoryParallaxComponent;
  let fixture: ComponentFixture<ShopCategoryParallaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopCategoryParallaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopCategoryParallaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
