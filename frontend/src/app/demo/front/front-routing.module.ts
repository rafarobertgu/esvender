import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ShopIndexOneComponent} from "./views/shop-index/shop-index-one/shop-index-one.component";
import {FrontComponent} from "./front.component";
import {ShopIndexTwoComponent} from "./views/shop-index/shop-index-two/shop-index-two.component";
import {LoginOneComponent} from "./views/login/login-one/login-one.component";
import {LoginTwoComponent} from "./views/login/login-two/login-two.component";
import {AboutOneComponent} from "./views/about/about-one/about-one.component";
import {AboutTwoComponent} from "./views/about/about-two/about-two.component";
import {CartComponent} from "./views/cart/cart.component";
import {ContactOneComponent} from "./views/contact/contact-one/contact-one.component";
import {ContactTwoComponent} from "./views/contact/contact-two/contact-two.component";
import {ContactThreeComponent} from "./views/contact/contact-three/contact-three.component";
import {LoginRegisterOneComponent} from "./views/login-register/login-register-one/login-register-one.component";
import {LoginRegisterTwoComponent} from "./views/login-register/login-register-two/login-register-two.component";
import {LoginRegisterThreeComponent} from "./views/login-register/login-register-three/login-register-three.component";
import {MaintenanceComponent} from "./views/maintenance/maintenance.component";
import {NotFoundThreeComponent} from "./views/not-found/not-found-three/not-found-three.component";
import {NotFoundTwoComponent} from "./views/not-found/not-found-two/not-found-two.component";
import {NotFoundOneComponent} from "./views/not-found/not-found-one/not-found-one.component";
import {ShopCategoryParallaxComponent} from "./views/shop-category-parallax/shop-category-parallax.component";
import {ShopFourComponent} from "./views/shop-menu/shop-four/shop-four.component";
import {ShopOneComponent} from "./views/shop-menu/shop-one/shop-one.component";
import {ShopOneBothSidebarComponent} from "./views/shop-menu/shop-one-both-sidebar/shop-one-both-sidebar.component";
import {ShopOneLeftSidebarComponent} from "./views/shop-menu/shop-one-left-sidebar/shop-one-left-sidebar.component";
import {ShopOneRightSidebarComponent} from "./views/shop-menu/shop-one-right-sidebar/shop-one-right-sidebar.component";
import {ShopThreeComponent} from "./views/shop-menu/shop-three/shop-three.component";
import {ShopThreeLeftSidebarComponent} from "./views/shop-menu/shop-three-left-sidebar/shop-three-left-sidebar.component";
import {ShopThreeRightSidebarComponent} from "./views/shop-menu/shop-three-right-sidebar/shop-three-right-sidebar.component";
import {ShopTwoBothSidebarComponent} from "./views/shop-menu/shop-two-both-sidebar/shop-two-both-sidebar.component";
import {ShopTwoLeftSidebarComponent} from "./views/shop-menu/shop-two-left-sidebar/shop-two-left-sidebar.component";
import {ShopTwoRightSidebarComponent} from "./views/shop-menu/shop-two-right-sidebar/shop-two-right-sidebar.component";
import {ShopSingleComponent} from "./views/shop-products/shop-single/shop-single.component";
import {ShopSingleBothSidebarComponent} from "./views/shop-products/shop-single-both-sidebar/shop-single-both-sidebar.component";
import {ShopSingleCustomLinkingComponent} from "./views/shop-products/shop-single-custom-linking/shop-single-custom-linking.component";
import {ShopSingleLeftSidebarComponent} from "./views/shop-products/shop-single-left-sidebar/shop-single-left-sidebar.component";
import {ShopSingleRightSidebarComponent} from "./views/shop-products/shop-single-right-sidebar/shop-single-right-sidebar.component";
import {PortafolioThreeLeftSideComponent} from "./views/portafolio/portafolio-three-left-side/portafolio-three-left-side.component";
import {LinksComponent} from "./views/links.component";
import {ContactSevenComponent} from "./views/contact/contact-seven/contact-seven.component";
import {environment} from "../../../environments/environment";
import {PortafolioThreeComponent} from "./views/portafolio/portafolio-three/portafolio-three.component";
import {CorporateIndexThreeComponent} from "./views/corporate/corporate-index-three/corporate-index-three.component";

const routes: Routes = [
  { path: '', component: FrontComponent },
  { path: 'about-one', component: AboutOneComponent },
  { path: 'about-two', component: AboutTwoComponent },
  { path: 'shop-one', component: ShopIndexOneComponent },
  { path: 'shop-index-one', component: ShopIndexOneComponent },
  { path: 'shop-index-two', component: ShopIndexTwoComponent },
  { path: 'login-one', component: LoginOneComponent},
  { path: 'login-two', component: LoginTwoComponent},
  { path: 'login-register-one', component: LoginRegisterOneComponent},
  { path: 'login-register-two', component: LoginRegisterTwoComponent},
  { path: 'login-register-three', component: LoginRegisterThreeComponent},
  { path: 'cart', component: CartComponent},
  { path: 'contact-one', component: ContactOneComponent},
  { path: 'contact-two', component: ContactTwoComponent},
  { path: 'contact-three', component: ContactThreeComponent},
  { path: 'contact-seven', component: ContactSevenComponent},
  { path: 'maintenance', component: MaintenanceComponent},
  { path: 'not-found-one', component: NotFoundOneComponent},
  { path: 'not-found-two', component: NotFoundTwoComponent},
  { path: 'not-found-three', component: NotFoundThreeComponent},
  { path: 'shop-category-parallax', component: ShopCategoryParallaxComponent},
  { path: 'shop-menu-four', component: ShopFourComponent},
  { path: 'shop-menu-one', component: ShopOneComponent},
  { path: 'shop-menu-one-both-side', component: ShopOneBothSidebarComponent},
  { path: 'shop-menu-one-left-side', component: ShopOneLeftSidebarComponent},
  { path: 'shop-menu-one-right-side', component: ShopOneRightSidebarComponent},
  { path: 'shop-menu-three', component: ShopThreeComponent},
  { path: 'shop-menu-three-left-side', component: ShopThreeLeftSidebarComponent},
  { path: 'shop-menu-three-right-side', component: ShopThreeRightSidebarComponent},
  { path: 'shop-menu-two-both-side', component: ShopTwoBothSidebarComponent},
  { path: 'shop-menu-two-left-side', component: ShopTwoLeftSidebarComponent},
  { path: 'shop-menu-two-right-side', component: ShopTwoRightSidebarComponent},
  { path: 'shop-single', component: ShopSingleComponent},
  { path: 'shop-single-both-side', component: ShopSingleBothSidebarComponent},
  { path: 'shop-single-custom-linking', component: ShopSingleCustomLinkingComponent},
  { path: 'shop-single-left-side', component: ShopSingleLeftSidebarComponent},
  { path: 'shop-single-right-side', component: ShopSingleRightSidebarComponent},
  { path: 'portafolio-three-left-side', component: PortafolioThreeLeftSideComponent},
  { path: 'portafolio-three', component: PortafolioThreeComponent},
  { path: 'corporate-thee', component: CorporateIndexThreeComponent},
  { path: 'links', component: LinksComponent},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class FrontRoutingModule {
  templatePath:string = `${environment.frontend.template.canvas}`;

constructor() {}
}
