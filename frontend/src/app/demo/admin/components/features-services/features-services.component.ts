import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-features-services',
  templateUrl: './features-services.component.html',
  styleUrls: ['./features-services.component.scss']
})
export class FeaturesServicesComponent implements OnInit {
  
  @ViewChild('FeaturesServicesComponent', {static: true}) FeaturesServicesComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.FeaturesServicesComponent);
  }

}
