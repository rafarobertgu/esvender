import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturesServicesComponent } from './features-services.component';

describe('FeaturesServicesComponent', () => {
  let component: FeaturesServicesComponent;
  let fixture: ComponentFixture<FeaturesServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturesServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturesServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
