import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnorderedListAdminComponent } from './unordered-list-admin.component';

describe('UnorderedListAdminComponent', () => {
  let component: UnorderedListAdminComponent;
  let fixture: ComponentFixture<UnorderedListAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnorderedListAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnorderedListAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
