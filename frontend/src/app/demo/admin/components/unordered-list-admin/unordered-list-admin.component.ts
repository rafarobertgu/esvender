import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-unordered-list-admin',
  templateUrl: './unordered-list-admin.component.html',
  styleUrls: ['./unordered-list-admin.component.scss']
})
export class UnorderedListAdminComponent implements OnInit {
  
  @ViewChild('UnorderedListAdminComponent', {static: true}) UnorderedListAdminComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.UnorderedListAdminComponent);
  }

}
