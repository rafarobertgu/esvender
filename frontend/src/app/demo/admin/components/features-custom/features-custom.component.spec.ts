import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturesCustomComponent } from './features-custom.component';

describe('FeaturesCustomComponent', () => {
  let component: FeaturesCustomComponent;
  let fixture: ComponentFixture<FeaturesCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturesCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturesCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
