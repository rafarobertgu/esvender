import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-features-custom',
  templateUrl: './features-custom.component.html',
  styleUrls: ['./features-custom.component.scss']
})
export class FeaturesCustomComponent implements OnInit {
  
  @ViewChild('FeaturesCustomComponent', {static: true}) FeaturesCustomComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.FeaturesCustomComponent);
  }

}
