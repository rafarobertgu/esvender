import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-carousel-fade',
  templateUrl: './carousel-fade.component.html',
  styleUrls: ['./carousel-fade.component.scss']
})
export class CarouselFadeComponent implements OnInit {
  
  @ViewChild('CarouselFadeComponent', {static: true}) CarouselFadeComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.CarouselFadeComponent);
  }

}
