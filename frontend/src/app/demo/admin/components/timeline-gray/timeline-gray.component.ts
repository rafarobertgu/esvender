import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-timeline-gray',
  templateUrl: './timeline-gray.component.html',
  styleUrls: ['./timeline-gray.component.scss']
})
export class TimelineGrayComponent implements OnInit {
  
  @ViewChild('TimelineGrayComponent', {static: true}) TimelineGrayComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.TimelineGrayComponent);
  }

}
