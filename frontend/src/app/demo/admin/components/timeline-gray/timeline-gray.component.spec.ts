import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineGrayComponent } from './timeline-gray.component';

describe('TimelineGrayComponent', () => {
  let component: TimelineGrayComponent;
  let fixture: ComponentFixture<TimelineGrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineGrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineGrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
