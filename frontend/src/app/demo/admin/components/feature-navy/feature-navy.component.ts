import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-feature-navy',
  templateUrl: './feature-navy.component.html',
  styleUrls: ['./feature-navy.component.scss']
})
export class FeatureNavyComponent implements OnInit {
  
  @ViewChild('FeatureNavyComponent', {static: true}) FeatureNavyComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.FeatureNavyComponent);
  }

}
