import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureNavyComponent } from './feature-navy.component';

describe('FeatureNavyComponent', () => {
  let component: FeatureNavyComponent;
  let fixture: ComponentFixture<FeatureNavyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureNavyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureNavyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
