import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-pricing-plan',
  templateUrl: './pricing-plan.component.html',
  styleUrls: ['./pricing-plan.component.scss']
})
export class PricingPlanComponent implements OnInit {
  
  @ViewChild('PricingPlanComponent', {static: true}) PricingPlanComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.PricingPlanComponent);
  }

}
