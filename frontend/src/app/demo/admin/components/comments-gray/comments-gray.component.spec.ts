import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsGrayComponent } from './comments-gray.component';

describe('CommentsGrayComponent', () => {
  let component: CommentsGrayComponent;
  let fixture: ComponentFixture<CommentsGrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentsGrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsGrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
