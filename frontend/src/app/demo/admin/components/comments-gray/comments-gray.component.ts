import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-comments-gray',
  templateUrl: './comments-gray.component.html',
  styleUrls: ['./comments-gray.component.scss']
})
export class CommentsGrayComponent implements OnInit {
  
  @ViewChild('CommentsGrayComponent', {static: true}) CommentsGrayComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.CommentsGrayComponent);
  }
}
