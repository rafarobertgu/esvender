import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-small-chat',
  templateUrl: './small-chat.component.html',
  styleUrls: ['./small-chat.component.scss']
})
export class SmallChatComponent implements OnInit {
  
  @ViewChild('SmallChatComponent', {static: true}) SmallChatComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.SmallChatComponent);
  }
  
  
}
