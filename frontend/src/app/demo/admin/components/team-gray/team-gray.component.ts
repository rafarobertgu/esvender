import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-team-gray',
  templateUrl: './team-gray.component.html',
  styleUrls: ['./team-gray.component.scss']
})
export class TeamGrayComponent implements OnInit {
  
  @ViewChild('TeamGrayComponent', {static: true}) TeamGrayComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.TeamGrayComponent);
  }

}
