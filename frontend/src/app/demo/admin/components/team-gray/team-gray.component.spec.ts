import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamGrayComponent } from './team-gray.component';

describe('TeamGrayComponent', () => {
  let component: TeamGrayComponent;
  let fixture: ComponentFixture<TeamGrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamGrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamGrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
