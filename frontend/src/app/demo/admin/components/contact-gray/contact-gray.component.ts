import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-contact-gray',
  templateUrl: './contact-gray.component.html',
  styleUrls: ['./contact-gray.component.scss']
})
export class ContactGrayComponent implements OnInit {
  
  @ViewChild('ContactGrayComponent', {static: true}) ContactGrayComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.ContactGrayComponent);
  }
  
}
