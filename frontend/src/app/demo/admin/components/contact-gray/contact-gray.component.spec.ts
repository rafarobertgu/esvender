import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactGrayComponent } from './contact-gray.component';

describe('ContactGrayComponent', () => {
  let component: ContactGrayComponent;
  let fixture: ComponentFixture<ContactGrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactGrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactGrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
