import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';
import {NotFoundComponent} from "./views/not-found/not-found.component";
import {ServerErrorComponent} from "./views/server-error/server-error.component";
import {AgileBoardComponent} from "./views/agile-board/agile-board.component";
import {CalendarComponent} from "./views/calendar/calendar.component";
import {ChatViewComponent} from "./views/chat-view/chat-view.component";
import {ClientsComponent} from "./views/clients/clients.component";
import {ContactsComponent} from "./views/contacts/contacts.component";
import {DashboardFourComponent} from "./views/dashboard/dashboard-four/dashboard-four.component";
import {DashboardOneComponent} from "./views/dashboard/dashboard-one/dashboard-one.component";
import {DashboardTwoComponent} from "./views/dashboard/dashboard-two/dashboard-two.component";
import {DashboardThreeComponent} from "./views/dashboard/dashboard-three/dashboard-three.component";
import {OrdersComponent} from "./views/ecommerce/orders/orders.component";
import {ProductComponent} from "./views/ecommerce/product/product.component";
import {ProductListComponent} from "./views/ecommerce/product-list/product-list.component";
import {ProductsGridComponent} from "./views/ecommerce/products-grid/products-grid.component";
import {FileManagerComponent} from "./views/file-manager/file-manager.component";
import {ForgotPasswordComponent} from "./views/forgot-password/forgot-password.component";
import {FileManagerUploadComponent} from "./views/file-manager-upload/file-manager-upload.component";
import {InvoiceComponent} from "./views/invoice/invoice.component";
import {InvoicePrintComponent} from "./views/invoice-print/invoice-print.component";
import {IssueTrackerComponent} from "./views/issue-tracker/issue-tracker.component";
import {LandingComponent} from "./views/landing/landing.component";
import {LayoutsComponent} from "./views/layouts/layouts.component";
import {LockscreenComponent} from "./views/lockscreen/lockscreen.component";
import {LoginComponent} from "./views/login/login.component";
import {LoginTwoColumnsComponent} from "./views/login-two-columns/login-two-columns.component";
import {ProfileComponent} from "./views/profile/profile.component";
import {RegisterComponent} from "./views/register/register.component";
import {ProjectDetailComponent} from "./views/project-detail/project-detail.component";
import {ProjectsComponent} from "./views/projects/projects.component";
import {LinksComponent} from "./views/links.component";

const routes: Routes = [
  { path: '', component: AdminComponent },
  { path: 'not-found', component: NotFoundComponent},
  { path: 'server-error', component: ServerErrorComponent},
  { path: 'agile-board', component: AgileBoardComponent},
  { path: 'calendar', component: CalendarComponent},
  { path: 'chat-view', component: ChatViewComponent},
  { path: 'clients', component: ClientsComponent},
  { path: 'contacts', component: ContactsComponent},
  { path: 'dashboard-one', component: DashboardOneComponent},
  { path: 'dashboard-two', component: DashboardTwoComponent},
  { path: 'dashboard-three', component: DashboardThreeComponent},
  { path: 'dashboard-four', component: DashboardFourComponent},
  { path: 'ecommerce-orders', component: OrdersComponent},
  { path: 'ecommerce-products', component: ProductComponent},
  { path: 'ecommerce-product-list', component: ProductListComponent},
  { path: 'ecommerce-products-grid', component: ProductsGridComponent},
  { path: 'file-manager', component: FileManagerComponent},
  { path: 'forgot-password', component: ForgotPasswordComponent},
  { path: 'file-manager-upload', component: FileManagerUploadComponent},
  { path: 'invoice', component: InvoiceComponent},
  { path: 'invoice-print', component: InvoicePrintComponent},
  { path: 'issue-tracker', component: IssueTrackerComponent},
  { path: 'landing', component: LandingComponent},
  { path: 'layouts', component: LayoutsComponent},
  { path: 'locked', component: LockscreenComponent},
  { path: 'login', component: LoginComponent},
  { path: 'login-two-columns', component: LoginTwoColumnsComponent},
  { path: 'profile', component: ProfileComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'project-detail', component: ProjectDetailComponent},
  { path: 'projects', component: ProjectsComponent},
  { path: 'links', component: LinksComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
