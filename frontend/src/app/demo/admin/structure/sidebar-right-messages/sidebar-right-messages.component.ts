import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-sidebar-right-messages',
  templateUrl: './sidebar-right-messages.component.html',
  styleUrls: ['./sidebar-right-messages.component.scss']
})
export class SidebarRightMessagesComponent implements OnInit {
  
  @ViewChild('SidebarRightMessagesComponent', {static: true}) SidebarRightMessagesComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.SidebarRightMessagesComponent);
  }

}
