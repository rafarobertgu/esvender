import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarRightMessagesComponent } from './sidebar-right-messages.component';

describe('SidebarRightMessagesComponent', () => {
  let component: SidebarRightMessagesComponent;
  let fixture: ComponentFixture<SidebarRightMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarRightMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarRightMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
