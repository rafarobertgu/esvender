import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-sidebar-left-two',
  templateUrl: './sidebar-left-two.component.html',
  styleUrls: ['./sidebar-left-two.component.scss']
})
export class SidebarLeftTwoComponent implements OnInit {
  
  @ViewChild('SidebarLeftTwoComponent', {static: true}) SidebarLeftTwoComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.SidebarLeftTwoComponent);
  }

}
