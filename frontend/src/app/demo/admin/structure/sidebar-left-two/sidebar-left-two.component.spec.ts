import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarLeftTwoComponent } from './sidebar-left-two.component';

describe('SidebarLeftTwoComponent', () => {
  let component: SidebarLeftTwoComponent;
  let fixture: ComponentFixture<SidebarLeftTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarLeftTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarLeftTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
