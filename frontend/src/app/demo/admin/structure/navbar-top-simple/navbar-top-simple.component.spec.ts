import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarTopSimpleComponent } from './navbar-top-simple.component';

describe('NavbarTopSimpleComponent', () => {
  let component: NavbarTopSimpleComponent;
  let fixture: ComponentFixture<NavbarTopSimpleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarTopSimpleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarTopSimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
