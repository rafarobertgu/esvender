import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-navbar-top-simple',
  templateUrl: './navbar-top-simple.component.html',
  styleUrls: ['./navbar-top-simple.component.scss']
})
export class NavbarTopSimpleComponent implements OnInit {
  
  @ViewChild('NavbarTopSimpleComponent', {static: true}) NavbarTopSimpleComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.NavbarTopSimpleComponent);
  }

}
