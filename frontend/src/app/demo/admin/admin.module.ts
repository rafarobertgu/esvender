import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { OrdersComponent } from './views/ecommerce/orders/orders.component';
import { ProductListComponent } from './views/ecommerce/product-list/product-list.component';
import { ProductComponent } from './views/ecommerce/product/product.component';
import { ProductsGridComponent } from './views/ecommerce/products-grid/products-grid.component';
import { DashboardOneComponent } from './views/dashboard/dashboard-one/dashboard-one.component';
import { DashboardTwoComponent } from './views/dashboard/dashboard-two/dashboard-two.component';
import { DashboardThreeComponent } from './views/dashboard/dashboard-three/dashboard-three.component';
import { DashboardFourComponent } from './views/dashboard/dashboard-four/dashboard-four.component';
import { NotFoundComponent } from './views/not-found/not-found.component';
import { ServerErrorComponent } from './views/server-error/server-error.component';
import { AgileBoardComponent } from './views/agile-board/agile-board.component';
import { ChatViewComponent } from './views/chat-view/chat-view.component';
import { ClientsComponent } from './views/clients/clients.component';
import { ContactsComponent } from './views/contacts/contacts.component';
import { InvoiceComponent } from './views/invoice/invoice.component';
import { InvoicePrintComponent } from './views/invoice-print/invoice-print.component';
import { IssueTrackerComponent } from './views/issue-tracker/issue-tracker.component';
import { LandingComponent } from './views/landing/landing.component';
import { LayoutsComponent } from './views/layouts/layouts.component';
import { LockscreenComponent } from './views/lockscreen/lockscreen.component';
import { LoginComponent } from './views/login/login.component';
import { LoginTwoColumnsComponent } from './views/login-two-columns/login-two-columns.component';
import { RegisterComponent } from './views/register/register.component';
import {CommonService} from "../../services/common.service";
import { NavbarOneComponent } from './structure/navbar-one/navbar-one.component';
import { FooterOneComponent } from './structure/footer-one/footer-one.component';
import { CalendarComponent } from './views/calendar/calendar.component';
import { SmallChatComponent } from './components/small-chat/small-chat.component';
import { SidebarRightComponent } from './structure/sidebar-right/sidebar-right.component';
import { NavbarTwoComponent } from './structure/navbar-two/navbar-two.component';
import { SidebarLeftComponent } from './structure/sidebar-left/sidebar-left.component';
import { SidebarRightMessagesComponent } from './structure/sidebar-right-messages/sidebar-right-messages.component';
import { SidebarLeftTwoComponent } from './structure/sidebar-left-two/sidebar-left-two.component';
import { UnorderedListAdminComponent } from './components/unordered-list-admin/unordered-list-admin.component';
import { FileManagerComponent } from './views/file-manager/file-manager.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { ForgotPasswordComponent } from './views/forgot-password/forgot-password.component';
import { FooterTwoComponent } from './structure/footer-two/footer-two.component';
import { FileManagerUploadComponent } from './views/file-manager-upload/file-manager-upload.component';
import { ContactGrayComponent } from './components/contact-gray/contact-gray.component';
import { PricingPlanComponent } from './components/pricing-plan/pricing-plan.component';
import { FeatureNavyComponent } from './components/feature-navy/feature-navy.component';
import { CommentsGrayComponent } from './components/comments-gray/comments-gray.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { TimelineGrayComponent } from './components/timeline-gray/timeline-gray.component';
import { FeaturesComponent } from './components/features/features.component';
import { TeamGrayComponent } from './components/team-gray/team-gray.component';
import { FeaturesCustomComponent } from './components/features-custom/features-custom.component';
import { FeaturesServicesComponent } from './components/features-services/features-services.component';
import { CarouselFadeComponent } from './components/carousel-fade/carousel-fade.component';
import { NavbarTopSimpleComponent } from './structure/navbar-top-simple/navbar-top-simple.component';
import { ProfileComponent } from './views/profile/profile.component';
import { ProjectDetailComponent } from './views/project-detail/project-detail.component';
import { ProjectManagerComponent } from './components/project-manager/project-manager.component';
import { ProjectsComponent } from './views/projects/projects.component';
import { LinksComponent } from './views/links.component';


@NgModule({
  declarations: [
    AdminComponent,
    OrdersComponent,
    ProductListComponent,
    ProductComponent,
    ProductsGridComponent,
    DashboardOneComponent,
    DashboardTwoComponent,
    DashboardThreeComponent,
    DashboardFourComponent,
    NotFoundComponent,
    ServerErrorComponent,
    AgileBoardComponent,
    ChatViewComponent,
    ClientsComponent,
    ContactsComponent,
    InvoiceComponent,
    InvoicePrintComponent,
    IssueTrackerComponent,
    LandingComponent,
    LayoutsComponent,
    LockscreenComponent,
    LoginComponent,
    LoginTwoColumnsComponent,
    RegisterComponent,
    NavbarOneComponent,
    FooterOneComponent,
    CalendarComponent,
    SmallChatComponent,
    SidebarRightComponent,
    NavbarTwoComponent,
    SidebarLeftComponent,
    SidebarRightMessagesComponent,
    SidebarLeftTwoComponent,
    UnorderedListAdminComponent,
    FileManagerComponent,
    FileUploadComponent,
    ForgotPasswordComponent,
    FooterTwoComponent,
    FileManagerUploadComponent,
    ContactGrayComponent,
    PricingPlanComponent,
    FeatureNavyComponent,
    CommentsGrayComponent,
    TestimonialsComponent,
    TimelineGrayComponent,
    FeaturesComponent,
    TeamGrayComponent,
    FeaturesCustomComponent,
    FeaturesServicesComponent,
    CarouselFadeComponent,
    NavbarTopSimpleComponent,
    ProfileComponent,
    ProjectDetailComponent,
    ProjectManagerComponent,
    ProjectsComponent,
    LinksComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule
  ],
  providers: [
    CommonService,
    {provide: 'module', useValue:'admin'}
  ]
})
export class AdminModule { }
