import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../services/common.service";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotFoundComponent implements OnInit {
  
  templatePath:string = `${environment.frontend.template.inspinia}`;
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadScript('jquery');
    this.commonService.loadByUrl([
      '/assets/inspinia/css/bootstrap.min.css',
      '/assets/inspinia/font-awesome/css/font-awesome.css',
      '/assets/inspinia/css/animate.css',
      '/assets/inspinia/css/style.css'
    ], [
      '/assets/inspinia/js/bootstrap.min.js'
    ]);
  }

  ngOnInit(): void {
  }

}
