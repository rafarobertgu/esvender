import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../../services/common.service";

@Component({
  selector: 'app-dashboard-one',
  templateUrl: './dashboard-one.component.html',
  styleUrls: ['./dashboard-one.component.scss']
})
export class DashboardOneComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/font-awesome/css/font-awesome.css",
      "/assets/inspinia/css/plugins/morris/morris-0.4.3.min.css",
      "/assets/inspinia/js/plugins/gritter/jquery.gritter.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/css/style.css"
    ], [
      "/assets/inspinia/js/jquery-2.1.1.js",
      "/assets/inspinia/js/bootstrap.min.js",
      "/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js",
      "/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.tooltip.min.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.spline.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.resize.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.pie.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.symbol.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.time.js",
      "/assets/inspinia/js/plugins/peity/jquery.peity.min.js",
      "/assets/inspinia/js/demo/peity-demo.js",
      "/assets/inspinia/js/inspinia.js",
      "/assets/inspinia/js/plugins/pace/pace.min.js",
      "/assets/inspinia/js/plugins/jquery-ui/jquery-ui.min.js",
      "/assets/inspinia/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js",
      "/assets/inspinia/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
      "/assets/inspinia/js/plugins/easypiechart/jquery.easypiechart.js",
      "/assets/inspinia/js/plugins/sparkline/jquery.sparkline.min.js",
      "/assets/inspinia/js/demo/sparkline-demo.js",
      "/assets/inspinia/js/libs/dashboard-easyPieChart.js"
    ]);
  }
  
  ngOnInit(): void {
  }

}
