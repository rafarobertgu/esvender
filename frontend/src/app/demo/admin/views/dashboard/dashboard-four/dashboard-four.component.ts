import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../../services/common.service";

@Component({
  selector: 'app-dashboard-four',
  templateUrl: './dashboard-four.component.html',
  styleUrls: ['./dashboard-four.component.scss']
})
export class DashboardFourComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/font-awesome/css/font-awesome.css",
      "/assets/inspinia/css/plugins/footable/footable.core.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/css/style.css",
      "/assets/inspinia/css/plugins/datapicker/datepicker3.css"
    ], [
      "/assets/inspinia/js/jquery-2.1.1.js",
      "/assets/inspinia/js/bootstrap.min.js",
      "/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js",
      "/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.tooltip.min.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.spline.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.resize.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.pie.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.symbol.js",
      "/assets/inspinia/js/plugins/flot/curvedLines.js",
      "/assets/inspinia/js/plugins/peity/jquery.peity.min.js",
      "/assets/inspinia/js/demo/peity-demo.js",
      "/assets/inspinia/js/inspinia.js",
      "/assets/inspinia/js/plugins/pace/pace.min.js",
      "/assets/inspinia/js/plugins/jquery-ui/jquery-ui.min.js",
      "/assets/inspinia/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js",
      "/assets/inspinia/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
      "/assets/inspinia/js/plugins/sparkline/jquery.sparkline.min.js",
      "/assets/inspinia/js/demo/sparkline-demo.js",
      "/assets/inspinia/js/plugins/chartJs/Chart.min.js",
      "/assets/inspinia/js/libs/dashboard-plot-chart.js"
    ]);
  }
  
  ngOnInit(): void {
  }
  
}
