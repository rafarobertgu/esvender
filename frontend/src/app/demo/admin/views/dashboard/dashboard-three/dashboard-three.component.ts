import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../../services/common.service";
declare var $:any;
@Component({
  selector: 'app-dashboard-three',
  templateUrl: './dashboard-three.component.html',
  styleUrls: ['./dashboard-three.component.scss']
})
export class DashboardThreeComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/font-awesome/css/font-awesome.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/css/style.css"
    ], [
      "/assets/inspinia/js/jquery-2.1.1.js",
      "/assets/inspinia/js/bootstrap.min.js",
      "/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js",
      "/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js",
      "/assets/inspinia/js/inspinia.js",
      "/assets/inspinia/js/plugins/pace/pace.min.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.tooltip.min.js",
      "/assets/inspinia/js/plugins/flot/jquery.flot.resize.js",
      "/assets/inspinia/js/plugins/chartJs/Chart.min.js",
      "/assets/inspinia/js/plugins/peity/jquery.peity.min.js",
      "/assets/inspinia/js/demo/peity-demo.js",
      "/assets/inspinia/js/libs/dashboard-plot-chart.js"
    ],() => {
      $('body').addClass('top-navigation');
    });
  }
  
  ngOnInit(): void {
  }

}
