import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadScript('jquery');
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/font-awesome/css/font-awesome.css",
      "/assets/inspinia/css/plugins/toastr/toastr.min.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/css/style.css",
    ], [
      "/assets/inspinia/js/bootstrap.min.js",
      "/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js",
      "/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js",
      "/assets/inspinia/js/inspinia.js",
      "/assets/inspinia/js/plugins/pace/pace.min.js",
    ]);
  }
  
  ngOnInit(): void {
  }

}
