import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginTwoColumnsComponent } from './login-two-columns.component';

describe('LoginTwoColumnsComponent', () => {
  let component: LoginTwoColumnsComponent;
  let fixture: ComponentFixture<LoginTwoColumnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginTwoColumnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginTwoColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
