import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/font-awesome/css/font-awesome.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/css/style.css",
    ], []);
  }
  ngOnInit(): void {
  }

}
