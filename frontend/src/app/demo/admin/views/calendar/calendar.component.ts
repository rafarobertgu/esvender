import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadScript('jquery');
    this.commonService.loadScriptByUrl("/assets/inspinia/js/plugins/fullcalendar/moment.min.js");
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/font-awesome/css/font-awesome.css",
      "/assets/inspinia/css/plugins/iCheck/custom.css",
      "/assets/inspinia/css/plugins/fullcalendar/fullcalendar.css",
      "/assets/inspinia/css/plugins/fullcalendar/fullcalendar.print.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/css/style.css",
    ], [
      "/assets/inspinia/js/bootstrap.min.js",
      "/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js",
      "/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js",
      "/assets/inspinia/js/inspinia.js",
      "/assets/inspinia/js/plugins/pace/pace.min.js",
      "/assets/inspinia/js/jquery-ui.custom.min.js",
      "/assets/inspinia/js/plugins/iCheck/icheck.min.js",
      "/assets/inspinia/js/plugins/fullcalendar/fullcalendar.min.js",
      "/assets/inspinia/js/libs/calendar-iChecks.js",
    ]);
    
  }
  ngOnInit(): void {
  }

}
