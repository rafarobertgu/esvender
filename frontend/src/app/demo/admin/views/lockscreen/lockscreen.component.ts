import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../services/common.service";
declare var $:any;
@Component({
  selector: 'app-lockscreen',
  templateUrl: './lockscreen.component.html',
  styleUrls: ['./lockscreen.component.scss']
})
export class LockscreenComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/font-awesome/css/font-awesome.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/css/style.css"
    ], [
      "/assets/inspinia/js/jquery-2.1.1.js",
      "/assets/inspinia/js/bootstrap.min.js"
    ], () => {
      // $('body').addClass('gray-bg');
    });
  }
  
  ngOnInit(): void {
  }

}
