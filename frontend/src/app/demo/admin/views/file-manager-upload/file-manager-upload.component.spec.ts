import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerUploadComponent } from './file-manager-upload.component';

describe('FileManagerUploadComponent', () => {
  let component: FileManagerUploadComponent;
  let fixture: ComponentFixture<FileManagerUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileManagerUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
