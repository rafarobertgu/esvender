import { Component, OnInit } from '@angular/core';
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {
  
  basePath:string = `${environment.frontend.server.webpath}`;
  
  constructor() { }

  ngOnInit(): void {
  }

}
