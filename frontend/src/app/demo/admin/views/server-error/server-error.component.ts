import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../services/common.service";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-server-error',
  templateUrl: './server-error.component.html',
  styleUrls: ['./server-error.component.scss'],
})
export class ServerErrorComponent implements OnInit {
  
  basePath:string = `${environment.backend.server.webpath}/`;
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadScript('jquery');
    this.commonService.loadByUrl([
      '/assets/inspinia/css/bootstrap.min.css',
      '/assets/inspinia/font-awesome/css/font-awesome.css',
      '/assets/inspinia/css/animate.css',
      '/assets/inspinia/css/style.css'
    ], [
      '/assets/inspinia/js/bootstrap.min.js'
    ]);
  }

  ngOnInit(): void {
  }

}
