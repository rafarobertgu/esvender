import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../../services/common.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/font-awesome/css/font-awesome.css",
      "/assets/inspinia/css/plugins/morris/morris-0.4.3.min.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/css/style.css",
    ], [
      "/assets/inspinia/js/jquery-2.1.1.js",
      "/assets/inspinia/js/bootstrap.min.js",
      "/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js",
      "/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js",
      "/assets/inspinia/js/inspinia.js",
      "/assets/inspinia/js/plugins/pace/pace.min.js",
      "/assets/inspinia/js/plugins/datapicker/bootstrap-datepicker.js",
      "/assets/inspinia/js/plugins/footable/footable.all.min.js",
      "/assets/inspinia/js/libs/footable-datepicker.js"
    ]);
  }
  
  ngOnInit(): void {
  }

}
