import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../../services/common.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/font-awesome/css/font-awesome.css",
      "/assets/inspinia/css/plugins/summernote/summernote.css",
      "/assets/inspinia/css/plugins/summernote/summernote-bs3.css",
      "/assets/inspinia/css/plugins/datapicker/datepicker3.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/css/style.css"
    ], [
      "/assets/inspinia/js/jquery-2.1.1.js",
      "/assets/inspinia/js/bootstrap.min.js",
      "/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js",
      "/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js",
      "/assets/inspinia/js/inspinia.js",
      "/assets/inspinia/js/plugins/pace/pace.min.js",
      "/assets/inspinia/js/plugins/summernote/summernote.min.js",
      "/assets/inspinia/js/plugins/datapicker/bootstrap-datepicker.js",
      "/assets/inspinia/js/libs/summernote-datepicker.js"
    ]);
  }
  ngOnInit(): void {
  }

}
