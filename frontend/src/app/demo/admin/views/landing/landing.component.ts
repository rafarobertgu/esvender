import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../services/common.service";
declare var $:any;

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/font-awesome/css/font-awesome.min.css",
      "/assets/inspinia/css/style.css",
    ], [
      "/assets/inspinia/js/jquery-2.1.1.js",
      "/assets/inspinia/js/bootstrap.min.js",
      "/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js",
      "/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js",
      "/assets/inspinia/js/inspinia.js",
      "/assets/inspinia/js/plugins/pace/pace.min.js",
      "/assets/inspinia/js/plugins/wow/wow.min.js",
      "/assets/inspinia/js/libs/landing-scrollspy.js"
    ], () => {
      $('body').addClass('landing-page');
    });
  }
  
  ngOnInit(): void {
  }

}
