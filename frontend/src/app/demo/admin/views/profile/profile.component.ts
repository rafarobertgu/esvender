import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  
  constructor(
    private commonService: CommonService
  ) {
    this.commonService.loadByUrl([
      "/assets/inspinia/css/bootstrap.min.css",
      "/assets/inspinia/font-awesome/css/font-awesome.css",
      "/assets/inspinia/css/animate.css",
      "/assets/inspinia/css/style.css",
    ], [
      "/assets/inspinia/js/jquery-2.1.1.js",
      "/assets/inspinia/js/bootstrap.js",
      "/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js",
      "/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js",
      "/assets/inspinia/js/inspinia.js",
      "/assets/inspinia/js/plugins/pace/pace.min.js",
      "/assets/inspinia/js/plugins/peity/jquery.peity.min.js",
      "/assets/inspinia/js/demo/peity-demo.js"
    ], () => {
      // $('body').addClass('gray-bg');
    });
  }
  
  ngOnInit(): void {
  }

}
