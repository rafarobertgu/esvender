import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EsComponent} from "./es.component";
import {EsComponentsComponent} from "./views/es-components/es-components.component";
import {EsModulesComponent} from "./views/es-modules/es-modules.component";
import {LoginComponent} from "./pages/login/login.component";
import {SignupComponent} from "./pages/signup/signup.component";


const routes: Routes = [
  { path: '', component: EsComponent },
  { path: 'login', component: LoginComponent, pathMatch: 'full'},
  { path: 'signup', component: SignupComponent, pathMatch: 'full'},
  { path: 'modules', component: EsModulesComponent, pathMatch: 'full'},
  { path: 'components', component: EsComponentsComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EsRoutingModule { }
