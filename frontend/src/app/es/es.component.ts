import { Component, OnInit } from '@angular/core';
import {EsUsers} from "../models/esUsers";

@Component({
  selector: 'app-es',
  templateUrl: './es.component.html',
  styleUrls: ['./es.component.scss']
})
export class EsComponent implements OnInit {

  currentUser:EsUsers;
  
  constructor(
  ) {
    this.currentUser = new EsUsers();
  }

  ngOnInit(): void {
  }
  
}
