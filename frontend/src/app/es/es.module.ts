import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EsRoutingModule } from './es-routing.module';
import { EsComponent } from './es.component';
import {CommonService} from "../services/common.service";
import {VenderComponent} from "../vender/vender.component";
import { FooterIndexComponent } from './structures/footer-index/footer-index.component';
import { SidebarLeftIndexComponent } from './structures/sidebar-left-index/sidebar-left-index.component';
import { UnorderedListEsComponent } from './components/unordered-list-es/unordered-list-es.component';
import { NavbarIndexComponent } from './structures/navbar-index/navbar-index.component';
import { EsAttributesComponent } from './views/es-attributes/es-attributes.component';
import { EsAuthsComponent } from './views/es-auths/es-auths.component';
import { EsComponentsComponent } from './views/es-components/es-components.component';
import { EsDictionariesComponent } from './views/es-dictionaries/es-dictionaries.component';
import { EsEmployeesComponent } from './views/es-employees/es-employees.component';
import { EsFileAttributesComponent } from './views/es-file-attributes/es-file-attributes.component';
import { EsFilesComponent } from './views/es-files/es-files.component';
import { EsLogsComponent } from './views/es-logs/es-logs.component';
import { EsModulesComponent } from './views/es-modules/es-modules.component';
import { EsObjectAttributesComponent } from './views/es-object-attributes/es-object-attributes.component';
import { EsObjectsComponent } from './views/es-objects/es-objects.component';
import { EsParamsComponent } from './views/es-params/es-params.component';
import { EsPeopleComponent } from './views/es-people/es-people.component';
import { EsPersonAttributesComponent } from './views/es-person-attributes/es-person-attributes.component';
import { EsProfileComponentsComponent } from './views/es-profile-components/es-profile-components.component';
import { EsProfileModulesComponent } from './views/es-profile-modules/es-profile-modules.component';
import { EsProfileViewsComponent } from './views/es-profile-views/es-profile-views.component';
import { EsProfilesComponent } from './views/es-profiles/es-profiles.component';
import { EsRoleProfilesComponent } from './views/es-role-profiles/es-role-profiles.component';
import { EsRolesComponent } from './views/es-roles/es-roles.component';
import { EsUserRolesComponent } from './views/es-user-roles/es-user-roles.component';
import { EsUsersComponent } from './views/es-users/es-users.component';
import { EsViewsComponent } from './views/es-views/es-views.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { LoginComponent } from './pages/login/login.component';
import {EsViewService} from "../services/es-view.service";
import {ErrorHandler} from "protractor/built/exitCodes";
import {GlobalErrorHandlerService} from "../services/global-error-handler.service";
import {BrowserModule} from "@angular/platform-browser";
import {EsAuthService} from "../services/es-auth.service";
import { SignupComponent } from './pages/signup/signup.component';

@NgModule({
  declarations: [
    EsComponent,
    FooterIndexComponent,
    SidebarLeftIndexComponent,
    UnorderedListEsComponent,
    NavbarIndexComponent,
    EsAttributesComponent,
    EsAuthsComponent,
    EsComponentsComponent,
    EsDictionariesComponent,
    EsEmployeesComponent,
    EsFileAttributesComponent,
    EsFilesComponent,
    EsLogsComponent,
    EsModulesComponent,
    EsObjectAttributesComponent,
    EsObjectsComponent,
    EsParamsComponent,
    EsPeopleComponent,
    EsPersonAttributesComponent,
    EsProfileComponentsComponent,
    EsProfileModulesComponent,
    EsProfileViewsComponent,
    EsProfilesComponent,
    EsRoleProfilesComponent,
    EsRolesComponent,
    EsUserRolesComponent,
    EsUsersComponent,
    EsViewsComponent,
    LoginComponent,
    SignupComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    EsRoutingModule
  ],
  providers: [
    EsViewService,
    EsAuthService,
    CommonService,
    {provide: 'module', useValue:'es'},
    {provide: 'moduleId', useValue:'2'},
    {provide:ErrorHandler, useClass: GlobalErrorHandlerService}
  ],
  bootstrap: [VenderComponent]
})
export class EsModule { }
