import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../../services/common.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {EsParamService} from "../../../services/es-param.service";
import {EsAuthService} from "../../../services/es-auth.service";
import {EsUsers} from "../../../models/esUsers";
import {environment} from "../../../../environments/environment";
import {first} from "rxjs/operators";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService} from "../../../services/alert.service";
import {EsViews} from "../../../models/esViews";
import {EsViewService} from "../../../services/es-view.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  
  esUser:EsUsers;
  templatePath:string = `${environment.frontend.template.es}`;
  esSignupForm:FormGroup;
  isLoading:boolean = false;
  isSubmitted:boolean = false;
  isEditing:boolean = false;
  returnUrl:string;
  esViews:EsViews[] = [];
  esViewPortada:EsViews;
  esViewLogin:EsViews;
  
  constructor(
    private commonService: CommonService,
    private esParamService: EsParamService,
    public esAuthService: EsAuthService,
    public esViewService: EsViewService,
    private fb: FormBuilder,
  ) {
    this.commonService.loadByUrl([
      this.templatePath + "/css/bootstrap.min.css",
      this.templatePath + "/font-awesome/css/font-awesome.css",
      this.templatePath + "/css/plugins/iCheck/custom.css",
      this.templatePath + "/css/animate.css",
      this.templatePath + "/css/style.css",
    ], [
      this.templatePath + "/js/jquery-2.1.1.js",
      this.templatePath + "/js/bootstrap.min.js",
      this.templatePath + "/js/plugins/iCheck/icheck.min.js",
      this.templatePath + "/js/libs/register-iCheck.js"
    ], () => {
      // $('body').addClass('gray-bg');
    });
    
    this.esUser = new EsUsers();
    this.esSignupForm = this.fb.group({
      usr_name: new FormControl('', Validators.required),
      usr_username: new FormControl('', Validators.required),
      usr_password: new FormControl('', Validators.required),
    });
  }
  
  ngOnInit(): void {
    if (this.esAuthService.isAuthenticated()) {
      this.esUser = this.esAuthService.getCurrentUser();
      this.esViewService.go(this.esAuthService.viewCover.id);
    }
  }
  
}
