import {Component, OnInit} from '@angular/core';
import {CommonService} from "../../../services/common.service";
import {environment} from "../../../../environments/environment";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {EsParamService} from "../../../services/es-param.service";
import {EsAuthService} from "../../../services/es-auth.service";
import {EsUsers} from "../../../models/esUsers";
import {AlertService} from "../../../services/alert.service";
import {EsViews} from "../../../models/esViews";
import {EsViewService} from "../../../services/es-view.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  esUser: EsUsers;
  templatePath: string = `${environment.frontend.template.es}`;
  esLoginForm: FormGroup;
  isLoading: boolean = false;
  isEditing: boolean = false;
  returnUrl: string;
  esViews: EsViews[] = [];
  
  constructor(
    private commonService: CommonService,
    private esParamService: EsParamService,
    public esAuthService: EsAuthService,
    private fb: FormBuilder,
    private alertService: AlertService,
    private esViewService: EsViewService
  ) {
    this.commonService.loadByUrl([
      this.templatePath + "/css/bootstrap.min.css",
      this.templatePath + "/font-awesome/css/font-awesome.css",
      this.templatePath + "/css/animate.css",
      this.templatePath + "/css/style.css",
    ], [
      this.templatePath + "/js/jquery-2.1.1.js",
      this.templatePath + "/js/bootstrap.min.js"
    ], () => {
      // $('body').addClass('gray-bg');
    });
    this.esUser = new EsUsers();
    this.esLoginForm = this.fb.group({
      usr_username: new FormControl('', Validators.required),
      usr_password: new FormControl('', Validators.required),
    });
  }
  
  ngOnInit(): void {
    if (this.esAuthService.isAuthenticated()) {
      this.esUser = this.esAuthService.getCurrentUser();
      this.esViewService.go(this.esAuthService.viewCover.id);
    }
  }
}
