import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnorderedListEsComponent } from './unordered-list-es.component';

describe('UnorderedListEsComponent', () => {
  let component: UnorderedListEsComponent;
  let fixture: ComponentFixture<UnorderedListEsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnorderedListEsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnorderedListEsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
