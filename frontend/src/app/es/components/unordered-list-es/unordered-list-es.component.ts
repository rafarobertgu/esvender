import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-unordered-list-es',
  templateUrl: './unordered-list-es.component.html',
  styleUrls: ['./unordered-list-es.component.scss']
})
export class UnorderedListEsComponent implements OnInit {
  
  @ViewChild('UnorderedListEsComponent', {static: true}) UnorderedListEsComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.UnorderedListEsComponent);
  }

}
