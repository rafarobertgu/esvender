import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-navbar-index',
  templateUrl: './navbar-index.component.html',
  styleUrls: ['./navbar-index.component.scss']
})
export class NavbarIndexComponent implements OnInit {
  
  @ViewChild('NavbarIndexComponent', {static: true}) NavbarIndexComponent;
  
  templatePath:string = `${environment.frontend.template.es}`;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) {}
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.NavbarIndexComponent);
  }

}
