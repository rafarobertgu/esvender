import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-footer-index',
  templateUrl: './footer-index.component.html',
  styleUrls: ['./footer-index.component.scss']
})
export class FooterIndexComponent implements OnInit {
  
  @ViewChild('FooterIndexComponent', {static: true}) FooterIndexComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.FooterIndexComponent);
  }
}
