import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarLeftIndexComponent } from './sidebar-left-index.component';

describe('SidebarLeftIndexComponent', () => {
  let component: SidebarLeftIndexComponent;
  let fixture: ComponentFixture<SidebarLeftIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarLeftIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarLeftIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
