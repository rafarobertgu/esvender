import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-sidebar-left-index',
  templateUrl: './sidebar-left-index.component.html',
  styleUrls: ['./sidebar-left-index.component.scss']
})
export class SidebarLeftIndexComponent implements OnInit {
  
  @ViewChild('SidebarLeftIndexComponent', {static: true}) SidebarLeftIndexComponent;
  
  constructor(
    private viewContainerRef: ViewContainerRef
  ) { }
  
  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.SidebarLeftIndexComponent);
  }
  
  
}
