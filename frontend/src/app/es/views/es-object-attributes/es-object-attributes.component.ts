import { Component, OnInit } from '@angular/core';
import {EsObjectAttributes} from "../../../models/esObjectAttributes";
import {EsObjectAttributeService} from "../../../services/es-object-attribute.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-object-attributes',
  templateUrl: './es-object-attributes.component.html',
  styleUrls: ['./es-object-attributes.component.scss']
})
export class EsObjectAttributesComponent implements OnInit {
  
  esObjectAttributes:EsObjectAttributes[] = [];
  esObjectAttribute:EsObjectAttributes;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esObjectAttributeService: EsObjectAttributeService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getObjectAttributes(id = null) {
    if(id) {
      this.esObjectAttributeService.getObjectAttribute(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsObjectAttributes };
        this.esObjectAttribute = response.data;
      });
    } else {
      this.esObjectAttributeService.getObjectAttributes().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsObjectAttributes[] };
        this.esObjectAttributes = response.data;
      });
    }
  }
  
  async saveObjectAttribute(id:any, esObjectAttribute:EsObjectAttributes) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esObjectAttributeService.updateObjectAttribute(id, esObjectAttribute).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsObjectAttributes};
        this.esObjectAttribute = response.data;
      });
    } else {
      this.esObjectAttributeService.createObjectAttribute(esObjectAttribute).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsObjectAttributes};
        this.esObjectAttribute = response.data;
      })
    }
  }
  
  async deleteObjectAttribute(id) {
    this.esObjectAttributeService.deleteObjectAttribute(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsObjectAttributes };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getObjectAttributes();
        this.esObjectAttribute = null;
      }
    });
  }
}
