import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsObjectAttributesComponent } from './es-object-attributes.component';

describe('EsObjectAttributesComponent', () => {
  let component: EsObjectAttributesComponent;
  let fixture: ComponentFixture<EsObjectAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsObjectAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsObjectAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
