import { Component, OnInit } from '@angular/core';
import {EsFiles} from "../../../models/esFiles";
import {EsFileService} from "../../../services/es-file.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-files',
  templateUrl: './es-files.component.html',
  styleUrls: ['./es-files.component.scss']
})
export class EsFilesComponent implements OnInit {
  
  esFiles:EsFiles[] = [];
  esFile:EsFiles;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esFileService: EsFileService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getFiles(id = null) {
    if(id) {
      this.esFileService.getFile(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsFiles };
        this.esFile = response.data;
      });
    } else {
      this.esFileService.getFiles().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsFiles[] };
        this.esFiles = response.data;
      });
    }
  }
  
  async saveFile(id:any, esFile:EsFiles) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esFileService.updateFile(id, esFile).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsFiles};
        this.esFile = response.data;
      });
    } else {
      this.esFileService.createFile(esFile).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsFiles};
        this.esFile = response.data;
      })
    }
  }
  
  async deleteFile(id) {
    this.esFileService.deleteFile(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsFiles };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getFiles();
        this.esFile = null;
      }
    });
  }
}
