import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsFilesComponent } from './es-files.component';

describe('EsFilesComponent', () => {
  let component: EsFilesComponent;
  let fixture: ComponentFixture<EsFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
