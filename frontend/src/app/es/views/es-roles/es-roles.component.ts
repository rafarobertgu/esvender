import { Component, OnInit } from '@angular/core';
import {EsRoles} from "../../../models/esRoles";
import {EsRoleService} from "../../../services/es-role.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-roles',
  templateUrl: './es-roles.component.html',
  styleUrls: ['./es-roles.component.scss']
})
export class EsRolesComponent implements OnInit {
  
  esRoles:EsRoles[] = [];
  esRole:EsRoles;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esRoleService: EsRoleService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getRoles(id = null) {
    if(id) {
      this.esRoleService.getRole(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsRoles };
        this.esRole = response.data;
      });
    } else {
      this.esRoleService.getRoles().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsRoles[] };
        this.esRoles = response.data;
      });
    }
  }
  
  async saveRole(id:any, esRole:EsRoles) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esRoleService.updateRole(id, esRole).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsRoles};
        this.esRole = response.data;
      });
    } else {
      this.esRoleService.createRole(esRole).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsRoles};
        this.esRole = response.data;
      })
    }
  }
  
  async deleteRole(id) {
    this.esRoleService.deleteRole(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsRoles };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getRoles();
        this.esRole = null;
      }
    });
  }
}
