import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsRolesComponent } from './es-roles.component';

describe('EsRolesComponent', () => {
  let component: EsRolesComponent;
  let fixture: ComponentFixture<EsRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
