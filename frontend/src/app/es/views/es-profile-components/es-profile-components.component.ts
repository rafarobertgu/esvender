import { Component, OnInit } from '@angular/core';
import {EsProfileComponents} from "../../../models/esProfileComponents";
import {EsProfileComponentService} from "../../../services/es-profile-component.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-profile-components',
  templateUrl: './es-profile-components.component.html',
  styleUrls: ['./es-profile-components.component.scss']
})
export class EsProfileComponentsComponent implements OnInit {
  
  esProfileComponents:EsProfileComponents[] = [];
  esProfileComponent:EsProfileComponents;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esProfileComponentService: EsProfileComponentService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getProfileComponents(id = null) {
    if(id) {
      this.esProfileComponentService.getProfileComponent(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileComponents };
        this.esProfileComponent = response.data;
      });
    } else {
      this.esProfileComponentService.getProfileComponents().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileComponents[] };
        this.esProfileComponents = response.data;
      });
    }
  }
  
  async saveProfileComponent(id:any, esProfileComponent:EsProfileComponents) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esProfileComponentService.updateProfileComponent(id, esProfileComponent).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileComponents};
        this.esProfileComponent = response.data;
      });
    } else {
      this.esProfileComponentService.createProfileComponent(esProfileComponent).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileComponents};
        this.esProfileComponent = response.data;
      })
    }
  }
  
  async deleteProfileComponent(id) {
    this.esProfileComponentService.deleteProfileComponent(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsProfileComponents };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getProfileComponents();
        this.esProfileComponent = null;
      }
    });
  }
}
