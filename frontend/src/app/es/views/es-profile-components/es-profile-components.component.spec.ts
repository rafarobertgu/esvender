import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsProfileComponentsComponent } from './es-profile-components.component';

describe('EsProfileComponentsComponent', () => {
  let component: EsProfileComponentsComponent;
  let fixture: ComponentFixture<EsProfileComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsProfileComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsProfileComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
