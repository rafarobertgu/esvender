import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsProfileViewsComponent } from './es-profile-views.component';

describe('EsProfileViewsComponent', () => {
  let component: EsProfileViewsComponent;
  let fixture: ComponentFixture<EsProfileViewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsProfileViewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsProfileViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
