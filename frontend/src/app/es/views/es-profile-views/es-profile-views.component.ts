import { Component, OnInit } from '@angular/core';
import {EsProfileViews} from "../../../models/esProfileViews";
import {EsProfileViewService} from "../../../services/es-profile-view.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-profile-views',
  templateUrl: './es-profile-views.component.html',
  styleUrls: ['./es-profile-views.component.scss']
})
export class EsProfileViewsComponent implements OnInit {
  
  esProfileViews:EsProfileViews[] = [];
  esProfileView:EsProfileViews;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esProfileViewService: EsProfileViewService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getProfileViews(id = null) {
    if(id) {
      this.esProfileViewService.getProfileView(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileViews };
        this.esProfileView = response.data;
      });
    } else {
      this.esProfileViewService.getProfileViews().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileViews[] };
        this.esProfileViews = response.data;
      });
    }
  }
  
  async saveProfileView(id:any, esProfileView:EsProfileViews) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esProfileViewService.updateProfileView(id, esProfileView).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileViews};
        this.esProfileView = response.data;
      });
    } else {
      this.esProfileViewService.createProfileView(esProfileView).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileViews};
        this.esProfileView = response.data;
      })
    }
  }
  
  async deleteProfileView(id) {
    this.esProfileViewService.deleteProfileView(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsProfileViews };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getProfileViews();
        this.esProfileView = null;
      }
    });
  }

}
