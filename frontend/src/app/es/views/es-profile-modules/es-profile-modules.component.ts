import { Component, OnInit } from '@angular/core';
import {EsProfileModules} from "../../../models/esProfileModules";
import {EsProfileModuleService} from "../../../services/es-profile-module.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-profile-modules',
  templateUrl: './es-profile-modules.component.html',
  styleUrls: ['./es-profile-modules.component.scss']
})
export class EsProfileModulesComponent implements OnInit {
  
  esProfileModules:EsProfileModules[] = [];
  esProfileModule:EsProfileModules;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esProfileModuleService: EsProfileModuleService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getProfileModules(id = null) {
    if(id) {
      this.esProfileModuleService.getProfileModule(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileModules };
        this.esProfileModule = response.data;
      });
    } else {
      this.esProfileModuleService.getProfileModules().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileModules[] };
        this.esProfileModules = response.data;
      });
    }
  }
  
  async saveProfileModule(id:any, esProfileModule:EsProfileModules) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esProfileModuleService.updateProfileModule(id, esProfileModule).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileModules};
        this.esProfileModule = response.data;
      });
    } else {
      this.esProfileModuleService.createProfileModule(esProfileModule).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfileModules};
        this.esProfileModule = response.data;
      })
    }
  }
  
  async deleteProfileModule(id) {
    this.esProfileModuleService.deleteProfileModule(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsProfileModules };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getProfileModules();
        this.esProfileModule = null;
      }
    });
  }
}
