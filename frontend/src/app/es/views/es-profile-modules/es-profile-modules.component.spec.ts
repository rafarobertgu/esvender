import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsProfileModulesComponent } from './es-profile-modules.component';

describe('EsProfileModulesComponent', () => {
  let component: EsProfileModulesComponent;
  let fixture: ComponentFixture<EsProfileModulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsProfileModulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsProfileModulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
