import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsProfilesComponent } from './es-profiles.component';

describe('EsProfilesComponent', () => {
  let component: EsProfilesComponent;
  let fixture: ComponentFixture<EsProfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsProfilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsProfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
