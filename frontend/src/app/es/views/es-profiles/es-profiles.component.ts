import { Component, OnInit } from '@angular/core';
import {EsProfiles} from "../../../models/esProfiles";
import {EsProfileService} from "../../../services/es-profile.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-profiles',
  templateUrl: './es-profiles.component.html',
  styleUrls: ['./es-profiles.component.scss']
})
export class EsProfilesComponent implements OnInit {
  
  esProfiles:EsProfiles[] = [];
  esProfile:EsProfiles;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esProfileService: EsProfileService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getProfiles(id = null) {
    if(id) {
      this.esProfileService.getProfile(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfiles };
        this.esProfile = response.data;
      });
    } else {
      this.esProfileService.getProfiles().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfiles[] };
        this.esProfiles = response.data;
      });
    }
  }
  
  async saveProfile(id:any, esProfile:EsProfiles) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esProfileService.updateProfile(id, esProfile).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfiles};
        this.esProfile = response.data;
      });
    } else {
      this.esProfileService.createProfile(esProfile).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsProfiles};
        this.esProfile = response.data;
      })
    }
  }
  
  async deleteProfile(id) {
    this.esProfileService.deleteProfile(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsProfiles };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getProfiles();
        this.esProfile = null;
      }
    });
  }

}
