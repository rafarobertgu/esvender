import { Component, OnInit } from '@angular/core';
import {EsDictionaries} from "../../../models/esDictionaries";
import {EsDictionaryService} from "../../../services/es-dictionary.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-dictionaries',
  templateUrl: './es-dictionaries.component.html',
  styleUrls: ['./es-dictionaries.component.scss']
})
export class EsDictionariesComponent implements OnInit {
  
  esDictionaries:EsDictionaries[] = [];
  esDictionary:EsDictionaries;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esDictionaryService: EsDictionaryService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getDictionaries(id = null) {
    if(id) {
      this.esDictionaryService.getDictionary(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsDictionaries };
        this.esDictionary = response.data;
      });
    } else {
      this.esDictionaryService.getDictionaries().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsDictionaries[] };
        this.esDictionaries = response.data;
      });
    }
  }
  
  async saveDictionary(id:any, esDictionary:EsDictionaries) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esDictionaryService.updateDictionary(id, esDictionary).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsDictionaries};
        this.esDictionary = response.data;
      });
    } else {
      this.esDictionaryService.createDictionary(esDictionary).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsDictionaries};
        this.esDictionary = response.data;
      })
    }
  }
  
  async deleteDictionary(id) {
    this.esDictionaryService.deleteDictionary(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsDictionaries };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getDictionaries();
        this.esDictionary = null;
      }
    });
  }
}
