import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsDictionariesComponent } from './es-dictionaries.component';

describe('EsDictionariesComponent', () => {
  let component: EsDictionariesComponent;
  let fixture: ComponentFixture<EsDictionariesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsDictionariesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsDictionariesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
