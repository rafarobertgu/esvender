import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsEmployeesComponent } from './es-employees.component';

describe('EsEmployeesComponent', () => {
  let component: EsEmployeesComponent;
  let fixture: ComponentFixture<EsEmployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsEmployeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
