import { Component, OnInit } from '@angular/core';
import {EsEmployees} from "../../../models/esEmployees";
import {EsEmployeeService} from "../../../services/es-employee.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-employees',
  templateUrl: './es-employees.component.html',
  styleUrls: ['./es-employees.component.scss']
})
export class EsEmployeesComponent implements OnInit {
  
  esEmployees:EsEmployees[] = [];
  esEmployee:EsEmployees;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esEmployeeService: EsEmployeeService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getEmployees(id = null) {
    if(id) {
      this.esEmployeeService.getEmployee(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsEmployees };
        this.esEmployee = response.data;
      });
    } else {
      this.esEmployeeService.getEmployees().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsEmployees[] };
        this.esEmployees = response.data;
      });
    }
  }
  
  async saveEmployee(id:any, esEmployee:EsEmployees) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esEmployeeService.updateEmployee(id, esEmployee).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsEmployees};
        this.esEmployee = response.data;
      });
    } else {
      this.esEmployeeService.createEmployee(esEmployee).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsEmployees};
        this.esEmployee = response.data;
      })
    }
  }
  
  async deleteEmployee(id) {
    this.esEmployeeService.deleteEmployee(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsEmployees };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getEmployees();
        this.esEmployee = null;
      }
    });
  }
}
