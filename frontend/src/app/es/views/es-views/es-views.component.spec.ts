import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsViewsComponent } from './es-views.component';

describe('EsViewsComponent', () => {
  let component: EsViewsComponent;
  let fixture: ComponentFixture<EsViewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsViewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
