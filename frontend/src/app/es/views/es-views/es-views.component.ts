import { Component, OnInit } from '@angular/core';
import {EsViews} from "../../../models/esViews";
import {EsViewService} from "../../../services/es-view.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-views',
  templateUrl: './es-views.component.html',
  styleUrls: ['./es-views.component.scss']
})
export class EsViewsComponent implements OnInit {
  
  esViews:EsViews[] = [];
  esView:EsViews;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esViewService: EsViewService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getViews(id = null) {
    if(id) {
      this.esViewService.getView(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsViews };
        this.esView = response.data;
      });
    } else {
      this.esViewService.getViews().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsViews[] };
        this.esViews = response.data;
      });
    }
  }
  
  async saveView(id:any, esView:EsViews) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esViewService.updateView(id, esView).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsViews};
        this.esView = response.data;
      });
    } else {
      this.esViewService.createView(esView).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsViews};
        this.esView = response.data;
      })
    }
  }
  
  async deleteView(id) {
    this.esViewService.deleteView(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsViews };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getViews();
        this.esView = null;
      }
    });
  }

}
