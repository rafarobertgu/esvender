import { Component, OnInit } from '@angular/core';
import {EsLogs} from "../../../models/esLogs";
import {EsLogService} from "../../../services/es-log.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-logs',
  templateUrl: './es-logs.component.html',
  styleUrls: ['./es-logs.component.scss']
})
export class EsLogsComponent implements OnInit {
  
  esLogs:EsLogs[] = [];
  esLog:EsLogs;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esLogService: EsLogService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getLogs(id = null) {
    if(id) {
      this.esLogService.getLog(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsLogs };
        this.esLog = response.data;
      });
    } else {
      this.esLogService.getLogs().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsLogs[] };
        this.esLogs = response.data;
      });
    }
  }
  
  async saveLog(id:any, esLog:EsLogs) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esLogService.updateLog(id, esLog).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsLogs};
        this.esLog = response.data;
      });
    } else {
      this.esLogService.createLog(esLog).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsLogs};
        this.esLog = response.data;
      })
    }
  }
  
  async deleteLog(id) {
    this.esLogService.deleteLog(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsLogs };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getLogs();
        this.esLog = null;
      }
    });
  }
}
