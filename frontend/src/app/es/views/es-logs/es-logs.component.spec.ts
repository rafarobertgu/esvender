import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsLogsComponent } from './es-logs.component';

describe('EsLogsComponent', () => {
  let component: EsLogsComponent;
  let fixture: ComponentFixture<EsLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsLogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
