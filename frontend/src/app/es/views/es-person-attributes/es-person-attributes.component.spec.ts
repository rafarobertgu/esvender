import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsPersonAttributesComponent } from './es-person-attributes.component';

describe('EsPersonAttributesComponent', () => {
  let component: EsPersonAttributesComponent;
  let fixture: ComponentFixture<EsPersonAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsPersonAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsPersonAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
