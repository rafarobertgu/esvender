import { Component, OnInit } from '@angular/core';
import {EsPersonAttributes} from "../../../models/esPersonAttributes";
import {EsPersonAttributeService} from "../../../services/es-person-attribute.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-person-attributes',
  templateUrl: './es-person-attributes.component.html',
  styleUrls: ['./es-person-attributes.component.scss']
})
export class EsPersonAttributesComponent implements OnInit {
  
  esPersonAttributes:EsPersonAttributes[] = [];
  esPersonAttribute:EsPersonAttributes;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esPersonAttributeService: EsPersonAttributeService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getPersonAttributes(id = null) {
    if(id) {
      this.esPersonAttributeService.getPersonAttribute(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsPersonAttributes };
        this.esPersonAttribute = response.data;
      });
    } else {
      this.esPersonAttributeService.getPersonAttributes().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsPersonAttributes[] };
        this.esPersonAttributes = response.data;
      });
    }
  }
  
  async savePersonAttribute(id:any, esPersonAttribute:EsPersonAttributes) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esPersonAttributeService.updatePersonAttribute(id, esPersonAttribute).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsPersonAttributes};
        this.esPersonAttribute = response.data;
      });
    } else {
      this.esPersonAttributeService.createPersonAttribute(esPersonAttribute).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsPersonAttributes};
        this.esPersonAttribute = response.data;
      })
    }
  }
  
  async deletePersonAttribute(id) {
    this.esPersonAttributeService.deletePersonAttribute(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsPersonAttributes };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getPersonAttributes();
        this.esPersonAttribute = null;
      }
    });
  }
}
