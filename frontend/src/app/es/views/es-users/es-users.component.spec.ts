import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsUsersComponent } from './es-users.component';

describe('EsUsersComponent', () => {
  let component: EsUsersComponent;
  let fixture: ComponentFixture<EsUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
