import { Component, OnInit } from '@angular/core';
import {EsUsers} from "../../../models/esUsers";
import {EsUserService} from "../../../services/es-user.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-users',
  templateUrl: './es-users.component.html',
  styleUrls: ['./es-users.component.scss']
})
export class EsUsersComponent implements OnInit {
  
  esUsers:EsUsers[] = [];
  esUser:EsUsers;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esUserService: EsUserService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getUsers(id = null) {
    if(id) {
      this.esUserService.getUser(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsUsers };
        this.esUser = response.data;
      });
    } else {
      this.esUserService.getUsers().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsUsers[] };
        this.esUsers = response.data;
      });
    }
  }
  
  async saveUser(id:any, esUser:EsUsers) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esUserService.updateUser(id, esUser).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsUsers};
        this.esUser = response.data;
      });
    } else {
      this.esUserService.createUser(esUser).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsUsers};
        this.esUser = response.data;
      })
    }
  }
  
  async deleteUser(id) {
    this.esUserService.deleteUser(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsUsers };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getUsers();
        this.esUser = null;
      }
    });
  }

}
