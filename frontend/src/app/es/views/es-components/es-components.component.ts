import { Component, OnInit } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {EsComponents} from "../../../models/esComponents";
import {CommonService} from "../../../services/common.service";
import {EsParamService} from "../../../services/es-param.service";
import {EsComponentsService} from "../../../services/es-components.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-es-components',
  templateUrl: './es-components.component.html',
  styleUrls: ['./es-components.component.scss']
})
export class EsComponentsComponent implements OnInit {
  
  templatePath:string = `${environment.frontend.template.es}`;
  
  esComponents:EsComponents[] = [];
  esComponent:EsComponents;
  esComponentForm:FormGroup;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private commonService: CommonService,
    private esParamService: EsParamService,
    private esComponentService: EsComponentsService,
    private fb: FormBuilder
  ) {
    this.commonService.loadByUrl([
      this.templatePath + '/css/bootstrap.min.css',
      this.templatePath + '/font-awesome/css/font-awesome.css',
      this.templatePath + '/css/plugins/iCheck/custom.css',
      this.templatePath + '/css/animate.css',
      this.templatePath + '/css/style.css',
      this.templatePath + '/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
    ], [
      this.templatePath + '/js/jquery-2.1.1.js',
      this.templatePath + '/js/bootstrap.min.js',
      this.templatePath + '/js/plugins/metisMenu/jquery.metisMenu.js',
      this.templatePath + '/js/plugins/slimscroll/jquery.slimscroll.min.js',
      this.templatePath + '/js/inspinia.js',
      this.templatePath + '/js/plugins/pace/pace.min.js',
      this.templatePath + '/js/plugins/iCheck/icheck.min.js',
      this.templatePath + '/js/libs/form-iCheck.js',
    ]);
  }
  
  ngOnInit(): void {
    this.esParamService.setParamsByDictionaryIds([1,2,3,4,5], () => {
      this.esComponentForm = this.fb.group({
        'vie_id': new FormControl('', [Validators.required]),
        'vie_uid': new FormControl('', Validators.required),
        'com_code': new FormControl(''),
        'com_tag': new FormControl('', Validators.required),
        'com_description': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
        'com_par_type_id': new FormControl('', Validators.required),
        'com_par_type_uid': new FormControl(''),
        'com_par_status_id': new FormControl(''),
        'com_par_status_uid': new FormControl(''),
        'createdById': new FormControl(''),
        'createdByUid': new FormControl('', Validators.required),
        'updatedByUid': new FormControl(''),
        'updatedById': new FormControl(''),
        'dueAt': new FormControl(''),
      });
    });
  }
  
  async getComponents(id = null) {
    if(id) {
      this.esComponentService.getComponent(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsComponents };
        this.esComponent = response.data;
      });
    } else {
      this.esComponentService.getComponents().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsComponents[] };
        this.esComponents = response.data;
      });
    }
  }
  
  async saveComponent(id:any, esComponent:EsComponents) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esComponentService.updateComponent(id, esComponent).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsComponents};
        this.esComponent = response.data;
      });
    } else {
      this.esComponentService.createComponent(esComponent).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsComponents};
        this.esComponent = response.data;
      })
    }
  }
  
  async deleteComponent(id) {
    this.esComponentService.deleteComponent(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsComponents };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getComponents();
        this.esComponent = null;
      }
    });
  }
}
