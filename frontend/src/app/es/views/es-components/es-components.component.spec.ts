import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsComponentsComponent } from './es-components.component';

describe('EsComponentsComponent', () => {
  let component: EsComponentsComponent;
  let fixture: ComponentFixture<EsComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
