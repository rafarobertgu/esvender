import { Component, OnInit } from '@angular/core';
import {EsRoleProfiles} from "../../../models/esRoleProfiles";
import {EsRoleProfileService} from "../../../services/es-role-profile.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-role-profiles',
  templateUrl: './es-role-profiles.component.html',
  styleUrls: ['./es-role-profiles.component.scss']
})
export class EsRoleProfilesComponent implements OnInit {
  
  esRoleProfiles:EsRoleProfiles[] = [];
  esRoleProfile:EsRoleProfiles;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esRoleProfileService: EsRoleProfileService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getRoleProfiles(id = null) {
    if(id) {
      this.esRoleProfileService.getRoleProfile(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsRoleProfiles };
        this.esRoleProfile = response.data;
      });
    } else {
      this.esRoleProfileService.getRoleProfiles().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsRoleProfiles[] };
        this.esRoleProfiles = response.data;
      });
    }
  }
  
  async saveRoleProfile(id:any, esRoleProfile:EsRoleProfiles) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esRoleProfileService.updateRoleProfile(id, esRoleProfile).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsRoleProfiles};
        this.esRoleProfile = response.data;
      });
    } else {
      this.esRoleProfileService.createRoleProfile(esRoleProfile).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsRoleProfiles};
        this.esRoleProfile = response.data;
      })
    }
  }
  
  async deleteRoleProfile(id) {
    this.esRoleProfileService.deleteRoleProfile(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsRoleProfiles };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getRoleProfiles();
        this.esRoleProfile = null;
      }
    });
  }

}
