import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsRoleProfilesComponent } from './es-role-profiles.component';

describe('EsRoleProfilesComponent', () => {
  let component: EsRoleProfilesComponent;
  let fixture: ComponentFixture<EsRoleProfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsRoleProfilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsRoleProfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
