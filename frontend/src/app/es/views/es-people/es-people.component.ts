import { Component, OnInit } from '@angular/core';
import {EsPeople} from "../../../models/esPeople";
import {EsPersonService} from "../../../services/es-person.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-people',
  templateUrl: './es-people.component.html',
  styleUrls: ['./es-people.component.scss']
})
export class EsPeopleComponent implements OnInit {
  
  esPeople:EsPeople[] = [];
  esPerson:EsPeople;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esPersonService: EsPersonService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getPeople(id = null) {
    if(id) {
      this.esPersonService.getPerson(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsPeople };
        this.esPerson = response.data;
      });
    } else {
      this.esPersonService.getPeople().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsPeople[] };
        this.esPeople = response.data;
      });
    }
  }
  
  async savePerson(id:any, esPerson:EsPeople) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esPersonService.updatePerson(id, esPerson).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsPeople};
        this.esPerson = response.data;
      });
    } else {
      this.esPersonService.createPerson(esPerson).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsPeople};
        this.esPerson = response.data;
      })
    }
  }
  
  async deletePerson(id) {
    this.esPersonService.deletePerson(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsPeople };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getPeople();
        this.esPerson = null;
      }
    });
  }
}
