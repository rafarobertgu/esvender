import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsPeopleComponent } from './es-people.component';

describe('EsPeopleComponent', () => {
  let component: EsPeopleComponent;
  let fixture: ComponentFixture<EsPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
