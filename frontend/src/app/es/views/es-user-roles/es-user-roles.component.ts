import { Component, OnInit } from '@angular/core';
import {EsUserRoles} from "../../../models/esUserRoles";
import {EsUserRoleService} from "../../../services/es-user-role.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-user-roles',
  templateUrl: './es-user-roles.component.html',
  styleUrls: ['./es-user-roles.component.scss']
})
export class EsUserRolesComponent implements OnInit {
  
  esUserRoles:EsUserRoles[] = [];
  esUserRole:EsUserRoles;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esUserRoleService: EsUserRoleService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getUserRoles(id = null) {
    if(id) {
      this.esUserRoleService.getUserRole(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsUserRoles };
        this.esUserRole = response.data;
      });
    } else {
      this.esUserRoleService.getUserRoles().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsUserRoles[] };
        this.esUserRoles = response.data;
      });
    }
  }
  
  async saveUserRole(id:any, esUserRole:EsUserRoles) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esUserRoleService.updateUserRole(id, esUserRole).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsUserRoles};
        this.esUserRole = response.data;
      });
    } else {
      this.esUserRoleService.createUserRole(esUserRole).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsUserRoles};
        this.esUserRole = response.data;
      })
    }
  }
  
  async deleteUserRole(id) {
    this.esUserRoleService.deleteUserRole(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsUserRoles };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getUserRoles();
        this.esUserRole = null;
      }
    });
  }
}
