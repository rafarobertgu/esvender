import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsUserRolesComponent } from './es-user-roles.component';

describe('EsUserRolesComponent', () => {
  let component: EsUserRolesComponent;
  let fixture: ComponentFixture<EsUserRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsUserRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsUserRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
