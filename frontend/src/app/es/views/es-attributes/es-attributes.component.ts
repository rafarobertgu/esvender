import { Component, OnInit } from '@angular/core';
import {EsAttributes} from "../../../models/esAttributes";
import {EsAttributeService} from "../../../services/es-attribute.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-attributes',
  templateUrl: './es-attributes.component.html',
  styleUrls: ['./es-attributes.component.scss']
})
export class EsAttributesComponent implements OnInit {
  
  esAttributes:EsAttributes[] = [];
  esAttribute:EsAttributes;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esAttributeService: EsAttributeService,
    private esParamService:EsParamService
  ) { }

  ngOnInit(): void {
  }
  
  async getAttributes(id = null) {
    if(id) {
      this.esAttributeService.getAttribute(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsAttributes };
        this.esAttribute = response.data;
      });
    } else {
      this.esAttributeService.getAttributes().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsAttributes[] };
        this.esAttributes = response.data;
      });
    }
  }
  
  async saveAttribute(id:any, esAttribute:EsAttributes) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esAttributeService.updateAttribute(id, esAttribute).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsAttributes};
        this.esAttribute = response.data;
      });
    } else {
      this.esAttributeService.createAttribute(esAttribute).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsAttributes};
        this.esAttribute = response.data;
      })
    }
  }
  
  async deleteAttribute(id) {
    this.esAttributeService.deleteAttribute(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsAttributes };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getAttributes();
        this.esAttribute = null;
      }
    });
  }
}
