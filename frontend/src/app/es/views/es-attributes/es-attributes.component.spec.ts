import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsAttributesComponent } from './es-attributes.component';

describe('EsAttributesComponent', () => {
  let component: EsAttributesComponent;
  let fixture: ComponentFixture<EsAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
