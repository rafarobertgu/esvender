import { Component, OnInit } from '@angular/core';
import {EsObjects} from "../../../models/esObjects";
import {EsObjectService} from "../../../services/es-object.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-objects',
  templateUrl: './es-objects.component.html',
  styleUrls: ['./es-objects.component.scss']
})
export class EsObjectsComponent implements OnInit {
  
  esObjects:EsObjects[] = [];
  esObject:EsObjects;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esObjectService: EsObjectService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getObjects(id = null) {
    if(id) {
      this.esObjectService.getObject(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsObjects };
        this.esObject = response.data;
      });
    } else {
      this.esObjectService.getObjects().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsObjects[] };
        this.esObjects = response.data;
      });
    }
  }
  
  async saveObject(id:any, esObject:EsObjects) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esObjectService.updateObject(id, esObject).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsObjects};
        this.esObject = response.data;
      });
    } else {
      this.esObjectService.createObject(esObject).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsObjects};
        this.esObject = response.data;
      })
    }
  }
  
  async deleteObject(id) {
    this.esObjectService.deleteObject(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsObjects };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getObjects();
        this.esObject = null;
      }
    });
  }

}
