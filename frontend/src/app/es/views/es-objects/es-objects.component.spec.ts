import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsObjectsComponent } from './es-objects.component';

describe('EsObjectsComponent', () => {
  let component: EsObjectsComponent;
  let fixture: ComponentFixture<EsObjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsObjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsObjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
