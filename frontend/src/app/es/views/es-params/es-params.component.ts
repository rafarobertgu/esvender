import { Component, OnInit } from '@angular/core';
import {EsParams} from "../../../models/esParams";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-params',
  templateUrl: './es-params.component.html',
  styleUrls: ['./es-params.component.scss']
})
export class EsParamsComponent implements OnInit {
  
  esParams:EsParams[] = [];
  esParam:EsParams;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getParams(id = null) {
    if(id) {
      this.esParamService.getParam(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsParams };
        this.esParam = response.data;
      });
    } else {
      this.esParamService.getParams().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsParams[] };
        this.esParams = response.data;
      });
    }
  }
  
  async saveParam(id:any, esParam:EsParams) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esParamService.updateParam(id, esParam).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsParams};
        this.esParam = response.data;
      });
    } else {
      this.esParamService.createParam(esParam).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsParams};
        this.esParam = response.data;
      })
    }
  }
  
  async deleteParam(id) {
    this.esParamService.deleteParam(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsParams };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getParams();
        this.esParam = null;
      }
    });
  }

}
