import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsParamsComponent } from './es-params.component';

describe('EsParamsComponent', () => {
  let component: EsParamsComponent;
  let fixture: ComponentFixture<EsParamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsParamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsParamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
