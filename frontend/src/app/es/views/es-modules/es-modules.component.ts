import { Component, OnInit } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {CommonService} from "../../../services/common.service";
import {EsModules} from "../../../models/esModules";
import {EsModuleService} from "../../../services/es-module.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-modules',
  templateUrl: './es-modules.component.html',
  styleUrls: ['./es-modules.component.scss']
})
export class EsModulesComponent implements OnInit {
  
  templatePath:string = `${environment.frontend.template.es}`;
  esModules:EsModules[] = [];
  esModule:EsModules;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private commonService: CommonService,
    private esModuleService: EsModuleService,
    private esParamService:EsParamService
  ) {
    this.commonService.loadByUrl([
      this.templatePath + '/css/bootstrap.min.css',
      this.templatePath + '/font-awesome/css/font-awesome.css',
      this.templatePath + '/css/plugins/iCheck/custom.css',
      this.templatePath + '/css/animate.css',
      this.templatePath + '/css/style.css',
      this.templatePath + '/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
    ], [
      this.templatePath + '/js/jquery-2.1.1.js',
      this.templatePath + '/js/bootstrap.min.js',
      this.templatePath + '/js/plugins/metisMenu/jquery.metisMenu.js',
      this.templatePath + '/js/plugins/slimscroll/jquery.slimscroll.min.js',
      this.templatePath + '/js/inspinia.js',
      this.templatePath + '/js/plugins/pace/pace.min.js',
      this.templatePath + '/js/plugins/iCheck/icheck.min.js',
      this.templatePath + '/js/libs/form-iCheck.js',
    ]);
  }
  
  ngOnInit(): void {
  }
  
  async getModules(id = null) {
    if(id) {
      this.esModuleService.getModule(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsModules };
        this.esModule = response.data;
      });
    } else {
      this.esModuleService.getModules().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsModules[] };
        this.esModules = response.data;
      });
    }
  }
  
  async saveModule(id:any, esModule:EsModules) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esModuleService.updateModule(id, esModule).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsModules};
        this.esModule = response.data;
      });
    } else {
      this.esModuleService.createModule(esModule).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsModules};
        this.esModule = response.data;
      })
    }
  }
  
  async deleteModule(id) {
    this.esModuleService.deleteModule(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsModules };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getModules();
        this.esModule = null;
      }
    });
  }
}
