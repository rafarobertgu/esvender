import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsModulesComponent } from './es-modules.component';

describe('EsModulesComponent', () => {
  let component: EsModulesComponent;
  let fixture: ComponentFixture<EsModulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsModulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsModulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
