import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsAuthsComponent } from './es-auths.component';

describe('EsAuthsComponent', () => {
  let component: EsAuthsComponent;
  let fixture: ComponentFixture<EsAuthsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsAuthsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsAuthsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
