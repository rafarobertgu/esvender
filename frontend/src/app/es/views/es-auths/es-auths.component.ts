import { Component, OnInit } from '@angular/core';
import {EsAuths} from "../../../models/esAuths";
import {EsAuthService} from "../../../services/es-auth.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-auths',
  templateUrl: './es-auths.component.html',
  styleUrls: ['./es-auths.component.scss']
})
export class EsAuthsComponent implements OnInit {
  
  esAuths:EsAuths[] = [];
  esAuth:EsAuths;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esAuthService: EsAuthService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getAuths(id = null) {
    if(id) {
      this.esAuthService.getAuth(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsAuths };
        this.esAuth = response.data;
      });
    } else {
      this.esAuthService.getAuths().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsAuths[] };
        this.esAuths = response.data;
      });
    }
  }
  
  async saveAuth(id:any, esAuth:EsAuths) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esAuthService.updateAuth(id, esAuth).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsAuths};
        this.esAuth = response.data;
      });
    } else {
      this.esAuthService.createAuth(esAuth).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsAuths};
        this.esAuth = response.data;
      })
    }
  }
  
  async deleteAuth(id) {
    this.esAuthService.deleteAuth(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsAuths };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getAuths();
        this.esAuth = null;
      }
    });
  }
}
