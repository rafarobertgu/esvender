import { Component, OnInit } from '@angular/core';
import {EsFileAttributes} from "../../../models/esFileAttributes";
import {EsFileAttributeService} from "../../../services/es-file-attribute.service";
import {EsParamService} from "../../../services/es-param.service";

@Component({
  selector: 'app-es-file-attributes',
  templateUrl: './es-file-attributes.component.html',
  styleUrls: ['./es-file-attributes.component.scss']
})
export class EsFileAttributesComponent implements OnInit {
  
  esFileAttributes:EsFileAttributes[] = [];
  esFileAttribute:EsFileAttributes;
  isLoading:boolean = false;
  isEditing:boolean = false;
  
  constructor(
    private esFileAttributeService: EsFileAttributeService,
    private esParamService:EsParamService
  ) { }
  
  ngOnInit(): void {
  }
  
  async getFileAttributes(id = null) {
    if(id) {
      this.esFileAttributeService.getFileAttribute(id).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsFileAttributes };
        this.esFileAttribute = response.data;
      });
    } else {
      this.esFileAttributeService.getFileAttributes().subscribe(async res => {
        let response = res as { status: string, message: string, data: EsFileAttributes[] };
        this.esFileAttributes = response.data;
      });
    }
  }
  
  async saveFileAttribute(id:any, esFileAttribute:EsFileAttributes) {
    this.isLoading = true;
    if(this.isEditing) {
      this.isEditing = false;
      this.esFileAttributeService.updateFileAttribute(id, esFileAttribute).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsFileAttributes};
        this.esFileAttribute = response.data;
      });
    } else {
      this.esFileAttributeService.createFileAttribute(esFileAttribute).subscribe(async res => {
        let response = res as { status: string, message: string, data: EsFileAttributes};
        this.esFileAttribute = response.data;
      })
    }
  }
  
  async deleteFileAttribute(id) {
    this.esFileAttributeService.deleteFileAttribute(id).subscribe(async res => {
      let response = res as { status: string, message: string, data: EsFileAttributes };
      if(this.esParamService.paramRestSuccess.par_description == response.status) {
        this.getFileAttributes();
        this.esFileAttribute = null;
      }
    });
  }
}
