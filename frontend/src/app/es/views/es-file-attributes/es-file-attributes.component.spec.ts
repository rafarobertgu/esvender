import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsFileAttributesComponent } from './es-file-attributes.component';

describe('EsFileAttributesComponent', () => {
  let component: EsFileAttributesComponent;
  let fixture: ComponentFixture<EsFileAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsFileAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsFileAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
