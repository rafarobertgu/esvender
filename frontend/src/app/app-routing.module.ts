import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'admin', loadChildren: () => import('./demo/admin/admin.module').then(m => m.AdminModule) },
  { path: 'front', loadChildren: () => import('./demo/front/front.module').then(m => m.FrontModule) },
  { path: 'es', loadChildren: () => import('./es/es.module').then(m => m.EsModule) },
  { path: '', loadChildren: () => import('./vender/vender.module').then(m => m.VenderModule) }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
