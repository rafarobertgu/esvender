jQuery(document).ready(function ($) {
  setTimeout(() => {
    fixAngularFeature();
  }, 500);
  // $('ul.tab-nav li a').click(() => {
  //   fixAngularFeature();
  //   return false
  // });
});


function fixAngularFeature() {
  let tabs = $('.tab-container .tab-content > div');
  let classList = $('.tab-container .tab-content').attr('class').split(/\s+/);
  $('.tab-container').html($('.ui-tabs-panel'));
  tabs.each((indTab, Tab) => {
    $($('.tab-container .ui-tabs-panel')[indTab]).html(Tab);
    classList.forEach((clas) => {
      $($('.tab-container .ui-tabs-panel')[indTab]).addClass(clas);
    })
  })
}
