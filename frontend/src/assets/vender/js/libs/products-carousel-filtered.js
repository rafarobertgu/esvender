var oTab = {
  init: function() {
    $('.tabs .tab-nav button').click((tab) => {
      oTab.changeTab(tab);
    });
    var ocProducts = $(".products-carousel");
    ocProducts.owlCarousel({
      margin: 20,
      nav: false,
      autoplay: false,
      autoplayHoverPause: true,
      dots: true,
      responsive:{
        0:{ items:1 },
        600:{ items:2 },
        1000:{ items:3 },
        1200:{ items:4 }
      }
    }).ready(() => {
      setTimeout(() => {
        oTab.showJustFirstTab();
      },1000);
    });
  },
  changeTab: function(tab) {
    var id = $(tab.target).attr('id');
    $('.tab-container .tab-content').css('display','none');
    var elment = $('.tab-container').find('#'+id);
    $(elment).css('display','block');
  },
  showJustFirstTab: function() {
    $('.tab-container .tab-content').css('display','none');
    $($('.tab-container .tab-content')[0]).css('display','block');
  }
}

jQuery(document).ready(function($) {
  oTab.init();
});
