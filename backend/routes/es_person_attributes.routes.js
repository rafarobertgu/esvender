const express = require("express");
const router = express.Router();

const esPersonAttributesCtrl = require("../controllers/es_person_attributes.controller");

router.get("/", async (req, res) => {
	res.render('person_attribute', {
		title: 'CRUD',
		person_attributes: await esPersonAttributesCtrl.service.getAllEsPersonAttributes()
	});
});

router.get("/api-esvender/", (req, res) => esPersonAttributesCtrl.getPersonAttributes(req, res));
router.post("/api-esvender/", (req, res) => esPersonAttributesCtrl.createPersonAttribute(req, res));
router.get("/api-esvender/:id", (req, res) => esPersonAttributesCtrl.getPersonAttribute(req, res));
router.put("/api-esvender/:id", (req, res) => esPersonAttributesCtrl.updatePersonAttribute(req, res));
router.delete("/api-esvender/:id", (req, res) => esPersonAttributesCtrl.deletePersonAttribute(req, res));

module.exports = router;
