const express = require("express");
const router = express.Router();

const esFileAttributesCtrl = require("../controllers/es_file_attributes.controller");

router.get("/", async (req, res) => {
	res.render('file_attributes', {
		title: 'CRUD',
		file_attributes: await esFileAttributesCtrl.service.getAllEsFileAttributes()
	});
});

router.get("/api-esvender/", (req, res) => esFileAttributesCtrl.getFileAttributes(req, res));
router.post("/api-esvender/", (req, res) => esFileAttributesCtrl.createFileAttribute(req, res));
router.get("/api-esvender/:id", (req, res) => esFileAttributesCtrl.getFileAttribute(req, res));
router.put("/api-esvender/:id", (req, res) => esFileAttributesCtrl.updateFileAttribute(req, res));
router.delete("/api-esvender/:id", (req, res) => esFileAttributesCtrl.deleteFileAttribute(req, res));

module.exports = router;
