const express = require("express");
const router = express.Router();

const esProfileModulesCtrl = require("../controllers/es_profile_modules.controller");

router.get("/", async (req, res) => {
	res.render('profile_modules', {
		title: 'CRUD',
		profile_modules: await esProfileModulesCtrl.service.getAllEsProfileModules()
	});
});

router.get("/api-esvender/", (req, res) => esProfileModulesCtrl.getProfileModules(req, res));
router.post("/api-esvender/", (req, res) => esProfileModulesCtrl.createProfileModule(req, res));
router.get("/api-esvender/:id", (req, res) => esProfileModulesCtrl.getProfileModule(req, res));
router.put("/api-esvender/:id", (req, res) => esProfileModulesCtrl.updateProfileModule(req, res));
router.delete("/api-esvender/:id", (req, res) => esProfileModulesCtrl.deleteProfileModule(req, res));

module.exports = router;
