const express = require("express");
const router = express.Router();

const esUsersCtrl = require("../controllers/es_users.controller");

router.get("/", async (req, res) => {
	res.render('users', {
		title: 'CRUD',
		users: await esUsersCtrl.service.getAllEsUsers()
	});
});

router.get("/api-esvender/", (req, res) => esUsersCtrl.getUsers(req, res));
router.post("/api-esvender/", (req, res) => esUsersCtrl.createUser(req, res));
router.get("/api-esvender/:id", (req, res) => esUsersCtrl.getUser(req, res));
router.put("/api-esvender/:id", (req, res) => esUsersCtrl.updateUser(req, res));
router.delete("/api-esvender/:id", (req, res) => esUsersCtrl.deleteUser(req, res));

module.exports = router;
