const express = require("express");
const router = express.Router();

const esProfileViewsCtrl = require("../controllers/es_profile_views.controller");

router.get("/", async (req, res) => {
	res.render('profile_views', {
		title: 'CRUD',
		profile_views: await esProfileViewsCtrl.service.getAllEsProfileViews()
	});
});

router.get("/api-esvender/", (req, res) => esProfileViewsCtrl.getProfileViews(req, res));
router.post("/api-esvender/", (req, res) => esProfileViewsCtrl.createProfileView(req, res));
router.get("/api-esvender/:id", (req, res) => esProfileViewsCtrl.getProfileView(req, res));
router.put("/api-esvender/:id", (req, res) => esProfileViewsCtrl.updateProfileView(req, res));
router.delete("/api-esvender/:id", (req, res) => esProfileViewsCtrl.deleteProfileView(req, res));

module.exports = router;
