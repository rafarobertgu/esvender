const express = require("express");
const router = express.Router();

const esFieldsCtrl = require("../controllers/es_fields.controller");

router.get("/", async (req, res) => {
	res.render('fields', {
		title: 'CRUD',
		fields: await esFieldsCtrl.service.getAllEsFields()
	});
});

router.get("/api-esvender/", (req, res) => esFieldsCtrl.getFields(req, res));
router.post("/api-esvender/", (req, res) => esFieldsCtrl.createField(req, res));
router.get("/api-esvender/:id", (req, res) => esFieldsCtrl.getField(req, res));
router.put("/api-esvender/:id", (req, res) => esFieldsCtrl.updateField(req, res));
router.delete("/api-esvender/:id", (req, res) => esFieldsCtrl.deleteField(req, res));

module.exports = router;
