const express = require("express");
const router = express.Router();

const esFilesCtrl = require("../controllers/es_files.controller");

router.get("/", async (req, res) => {
	res.render('files', {
		title: 'CRUD',
		files: await esFilesCtrl.service.getAllEsFiles()
	});
});

router.get("/api-esvender/", (req, res) => esFilesCtrl.getFiles(req, res));
router.post("/api-esvender/", (req, res) => esFilesCtrl.createFile(req, res));
router.get("/api-esvender/:id", (req, res) => esFilesCtrl.getFile(req, res));
router.put("/api-esvender/:id", (req, res) => esFilesCtrl.updateFile(req, res));
router.delete("/api-esvender/:id", (req, res) => esFilesCtrl.deleteFile(req, res));

module.exports = router;
