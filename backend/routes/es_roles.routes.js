const express = require("express");
const router = express.Router();

const esRolesCtrl = require("../controllers/es_roles.controller");

router.get("/", async (req, res) => {
	res.render('roles', {
		title: 'CRUD',
		roles: await esRolesCtrl.service.getAllEsRoles()
	});
});

router.get("/api-esvender/", (req, res) => esRolesCtrl.getRoles(req, res));
router.post("/api-esvender/", (req, res) => esRolesCtrl.createRole(req, res));
router.get("/api-esvender/:id", (req, res) => esRolesCtrl.getRole(req, res));
router.put("/api-esvender/:id", (req, res) => esRolesCtrl.updateRole(req, res));
router.delete("/api-esvender/:id", (req, res) => esRolesCtrl.deleteRole(req, res));

module.exports = router;
