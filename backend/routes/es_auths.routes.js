const express = require("express");
const router = express.Router();
const passport = require('passport');
const esAuthsCtrl = require("../controllers/es_auths.controller");

const isLoggedIn = (req, res, next) => {
  if(req.isAuthenticated()){
    return next()
  }
  return res.status(400).json({"statusCode" : 400, "message" : "not authenticated"})
}

router.get("/", async (req, res) => {
	res.json({
		title: 'AUTH',
		message: req.flash('loginMessage')
	});
	// res.render('index', {
	// 	title: 'AUTH',
	// });
});
router.get("/login", async (req, res) => {
	res.json(req.user);

	// res.render('login', {
	// 	title: 'Login',
	// 	message: req.flash('loginMessage')
	// });
});
router.get("/signup", async (req, res) => {
	res.json({
		title: 'signup',
		message: req.flash('signupMessage')
	});
	// res.render('signup', {
	// 	title: 'Login',
	// 	message: req.flash('signupMessage')
	// });
});

// router.post("/signup", passport.authenticate('local-signup', {
// 	successRedirect: '/es-auths/profile',
// 	failureRedirect: '/es-auths/signup',
// 	failureFlash: true
// }));

// router.post("/login", passport.authenticate('local-login', {
//   successRedirect: '/es-auths/profile',
//   failureRedirect: '/es-auths/signup',
//   failureFlash: true
// }));

const login = () => {
  return (req, res, next) => {
    passport.authenticate('local-login', (error, user, info) => {
      if(error) {
        if(!user) {
          res.status(400).json({"statusCode" : 200 ,"message" : info});
        }
        res.status(400).json({"statusCode" : 200 ,"message" : error});
      }
      req.login(user, (error) => {
        if (error) return next(error);
        next();
      });
    })(req, res, next);
  }
}
const signup = () => {
	return (req, res, next) => {
		passport.authenticate('local-signup', (error, user, info) => {
			if(error) {
				if(!user) {
					res.status(400).json({"statusCode" : 200 ,"message" : info});
				}
				res.status(400).json({"statusCode" : 200 ,"message" : error});
			}
			req.login(user, (error) => {
				if (error) return next(error);
				next();
			});
		})(req, res, next);
	}
}
router.post('/login', login() , (req, res) => {
  res.status(200).json({statusCode : 200 ,message : "Hello, nice to see you again", data: req.user});
});
router.post('/signup', signup() , (req, res) => {
  res.status(200).json({statusCode : 200 ,message : "Hello, thank you for sign up", data: req.user});
});
router.get("/profile", isLoggedIn, (req, res) => {
  res.json(req.user);
	// res.render('profile', {
	// 	title: 'Profile',
	// 	esUser: req.user
	// });
});

router.get("/facebook", passport.authenticate('facebook', {scope: ['email', 'user_age_range', 'user_gender']}));

router.get("/facebook/callback", passport.authenticate('facebook', {
	successRedirect: '/es-auths/profile',
	failureRedirect: '/es-auths/'
}));

router.get('/logout', (req, res) => {
	req.logOut();
	res.redirect('/es-auths/');
});

module.exports = router;
