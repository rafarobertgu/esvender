const express = require("express");
const router = express.Router();

const esFieldParamsCtrl = require("../controllers/es_field_params.controller");

router.get("/", async (req, res) => {
	res.render('field_params', {
		title: 'CRUD',
		views: await esFieldParamsCtrl.service.getAllEsFieldParams()
	});
});

router.get("/api-esvender/", (req, res) => esFieldParamsCtrl.getFieldParams(req, res));
router.post("/api-esvender/", (req, res) => esFieldParamsCtrl.createFieldParam(req, res));
router.get("/api-esvender/:id", (req, res) => esFieldParamsCtrl.getFieldParam(req, res));
router.put("/api-esvender/:id", (req, res) => esFieldParamsCtrl.updateFieldParam(req, res));
router.delete("/api-esvender/:id", (req, res) => esFieldParamsCtrl.deleteFieldParam(req, res));

module.exports = router;
