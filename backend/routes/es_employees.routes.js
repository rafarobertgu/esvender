const express = require("express");
const router = express.Router();

const esEmployeesCtrl = require("../controllers/es_employees.controller");

router.get("/", async (req, res) => {
	res.render('employees', {
		title: 'CRUD',
		employees: await esEmployeesCtrl.service.getAllEsEmployees()
	});
});

router.get("/api-esvender/", (req, res) => esEmployeesCtrl.getEmployees(req, res));
router.post("/api-esvender/", (req, res) => esEmployeesCtrl.createEmployee(req, res));
router.get("/api-esvender/:id", (req, res) => esEmployeesCtrl.getEmployees(req, res));
router.put("/api-esvender/:id", (req, res) => esEmployeesCtrl.updateEmployee(req, res));
router.delete("/api-esvender/:id", (req, res) => esEmployeesCtrl.deleteEmployee(req, res));

module.exports = router;
