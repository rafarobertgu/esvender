const express = require("express");
const router = express.Router();

const esViewsCtrl = require("../controllers/es_views.controller");

router.get("/", async (req, res) => {
	res.render('views', {
		title: 'CRUD',
		views: await esViewsCtrl.service.getAllEsViews()
	});
});

router.get("/api-esvender/", (req, res) => esViewsCtrl.getViews(req, res));
router.post("/api-esvender/", (req, res) => esViewsCtrl.createView(req, res));
router.get("/api-esvender/:id", (req, res) => esViewsCtrl.getView(req, res));
router.put("/api-esvender/:id", (req, res) => esViewsCtrl.updateView(req, res));
router.delete("/api-esvender/:id", (req, res) => esViewsCtrl.deleteView(req, res));

router.get("/api-esvender/findViewsByModuleId/:ids", (req, res) => esViewsCtrl.findViewsByModuleId(req, res));

module.exports = router;
