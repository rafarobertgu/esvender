const express = require("express");
const router = express.Router();

const esPeopleCtrl = require("../controllers/es_people.controller");

router.get("/", async (req, res) => {
	res.render('persons', {
		title: 'CRUD',
		persons: await esPeopleCtrl.service.getAllEsPeople()
	});
});

router.get("/api-esvender/", (req, res) => esPeopleCtrl.getPeople(req, res));
router.post("/api-esvender/", (req, res) => esPeopleCtrl.createPerson(req, res));
router.get("/api-esvender/:id", (req, res) => esPeopleCtrl.getPerson(req, res));
router.put("/api-esvender/:id", (req, res) => esPeopleCtrl.updatePerson(req, res));
router.delete("/api-esvender/:id", (req, res) => esPeopleCtrl.deletePerson(req, res));

module.exports = router;
