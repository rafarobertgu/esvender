const express = require("express");
const router = express.Router();

const esUserRolesCtrl = require("../controllers/es_user_roles.controller");

router.get("/", async (req, res) => {
	res.render('user_roles', {
		title: 'CRUD',
		user_roles: await esUserRolesCtrl.service.getAllEsUserRoles()
	});
});

router.get("/api-esvender/", (req, res) => esUserRolesCtrl.getUserRoles(req, res));
router.post("/api-esvender/", (req, res) => esUserRolesCtrl.createUserRole(req, res));
router.get("/api-esvender/:id", (req, res) => esUserRolesCtrl.getUserRole(req, res));
router.put("/api-esvender/:id", (req, res) => esUserRolesCtrl.updateUserRole(req, res));
router.delete("/api-esvender/:id", (req, res) => esUserRolesCtrl.deleteUserRole(req, res));

module.exports = router;
