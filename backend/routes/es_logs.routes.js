const express = require("express");
const router = express.Router();

const esLogsCtrl = require("../controllers/es_logs.controller");

router.get("/", async (req, res) => {
	res.render('logs', {
		title: 'CRUD',
		logs: await esLogsCtrl.service.getAllEsLogs()
	});
});

router.get("/api-esvender/", (req, res) => esLogsCtrl.getLogs(req, res));
router.post("/api-esvender/", (req, res) => esLogsCtrl.createLog(req, res));
router.get("/api-esvender/:id", (req, res) => esLogsCtrl.getLog(req, res));
router.put("/api-esvender/:id", (req, res) => esLogsCtrl.updateLog(req, res));
router.delete("/api-esvender/:id", (req, res) => esLogsCtrl.deleteLog(req, res));

module.exports = router;
