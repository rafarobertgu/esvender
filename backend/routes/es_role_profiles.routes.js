const express = require("express");
const router = express.Router();

const esRoleProfilesCtrl = require("../controllers/es_role_profiles.controller");

router.get("/", async (req, res) => {
	res.render('role_profiles', {
		title: 'CRUD',
		role_profiles: await esRoleProfilesCtrl.service.getAllEsRoleProfiles()
	});
});

router.get("/api-esvender/", (req, res) => esRoleProfilesCtrl.getRoleProfiles(req, res));
router.post("/api-esvender/", (req, res) => esRoleProfilesCtrl.createRoleProfile(req, res));
router.get("/api-esvender/:id", (req, res) => esRoleProfilesCtrl.getRoleProfile(req, res));
router.put("/api-esvender/:id", (req, res) => esRoleProfilesCtrl.updateRoleProfile(req, res));
router.delete("/api-esvender/:id", (req, res) => esRoleProfilesCtrl.deleteRoleProfile(req, res));

module.exports = router;
