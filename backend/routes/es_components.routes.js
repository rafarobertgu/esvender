const express = require("express");
const router = express.Router();

const esComponentsCtrl = require("../controllers/es_components.controller");

router.get("/", async (req, res) => {
	res.render('components', {
		title: 'CRUD',
		components: await esComponentsCtrl.service.getAllEsComponents()
	});
});

router.get("/api-esvender/", (req, res) => esComponentsCtrl.getComponents(req, res));
router.post("/api-esvender/", (req, res) => esComponentsCtrl.createComponent(req, res));
router.get("/api-esvender/:id", (req, res) => esComponentsCtrl.getComponent(req, res));
router.put("/api-esvender/:id", (req, res) => esComponentsCtrl.updateComponent(req, res));
router.delete("/api-esvender/:id", (req, res) => esComponentsCtrl.deleteComponent(req, res));

module.exports = router;
