const express = require("express");
const router = express.Router();

const esModulesCtrl = require("../controllers/es_modules.controller");

router.get("/", async (req, res) => {
	res.render('modules', {
		title: 'CRUD',
		modules: await esModulesCtrl.service.getAllEsModules()
	});
});

router.get("/api-esvender/", (req, res) => esModulesCtrl.getModules(req, res));
router.post("/api-esvender/", (req, res) => esModulesCtrl.createModule(req, res));
router.get("/api-esvender/:id", (req, res) => esModulesCtrl.getModule(req, res));
router.put("/api-esvender/:id", (req, res) => esModulesCtrl.updateModule(req, res));
router.delete("/api-esvender/:id", (req, res) => esModulesCtrl.deleteModule(req, res));

module.exports = router;
