const express = require("express");
const router = express.Router();

const esParamsCtrl = require("../controllers/es_params.controller");

router.get("/", async (req, res) => {
	res.render('params', {
		title: 'CRUD',
		params: await esParamsCtrl.service.getAllEsParams()
	});
});

router.get("/api-esvender/", (req, res) => esParamsCtrl.getParams(req, res));
router.post("/api-esvender/", (req, res) => esParamsCtrl.createParam(req, res));
router.get("/api-esvender/:id", (req, res) => esParamsCtrl.getParam(req, res));
router.put("/api-esvender/:id", (req, res) => esParamsCtrl.updateParam(req, res));
router.delete("/api-esvender/:id", (req, res) => esParamsCtrl.deleteParam(req, res));

router.get("/api-esvender/findParamsByDictionaryIds/:ids", (req, res) => esParamsCtrl.findParamsByDictionaryIds(req, res));

module.exports = router;
