const express = require("express");
const router = express.Router();

const esObjectAttributesCtrl = require("../controllers/es_object_attributes.controller");

router.get("/", async (req, res) => {
	res.render('object_attributes', {
		title: 'CRUD',
		object_attributes: await esObjectAttributesCtrl.service.getAllEsObjectAttributes()
	});
});

router.get("/api-esvender/", (req, res) => esObjectAttributesCtrl.getObjectAttributes(req, res));
router.post("/api-esvender/", (req, res) => esObjectAttributesCtrl.createObjectAttribute(req, res));
router.get("/api-esvender/:id", (req, res) => esObjectAttributesCtrl.getObjectAttribute(req, res));
router.put("/api-esvender/:id", (req, res) => esObjectAttributesCtrl.updateObjectAttribute(req, res));
router.delete("/api-esvender/:id", (req, res) => esObjectAttributesCtrl.deleteObjectAttribute(req, res));

module.exports = router;
