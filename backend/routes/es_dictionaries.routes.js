const express = require("express");
const router = express.Router();

const esDictionariesCtrl = require("../controllers/es_dictionaries.controller");

router.get("/", async (req, res) => {
	res.render('dictionaries', {
		title: 'CRUD',
		dictionaries: await esDictionariesCtrl.service.getAllEsDictionaries()
	});
});

router.get("/api-esvender/", (req, res) => esDictionariesCtrl.getDictionaries(req, res));
router.post("/api-esvender/", (req, res) => esDictionariesCtrl.createDictionary(req, res));
router.get("/api-esvender/:id", (req, res) => esDictionariesCtrl.getDictionary(req, res));
router.put("/api-esvender/:id", (req, res) => esDictionariesCtrl.updateDictionary(req, res));
router.delete("/api-esvender/:id", (req, res) => esDictionariesCtrl.deleteDictionary(req, res));

module.exports = router;
