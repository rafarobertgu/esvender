const express = require("express");
const router = express.Router();

const esObjectsCtrl = require("../controllers/es_objects.controller");

router.get("/", async (req, res) => {
	res.render('objects', {
		title: 'CRUD',
		objects: await esObjectsCtrl.service.getAllEsObjects()
	});
});

router.get("/api-esvender/", (req, res) => esObjectsCtrl.getObjects(req, res));
router.post("/api-esvender/", (req, res) => esObjectsCtrl.createObject(req, res));
router.get("/api-esvender/:id", (req, res) => esObjectsCtrl.getObject(req, res));
router.put("/api-esvender/:id", (req, res) => esObjectsCtrl.updateObject(req, res));
router.delete("/api-esvender/:id", (req, res) => esObjectsCtrl.deleteObject(req, res));

module.exports = router;
