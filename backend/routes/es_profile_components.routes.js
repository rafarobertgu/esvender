const express = require("express");
const router = express.Router();

const esProfileComponentsCtrl = require("../controllers/es_profile_components.controller");

router.get("/", async (req, res) => {
	res.render('profile_components', {
		title: 'CRUD',
		profile_components: await esProfileComponentsCtrl.service.getAllEsProfileComponents()
	});
});

router.get("/api-esvender/", (req, res) => esProfileComponentsCtrl.getProfileComponents(req, res));
router.post("/api-esvender/", (req, res) => esProfileComponentsCtrl.createProfileComponent(req, res));
router.get("/api-esvender/:id", (req, res) => esProfileComponentsCtrl.getProfileComponent(req, res));
router.put("/api-esvender/:id", (req, res) => esProfileComponentsCtrl.updateProfileComponent(req, res));
router.delete("/api-esvender/:id", (req, res) => esProfileComponentsCtrl.deleteProfileComponent(req, res));

module.exports = router;
