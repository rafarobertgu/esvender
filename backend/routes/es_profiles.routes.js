const express = require("express");
const router = express.Router();

const esProfilesCtrl = require("../controllers/es_profiles.controller");

router.get("/", async (req, res) => {
	res.render('profiles', {
		title: 'CRUD',
		profiles: await esProfilesCtrl.service.getAllEsProfiles()
	});
});

router.get("/api-esvender/", (req, res) => esProfilesCtrl.getProfiles(req, res));
router.post("/api-esvender/", (req, res) => esProfilesCtrl.createProfile(req, res));
router.get("/api-esvender/:id", (req, res) => esProfilesCtrl.getProfile(req, res));
router.put("/api-esvender/:id", (req, res) => esProfilesCtrl.updateProfile(req, res));
router.delete("/api-esvender/:id", (req, res) => esProfilesCtrl.deleteProfile(req, res));

module.exports = router;
