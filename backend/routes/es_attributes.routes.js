const express = require("express");
const router = express.Router();

const esAttributesCtrl = require("../controllers/es_attributes.controller");

router.get("/", async (req, res) => {
	res.render('attributes', {
		title: 'CRUD',
		attributes: await esAttributesCtrl.service.getAllEsAttributes()
	});
});

router.get("/api-esvender/", (req, res) => esAttributesCtrl.getAttributes(req, res));
router.post("/api-esvender/", (req, res) => esAttributesCtrl.createAttribute(req, res));
router.get("/api-esvender/:id", (req, res) => esAttributesCtrl.getAttribute(req, res));
router.put("/api-esvender/:id", (req, res) => esAttributesCtrl.updateAttribute(req, res));
router.delete("/api-esvender/:id", (req, res) => esAttributesCtrl.deleteAttribute(req, res));

module.exports = router;
