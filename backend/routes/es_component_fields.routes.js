const express = require("express");
const router = express.Router();

const esComponentFieldsCtrl = require("../controllers/es_component_fields.controller");

router.get("/", async (req, res) => {
	res.render('component_fields', {
		title: 'CRUD',
		views: await esComponentFieldsCtrl.service.getAllEsComponentFields()
	});
});

router.get("/api-esvender/", (req, res) => esComponentFieldsCtrl.getComponentFields(req, res));
router.post("/api-esvender/", (req, res) => esComponentFieldsCtrl.createComponentField(req, res));
router.get("/api-esvender/:id", (req, res) => esComponentFieldsCtrl.getComponentField(req, res));
router.put("/api-esvender/:id", (req, res) => esComponentFieldsCtrl.updateComponentField(req, res));
router.delete("/api-esvender/:id", (req, res) => esComponentFieldsCtrl.deleteComponentField(req, res));

module.exports = router;
