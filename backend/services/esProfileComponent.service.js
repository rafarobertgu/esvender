const models = require('../models');
const sql = process.env.SQL;
class EsProfileComponentService {
	static async getAllEsProfileComponents() {
		try {
			if(sql) {
				return await models.esProfileComponents.findAll();
			} else {
				return await models.esProfileComponents.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsProfileComponent(newEsProfileComponent) {
		try {
			let esProfileComponent;
			if(sql) {
				esProfileComponent = await models.esProfileComponents.create(newEsProfileComponent);
			} else {
				esProfileComponent = new models.esProfileComponents(newEsProfileComponent);
				await esProfileComponent.save();
			}
			return esProfileComponent;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsProfileComponent(id, updateEsProfileComponent) {
		try {
			let esProfileComponent;
			if(sql) {
				esProfileComponent = await models.esProfileComponents.findOne({where: { id: Number(id) }});
				if (esProfileComponent) {
					esProfileComponent = await models.esProfileComponents.update(updateEsProfileComponent, { where: { id: Number(id) } });
				}
			} else {
				esProfileComponent = await models.esProfileComponents.findByIdAndUpdate(id, {$set: updateEsProfileComponent}, {new: true});
			}
			return esProfileComponent;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsProfileComponent(id) {
		try {
			let esProfileComponent;
			if(sql) {
					esProfileComponent = await models.esProfileComponents.findOne({where: { id: Number(id) }});
			} else {
					esProfileComponent = await models.esProfileComponents.findById(id);
			}
			return esProfileComponent;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsProfileComponent(id) {
		try {
			let esProfileComponent;
			if(sql) {
				esProfileComponent = await models.esProfileComponents.findOne({ where: { id: Number(id) } });
				if (esProfileComponent) {
					esProfileComponent = await models.esProfileComponents.destroy({where: { id: Number(id) }});
				}
			} else {
				esProfileComponent = await models.esProfileComponents.findByIdAndRemove(id);
			}
			return esProfileComponent;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsProfileComponentService;
