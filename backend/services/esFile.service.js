const models = require('../models');
const sql = process.env.SQL;
class EsFileService {
	static async getAllEsFiles() {
		try {
			if(sql) {
				return await models.esFiles.findAll();
			} else {
				return await models.esFiles.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsFile(newEsFile) {
		try {
			let esFile;
			if(sql) {
				esFile = await models.esFiles.create(newEsFile);
			} else {
				esFile = new models.esFiles(newEsFile);
				await esFile.save();
			}
			return esFile;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsFile(id, updateEsFile) {
		try {
			let esFile;
			if(sql) {
				esFile = await models.esFiles.findOne({where: { id: Number(id) }});
				if (esFile) {
					esFile = await models.esFiles.update(updateEsFile, { where: { id: Number(id) } });
				}
			} else {
				esFile = await models.esFiles.findByIdAndUpdate(id, {$set: updateEsFile}, {new: true});
			}
			return esFile;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsFile(id) {
		try {
			let esFile;
			if(sql) {
					esFile = await models.esFiles.findOne({where: { id: Number(id) }});
			} else {
					esFile = await models.esFiles.findById(id);
			}
			return esFile;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsFile(id) {
		try {
			let esFile;
			if(sql) {
				esFile = await models.esFiles.findOne({ where: { id: Number(id) } });
				if (esFile) {
					esFile = await models.esFiles.destroy({where: { id: Number(id) }});
				}
			} else {
				esFile = await models.esFiles.findByIdAndRemove(id);
			}
			return esFile;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsFileService;
