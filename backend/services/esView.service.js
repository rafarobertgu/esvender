const models = require('../models');
const sql = process.env.SQL;
class EsViewService {
	static async getAllEsViews() {
		try {
			if(sql) {
				return await models.esViews.findAll();
			} else {
				return await models.esViews.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsView(newEsView) {
		try {
			let esView;
			if(sql) {
				esView = await models.esViews.create(newEsView);
			} else {
				esView = new models.esViews(newEsView);
				await esView.save();
			}
			return esView;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsView(id, updateEsView) {
		try {
			let esView;
			if(sql) {
				esView = await models.esViews.findOne({where: { id: Number(id) }});
				if (esView) {
					esView = await models.esViews.update(updateEsView, { where: { id: Number(id) } });
				}
			} else {
				esView = await models.esViews.findByIdAndUpdate(id, {$set: updateEsView}, {new: true});
			}
			return esView;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsView(id) {
		try {
			let esView;
			if(sql) {
					esView = await models.esViews.findOne({where: { id: Number(id) }});
			} else {
					esView = await models.esViews.findById(id);
			}
			return esView;
		} catch (error) {
			throw error;
		}
	}

	static async findViewsByModuleId(ids) {
		try {
			let esView;
			if(sql) {
				esView = await models.esViews.findAll({where: { vie_module_id: ids },
					include: [
						{
							model: models.esModules,
							as: 'VieModuleId'
						},{
							model: models.esParams,
							as: 'VieParStatusId'
						}
					]
				});
			} else {
				esView = await models.esViews.find({ vie_module_uid: ids,
					$lookup: [
						{
							from: models.esModules,
							as: 'VieModuleId'
						}, {
							from: models.esParams,
							as: 'VieParStatusId'
						}
					]});
			}
			return esView;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsView(id) {
		try {
			let esView;
			if(sql) {
				esView = await models.esViews.findOne({ where: { id: Number(id) } });
				if (esView) {
					esView = await models.esViews.destroy({where: { id: Number(id) }});
				}
			} else {
				esView = await models.esViews.findByIdAndRemove(id);
			}
			return esView;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsViewService;
