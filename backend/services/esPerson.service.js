const models = require('../models');
const sql = process.env.SQL;
class EsPersonService {
	static async getAllEsPeople() {
		try {
			if(sql) {
				return await models.esPeople.findAll();
			} else {
				return await models.esPeople.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsPerson(newEsPerson) {
		try {
			let esPerson;
			if(sql) {
				esPerson = await models.esPeople.create(newEsPerson);
			} else {
				esPerson = new models.esPeople(newEsPerson);
				await esPerson.save();
			}
			return esPerson;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsPerson(id, updateEsPerson) {
		try {
			let esPerson;
			if(sql) {
				esPerson = await models.esPeople.findOne({where: { id: Number(id) }});
				if (esPerson) {
					esPerson = await models.esPeople.update(updateEsPerson, { where: { id: Number(id) } });
				}
			} else {
				esPerson = await models.esPeople.findByIdAndUpdate(id, {$set: updateEsPerson}, {new: true});
			}
			return esPerson;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsPerson(id) {
		try {
			let esPerson;
			if(sql) {
					esPerson = await models.esPeople.findOne({where: { id: Number(id) }});
			} else {
					esPerson = await models.esPeople.findById(id);
			}
			return esPerson;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsPerson(id) {
		try {
			let esPerson;
			if(sql) {
				esPerson = await models.esPeople.findOne({ where: { id: Number(id) } });
				if (esPerson) {
					esPerson = await models.esPeople.destroy({where: { id: Number(id) }});
				}
			} else {
				esPerson = await models.esPeople.findByIdAndRemove(id);
			}
			return esPerson;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsPersonService;
