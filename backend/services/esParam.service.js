const models = require('../models');
const sql = process.env.SQL;
class EsParamService {
	static async getAllEsParams() {
		try {
			if(sql) {
				return await models.esParams.findAll();
			} else {
				return await models.esParams.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsParam(newEsParam) {
		try {
			let esParam;
			if(sql) {
				esParam = await models.esParams.create(newEsParam);
			} else {
				esParam = new models.esParams(newEsParam);
				await esParam.save();
			}
			return esParam;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsParam(id, updateEsParam) {
		try {
			let esParam;
			if(sql) {
				esParam = await models.esParams.findOne({where: { id: Number(id) }});
				if (esParam) {
					esParam = await models.esParams.update(updateEsParam, { where: { id: Number(id) } });
				}
			} else {
				esParam = await models.esParams.findByIdAndUpdate(id, {$set: updateEsParam}, {new: true});
			}
			return esParam;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsParam(id) {
		try {
			let esParam;
			if(sql) {
					esParam = await models.esParams.findOne({where: { id: Number(id) }});
			} else {
					esParam = await models.esParams.findById(id);
			}
			return esParam;
		} catch (error) {
			throw error;
		}
	}

	static async getEsParamsByDictionaryIds(ids) {
		try {
			let esParam;
			if(sql) {
					esParam = await models.esParams.findAll({where: { par_dictionry_id: ids }});
			} else {
					esParam = await models.esParams.find({ par_dictionry_id: ids});
			}
			return esParam;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsParam(id) {
		try {
			let esParam;
			if(sql) {
				esParam = await models.esParams.findOne({ where: { id: Number(id) } });
				if (esParam) {
					esParam = await models.esParams.destroy({where: { id: Number(id) }});
				}
			} else {
				esParam = await models.esParams.findByIdAndRemove(id);
			}
			return esParam;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsParamService;
