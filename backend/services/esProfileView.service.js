const models = require('../models');
const sql = process.env.SQL;
class EsProfileViewService {
	static async getAllEsProfileViews() {
		try {
			if(sql) {
				return await models.esProfileViews.findAll();
			} else {
				return await models.esProfileViews.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsProfileView(newEsProfileView) {
		try {
			let esProfileView;
			if(sql) {
				esProfileView = await models.esProfileViews.create(newEsProfileView);
			} else {
				esProfileView = new models.esProfileViews(newEsProfileView);
				await esProfileView.save();
			}
			return esProfileView;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsProfileView(id, updateEsProfileView) {
		try {
			let esProfileView;
			if(sql) {
				esProfileView = await models.esProfileViews.findOne({where: { id: Number(id) }});
				if (esProfileView) {
					esProfileView = await models.esProfileViews.update(updateEsProfileView, { where: { id: Number(id) } });
				}
			} else {
				esProfileView = await models.esProfileViews.findByIdAndUpdate(id, {$set: updateEsProfileView}, {new: true});
			}
			return esProfileView;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsProfileView(id) {
		try {
			let esProfileView;
			if(sql) {
					esProfileView = await models.esProfileViews.findOne({where: { id: Number(id) }});
			} else {
					esProfileView = await models.esProfileViews.findById(id);
			}
			return esProfileView;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsProfileView(id) {
		try {
			let esProfileView;
			if(sql) {
				esProfileView = await models.esProfileViews.findOne({ where: { id: Number(id) } });
				if (esProfileView) {
					esProfileView = await models.esProfileViews.destroy({where: { id: Number(id) }});
				}
			} else {
				esProfileView = await models.esProfileViews.findByIdAndRemove(id);
			}
			return esProfileView;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsProfileViewService;
