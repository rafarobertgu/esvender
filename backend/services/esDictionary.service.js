const models = require('../models');
const sql = process.env.SQL;
class EsDictionaryService {
	static async getAllEsDictionaries() {
		try {
			if(sql) {
				return await models.esDictionaries.findAll();
			} else {
				return await models.esDictionaries.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsDictionary(newEsDictionary) {
		try {
			let esDictionary;
			if(sql) {
				esDictionary = await models.esDictionaries.create(newEsDictionary);
			} else {
				esDictionary = new models.esDictionaries(newEsDictionary);
				await esDictionary.save();
			}
			return esDictionary;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsDictionary(id, updateEsDictionary) {
		try {
			let esDictionary;
			if(sql) {
				esDictionary = await models.esDictionaries.findOne({where: { id: Number(id) }});
				if (esDictionary) {
					esDictionary = await models.esDictionaries.update(updateEsDictionary, { where: { id: Number(id) } });
				}
			} else {
				esDictionary = await models.esDictionaries.findByIdAndUpdate(id, {$set: updateEsDictionary}, {new: true});
			}
			return esDictionary;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsDictionary(id) {
		try {
			let esDictionary;
			if(sql) {
					esDictionary = await models.esDictionaries.findOne({where: { id: Number(id) }});
			} else {
					esDictionary = await models.esDictionaries.findById(id);
			}
			return esDictionary;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsDictionary(id) {
		try {
			let esDictionary;
			if(sql) {
				esDictionary = await models.esDictionaries.findOne({ where: { id: Number(id) } });
				if (esDictionary) {
					esDictionary = await models.esDictionaries.destroy({where: { id: Number(id) }});
				}
			} else {
				esDictionary = await models.esDictionaries.findByIdAndRemove(id);
			}
			return esDictionary;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsDictionaryService;
