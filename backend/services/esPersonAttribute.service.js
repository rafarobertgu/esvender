const models = require('../models');
const sql = process.env.SQL;
class EsPersonAttributeService {
	static async getAllEsPersonAttributes() {
		try {
			if(sql) {
				return await models.esPersonAttributes.findAll();
			} else {
				return await models.esPersonAttributes.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsPersonAttribute(newEsPersonAttribute) {
		try {
			let esPersonAttribute;
			if(sql) {
				esPersonAttribute = await models.esPersonAttributes.create(newEsPersonAttribute);
			} else {
				esPersonAttribute = new models.esPersonAttributes(newEsPersonAttribute);
				await esPersonAttribute.save();
			}
			return esPersonAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsPersonAttribute(id, updateEsPersonAttribute) {
		try {
			let esPersonAttribute;
			if(sql) {
				esPersonAttribute = await models.esPersonAttributes.findOne({where: { id: Number(id) }});
				if (esPersonAttribute) {
					esPersonAttribute = await models.esPersonAttributes.update(updateEsPersonAttribute, { where: { id: Number(id) } });
				}
			} else {
				esPersonAttribute = await models.esPersonAttributes.findByIdAndUpdate(id, {$set: updateEsPersonAttribute}, {new: true});
			}
			return esPersonAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsPersonAttribute(id) {
		try {
			let esPersonAttribute;
			if(sql) {
					esPersonAttribute = await models.esPersonAttributes.findOne({where: { id: Number(id) }});
			} else {
					esPersonAttribute = await models.esPersonAttributes.findById(id);
			}
			return esPersonAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsPersonAttribute(id) {
		try {
			let esPersonAttribute;
			if(sql) {
				esPersonAttribute = await models.esPersonAttributes.findOne({ where: { id: Number(id) } });
				if (esPersonAttribute) {
					esPersonAttribute = await models.esPersonAttributes.destroy({where: { id: Number(id) }});
				}
			} else {
				esPersonAttribute = await models.esPersonAttributes.findByIdAndRemove(id);
			}
			return esPersonAttribute;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsPersonAttributeService;
