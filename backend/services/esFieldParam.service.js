const models = require('../models');
const sql = process.env.SQL;
class EsFieldParamService {
	static async getAllEsFieldParams() {
		try {
			if(sql) {
				return await models.esFieldParams.findAll();
			} else {
				return await models.esFieldParams.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsFieldParam(newEsFieldParam) {
		try {
			let esFieldParam;
			if(sql) {
				esFieldParam = await models.esFieldParams.create(newEsFieldParam);
			} else {
				esFieldParam = new models.esFieldParams(newEsFieldParam);
				await esFieldParam.save();
			}
			return esFieldParam;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsFieldParam(id, updateEsFieldParam) {
		try {
			let esFieldParam;
			if(sql) {
				esFieldParam = await models.esFieldParams.findOne({where: { id: Number(id) }});
				if (esFieldParam) {
					esFieldParam = await models.esFieldParams.update(updateEsFieldParam, { where: { id: Number(id) } });
				}
			} else {
				esFieldParam = await models.esFieldParams.findByIdAndUpdate(id, {$set: updateEsFieldParam}, {new: true});
			}
			return esFieldParam;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsFieldParam(id) {
		try {
			let esFieldParam;
			if(sql) {
					esFieldParam = await models.esFieldParams.findOne({where: { id: Number(id) }});
			} else {
					esFieldParam = await models.esFieldParams.findById(id);
			}
			return esFieldParam;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsFieldParam(id) {
		try {
			let esFieldParam;
			if(sql) {
				esFieldParam = await models.esFieldParams.findOne({ where: { id: Number(id) } });
				if (esFieldParam) {
					esFieldParam = await models.esFieldParams.destroy({where: { id: Number(id) }});
				}
			} else {
				esFieldParam = await models.esFieldParams.findByIdAndRemove(id);
			}
			return esFieldParam;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsFieldParamService;
