const models = require('../models');
const sql = process.env.SQL;
class EsComponentFieldService {
	static async getAllEsComponentFields() {
		try {
			if(sql) {
				return await models.esComponentFields.findAll();
			} else {
				return await models.esComponentFields.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsComponentField(newEsComponentField) {
		try {
			let esComponentField;
			if(sql) {
				esComponentField = await models.esComponentFields.create(newEsComponentField);
			} else {
				esComponentField = new models.esComponentFields(newEsComponentField);
				await esComponentField.save();
			}
			return esComponentField;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsComponentField(id, updateEsComponentField) {
		try {
			let esComponentField;
			if(sql) {
				esComponentField = await models.esComponentFields.findOne({where: { id: Number(id) }});
				if (esComponentField) {
					esComponentField = await models.esComponentFields.update(updateEsComponentField, { where: { id: Number(id) } });
				}
			} else {
				esComponentField = await models.esComponentFields.findByIdAndUpdate(id, {$set: updateEsComponentField}, {new: true});
			}
			return esComponentField;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsComponentField(id) {
		try {
			let esComponentField;
			if(sql) {
					esComponentField = await models.esComponentFields.findOne({where: { id: Number(id) }});
			} else {
					esComponentField = await models.esComponentFields.findById(id);
			}
			return esComponentField;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsComponentField(id) {
		try {
			let esComponentField;
			if(sql) {
				esComponentField = await models.esComponentFields.findOne({ where: { id: Number(id) } });
				if (esComponentField) {
					esComponentField = await models.esComponentFields.destroy({where: { id: Number(id) }});
				}
			} else {
				esComponentField = await models.esComponentFields.findByIdAndRemove(id);
			}
			return esComponentField;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsComponentFieldService;
