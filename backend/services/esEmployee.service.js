const models = require('../models');
const sql = process.env.SQL;
class EsEmployeeService {
	static async getAllEsEmployees() {
		try {
			if(sql) {
				return await models.esEmployees.findAll();
			} else {
				return await models.esEmployees.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsEmployee(newEsEmployee) {
		try {
			let esEmployee;
			if(sql) {
				esEmployee = await models.esEmployees.create(newEsEmployee);
			} else {
				esEmployee = new models.esEmployees(newEsEmployee);
				await esEmployee.save();
			}
			return esEmployee;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsEmployee(id, updateEsEmployee) {
		try {
			let esEmployee;
			if(sql) {
				esEmployee = await models.esEmployees.findOne({where: { id: Number(id) }});
				if (esEmployee) {
					esEmployee = await models.esEmployees.update(updateEsEmployee, { where: { id: Number(id) } });
				}
			} else {
				esEmployee = await models.esEmployees.findByIdAndUpdate(id, {$set: updateEsEmployee}, {new: true});
			}
			return esEmployee;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsEmployee(id) {
		try {
			let esEmployee;
			if(sql) {
					esEmployee = await models.esEmployees.findOne({where: { id: Number(id) }});
			} else {
					esEmployee = await models.esEmployees.findById(id);
			}
			return esEmployee;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsEmployee(id) {
		try {
			let esEmployee;
			if(sql) {
				esEmployee = await models.esEmployees.findOne({ where: { id: Number(id) } });
				if (esEmployee) {
					esEmployee = await models.esEmployees.destroy({where: { id: Number(id) }});
				}
			} else {
				esEmployee = await models.esEmployees.findByIdAndRemove(id);
			}
			return esEmployee;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsEmployeeService;
