const models = require('../models');
const sql = process.env.SQL;
class EsModuleService {
	static async getAllEsModules() {
		try {
			if(sql) {
				return await models.esModules.findAll();
			} else {
				return await models.esModules.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsModule(newEsModule) {
		try {
			let esModule;
			if(sql) {
				esModule = await models.esModules.create(newEsModule);
			} else {
				esModule = new models.esModules(newEsModule);
				await esModule.save();
			}
			return esModule;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsModule(id, updateEsModule) {
		try {
			let esModule;
			if(sql) {
				esModule = await models.esModules.findOne({where: { id: Number(id) }});
				if (esModule) {
					esModule = await models.esModules.update(updateEsModule, { where: { id: Number(id) } });
				}
			} else {
				esModule = await models.esModules.findByIdAndUpdate(id, {$set: updateEsModule}, {new: true});
			}
			return esModule;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsModule(id) {
		try {
			let esModule;
			if(sql) {
					esModule = await models.esModules.findOne({where: { id: Number(id) }});
			} else {
					esModule = await models.esModules.findById(id);
			}
			return esModule;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsModule(id) {
		try {
			let esModule;
			if(sql) {
				esModule = await models.esModules.findOne({ where: { id: Number(id) } });
				if (esModule) {
					esModule = await models.esModules.destroy({where: { id: Number(id) }});
				}
			} else {
				esModule = await models.esModules.findByIdAndRemove(id);
			}
			return esModule;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsModuleService;
