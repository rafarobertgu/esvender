const models = require('../models');
const sql = process.env.SQL;
class Es_profile_moduleService {
	static async getAllEsProfileModules() {
		try {
			if(sql) {
				return await models.esProfileModules.findAll();
			} else {
				return await models.esProfileModules.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsProfileModule(newEsProfileModule) {
		try {
			let esProfileModule;
			if(sql) {
				esProfileModule = await models.esProfileModules.create(newEsProfileModule);
			} else {
				esProfileModule = new models.esProfileModules(newEsProfileModule);
				await esProfileModule.save();
			}
			return esProfileModule;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsProfileModule(id, updateEsProfileModule) {
		try {
			let esProfileModule;
			if(sql) {
				esProfileModule = await models.esProfileModules.findOne({where: { id: Number(id) }});
				if (esProfileModule) {
					esProfileModule = await models.esProfileModules.update(updateEsProfileModule, { where: { id: Number(id) } });
				}
			} else {
				esProfileModule = await models.esProfileModules.findByIdAndUpdate(id, {$set: updateEsProfileModule}, {new: true});
			}
			return esProfileModule;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsProfileModule(id) {
		try {
			let esProfileModule;
			if(sql) {
					esProfileModule = await models.esProfileModules.findOne({where: { id: Number(id) }});
			} else {
					esProfileModule = await models.esProfileModules.findById(id);
			}
			return esProfileModule;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsProfileModule(id) {
		try {
			let esProfileModule;
			if(sql) {
				esProfileModule = await models.esProfileModules.findOne({ where: { id: Number(id) } });
				if (esProfileModule) {
					esProfileModule = await models.esProfileModules.destroy({where: { id: Number(id) }});
				}
			} else {
				esProfileModule = await models.esProfileModules.findByIdAndRemove(id);
			}
			return esProfileModule;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = Es_profile_moduleService;
