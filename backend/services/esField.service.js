const models = require('../models');
const sql = process.env.SQL;
class EsFieldService {
	static async getAllEsFields() {
		try {
			if(sql) {
				return await models.esFields.findAll();
			} else {
				return await models.esFields.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsField(newEsField) {
		try {
			let esField;
			if(sql) {
				esField = await models.esFields.create(newEsField);
			} else {
				esField = new models.esFields(newEsField);
				await esField.save();
			}
			return esField;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsField(id, updateEsField) {
		try {
			let esField;
			if(sql) {
				esField = await models.esFields.findOne({where: { id: Number(id) }});
				if (esField) {
					esField = await models.esFields.update(updateEsField, { where: { id: Number(id) } });
				}
			} else {
				esField = await models.esFields.findByIdAndUpdate(id, {$set: updateEsField}, {new: true});
			}
			return esField;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsField(id) {
		try {
			let esField;
			if(sql) {
					esField = await models.esFields.findOne({where: { id: Number(id) }});
			} else {
					esField = await models.esFields.findById(id);
			}
			return esField;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsField(id) {
		try {
			let esField;
			if(sql) {
				esField = await models.esFields.findOne({ where: { id: Number(id) } });
				if (esField) {
					esField = await models.esFields.destroy({where: { id: Number(id) }});
				}
			} else {
				esField = await models.esFields.findByIdAndRemove(id);
			}
			return esField;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsFieldService;
