const models = require('../models');
const sql = process.env.SQL;
const bcrypt = require("bcrypt");

class EsUserService {
	static async getAllEsUsers() {
		try {
			if(sql) {
				return await models.esUsers.findAll();
			} else {
				return await models.esUsers.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsUser(newEsUser) {
		try {
			let esUser;
			newEsUser.usr_password = await bcrypt.hash(newEsUser.usr_password, bcrypt.genSaltSync(8)).then(encripted => {return encripted});
			newEsUser.usr_id = await Math.floor(Math.random() * 10000000000);
			if(sql) {
				esUser = await models.esUsers.create(newEsUser);
			} else {
				esUser = new models.esUsers(newEsUser);
				await esUser.save();
			}
			return esUser;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsUser(id, updateEsUser) {
		try {
			let esUser;
			if(sql) {
				esUser = await models.esUsers.findOne({where: { id: Number(id) }});
				if (esUser) {
					esUser = await models.esUsers.update(updateEsUser, { where: { id: Number(id) } });
				}
			} else {
				esUser = await models.esUsers.findByIdAndUpdate(id, {$set: updateEsUser}, {new: true});
			}
			return esUser;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsUserByUsrId(usrId, updateEsUser) {
		try {
			let esUser;
			if(sql) {
				esUser = await models.esUsers.findOne({where: { usr_id: usrId }});
				if (esUser) {
					esUser = await models.esUsers.update(updateEsUser, { where: { usr_id: usrId } });
				}
			} else {
				esUser = await models.esUsers.findOneAndUpdate({usr_id: usrId}, {$set: updateEsUser}, {new: true});
			}
			return esUser;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsUser(id) {
		try {
			let esUser;
			if(sql) {
					esUser = await models.esUsers.findOne({where: { id: Number(id) }});
			} else {
					esUser = await models.esUsers.findById(id);
			}
			return esUser;
		} catch (error) {
			throw error;
		}
	}

	static async findOneByUsrUserName(usrUsername) {
		try {
			let esUser;
			if(sql) {
					esUser = await models.esUsers.findOne({where: { usr_username: usrUsername }});
			} else {
					esUser = await models.esUsers.findOne({usr_username: usrUsername});
			}
			return esUser;
		} catch (error) {
			throw error;
		}
	}
	static async findOneByUsrUserNameAndPassword(usrUsername, password) {
		try {
			let esUser;
			if(sql) {
					esUser = await models.esUsers.findOne({where: { usr_username: usrUsername }});
			} else {
					esUser = await models.esUsers.findOne({usr_username: usrUsername});
			}
			let compare = await bcrypt.compare(password, esUser.usr_password).then((resp)=>{return resp});
			if(compare) {
				return esUser;
			} else {
				return null;
			}
		} catch (error) {
			throw error;
		}
	}

	static async findOneByUsrId(usrId) {
		try {
			let esUser;
			if(sql) {
					esUser = await models.esUsers.findOne({where: { usr_id: usrId }});
			} else {
					esUser = await models.esUsers.findOne({usr_id: usrId});
			}
			return esUser;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsUser(id) {
		try {
			let esUser;
			if(sql) {
				esUser = await models.esUsers.findOne({ where: { id: Number(id) } });
				if (esUser) {
					esUser = await models.esUsers.destroy({where: { id: Number(id) }});
				}
			} else {
				esUser = await models.esUsers.findByIdAndRemove(id);
			}
			return esUser;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsUserService;
