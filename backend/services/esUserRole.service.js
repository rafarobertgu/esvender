const models = require('../models');
const sql = process.env.SQL;
class EsUserRoleService {
	static async getAllEsUserRoles() {
		try {
			if(sql) {
				return await models.esUserRoles.findAll();
			} else {
				return await models.esUserRoles.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsUserRole(newEsUserRole) {
		try {
			let esUserRole;
			if(sql) {
				esUserRole = await models.esUserRoles.create(newEsUserRole);
			} else {
				esUserRole = new models.esUserRoles(newEsUserRole);
				await esUserRole.save();
			}
			return esUserRole;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsUserRole(id, updateEsUserRole) {
		try {
			let esUserRole;
			if(sql) {
				esUserRole = await models.esUserRoles.findOne({where: { id: Number(id) }});
				if (esUserRole) {
					esUserRole = await models.esUserRoles.update(updateEsUserRole, { where: { id: Number(id) } });
				}
			} else {
				esUserRole = await models.esUserRoles.findByIdAndUpdate(id, {$set: updateEsUserRole}, {new: true});
			}
			return esUserRole;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsUserRole(id) {
		try {
			let esUserRole;
			if(sql) {
					esUserRole = await models.esUserRoles.findOne({where: { id: Number(id) }});
			} else {
					esUserRole = await models.esUserRoles.findById(id);
			}
			return esUserRole;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsUserRole(id) {
		try {
			let esUserRole;
			if(sql) {
				esUserRole = await models.esUserRoles.findOne({ where: { id: Number(id) } });
				if (esUserRole) {
					esUserRole = await models.esUserRoles.destroy({where: { id: Number(id) }});
				}
			} else {
				esUserRole = await models.esUserRoles.findByIdAndRemove(id);
			}
			return esUserRole;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsUserRoleService;
