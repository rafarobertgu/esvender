const models = require('../models');
const sql = process.env.SQL;
class EsObjectAttributeService {
	static async getAllEsObjectAttributes() {
		try {
			if(sql) {
				return await models.esObjectAttributes.findAll();
			} else {
				return await models.esObjectAttributes.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsObjectAttribute(newEsObjectAttribute) {
		try {
			let esObjectAttribute;
			if(sql) {
				esObjectAttribute = await models.esObjectAttributes.create(newEsObjectAttribute);
			} else {
				esObjectAttribute = new models.esObjectAttributes(newEsObjectAttribute);
				await esObjectAttribute.save();
			}
			return esObjectAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsObjectAttribute(id, updateEsObjectAttribute) {
		try {
			let esObjectAttribute;
			if(sql) {
				esObjectAttribute = await models.esObjectAttributes.findOne({where: { id: Number(id) }});
				if (esObjectAttribute) {
					esObjectAttribute = await models.esObjectAttributes.update(updateEsObjectAttribute, { where: { id: Number(id) } });
				}
			} else {
				esObjectAttribute = await models.esObjectAttributes.findByIdAndUpdate(id, {$set: updateEsObjectAttribute}, {new: true});
			}
			return esObjectAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsObjectAttribute(id) {
		try {
			let esObjectAttribute;
			if(sql) {
					esObjectAttribute = await models.esObjectAttributes.findOne({where: { id: Number(id) }});
			} else {
					esObjectAttribute = await models.esObjectAttributes.findById(id);
			}
			return esObjectAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsObjectAttribute(id) {
		try {
			let esObjectAttribute;
			if(sql) {
				esObjectAttribute = await models.esObjectAttributes.findOne({ where: { id: Number(id) } });
				if (esObjectAttribute) {
					esObjectAttribute = await models.esObjectAttributes.destroy({where: { id: Number(id) }});
				}
			} else {
				esObjectAttribute = await models.esObjectAttributes.findByIdAndRemove(id);
			}
			return esObjectAttribute;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsObjectAttributeService;
