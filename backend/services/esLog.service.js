const models = require('../models');
const sql = process.env.SQL;
class EsLogService {
	static async getAllEsLogs() {
		try {
			if(sql) {
				return await models.esLogs.findAll();
			} else {
				return await models.esLogs.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsLog(newEsLog) {
		try {
			let esLog;
			if(sql) {
				esLog = await models.esLogs.create(newEsLog);
			} else {
				esLog = new models.esLogs(newEsLog);
				await esLog.save();
			}
			return esLog;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsLog(id, updateEsLog) {
		try {
			let esLog;
			if(sql) {
				esLog = await models.esLogs.findOne({where: { id: Number(id) }});
				if (esLog) {
					esLog = await models.esLogs.update(updateEsLog, { where: { id: Number(id) } });
				}
			} else {
				esLog = await models.esLogs.findByIdAndUpdate(id, {$set: updateEsLog}, {new: true});
			}
			return esLog;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsLog(id) {
		try {
			let esLog;
			if(sql) {
					esLog = await models.esLogs.findOne({where: { id: Number(id) }});
			} else {
					esLog = await models.esLogs.findById(id);
			}
			return esLog;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsLog(id) {
		try {
			let esLog;
			if(sql) {
				esLog = await models.esLogs.findOne({ where: { id: Number(id) } });
				if (esLog) {
					esLog = await models.esLogs.destroy({where: { id: Number(id) }});
				}
			} else {
				esLog = await models.esLogs.findByIdAndRemove(id);
			}
			return esLog;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsLogService;
