const models = require('../models');
const sql = process.env.SQL;

class EsAttributeService {
	static async getAllEsAttributes() {
		try {
			if(sql) {
				return await models.esAttributes.findAll();
			} else {
				return await models.esAttributes.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsAttribute(newEsAttribute) {
		try {
			let esAttribute;
			if(sql) {
				esAttribute = await models.esAttributes.create(newEsAttribute);
			} else {
				esAttribute = new models.esAttributes(newEsAttribute);
				await esAttribute.save();
			}
			return esAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsAttribute(id, updateEsAttribute) {
		try {
			let esAttribute;
			if(sql) {
				esAttribute = await models.esAttributes.findOne({where: { id: Number(id) }});
				if (esAttribute) {
					esAttribute = await models.esAttributes.update(updateEsAttribute, { where: { id: Number(id) } });
				}
			} else {
				esAttribute = await models.esAttributes.findByIdAndUpdate(id, {$set: updateEsAttribute}, {new: true});
			}
			return esAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsAttribute(id) {
		try {
			let esAttribute;
			if(sql) {
					esAttribute = await models.esAttributes.findOne({where: { id: Number(id) }});
			} else {
					esAttribute = await models.esAttributes.findById(id);
			}
			return esAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsAttribute(id) {
		try {
			let esAttribute;
			if(sql) {
				esAttribute = await models.esAttributes.findOne({ where: { id: Number(id) } });
				if (esAttribute) {
					esAttribute = await models.esAttributes.destroy({where: { id: Number(id) }});
				}
			} else {
				esAttribute = await models.esAttributes.findByIdAndRemove(id);
			}
			return esAttribute;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsAttributeService;
