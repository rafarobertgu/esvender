const models = require('../models');
const sql = process.env.SQL;
class EsProfileService {
	static async getAllEsProfiles() {
		try {
			if(sql) {
				return await models.esProfiles.findAll();
			} else {
				return await models.esProfiles.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsProfile(newEsProfile) {
		try {
			let esProfile;
			if(sql) {
				esProfile = await models.esProfiles.create(newEsProfile);
			} else {
				esProfile = new models.esProfiles(newEsProfile);
				await esProfile.save();
			}
			return esProfile;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsProfile(id, updateEsProfile) {
		try {
			let esProfile;
			if(sql) {
				esProfile = await models.esProfiles.findOne({where: { id: Number(id) }});
				if (esProfile) {
					esProfile = await models.esProfiles.update(updateEsProfile, { where: { id: Number(id) } });
				}
			} else {
				esProfile = await models.esProfiles.findByIdAndUpdate(id, {$set: updateEsProfile}, {new: true});
			}
			return esProfile;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsProfile(id) {
		try {
			let esProfile;
			if(sql) {
					esProfile = await models.esProfiles.findOne({where: { id: Number(id) }});
			} else {
					esProfile = await models.esProfiles.findById(id);
			}
			return esProfile;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsProfile(id) {
		try {
			let esProfile;
			if(sql) {
				esProfile = await models.esProfiles.findOne({ where: { id: Number(id) } });
				if (esProfile) {
					esProfile = await models.esProfiles.destroy({where: { id: Number(id) }});
				}
			} else {
				esProfile = await models.esProfiles.findByIdAndRemove(id);
			}
			return esProfile;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsProfileService;
