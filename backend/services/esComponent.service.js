const models = require('../models');
const sql = process.env.SQL;
class EsComponentService {
	static async getAllEsComponents() {
		try {
			if(sql) {
				return await models.esComponents.findAll();
			} else {
				return await models.esComponents.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsComponent(newEsComponent) {
		try {
			let esComponent;
			if(sql) {
				esComponent = await models.esComponents.create(newEsComponent);
			} else {
				esComponent = new models.esComponents(newEsComponent);
				await esComponent.save();
			}
			return esComponent;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsComponent(id, updateEsComponent) {
		try {
			let esComponent;
			if(sql) {
				esComponent = await models.esComponents.findOne({where: { id: Number(id) }});
				if (esComponent) {
					esComponent = await models.esComponents.update(updateEsComponent, { where: { id: Number(id) } });
				}
			} else {
				esComponent = await models.esComponents.findByIdAndUpdate(id, {$set: updateEsComponent}, {new: true});
			}
			return esComponent;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsComponent(id) {
		try {
			let esComponent;
			if(sql) {
					esComponent = await models.esComponents.findOne({where: { id: Number(id) }});
			} else {
					esComponent = await models.esComponents.findById(id);
			}
			return esComponent;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsComponent(id) {
		try {
			let esComponent;
			if(sql) {
				esComponent = await models.esComponents.findOne({ where: { id: Number(id) } });
				if (esComponent) {
					esComponent = await models.esComponents.destroy({where: { id: Number(id) }});
				}
			} else {
				esComponent = await models.esComponents.findByIdAndRemove(id);
			}
			return esComponent;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsComponentService;
