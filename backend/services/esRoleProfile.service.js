const models = require('../models');
const sql = process.env.SQL;
class EsRoleProfileService {
	static async getAllEsRoleProfiles() {
		try {
			if(sql) {
				return await models.esRoleProfiles.findAll();
			} else {
				return await models.esRoleProfiles.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsRoleProfile(newEsRoleProfile) {
		try {
			let esRoleProfile;
			if(sql) {
				esRoleProfile = await models.esRoleProfiles.create(newEsRoleProfile);
			} else {
				esRoleProfile = new models.esRoleProfiles(newEsRoleProfile);
				await esRoleProfile.save();
			}
			return esRoleProfile;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsRoleProfile(id, updateEsRoleProfile) {
		try {
			let esRoleProfile;
			if(sql) {
				esRoleProfile = await models.esRoleProfiles.findOne({where: { id: Number(id) }});
				if (esRoleProfile) {
					esRoleProfile = await models.esRoleProfiles.update(updateEsRoleProfile, { where: { id: Number(id) } });
				}
			} else {
				esRoleProfile = await models.esRoleProfiles.findByIdAndUpdate(id, {$set: updateEsRoleProfile}, {new: true});
			}
			return esRoleProfile;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsRoleProfile(id) {
		try {
			let esRoleProfile;
			if(sql) {
					esRoleProfile = await models.esRoleProfiles.findOne({where: { id: Number(id) }});
			} else {
					esRoleProfile = await models.esRoleProfiles.findById(id);
			}
			return esRoleProfile;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsRoleProfile(id) {
		try {
			let esRoleProfile;
			if(sql) {
				esRoleProfile = await models.esRoleProfiles.findOne({ where: { id: Number(id) } });
				if (esRoleProfile) {
					esRoleProfile = await models.esRoleProfiles.destroy({where: { id: Number(id) }});
				}
			} else {
				esRoleProfile = await models.esRoleProfiles.findByIdAndRemove(id);
			}
			return esRoleProfile;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsRoleProfileService;
