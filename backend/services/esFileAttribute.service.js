const models = require('../models');
const sql = process.env.SQL;
class EsFileAttributeService {
	static async getAllEsFileAttributes() {
		try {
			if(sql) {
				return await models.esFileAttributes.findAll();
			} else {
				return await models.esFileAttributes.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsFileAttribute(newEsFileAttribute) {
		try {
			let esFileAttribute;
			if(sql) {
				esFileAttribute = await models.esFileAttributes.create(newEsFileAttribute);
			} else {
				esFileAttribute = new models.esFileAttributes(newEsFileAttribute);
				await esFileAttribute.save();
			}
			return esFileAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsFileAttribute(id, updateEsFileAttribute) {
		try {
			let esFileAttribute;
			if(sql) {
				esFileAttribute = await models.esFileAttributes.findOne({where: { id: Number(id) }});
				if (esFileAttribute) {
					esFileAttribute = await models.esFileAttributes.update(updateEsFileAttribute, { where: { id: Number(id) } });
				}
			} else {
				esFileAttribute = await models.esFileAttributes.findByIdAndUpdate(id, {$set: updateEsFileAttribute}, {new: true});
			}
			return esFileAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsFileAttribute(id) {
		try {
			let esFileAttribute;
			if(sql) {
					esFileAttribute = await models.esFileAttributes.findOne({where: { id: Number(id) }});
			} else {
					esFileAttribute = await models.esFileAttributes.findById(id);
			}
			return esFileAttribute;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsFileAttribute(id) {
		try {
			let esFileAttribute;
			if(sql) {
				esFileAttribute = await models.esFileAttributes.findOne({ where: { id: Number(id) } });
				if (esFileAttribute) {
					esFileAttribute = await models.esFileAttributes.destroy({where: { id: Number(id) }});
				}
			} else {
				esFileAttribute = await models.esFileAttributes.findByIdAndRemove(id);
			}
			return esFileAttribute;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsFileAttributeService;
