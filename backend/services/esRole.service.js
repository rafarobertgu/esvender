const models = require('../models');
const sql = process.env.SQL;
class EsRoleService {
	static async getAllEsRoles() {
		try {
			if(sql) {
				return await models.esRoles.findAll();
			} else {
				return await models.esRoles.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsRole(newEsRole) {
		try {
			let esRole;
			if(sql) {
				esRole = await models.esRoles.create(newEsRole);
			} else {
				esRole = new models.esRoles(newEsRole);
				await esRole.save();
			}
			return esRole;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsRole(id, updateEsRole) {
		try {
			let esRole;
			if(sql) {
				esRole = await models.esRoles.findOne({where: { id: Number(id) }});
				if (esRole) {
					esRole = await models.esRoles.update(updateEsRole, { where: { id: Number(id) } });
				}
			} else {
				esRole = await models.esRoles.findByIdAndUpdate(id, {$set: updateEsRole}, {new: true});
			}
			return esRole;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsRole(id) {
		try {
			let esRole;
			if(sql) {
					esRole = await models.esRoles.findOne({where: { id: Number(id) }});
			} else {
					esRole = await models.esRoles.findById(id);
			}
			return esRole;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsRole(id) {
		try {
			let esRole;
			if(sql) {
				esRole = await models.esRoles.findOne({ where: { id: Number(id) } });
				if (esRole) {
					esRole = await models.esRoles.destroy({where: { id: Number(id) }});
				}
			} else {
				esRole = await models.esRoles.findByIdAndRemove(id);
			}
			return esRole;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsRoleService;
