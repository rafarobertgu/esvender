const models = require('../models');
const sql = process.env.SQL;
const LocalStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const configAuth = require('../config/auth');
const passport = require('passport');
const esUserService = require('./esUser.service');

class EsAuthService {

	static async getAllEsAuths() {
		try {
			if(sql) {
				return await models.esAuths.findAll();
			} else {
				return await models.esAuths.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsAuth(newEsAuth) {
		try {
			let esAuth;
			if(sql) {
				esAuth = await models.esAuths.create(newEsAuth);
			} else {
				esAuth = new models.esAuths(newEsAuth);
				await esAuth.save();
			}
			return esAuth;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsAuth(id, updateEsAuth) {
		try {
			let esAuth;
			if(sql) {
				esAuth = await models.esAuths.findOne({where: { id: Number(id) }});
				if (esAuth) {
					esAuth = await models.esAuths.update(updateEsAuth, { where: { id: Number(id) } });
				}
			} else {
				esAuth = await models.esAuths.findByIdAndUpdate(id, {$set: updateEsAuth}, {new: true});
			}
			return esAuth;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsAuth(id) {
		try {
			let esAuth;
			if(sql) {
				esAuth = await models.esAuths.findOne({where: { id: Number(id) }});
			} else {
				esAuth = await models.esAuths.findById(id);
			}
			return esAuth;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsAuth(id) {
		try {
			let esAuth;
			if(sql) {
				esAuth = await models.esAuths.findOne({ where: { id: Number(id) } });
				if (esAuth) {
					esAuth = await models.esAuths.destroy({where: { id: Number(id) }});
				}
			} else {
				esAuth = await models.esAuths.findByIdAndRemove(id);
			}
			return esAuth;
		} catch (error) {
			throw error;
		}
	}

}

module.exports = EsAuthService;
