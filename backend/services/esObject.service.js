const models = require('../models');
const sql = process.env.SQL;
class EsObjectService {
	static async getAllEsObjects() {
		try {
			if(sql) {
				return await models.esObjects.findAll();
			} else {
				return await models.esObjects.find();
			}
		} catch (error) {
			throw error;
		}
	}

	static async addEsObject(newEsObject) {
		try {
			let esObject;
			if(sql) {
				esObject = await models.esObjects.create(newEsObject);
			} else {
				esObject = new models.esObjects(newEsObject);
				await esObject.save();
			}
			return esObject;
		} catch (error) {
			throw error;
		}
	}

	static async updateEsObject(id, updateEsObject) {
		try {
			let esObject;
			if(sql) {
				esObject = await models.esObjects.findOne({where: { id: Number(id) }});
				if (esObject) {
					esObject = await models.esObjects.update(updateEsObject, { where: { id: Number(id) } });
				}
			} else {
				esObject = await models.esObjects.findByIdAndUpdate(id, {$set: updateEsObject}, {new: true});
			}
			return esObject;
		} catch (error) {
			throw error;
		}
	}

	static async getAEsObject(id) {
		try {
			let esObject;
			if(sql) {
					esObject = await models.esObjects.findOne({where: { id: Number(id) }});
			} else {
					esObject = await models.esObjects.findById(id);
			}
			return esObject;
		} catch (error) {
			throw error;
		}
	}

	static async deleteEsObject(id) {
		try {
			let esObject;
			if(sql) {
				esObject = await models.esObjects.findOne({ where: { id: Number(id) } });
				if (esObject) {
					esObject = await models.esObjects.destroy({where: { id: Number(id) }});
				}
			} else {
				esObject = await models.esObjects.findByIdAndRemove(id);
			}
			return esObject;
		} catch (error) {
			throw error;
		}
	}
}

module.exports = EsObjectService;
