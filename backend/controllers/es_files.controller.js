const esFileService = require('../services/esFile.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esFilesCtrl = {};

esFilesCtrl.service = esFileService;

esFilesCtrl.getFiles = async (req, res) => {
    try {
        const esFiles = await esFileService.getAllEsFiles();
        if (esFiles.length > 0) {
            util.setSuccess(200, 'Files retrieved', esFiles);
        } else {
            util.setSuccess(200, 'No esFile found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFilesCtrl.getFile = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esFile = await esFileService.getAEsFile(id);
        if (!esFile) {
            util.setError(404, `Cannot find esFile with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found File', esFile);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFilesCtrl.createFile = async (req, res) => {
    try {
        const esFile = await esFileService.addEsFile(req.body);
        util.setSuccess(201, 'File Added!', esFile);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFilesCtrl.updateFile = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esFile = await esFileService.updateEsFile(id, req.body);
        if (!esFile) {
            util.setError(404, `Cannot find es File with the id: ${id}`);
        } else {
            util.setSuccess(200, 'File updated', esFile);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esFilesCtrl.deleteFile = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esFile = await esFileService.deleteEsFile(id);
        if (esFile) {
            util.setSuccess(200, 'esFile deleted');
        } else {
            util.setError(404, `esFile with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esFilesCtrl;


