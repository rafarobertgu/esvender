const esProfileService = require('../services/esProfile.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esProfilesCtrl = {};

esProfilesCtrl.service = esProfileService;

esProfilesCtrl.getProfiles = async (req, res) => {
    try {
        const esProfiles = await esProfileService.getAllEsProfiles();
        if (esProfiles.length > 0) {
            util.setSuccess(200, 'Profiles retrieved', esProfiles);
        } else {
            util.setSuccess(200, 'No esProfile found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfilesCtrl.getProfile = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esProfile = await esProfileService.getAEsProfile(id);
        if (!esProfile) {
            util.setError(404, `Cannot find esProfile with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Profile', esProfile);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfilesCtrl.createProfile = async (req, res) => {
    try {
        const esProfile = await esProfileService.addEsProfile(req.body);
        util.setSuccess(201, 'Profile Added!', esProfile);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfilesCtrl.updateProfile = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esProfile = await esProfileService.updateEsProfile(id, req.body);
        if (!esProfile) {
            util.setError(404, `Cannot find es Profile with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Profile updated', esProfile);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esProfilesCtrl.deleteProfile = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esProfile = await esProfileService.deleteEsProfile(id);
        if (esProfile) {
            util.setSuccess(200, 'esProfile deleted');
        } else {
            util.setError(404, `esProfile with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esProfilesCtrl;


