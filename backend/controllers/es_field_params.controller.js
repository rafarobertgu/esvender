const esFieldParamService = require('../services/esFieldParam.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esFieldParamsCtrl = {};

esFieldParamsCtrl.service = esFieldParamService;

esFieldParamsCtrl.getFieldParams = async (req, res) => {
    try {
        const esFieldParams = await esFieldParamService.getAllEsFieldParams();
        if (esFieldParams.length > 0) {
            util.setSuccess(200, 'FieldParams retrieved', esFieldParams);
        } else {
            util.setSuccess(200, 'No esFieldParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFieldParamsCtrl.getFieldParam = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esFieldParam = await esFieldParamService.getAEsFieldParam(id);
        if (!esFieldParam) {
            util.setError(404, `Cannot find esFieldParam with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found FieldParam', esFieldParam);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFieldParamsCtrl.createFieldParam = async (req, res) => {
    try {
        const esFieldParam = await esFieldParamService.addEsFieldParam(req.body);
        util.setSuccess(201, 'FieldParam Added!', esFieldParam);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFieldParamsCtrl.updateFieldParam = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esFieldParam = await esFieldParamService.updateEsFieldParam(id, req.body);
        if (!esFieldParam) {
            util.setError(404, `Cannot find es FieldParam with the id: ${id}`);
        } else {
            util.setSuccess(200, 'FieldParam updated', esFieldParam);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esFieldParamsCtrl.deleteFieldParam = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esFieldParam = await esFieldParamService.deleteEsFieldParam(id);
        if (esFieldParam) {
            util.setSuccess(200, 'esFieldParam deleted');
        } else {
            util.setError(404, `esFieldParam with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esFieldParamsCtrl;


