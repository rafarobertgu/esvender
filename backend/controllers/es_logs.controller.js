const esLogService = require('../services/esLog.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esLogsCtrl = {};

esLogsCtrl.service = esLogService;

esLogsCtrl.getLogs = async (req, res) => {
    try {
        const esLogs = await esLogService.getAllEsLogs();
        if (esLogs.length > 0) {
            util.setSuccess(200, 'Logs retrieved', esLogs);
        } else {
            util.setSuccess(200, 'No esLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esLogsCtrl.getLog = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esLog = await esLogService.getAEsLog(id);
        if (!esLog) {
            util.setError(404, `Cannot find esLog with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Log', esLog);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esLogsCtrl.createLog = async (req, res) => {
    try {
        const esLog = await esLogService.addEsLog(req.body);
        util.setSuccess(201, 'Log Added!', esLog);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esLogsCtrl.updateLog = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esLog = await esLogService.updateEsLog(id, req.body);
        if (!esLog) {
            util.setError(404, `Cannot find es Log with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Log updated', esLog);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esLogsCtrl.deleteLog = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esLog = await esLogService.deleteEsLog(id);
        if (esLog) {
            util.setSuccess(200, 'esLog deleted');
        } else {
            util.setError(404, `esLog with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esLogsCtrl;


