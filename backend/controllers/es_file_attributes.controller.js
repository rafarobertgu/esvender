const esFileAttributeService = require('../services/esFileAttribute.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esFileAttributesCtrl = {};

esFileAttributesCtrl.service = esFileAttributeService;

esFileAttributesCtrl.getFileAttributes = async (req, res) => {
    try {
        const esFileAttributes = await esFileAttributeService.getAllEsFileAttributes();
        if (esFileAttributes.length > 0) {
            util.setSuccess(200, 'FileAttributes retrieved', esFileAttributes);
        } else {
            util.setSuccess(200, 'No esFileAttribute found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFileAttributesCtrl.getFileAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esFileAttribute = await esFileAttributeService.getAEsFileAttribute(id);
        if (!esFileAttribute) {
            util.setError(404, `Cannot find esFileAttribute with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Attribute', esFileAttribute);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFileAttributesCtrl.createFileAttribute = async (req, res) => {
    try {
        const esFileAttribute = await esFileAttributeService.addEsFileAttribute(req.body);
        util.setSuccess(201, 'Attribute Added!', esFileAttribute);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFileAttributesCtrl.updateFileAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esFileAttribute = await esFileAttributeService.updateEsFileAttribute(id, req.body);
        if (!esFileAttribute) {
            util.setError(404, `Cannot find es Attribute with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Attribute updated', esFileAttribute);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esFileAttributesCtrl.deleteFileAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esFileAttribute = await esFileAttributeService.deleteEsFileAttribute(id);
        if (esFileAttribute) {
            util.setSuccess(200, 'esFileAttribute deleted');
        } else {
            util.setError(404, `esFileAttribute with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esFileAttributesCtrl;


