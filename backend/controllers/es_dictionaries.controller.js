const esDictionaryService = require('../services/esDictionary.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esDictionariesCtrl = {};

esDictionariesCtrl.service = esDictionaryService;

esDictionariesCtrl.getDictionaries = async (req, res) => {
    try {
        const esDictionaries = await esDictionaryService.getAllEsDictionaries();
        if (esDictionaries.length > 0) {
            util.setSuccess(200, 'Dictionaries retrieved', esDictionaries);
        } else {
            util.setSuccess(200, 'No esDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esDictionariesCtrl.getDictionary = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esDictionary = await esDictionaryService.getAEsDictionary(id);
        if (!esDictionary) {
            util.setError(404, `Cannot find esDictionary with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Dictionary', esDictionary);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esDictionariesCtrl.createDictionary = async (req, res) => {
    try {
        const esDictionary = await esDictionaryService.addEsDictionary(req.body);
        util.setSuccess(201, 'Dictionary Added!', esDictionary);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esDictionariesCtrl.updateDictionary = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esDictionary = await esDictionaryService.updateEsDictionary(id, req.body);
        if (!esDictionary) {
            util.setError(404, `Cannot find es Dictionary with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Dictionary updated', esDictionary);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esDictionariesCtrl.deleteDictionary = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esDictionary = await esDictionaryService.deleteEsDictionary(id);
        if (esDictionary) {
            util.setSuccess(200, 'esDictionary deleted');
        } else {
            util.setError(404, `esDictionary with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esDictionariesCtrl;


