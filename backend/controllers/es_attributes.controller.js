const esAttributeService = require('../services/esAttribute.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esAttributesCtrl = {};

esAttributesCtrl.service = esAttributeService;

esAttributesCtrl.getAttributes = async (req, res) => {
    try {
        const esAttributes = await esAttributeService.getAllEsAttributes();
        if (esAttributes.length > 0) {
            util.setSuccess(200, 'Attributes retrieved', esAttributes);
        } else {
            util.setSuccess(200, 'No esAttribute found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esAttributesCtrl.getAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esAttribute = await esAttributeService.getAEsAttribute(id);
        if (!esAttribute) {
            util.setError(404, `Cannot find esAttribute with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Attribute', esAttribute);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esAttributesCtrl.createAttribute = async (req, res) => {
    try {
        const esAttribute = await esAttributeService.addEsAttribute(req.body);
        util.setSuccess(201, 'Attribute Added!', esAttribute);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esAttributesCtrl.updateAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esAttribute = await esAttributeService.updateEsAttribute(id, req.body);
        if (!esAttribute) {
            util.setError(404, `Cannot find es Attribute with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Attribute updated', esAttribute);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esAttributesCtrl.deleteAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esAttribute = await esAttributeService.deleteEsAttribute(id);
        if (esAttribute) {
            util.setSuccess(200, 'esAttribute deleted');
        } else {
            util.setError(404, `esAttribute with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esAttributesCtrl;


