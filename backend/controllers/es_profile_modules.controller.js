const esProfileModuleService = require('../services/esProfileModule.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esProfileModulesCtrl = {};

esProfileModulesCtrl.service = esProfileModuleService;

esProfileModulesCtrl.getProfileModules = async (req, res) => {
    try {
        const esProfileModules = await esProfileModuleService.getAllEsProfileModules();
        if (esProfileModules.length > 0) {
            util.setSuccess(200, 'ProfileModules retrieved', esProfileModules);
        } else {
            util.setSuccess(200, 'No esProfileModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfileModulesCtrl.getProfileModule = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esProfileModule = await esProfileModuleService.getAEsProfileModule(id);
        if (!esProfileModule) {
            util.setError(404, `Cannot find esProfileModule with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found ProfileModule', esProfileModule);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfileModulesCtrl.createProfileModule = async (req, res) => {
    try {
        const esProfileModule = await esProfileModuleService.addEsProfileModule(req.body);
        util.setSuccess(201, 'ProfileModule Added!', esProfileModule);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfileModulesCtrl.updateProfileModule = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esProfileModule = await esProfileModuleService.updateEsProfileModule(id, req.body);
        if (!esProfileModule) {
            util.setError(404, `Cannot find es ProfileModule with the id: ${id}`);
        } else {
            util.setSuccess(200, 'ProfileModule updated', esProfileModule);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esProfileModulesCtrl.deleteProfileModule = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esProfileModule = await esProfileModuleService.deleteEsProfileModule(id);
        if (esProfileModule) {
            util.setSuccess(200, 'esProfileModule deleted');
        } else {
            util.setError(404, `esProfileModule with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esProfileModulesCtrl;


