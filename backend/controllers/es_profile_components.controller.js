const esProfileComponentService = require('../services/esProfileComponent.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esProfileComponentsCtrl = {};

esProfileComponentsCtrl.service = esProfileComponentService;

esProfileComponentsCtrl.getProfileComponents = async (req, res) => {
    try {
        const esProfileComponents = await esProfileComponentService.getAllEsProfileComponents();
        if (esProfileComponents.length > 0) {
            util.setSuccess(200, 'ProfileComponents retrieved', esProfileComponents);
        } else {
            util.setSuccess(200, 'No esProfileComponent found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfileComponentsCtrl.getProfileComponent = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esProfileComponent = await esProfileComponentService.getAEsProfileComponent(id);
        if (!esProfileComponent) {
            util.setError(404, `Cannot find esProfileComponent with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found ProfileComponent', esProfileComponent);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfileComponentsCtrl.createProfileComponent = async (req, res) => {
    try {
        const esProfileComponent = await esProfileComponentService.addEsProfileComponent(req.body);
        util.setSuccess(201, 'ProfileComponent Added!', esProfileComponent);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfileComponentsCtrl.updateProfileComponent = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esProfileComponent = await esProfileComponentService.updateEsProfileComponent(id, req.body);
        if (!esProfileComponent) {
            util.setError(404, `Cannot find es ProfileComponent with the id: ${id}`);
        } else {
            util.setSuccess(200, 'ProfileComponent updated', esProfileComponent);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esProfileComponentsCtrl.deleteProfileComponent = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esProfileComponent = await esProfileComponentService.deleteEsProfileComponent(id);
        if (esProfileComponent) {
            util.setSuccess(200, 'esProfileComponent deleted');
        } else {
            util.setError(404, `esProfileComponent with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esProfileComponentsCtrl;


