const esProfileViewService = require('../services/esProfileView.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esProfileViewsCtrl = {};

esProfileViewsCtrl.service = esProfileViewService;

esProfileViewsCtrl.getProfileViews = async (req, res) => {
    try {
        const esProfileViews = await esProfileViewService.getAllEsProfileViews();
        if (esProfileViews.length > 0) {
            util.setSuccess(200, 'ProfileViews retrieved', esProfileViews);
        } else {
            util.setSuccess(200, 'No esProfileView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfileViewsCtrl.getProfileView = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esProfileView = await esProfileViewService.getAEsProfileView(id);
        if (!esProfileView) {
            util.setError(404, `Cannot find esProfileView with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found ProfileView', esProfileView);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfileViewsCtrl.createProfileView = async (req, res) => {
    try {
        const esProfileView = await esProfileViewService.addEsProfileView(req.body);
        util.setSuccess(201, 'ProfileView Added!', esProfileView);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esProfileViewsCtrl.updateProfileView = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esProfileView = await esProfileViewService.updateEsProfileView(id, req.body);
        if (!esProfileView) {
            util.setError(404, `Cannot find es ProfileView with the id: ${id}`);
        } else {
            util.setSuccess(200, 'ProfileView updated', esProfileView);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esProfileViewsCtrl.deleteProfileView = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esProfileView = await esProfileViewService.deleteEsProfileView(id);
        if (esProfileView) {
            util.setSuccess(200, 'esProfileView deleted');
        } else {
            util.setError(404, `esProfileView with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esProfileViewsCtrl;


