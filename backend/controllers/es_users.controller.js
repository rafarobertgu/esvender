const esUserService = require('../services/esUser.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esUsersCtrl = {};

esUsersCtrl.service = esUserService;

esUsersCtrl.getUsers = async (req, res) => {
    try {
        const esUsers = await esUserService.getAllEsUsers();
        if (esUsers.length > 0) {
            util.setSuccess(200, 'Users retrieved', esUsers);
        } else {
            util.setSuccess(200, 'No esUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esUsersCtrl.getUser = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esUser = await esUserService.getAEsUser(id);
        if (!esUser) {
            util.setError(404, `Cannot find esUser with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found User', esUser);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esUsersCtrl.createUser = async (req, res) => {
    try {
        const esUser = await esUserService.addEsUser(req.body);
        util.setSuccess(201, 'User Added!', esUser);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esUsersCtrl.updateUser = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esUser = await esUserService.updateEsUser(id, req.body);
        if (!esUser) {
            util.setError(404, `Cannot find es User with the id: ${id}`);
        } else {
            util.setSuccess(200, 'User updated', esUser);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esUsersCtrl.deleteUser = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esUser = await esUserService.deleteEsUser(id);
        if (esUser) {
            util.setSuccess(200, 'esUser deleted');
        } else {
            util.setError(404, `esUser with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esUsersCtrl;


