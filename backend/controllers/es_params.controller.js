const esParamService = require('../services/esParam.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esParamsCtrl = {};

esParamsCtrl.service = esParamService;

esParamsCtrl.getParams = async (req, res) => {
    try {
        const esParams = await esParamService.getAllEsParams();
        if (esParams.length > 0) {
            util.setSuccess(200, 'Params retrieved', esParams);
        } else {
            util.setSuccess(200, 'No esParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esParamsCtrl.getParam = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esParam = await esParamService.getAEsParam(id);
        if (!esParam) {
            util.setError(404, `Cannot find esParam with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Param', esParam);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esParamsCtrl.findParamsByDictionaryIds = async (req, res) => {
    const ids = req.params.ids.split(',');
    try {
        const esParams = await esParamService.getEsParamsByDictionaryIds(ids);
        if (esParams.length > 0) {
            util.setSuccess(200, 'Params retrieved', esParams);
        } else {
            util.setSuccess(200, 'No esParam found');
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esParamsCtrl.createParam = async (req, res) => {
    try {
        const esParam = await esParamService.addEsParam(req.body);
        util.setSuccess(201, 'Param Added!', esParam);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esParamsCtrl.updateParam = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esParam = await esParamService.updateEsParam(id, req.body);
        if (!esParam) {
            util.setError(404, `Cannot find es Param with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Param updated', esParam);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esParamsCtrl.deleteParam = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esParam = await esParamService.deleteEsParam(id);
        if (esParam) {
            util.setSuccess(200, 'esParam deleted');
        } else {
            util.setError(404, `esParam with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esParamsCtrl;


