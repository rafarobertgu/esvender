const esFieldService = require('../services/esField.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esFieldsCtrl = {};

esFieldsCtrl.service = esFieldService;

esFieldsCtrl.getFields = async (req, res) => {
    try {
        const esFields = await esFieldService.getAllEsFields();
        if (esFields.length > 0) {
            util.setSuccess(200, 'Fields retrieved', esFields);
        } else {
            util.setSuccess(200, 'No esField found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFieldsCtrl.getField = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esField = await esFieldService.getAEsField(id);
        if (!esField) {
            util.setError(404, `Cannot find esField with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Field', esField);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFieldsCtrl.createField = async (req, res) => {
    try {
        const esField = await esFieldService.addEsField(req.body);
        util.setSuccess(201, 'Field Added!', esField);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esFieldsCtrl.updateField = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esField = await esFieldService.updateEsField(id, req.body);
        if (!esField) {
            util.setError(404, `Cannot find es Field with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Field updated', esField);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esFieldsCtrl.deleteField = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esField = await esFieldService.deleteEsField(id);
        if (esField) {
            util.setSuccess(200, 'esField deleted');
        } else {
            util.setError(404, `esField with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esFieldsCtrl;


