const esPersonAttributeService = require('../services/esPersonAttribute.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esPersonAttributesCtrl = {};

esPersonAttributesCtrl.service = esPersonAttributeService;

esPersonAttributesCtrl.getPersonAttributes = async (req, res) => {
    try {
        const esPersonAttributes = await esPersonAttributeService.getAllEsPersonAttributes();
        if (esPersonAttributes.length > 0) {
            util.setSuccess(200, 'PersonAttributes retrieved', esPersonAttributes);
        } else {
            util.setSuccess(200, 'No esPersonAttribute found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esPersonAttributesCtrl.getPersonAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esPersonAttribute = await esPersonAttributeService.getAEsPersonAttribute(id);
        if (!esPersonAttribute) {
            util.setError(404, `Cannot find esPersonAttribute with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found PersonAttribute', esPersonAttribute);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esPersonAttributesCtrl.createPersonAttribute = async (req, res) => {
    try {
        const esPersonAttribute = await esPersonAttributeService.addEsPersonAttribute(req.body);
        util.setSuccess(201, 'PersonAttribute Added!', esPersonAttribute);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esPersonAttributesCtrl.updatePersonAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esPersonAttribute = await esPersonAttributeService.updateEsPersonAttribute(id, req.body);
        if (!esPersonAttribute) {
            util.setError(404, `Cannot find es PersonAttribute with the id: ${id}`);
        } else {
            util.setSuccess(200, 'PersonAttribute updated', esPersonAttribute);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esPersonAttributesCtrl.deletePersonAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esPersonAttribute = await esPersonAttributeService.deleteEsPersonAttribute(id);
        if (esPersonAttribute) {
            util.setSuccess(200, 'esPersonAttribute deleted');
        } else {
            util.setError(404, `esPersonAttribute with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esPersonAttributesCtrl;


