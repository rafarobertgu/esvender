const esPersonService = require('../services/esPerson.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esPeopleCtrl = {};

esPeopleCtrl.service = esPersonService;

esPeopleCtrl.getPeople = async (req, res) => {
    try {
        const esPersons = await esPersonService.getAllEsPeople();
        if (esPersons.length > 0) {
            util.setSuccess(200, 'Persons retrieved', esPersons);
        } else {
            util.setSuccess(200, 'No esPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esPeopleCtrl.getPerson = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esPerson = await esPersonService.getAEsPerson(id);
        if (!esPerson) {
            util.setError(404, `Cannot find esPerson with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Person', esPerson);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esPeopleCtrl.createPerson = async (req, res) => {
    try {
        const esPerson = await esPersonService.addEsPerson(req.body);
        util.setSuccess(201, 'Person Added!', esPerson);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esPeopleCtrl.updatePerson = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esPerson = await esPersonService.updateEsPerson(id, req.body);
        if (!esPerson) {
            util.setError(404, `Cannot find es Person with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Person updated', esPerson);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esPeopleCtrl.deletePerson = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esPerson = await esPersonService.deleteEsPerson(id);
        if (esPerson) {
            util.setSuccess(200, 'esPerson deleted');
        } else {
            util.setError(404, `esPerson with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esPeopleCtrl;


