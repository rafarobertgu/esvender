const esAuthService = require('../services/esAuth.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esAuthsCtrl = {};

esAuthsCtrl.service = esAuthService;

esAuthsCtrl.getAuths = async (req, res) => {
    try {
        const esAuths = await esAuthService.getAllEsAuths();
        if (esAuths.length > 0) {
            util.setSuccess(200, 'Auths retrieved', esAuths);
        } else {
            util.setSuccess(200, 'No esAuth found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esAuthsCtrl.getAuth = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esAuth = await esAuthService.getAEsAuth(id);
        if (!esAuth) {
            util.setError(404, `Cannot find esAuth with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Auth', esAuth);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esAuthsCtrl.createAuth = async (req, res) => {
    try {
        const esAuth = await esAuthService.addEsAuth(req.body);
        util.setSuccess(201, 'Auth Added!', esAuth);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esAuthsCtrl.updateAuth = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esAuth = await esAuthService.updateEsAuth(id, req.body);
        if (!esAuth) {
            util.setError(404, `Cannot find es Auth with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Auth updated', esAuth);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esAuthsCtrl.deleteAuth = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esAuth = await esAuthService.deleteEsAuth(id);
        if (esAuth) {
            util.setSuccess(200, 'esAuth deleted');
        } else {
            util.setError(404, `esAuth with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esAuthsCtrl;


