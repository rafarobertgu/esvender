const esEmployeeService = require('../services/esEmployee.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esEmployeesCtrl = {};

esEmployeesCtrl.service = esEmployeeService;

esEmployeesCtrl.getEmployees = async (req, res) => {
    try {
        const esEmployees = await esEmployeeService.getAllEsEmployees();
        if (esEmployees.length > 0) {
            util.setSuccess(200, 'Employees retrieved', esEmployees);
        } else {
            util.setSuccess(200, 'No esEmployee found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esEmployeesCtrl.getEmployee = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esEmployee = await esEmployeeService.getAEsEmployee(id);
        if (!esEmployee) {
            util.setError(404, `Cannot find esEmployee with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Employee', esEmployee);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esEmployeesCtrl.createEmployee = async (req, res) => {
    try {
        const esEmployee = await esEmployeeService.addEsEmployee(req.body);
        util.setSuccess(201, 'Employee Added!', esEmployee);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esEmployeesCtrl.updateEmployee = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esEmployee = await esEmployeeService.updateEsEmployee(id, req.body);
        if (!esEmployee) {
            util.setError(404, `Cannot find es Employee with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Employee updated', esEmployee);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esEmployeesCtrl.deleteEmployee = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esEmployee = await esEmployeeService.deleteEsEmployee(id);
        if (esEmployee) {
            util.setSuccess(200, 'esEmployee deleted');
        } else {
            util.setError(404, `esEmployee with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esEmployeesCtrl;


