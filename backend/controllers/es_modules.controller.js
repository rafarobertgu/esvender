const esModuleService = require('../services/esModule.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esModulesCtrl = {};

esModulesCtrl.service = esModuleService;

esModulesCtrl.getModules = async (req, res) => {
    try {
        const esModules = await esModuleService.getAllEsModules();
        if (esModules.length > 0) {
            util.setSuccess(200, 'Modules retrieved', esModules);
        } else {
            util.setSuccess(200, 'No esModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esModulesCtrl.getModule = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esModule = await esModuleService.getAEsModule(id);
        if (!esModule) {
            util.setError(404, `Cannot find esModule with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Module', esModule);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esModulesCtrl.createModule = async (req, res) => {
    try {
        const esModule = await esModuleService.addEsModule(req.body);
        util.setSuccess(201, 'Module Added!', esModule);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esModulesCtrl.updateModule = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esModule = await esModuleService.updateEsModule(id, req.body);
        if (!esModule) {
            util.setError(404, `Cannot find es Module with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Module updated', esModule);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esModulesCtrl.deleteModule = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esModule = await esModuleService.deleteEsModule(id);
        if (esModule) {
            util.setSuccess(200, 'esModule deleted');
        } else {
            util.setError(404, `esModule with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esModulesCtrl;


