const esViewService = require('../services/esView.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esViewsCtrl = {};

esViewsCtrl.service = esViewService;

esViewsCtrl.getViews = async (req, res) => {
    try {
        const esViews = await esViewService.getAllEsViews();
        if (esViews.length > 0) {
            util.setSuccess(200, 'Views retrieved', esViews);
        } else {
            util.setSuccess(200, 'No esView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esViewsCtrl.findViewsByModuleId = async (req, res) => {
    const ids = req.params.ids.split(',');
    try {
        const esViews = await esViewService.findViewsByModuleId(ids);
        if (esViews.length > 0) {
            util.setSuccess(200, 'Params retrieved', esViews);
        } else {
            util.setSuccess(200, 'No esParam found');
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esViewsCtrl.getView = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esView = await esViewService.getAEsView(id);
        if (!esView) {
            util.setError(404, `Cannot find esView with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found View', esView);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esViewsCtrl.createView = async (req, res) => {
    try {
        const esView = await esViewService.addEsView(req.body);
        util.setSuccess(201, 'View Added!', esView);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esViewsCtrl.updateView = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esView = await esViewService.updateEsView(id, req.body);
        if (!esView) {
            util.setError(404, `Cannot find es View with the id: ${id}`);
        } else {
            util.setSuccess(200, 'View updated', esView);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esViewsCtrl.deleteView = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esView = await esViewService.deleteEsView(id);
        if (esView) {
            util.setSuccess(200, 'esView deleted');
        } else {
            util.setError(404, `esView with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esViewsCtrl;


