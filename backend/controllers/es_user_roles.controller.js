const esUserRoleService = require('../services/esUserRole.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esUserRolesCtrl = {};

esUserRolesCtrl.service = esUserRoleService;

esUserRolesCtrl.getUserRoles = async (req, res) => {
    try {
        const esUserRoles = await esUserRoleService.getAllEsUserRoles();
        if (esUserRoles.length > 0) {
            util.setSuccess(200, 'UserRoles retrieved', esUserRoles);
        } else {
            util.setSuccess(200, 'No esUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esUserRolesCtrl.getUserRole = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esUserRole = await esUserRoleService.getAEsUserRole(id);
        if (!esUserRole) {
            util.setError(404, `Cannot find esUserRole with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found UserRole', esUserRole);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esUserRolesCtrl.createUserRole = async (req, res) => {
    try {
        const esUserRole = await esUserRoleService.addEsUserRole(req.body);
        util.setSuccess(201, 'UserRole Added!', esUserRole);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esUserRolesCtrl.updateUserRole = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esUserRole = await esUserRoleService.updateEsUserRole(id, req.body);
        if (!esUserRole) {
            util.setError(404, `Cannot find es UserRole with the id: ${id}`);
        } else {
            util.setSuccess(200, 'UserRole updated', esUserRole);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esUserRolesCtrl.deleteUserRole = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esUserRole = await esUserRoleService.deleteEsUserRole(id);
        if (esUserRole) {
            util.setSuccess(200, 'esUserRole deleted');
        } else {
            util.setError(404, `esUserRole with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esUserRolesCtrl;


