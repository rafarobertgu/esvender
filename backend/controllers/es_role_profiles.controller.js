const esRoleProfileService = require('../services/esRoleProfile.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esRoleProfilesCtrl = {};

esRoleProfilesCtrl.service = esRoleProfileService;

esRoleProfilesCtrl.getRoleProfiles = async (req, res) => {
    try {
        const esRoleProfiles = await esRoleProfileService.getAllEsRoleProfiles();
        if (esRoleProfiles.length > 0) {
            util.setSuccess(200, 'RoleProfiles retrieved', esRoleProfiles);
        } else {
            util.setSuccess(200, 'No esRoleProfile found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esRoleProfilesCtrl.getRoleProfile = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esRoleProfile = await esRoleProfileService.getAEsRoleProfile(id);
        if (!esRoleProfile) {
            util.setError(404, `Cannot find esRoleProfile with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found RoleProfile', esRoleProfile);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esRoleProfilesCtrl.createRoleProfile = async (req, res) => {
    try {
        const esRoleProfile = await esRoleProfileService.addEsRoleProfile(req.body);
        util.setSuccess(201, 'RoleProfile Added!', esRoleProfile);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esRoleProfilesCtrl.updateRoleProfile = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esRoleProfile = await esRoleProfileService.updateEsRoleProfile(id, req.body);
        if (!esRoleProfile) {
            util.setError(404, `Cannot find es RoleProfile with the id: ${id}`);
        } else {
            util.setSuccess(200, 'RoleProfile updated', esRoleProfile);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esRoleProfilesCtrl.deleteRoleProfile = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esRoleProfile = await esRoleProfileService.deleteEsRoleProfile(id);
        if (esRoleProfile) {
            util.setSuccess(200, 'esRoleProfile deleted');
        } else {
            util.setError(404, `esRoleProfile with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esRoleProfilesCtrl;


