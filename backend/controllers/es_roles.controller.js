const esRoleService = require('../services/esRole.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esRolesCtrl = {};

esRolesCtrl.service = esRoleService;

esRolesCtrl.getRoles = async (req, res) => {
    try {
        const esRoless = await esRoleService.getAllEsRoles();
        if (esRoless.length > 0) {
            util.setSuccess(200, 'Roless retrieved', esRoless);
        } else {
            util.setSuccess(200, 'No esRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esRolesCtrl.getRole = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esRole = await esRoleService.getAEsRole(id);
        if (!esRole) {
            util.setError(404, `Cannot find esRole with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Role', esRole);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esRolesCtrl.createRole = async (req, res) => {
    try {
        const esRole = await esRoleService.addEsRole(req.body);
        util.setSuccess(201, 'Role Added!', esRole);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esRolesCtrl.updateRole = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esRole = await esRoleService.updateEsRole(id, req.body);
        if (!esRole) {
            util.setError(404, `Cannot find es Role with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Role updated', esRole);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esRolesCtrl.deleteRole = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esRole = await esRoleService.deleteEsRole(id);
        if (esRole) {
            util.setSuccess(200, 'esRole deleted');
        } else {
            util.setError(404, `esRole with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esRolesCtrl;


