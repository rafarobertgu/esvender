const esObjectService = require('../services/esObject.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esObjectsCtrl = {};

esObjectsCtrl.service = esObjectService;

esObjectsCtrl.getObjects = async (req, res) => {
    try {
        const esObjects = await esObjectService.getAllEsObjects();
        if (esObjects.length > 0) {
            util.setSuccess(200, 'Objects retrieved', esObjects);
        } else {
            util.setSuccess(200, 'No esObject found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esObjectsCtrl.getObject = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esObject = await esObjectService.getAEsObject(id);
        if (!esObject) {
            util.setError(404, `Cannot find esObject with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Object', esObject);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esObjectsCtrl.createObject = async (req, res) => {
    try {
        const esObject = await esObjectService.addEsObject(req.body);
        util.setSuccess(201, 'Object Added!', esObject);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esObjectsCtrl.updateObject = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esObject = await esObjectService.updateEsObject(id, req.body);
        if (!esObject) {
            util.setError(404, `Cannot find es Object with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Object updated', esObject);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esObjectsCtrl.deleteObject = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esObject = await esObjectService.deleteEsObject(id);
        if (esObject) {
            util.setSuccess(200, 'esObject deleted');
        } else {
            util.setError(404, `esObject with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esObjectsCtrl;


