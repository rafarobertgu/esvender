const esComponentService = require('../services/esComponent.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esComponentsCtrl = {};

esComponentsCtrl.service = esComponentService;

esComponentsCtrl.getComponents = async (req, res) => {
    try {
        const esComponents = await esComponentService.getAllEsComponents();
        if (esComponents.length > 0) {
            util.setSuccess(200, 'Components retrieved', esComponents);
        } else {
            util.setSuccess(200, 'No esComponent found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esComponentsCtrl.getComponent = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esComponent = await esComponentService.getAEsComponent(id);
        if (!esComponent) {
            util.setError(404, `Cannot find esComponent with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found Component', esComponent);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esComponentsCtrl.createComponent = async (req, res) => {
    try {
        const esComponent = await esComponentService.addEsComponent(req.body);
        util.setSuccess(201, 'Component Added!', esComponent);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esComponentsCtrl.updateComponent = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esComponent = await esComponentService.updateEsComponent(id, req.body);
        if (!esComponent) {
            util.setError(404, `Cannot find es Component with the id: ${id}`);
        } else {
            util.setSuccess(200, 'Component updated', esComponent);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esComponentsCtrl.deleteComponent = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esComponent = await esComponentService.deleteEsComponent(id);
        if (esComponent) {
            util.setSuccess(200, 'esComponent deleted');
        } else {
            util.setError(404, `esComponent with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esComponentsCtrl;


