const esComponentFieldService = require('../services/esComponentField.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esComponentFieldsCtrl = {};

esComponentFieldsCtrl.service = esComponentFieldService;

esComponentFieldsCtrl.getComponentFields = async (req, res) => {
    try {
        const esComponentFields = await esComponentFieldService.getAllEsComponentFields();
        if (esComponentFields.length > 0) {
            util.setSuccess(200, 'ComponentFields retrieved', esComponentFields);
        } else {
            util.setSuccess(200, 'No esComponentField found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esComponentFieldsCtrl.getComponentField = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esComponentField = await esComponentFieldService.getAEsComponentField(id);
        if (!esComponentField) {
            util.setError(404, `Cannot find esComponentField with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found ComponentField', esComponentField);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esComponentFieldsCtrl.createComponentField = async (req, res) => {
    try {
        const esComponentField = await esComponentFieldService.addEsComponentField(req.body);
        util.setSuccess(201, 'ComponentField Added!', esComponentField);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esComponentFieldsCtrl.updateComponentField = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esComponentField = await esComponentFieldService.updateEsComponentField(id, req.body);
        if (!esComponentField) {
            util.setError(404, `Cannot find es ComponentField with the id: ${id}`);
        } else {
            util.setSuccess(200, 'ComponentField updated', esComponentField);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esComponentFieldsCtrl.deleteComponentField = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esComponentField = await esComponentFieldService.deleteEsComponentField(id);
        if (esComponentField) {
            util.setSuccess(200, 'esComponentField deleted');
        } else {
            util.setError(404, `esComponentField with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esComponentFieldsCtrl;


