const esObjectAttributeService = require('../services/esObjectAttribute.service');
const Util = require('../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

const esObjectAttributesCtrl = {};

esObjectAttributesCtrl.service = esObjectAttributeService;

esObjectAttributesCtrl.getObjectAttributes = async (req, res) => {
    try {
        const esObjectAttributes = await esObjectAttributeService.getAllEsObjectAttributes();
        if (esObjectAttributes.length > 0) {
            util.setSuccess(200, 'ObjectAttributes retrieved', esObjectAttributes);
        } else {
            util.setSuccess(200, 'No esObjectAttribute found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esObjectAttributesCtrl.getObjectAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esObjectAttribute = await esObjectAttributeService.getAEsObjectAttribute(id);
        if (!esObjectAttribute) {
            util.setError(404, `Cannot find esObjectAttribute with the id ${id}`);
        } else {
            util.setSuccess(200, 'Found ObjectAttribute', esObjectAttribute);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esObjectAttributesCtrl.createObjectAttribute = async (req, res) => {
    try {
        const esObjectAttribute = await esObjectAttributeService.addEsObjectAttribute(req.body);
        util.setSuccess(201, 'ObjectAttribute Added!', esObjectAttribute);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

esObjectAttributesCtrl.updateObjectAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please input a valid string value');
        return util.send(res);
    }
    try {
        const esObjectAttribute = await esObjectAttributeService.updateEsObjectAttribute(id, req.body);
        if (!esObjectAttribute) {
            util.setError(404, `Cannot find es ObjectAttribute with the id: ${id}`);
        } else {
            util.setSuccess(200, 'ObjectAttribute updated', esObjectAttribute);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

esObjectAttributesCtrl.deleteObjectAttribute = async (req, res) => {
    const { id } = req.params;
    if (!String(id)) {
        util.setError(400, 'Please provide a string value');
        return util.send(res);
    }
    try {
        const esObjectAttribute = await esObjectAttributeService.deleteEsObjectAttribute(id);
        if (esObjectAttribute) {
            util.setSuccess(200, 'esObjectAttribute deleted');
        } else {
            util.setError(404, `esObjectAttribute with the id ${id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

module.exports = esObjectAttributesCtrl;


