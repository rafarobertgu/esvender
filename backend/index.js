var fs = require('fs');
const path = require('path');
const config = require('dotenv');
const morgan = require("morgan");
const express = require("express");
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const expressSession = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');

const initPassport = require('./config/passport');
initPassport();
config.config();

const app = express();

const {mongoose} = require("./config/mongoose");
//Views
app.use(express.static('assets'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Settings
app.set("port", process.env.PORT || 3000);
app.set("sql", process.env.SQL || 1);
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:4200"); // update to match the domain you will make the request from
  // res.header("Access-Control-Allow-Origin", "https://corredoresecofuturo.com.bo:7000"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-AUTHENTICATION, X-IP, Content-Type, Accept");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  next();
});

// Midlewares
app.use(morgan("dev"));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(expressSession({secret: 'anystringoftext',
                                saveUninitialized: true,
                                resave: true}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use(express.json());

// Routes API
app.use("/es-attributes", require("./routes/es_attributes.routes"));
app.use("/es-components", require("./routes/es_components.routes"));
app.use("/es-dictionaries", require("./routes/es_dictionaries.routes"));
app.use("/es-employees", require("./routes/es_employees.routes"));
app.use("/es-file-attributes", require("./routes/es_file_attributes.routes"));
app.use("/es-files", require("./routes/es_files.routes"));
app.use("/es-modules", require("./routes/es_modules.routes"));
app.use("/es-object-attributes", require("./routes/es_object_attributes.routes"));
app.use("/es-objects", require("./routes/es_objects.routes"));
app.use("/es-params", require("./routes/es_params.routes"));
app.use("/es-people", require("./routes/es_people.routes"));
app.use("/es-person-attributes", require("./routes/es_person_attributes.routes"));
app.use("/es-profile-components", require("./routes/es_profile_components.routes"));
app.use("/es-profile-modules", require("./routes/es_profile_modules.routes"));
app.use("/es-profile-views", require("./routes/es_profile_views.routes"));
app.use("/es-profiles", require("./routes/es_profiles.routes"));
app.use("/es-role-profiles", require("./routes/es_role_profiles.routes"));
app.use("/es-roles", require("./routes/es_roles.routes"));
app.use("/es-user-roles", require("./routes/es_user_roles.routes"));
app.use("/es-users", require("./routes/es_users.routes"));
app.use("/es-views", require("./routes/es_views.routes"));
app.use("/es-fields", require("./routes/es_fields.routes"));
app.use("/es-field-params", require("./routes/es_field_params.routes"));
app.use("/es-component-fields", require("./routes/es_component_fields.routes"));

app.use("/es-auths", require("./routes/es_auths.routes"));

// Starting the server
app.listen(app.get("port"), () => {
    console.log(app.get("port"));
});
