const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esAuths = sequelize.define('esAuths', {
      uid: DataTypes.STRING,

      aut_par_status_id: DataTypes.INTEGER,
      aut_par_status_uid: DataTypes.STRING,
      usr_id: DataTypes.INTEGER,
      usr_uid: DataTypes.STRING,
      aut_data: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_auths'
    });
    esAuths.associate = (models) => {
      // associations can be defined here

    };
    return esAuths;
  };
} else {
  module.exports = mongoose.model("esAuths", new Schema({
    id: {type: Number},
    uid: {type: String},

    aut_par_status_id: {type: Number},
    aut_par_status_uid: {type: String},
    usr_id: {type: Number},
    usr_uid: {type: String},
    aut_data: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_auths');
}






