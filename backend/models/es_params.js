const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esParams = sequelize.define('esParams', {
      uid: DataTypes.STRING,
      par_cod: DataTypes.STRING,
      par_description: DataTypes.STRING,
      par_abbr: DataTypes.STRING,
      par_dictionry_id: DataTypes.INTEGER,
      par_dictionry_uid: DataTypes.STRING,
      par_order: DataTypes.INTEGER,
      par_status_id: DataTypes.INTEGER,
      par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_params'
    });
    esParams.associate = (models) => {
      // associations can be defined here

    };
    return esParams;
  };
} else {
  module.exports = mongoose.model("esParams", new Schema({
    id: {type: Number},
    uid: {type: String},
    par_cod: {type: String},
    par_description: {type: String},
    par_abbr: {type: String},
    par_dictionry_id: {type: Number},
    par_dictionry_uid: {type: String},
    par_order: {type: Number},
    par_status_id: {type: Number},
    par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_params');
}




