const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esRoles = sequelize.define('esRoles', {
      uid: DataTypes.STRING,
      rol_code: DataTypes.STRING,
      rol_description: DataTypes.STRING,
      rol_abbr: DataTypes.STRING,
      rol_par_status_id: DataTypes.INTEGER,
      rol_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_roles'
    });
    esRoles.associate = (models) => {
      // associations can be defined here

    };
    return esRoles;
  };
} else {
  module.exports = mongoose.model("esRoles", new Schema({
    id: {type: Number},
    uid: {type: String},
    rol_code: {type: String},
    rol_description: {type: String},
    rol_abbr: {type: String},
    rol_par_status_id: {type: Number},
    rol_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_roles');
}



