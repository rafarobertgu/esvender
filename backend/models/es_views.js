const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esViews = sequelize.define('esViews', {
      uid: DataTypes.STRING,
      vie_module_id: DataTypes.INTEGER,
      vie_module_uid: DataTypes.STRING,
      vie_code: DataTypes.STRING,
      vie_description: DataTypes.STRING,
      vie_route: DataTypes.STRING,
      vie_params: DataTypes.STRING,
      vie_return_id: DataTypes.INTEGER,
      vie_return_uid: DataTypes.STRING,
      vie_icon: DataTypes.STRING,
      vie_par_status_id: DataTypes.INTEGER,
      vie_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_views'
    });
    esViews.associate = (models) => {
      // associations can be defined here
      models.esViews.belongsTo(models.esModules,{foreignKey:'vie_module_id', as:'VieModuleId'});
      models.esModules.hasMany(models.esViews,{foreignKey:'vie_module_id', as:'VieModuleId'});
      models.esViews.belongsTo(models.esModules,{foreignKey:'vie_module_uid', targetKey: 'uid', as:'VieModuleUid'});
      models.esModules.hasMany(models.esViews,{foreignKey:'vie_module_uid', sourceKey: 'uid', as:'VieModuleUid'});
      models.esViews.belongsTo(models.esParams,{foreignKey:'vie_par_status_id', as:'VieParStatusId'});
      models.esParams.hasMany(models.esViews,{foreignKey:'vie_par_status_id', as:'VieParStatusId'});
      models.esViews.belongsTo(models.esParams,{foreignKey:'vie_par_status_uid', targetKey: 'uid', as:'VieParStatusUid'});
      models.esParams.hasMany(models.esViews,{foreignKey:'vie_par_status_uid', sourceKey: 'uid', as:'VieParStatusUid'});

      models.esViews.belongsTo(models.esUsers,{foreignKey:'createdById', as:'ViewCreatedById'});
      models.esUsers.hasMany(models.esViews,{foreignKey:'createdById', as:'ViewCreatedById'});
      models.esViews.belongsTo(models.esUsers,{foreignKey:'createdByUid', targetKey: 'uid', as:'ViewCreatedByUid'});
      models.esUsers.hasMany(models.esViews,{foreignKey:'createdByUid', sourceKey: 'uid', as:'ViewCreatedByUid'});
      models.esViews.belongsTo(models.esUsers,{foreignKey:'updatedByUid', targetKey: 'uid', as:'ViewUpdatedByUid'});
      models.esUsers.hasMany(models.esViews,{foreignKey:'updatedByUid', sourceKey: 'uid', as:'ViewUpdatedByUid'});
      models.esViews.belongsTo(models.esUsers,{foreignKey:'updatedById', as:'ViewUpdatedById'});
      models.esUsers.hasMany(models.esViews,{foreignKey:'updatedById', as:'ViewUpdatedById'});

    };
    return esViews;
  };
} else {
  module.exports = mongoose.model("esViews", new Schema({
    id: {type: Number},
    uid: {type: String},
    vie_module_id: {type: Number},
    vie_module_uid: {type: String},
    vie_code: {type: String},
    vie_description: {type: String},
    vie_route: {type: String},
    vie_params: {type: String},
    vie_return_id: {type: Number},
    vie_return_uid: {type: String},
    vie_icon: {type: String},
    vie_par_status_id: {type: Number},
    vie_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_views');
}






