const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esViews = sequelize.define('esFields', {
      uid: DataTypes.STRING,
      fie_com_id: DataTypes.INTEGER,
      fie_com_uid: DataTypes.STRING,
      fie_code: DataTypes.STRING,
      fie_description: DataTypes.STRING,
      fie_par_status_id: DataTypes.INTEGER,
      fie_par_status_uid: DataTypes.STRING,
      fie_par_severity_id: DataTypes.INTEGER,
      fie_par_severity_uid: DataTypes.STRING,
      fie_par_validator_id: DataTypes.INTEGER,
      fie_par_validator_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_fields'
    });
    esViews.associate = (models) => {
      // associations can be defined here

    };
    return esViews;
  };
} else {
  module.exports = mongoose.model("esFields", new Schema({
    id: {type: Number},
    uid: {type: String},
    fie_com_id: {type: Number},
    fie_com_uid: {type: String},
    fie_code: {type: String},
    fie_description: {type: String},
    fie_par_status_id: {type: Number},
    fie_par_status_uid: {type: String},
    fie_par_severity_id: {type: Number},
    fie_par_severity_uid: {type: String},
    fie_par_validator_id: {type: Number},
    fie_par_validator_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},

  }),'es_fields');
}






