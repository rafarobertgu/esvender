const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esAttributes = sequelize.define('esAttributes', {
      uid: DataTypes.STRING,
      att_par_type_id: DataTypes.INTEGER,
      att_par_type_uid: DataTypes.STRING,
      att_code: DataTypes.STRING,
      att_description: DataTypes.STRING,
      att_par_status_id: DataTypes.INTEGER,
      att_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE
    }, {
      tableName:'es_attributes'
    })
    esAttributes.associate = (models) => {
      models.esAttributes.belongsTo(models.esParams,{foreignKey:'att_par_type_id', as:'AttParTypeId'});
      models.esParams.hasMany(models.esAttributes,{foreignKey:'att_par_type_id', as:'AttParTypeId'});
      models.esAttributes.belongsTo(models.esParams,{foreignKey:'att_par_type_uid', targetKey: 'uid', as:'AttParTypeUid'});
      models.esParams.hasMany(models.esAttributes,{foreignKey:'att_par_type_uid', sourceKey: 'uid', as:'AttParTypeUid'});
      models.esAttributes.belongsTo(models.esParams,{foreignKey:'att_par_status_id', as:'AttParStatusId'});
      models.esParams.hasMany(models.esAttributes,{foreignKey:'att_par_status_id', as:'AttParStatusId'});
      models.esAttributes.belongsTo(models.esParams,{foreignKey:'att_par_status_uid', targetKey: 'uid', as:'AttParStatusUid'});
      models.esParams.hasMany(models.esAttributes,{foreignKey:'att_par_status_uid', sourceKey: 'uid', as:'AttParStatusUid'});

      models.esAttributes.belongsTo(models.esUsers,{foreignKey:'createdById', as:'AttributeCreatedById'});
      models.esUsers.hasMany(models.esAttributes,{foreignKey:'createdById', as:'AttributeCreatedById'});
      models.esAttributes.belongsTo(models.esUsers,{foreignKey:'createdByUid', targetKey: 'uid', as:'AttributeCreatedByUid'});
      models.esUsers.hasMany(models.esAttributes,{foreignKey:'createdByUid', sourceKey: 'uid', as:'AttributeCreatedByUid'});
      models.esAttributes.belongsTo(models.esUsers,{foreignKey:'updatedByUid', targetKey: 'uid', as:'AttributeUpdatedByUid'});
      models.esUsers.hasMany(models.esAttributes,{foreignKey:'updatedByUid', sourceKey: 'uid', as:'AttributeUpdatedByUid'});
      models.esAttributes.belongsTo(models.esUsers,{foreignKey:'updatedById', as:'AttributeUpdatedById'});
      models.esUsers.hasMany(models.esAttributes,{foreignKey:'updatedById', as:'AttributeUpdatedById'});
    };
    return esAttributes;
  };
} else {
  module.exports = mongoose.model("esAttributes", new Schema({
    id: {type: Number},
    uid: {type: String},
    att_par_type_id: {type: Number},
    att_par_type_uid: {type: String},
    att_code: {type: String},
    att_description: {type: String},
    att_par_status_id: {type: Number},
    att_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date, allowNull: false},
    updatedAt: {type: Date, allowNull: false}
  }),'es_attributes');
}


