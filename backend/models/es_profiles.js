const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esProfiles = sequelize.define('esProfiles', {
      uid: DataTypes.STRING,
      pro_code: DataTypes.STRING,
      pro_description: DataTypes.STRING,
      pro_abbr: DataTypes.STRING,
      pro_par_status_id: DataTypes.INTEGER,
      pro_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_profiles'
    });
    esProfiles.associate = (models) => {
      // associations can be defined here

    };
    return esProfiles;
  };
} else {
  module.exports = mongoose.model("esProfiles", new Schema({
    id: {type: Number},
    uid: {type: String},
    pro_code: {type: String},
    pro_description: {type: String},
    pro_abbr: {type: String},
    pro_par_status_id: {type: Number},
    pro_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }), 'es_profiles');
}



