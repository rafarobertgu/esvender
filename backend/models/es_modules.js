const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esModules = sequelize.define('esModules', {
      uid: DataTypes.STRING,
      mod_parent_id: DataTypes.INTEGER,
      mod_parent_uid: DataTypes.STRING,
      mod_code: DataTypes.STRING,
      mod_description: DataTypes.STRING,
      mod_abbr: DataTypes.STRING,
      mod_icon: DataTypes.STRING,
      mod_par_status_id: DataTypes.INTEGER,
      mod_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_modules'
    });
    esModules.associate = (models) => {
      // associations can be defined here

    };
    return esModules;
  };
} else {
  module.exports = mongoose.model("esModules", new Schema({
    id: {type: Number},
    uid: {type: String},
    mod_parent_id: {type: Number},
    mod_parent_uid: {type: String},
    mod_code: {type: String},
    mod_description: {type: String},
    mod_abbr: {type: String},
    mod_icon: {type: String},
    mod_par_status_id: {type: Number},
    mod_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_modules');
}




