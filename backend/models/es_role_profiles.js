const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esRoleProfiles = sequelize.define('esRoleProfiles', {
      uid: DataTypes.STRING,
      rol_id: DataTypes.INTEGER,
      rol_uid: DataTypes.STRING,
      pro_id: DataTypes.INTEGER,
      pro_uid: DataTypes.STRING,
      rol_pro_par_status_id: DataTypes.INTEGER,
      rol_pro_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_role_profiles'
    });
    esRoleProfiles.associate = (models) => {
      // associations can be defined here

    };
    return esRoleProfiles;
  };
} else {
  module.exports = mongoose.model("esRoleProfiles", new Schema({
    id: {type: Number},
    uid: {type: String},
    rol_id: {type: Number},
    rol_uid: {type: String},
    pro_id: {type: Number},
    pro_uid: {type: String},
    rol_pro_par_status_id: {type: Number},
    rol_pro_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_role_profiles');
}



