const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esFiles = sequelize.define('esFiles', {
      uid: DataTypes.STRING,
      fil_rute: DataTypes.STRING,
      fil_par_type_id: DataTypes.INTEGER,
      fil_par_type_uid: DataTypes.STRING,
      fil_par_status_id: DataTypes.INTEGER,
      fil_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_files'
    });
    esFiles.associate = (models) => {
      // associations can be defined here

    };
    return esFiles;
  };
} else {
  module.exports = mongoose.model("esFiles", new Schema({
    id: {type: Number},
    uid: {type: String},
    fil_rute: {type: String},
    fil_par_type_id: {type: Number},
    fil_par_type_uid: {type: String},
    fil_par_status_id: {type: Number},
    fil_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_files');
}




