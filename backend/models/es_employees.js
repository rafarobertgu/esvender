const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
    module.exports = (sequelize, DataTypes) => {
        const esEmployees = sequelize.define('esEmployees', {
            uid: DataTypes.STRING,
            emp_name: DataTypes.STRING,
            emp_position: DataTypes.STRING,
            emp_office: DataTypes.STRING,
            emp_salary: DataTypes.FLOAT,
            emp_par_status_id: DataTypes.INTEGER,
            emp_par_status_uid: DataTypes.STRING,
            createdById: DataTypes.INTEGER,
            createdByUid: DataTypes.STRING,
            updatedById: DataTypes.INTEGER,
            updatedByUid: DataTypes.STRING,
            dueAt: DataTypes.DATE,
        }, {
            tableName:'es_employees'
        });
        esEmployees.associate = (models) => {
            // associations can be defined here

        };
        return esEmployees;
    };
} else {
    module.exports = mongoose.model("esEmployees", new Schema({
        id: {type: Number},
        uid: {type: String},
        emp_name: {type: String},
        emp_position: {type: String},
        emp_office: {type: String},
        emp_salary: {type: mongoose.Types.Decimal128},
        emp_par_status_id: {type: Number},
        emp_par_status_uid: {type: String},
        createdById: {type: Number},
        createdByUid: {type: String},
        updatedById: {type: Number},
        updatedByUid: {type: String},
        dueAt: {type: Date},
        createdAt: {type: Date},
        updatedAt: {type: Date},
    }),'es_employees');
}




