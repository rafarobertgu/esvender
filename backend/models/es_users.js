const mongoose = require("mongoose");
const {Schema} = mongoose;
const bcrypt = require("bcrypt");
const sql = process.env.SQL;

'use strict';
if(sql) {
	module.exports = (sequelize, DataTypes) => {
		const esUsers = sequelize.define('esUsers', {
			uid: DataTypes.STRING,

			usr_id: DataTypes.STRING,
			usr_username: DataTypes.STRING,
			usr_password: DataTypes.STRING,
			usr_mail: DataTypes.STRING,
			usr_token: DataTypes.STRING,
			usr_name: DataTypes.STRING,
			usr_lastname: DataTypes.STRING,
			usr_par_auth_strategy_id: DataTypes.INTEGER,
			usr_par_auth_strategy_uid: DataTypes.STRING,
			usr_accept_terms: DataTypes.BOOLEAN,

			usr_rol_id: DataTypes.INTEGER,
			usr_rol_uid: DataTypes.STRING,
			usr_mod_id: DataTypes.INTEGER,
			usr_mod_uid: DataTypes.STRING,
			usr_person_id: DataTypes.INTEGER,
			usr_person_uid: DataTypes.STRING,
			usr_pwd_change: DataTypes.INTEGER,
			usr_fil_img_id: DataTypes.INTEGER,
			usr_fil_img_uid: DataTypes.STRING,
			usr_last_login: DataTypes.DATE,

			usr_par_status_uid: DataTypes.STRING,
			usr_par_status_id: DataTypes.INTEGER,
			createdById: DataTypes.INTEGER,
			createdByUid: DataTypes.STRING,
			updatedById: DataTypes.INTEGER,
			updatedByUid: DataTypes.STRING,
			dueAt: DataTypes.DATE,
		}, {
			tableName:'es_users',
		});
		esUsers.associate = (models) => {
			models.esAttributes.belongsTo(models.esParams,{foreignKey:'att_par_type_id'});
			models.esParams.hasMany(models.esAttributes,{foreignKey:'att_par_type_id'});
		};
		return esUsers;
	};
} else {
	module.exports = mongoose.model("esUsers", new Schema({
		id: {type: Number},
		uid: {type: String},

		usr_id: {type: String},
		usr_username: {type: String},
		usr_password: {type: String},
		usr_mail: {type: String},
		usr_token: {type: String},
		usr_name: {type: String},
		usr_lastname: {type: String},
		usr_par_auth_strategy_id: {type: Number},
		usr_par_auth_strategy_uid: {type: String},
		usr_accept_terms: {type: Boolean},

		usr_rol_id: {type: Number},
		usr_rol_uid: {type: String},
		usr_mod_id: {type: Number},
		usr_mod_uid: {type: String},
		usr_person_id: {type: Number},
		usr_person_uid: {type: String},
		usr_fil_img_id: {type: Number},
		usr_fil_img_uid: {type: String},
		usr_last_login: {type: Date},
		usr_pwd_change: {type: Number},

		usr_par_status_uid: {type: String},
		usr_par_status_id: {type: Number},
		createdById: {type: Number},
		createdByUid: {type: String},
		updatedById: {type: Number},
		updatedByUid: {type: String},
		dueAt: {type: Date},
		createdAt: {type: Date},
		updatedAt: {type: Date},
	}),'es_users');
}






