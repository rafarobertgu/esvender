const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
	module.exports = (sequelize, DataTypes) => {
		const esPeople = sequelize.define('esPeople', {
			uid: DataTypes.STRING,
			per_first_name: DataTypes.STRING,
			per_second_name: DataTypes.INTEGER,
			per_first_lastname: DataTypes.STRING,
			per_second_lastname: DataTypes.INTEGER,
			per_license: DataTypes.STRING,
			per_license_comp: DataTypes.STRING,
			per_home_address: DataTypes.STRING,
			per_mail: DataTypes.STRING,
			per_home_phone: DataTypes.STRING,
			per_cellphone: DataTypes.STRING,
			per_birthday: DataTypes.DATE,
			per_parent_id: DataTypes.INTEGER,
			per_parent_uid: DataTypes.STRING,
			per_par_type_doc_id: DataTypes.INTEGER,
			per_par_type_doc_uid: DataTypes.STRING,
			per_par_city_id: DataTypes.INTEGER,
			per_par_city_uid: DataTypes.STRING,
			per_par_sex_id: DataTypes.INTEGER,
			per_par_sex_uid: DataTypes.STRING,
			per_par_country_id: DataTypes.INTEGER,
			per_par_country_uid: DataTypes.STRING,
			per_par_nacionality_id: DataTypes.INTEGER,
			per_par_nacionality_uid: DataTypes.STRING,
			per_par_status_uid: DataTypes.STRING,
			per_par_status_id: DataTypes.INTEGER,
			createdById: DataTypes.INTEGER,
			createdByUid: DataTypes.STRING,
			updatedById: DataTypes.INTEGER,
			updatedByUid: DataTypes.STRING,
			dueAt: DataTypes.DATE,
		}, {
			tableName:'es_people'
		});
		esPeople.associate = (models) => {
			// associations can be defined here

		};
		return esPeople;
	};
} else {
	module.exports = mongoose.model("esPeople", new Schema({
		id: {type: Number},
		uid: {type: String},
		per_first_name: {type: String},
		per_second_name: {type: Number},
		per_first_lastname: {type: String},
		per_second_lastname: {type: Number},
		per_license: {type: String},
		per_license_comp: {type: String},
		per_home_address: {type: String},
		per_mail: {type: String},
		per_home_phone: {type: String},
		per_cellphone: {type: String},
		per_birthday: {type: Date},
		per_parent_id: {type: Number},
		per_parent_uid: {type: String},
		per_par_type_doc_id: {type: Number},
		per_par_type_doc_uid: {type: String},
		per_par_city_id: {type: Number},
		per_par_city_uid: {type: String},
		per_par_sex_id: {type: Number},
		per_par_sex_uid: {type: String},
		per_par_country_id: {type: Number},
		per_par_country_uid: {type: String},
		per_par_nacionality_id: {type: Number},
		per_par_nacionality_uid: {type: String},
		per_par_status_uid: {type: String},
		per_par_status_id: {type: Number},
		createdById: {type: Number},
		createdByUid: {type: String},
		updatedById: {type: Number},
		updatedByUid: {type: String},
		dueAt: {type: Date},
		createdAt: {type: Date},
		updatedAt: {type: Date},
	}),'es_people');
}



