const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esProfileViews = sequelize.define('esProfileViews', {
      uid: DataTypes.STRING,
      pro_id: DataTypes.INTEGER,
      pro_uid: DataTypes.STRING,
      vie_id: DataTypes.INTEGER,
      vie_uid: DataTypes.STRING,
      pro_vie_par_status_id: DataTypes.INTEGER,
      pro_vie_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_profile_views'
    });
    esProfileViews.associate = (models) => {
      // associations can be defined here

    };
    return esProfileViews;
  };
} else {
  module.exports = mongoose.model("esProfileViews", new Schema({
    id: {type: Number},
    uid: {type: String},
    pro_id: {type: Number},
    pro_uid: {type: String},
    vie_id: {type: Number},
    vie_uid: {type: String},
    pro_vie_par_status_id: {type: Number},
    pro_vie_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_profile_views');
}



