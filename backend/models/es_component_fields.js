const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esViews = sequelize.define('esComponentFields', {
      uid: DataTypes.STRING,
      com_fie_par_status_id: DataTypes.INTEGER,
      com_fie_par_status_uid: DataTypes.STRING,
      com_fie_par_severity_id: DataTypes.INTEGER,
      com_fie_par_severity_uid: DataTypes.STRING,
      com_fie_severity_description: DataTypes.STRING,
      com_id: DataTypes.INTEGER,
      com_uid: DataTypes.STRING,
      fie_id: DataTypes.INTEGER,
      fie_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_component_fields'
    });
    esViews.associate = (models) => {
      // associations can be defined here

    };
    return esViews;
  };
} else {
  module.exports = mongoose.model("esComponentFields", new Schema({
    id: {type: Number},
    uid: {type: String},
    com_fie_par_status_id: {type: Number},
    com_fie_par_status_uid: {type: String},
    com_fie_par_severity_id: {type: Number},
    com_fie_par_severity_uid: {type: String},
    com_fie_severity_description: {type: String},
    com_id: {type: Number},
    com_uid: {type: String},
    fie_id: {type: Number},
    fie_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_component_fields');
}






