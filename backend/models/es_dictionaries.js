const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esDictionaries = sequelize.define('esDictionaries', {
      uid: DataTypes.STRING,
      dic_code: DataTypes.STRING,
      dic_description: DataTypes.STRING,
      dic_par_status_id: DataTypes.INTEGER,
      dic_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_dictionaries'
    });
    esDictionaries.associate = (models) => {
      // associations can be defined here

    };
    return esDictionaries
  };
} else {
  module.exports = mongoose.model("esDictionaries", new Schema({
    id: {type: Number},
    uid: {type: String},
    dic_code: {type: String},
    dic_description: {type: String},
    dic_par_status_id: {type: Number},
    dic_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }), 'es_dictionaries');
}

