const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if (sql) {
	module.exports = (sequelize, DataTypes) => {
		const esProfileModules = sequelize.define('esProfileModules', {
			uid: DataTypes.STRING,
			pro_id: DataTypes.INTEGER,
			pro_uid: DataTypes.STRING,
			mod_id: DataTypes.INTEGER,
			mod_uid: DataTypes.STRING,
			pro_mod_par_status_id: DataTypes.INTEGER,
			pro_mod_par_status_uid: DataTypes.STRING,
			createdById: DataTypes.INTEGER,
			createdByUid: DataTypes.STRING,
			updatedById: DataTypes.INTEGER,
			updatedByUid: DataTypes.STRING,
			dueAt: DataTypes.DATE,
		}, {
			tableName: 'es_profile_modules'
		});
		esProfileModules.associate = (models) => {
			// associations can be defined here

		};
		return esProfileModules;
	};
} else {
	module.exports = mongoose.model("esProfileModules", new Schema({
		id: {type: Number},
		uid: {type: String},
		pro_id: {type: Number},
		pro_uid: {type: String},
		mod_id: {type: Number},
		mod_uid: {type: String},
		pro_mod_par_status_id: {type: Number},
		pro_mod_par_status_uid: {type: String},
		createdById: {type: Number},
		createdByUid: {type: String},
		updatedById: {type: Number},
		updatedByUid: {type: String},
		dueAt: {type: Date},
		createdAt: {type: Date},
		updatedAt: {type: Date},
	}), 'es_profile_modules');
}



