const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esObjects = sequelize.define('esObjects', {
      uid: DataTypes.STRING,
      obj_description: DataTypes.STRING,
      obj_par_status_id: DataTypes.INTEGER,
      obj_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_objects'
    });
    esObjects.associate = (models) => {
      // associations can be defined here

    };
    return esObjects;
  };
} else {
  module.exports = mongoose.model("esObjects", new Schema({
    id: {type: Number},
    uid: {type: String},
    obj_description: {type: String},
    obj_par_status_id: {type: Number},
    obj_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_objects');
}




