const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esLogs = sequelize.define('esLogs', {
      uid: DataTypes.STRING,
      log_description: DataTypes.STRING,
      log_par_severity_id: DataTypes.INTEGER,
      log_par_severity_uid: DataTypes.STRING,
      log_par_status_id: DataTypes.INTEGER,
      log_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_logs'
    });
    esLogs.associate = (models) => {
      // associations can be defined here

    };
    return esLogs;
  };
} else {
  module.exports = mongoose.model("esLogs", new Schema({
    id: {type: Number},
    uid: {type: String},
    log_description: {type: String},
    log_par_severity_id: {type: Number},
    log_par_severity_uid: {type: String},
    log_par_status_id: {type: Number},
    log_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_logs');
}






