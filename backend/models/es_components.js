const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esComponents = sequelize.define('esComponents', {
      uid: DataTypes.STRING,
      vie_id: DataTypes.INTEGER,
      vie_uid: DataTypes.STRING,
      com_code: DataTypes.STRING,
      com_tag: DataTypes.STRING,
      com_description: DataTypes.STRING,
      com_par_type_id: DataTypes.INTEGER,
      com_par_type_uid: DataTypes.STRING,
      com_par_status_id: DataTypes.INTEGER,
      com_par_status_uid: DataTypes.STRING,
      com_parent_id: DataTypes.INTEGER,
      com_parent_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_components'
    });
    esComponents.associate = (models) => {
      // associations can be defined here

    };
    return esComponents;
  };
} else {
  module.exports = mongoose.model("esComponents", new Schema({
    id: {type: Number},
    uid: {type: String},
    vie_id: {type: Number},
    vie_uid: {type: String},
    com_code: {type: String},
    com_tag: {type: String},
    com_description: {type: String},
    com_par_type_id: {type: Number},
    com_par_type_uid: {type: String},
    com_par_status_id: {type: Number},
    com_par_status_uid: {type: String},
    com_parent_id: {type: Number},
    com_parent_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_components');
}

