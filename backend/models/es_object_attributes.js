const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esObjectAttributes = sequelize.define('esObjectAttributes', {
      uid: DataTypes.STRING,
      obj_id: DataTypes.INTEGER,
      obj_uid: DataTypes.STRING,
      att_id: DataTypes.INTEGER,
      att_uid: DataTypes.STRING,
      obj_att_default_value: DataTypes.STRING,
      obj_att_par_editable_id: DataTypes.INTEGER,
      obj_att_par_editable_uid: DataTypes.STRING,
      obj_att_par_interface_behavior_id: DataTypes.INTEGER,
      obj_att_par_interface_behavior_uid: DataTypes.STRING,
      obj_att_par_status_id: DataTypes.INTEGER,
      obj_att_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_object_attributes'
    });
    esObjectAttributes.associate = (models) => {
      // associations can be defined here

    };
    return esObjectAttributes;
  };
} else {
  module.exports = mongoose.model("esObjectAttributes", new Schema({
    id: {type: Number},
    uid: {type: String},
    obj_id: {type: Number},
    obj_uid: {type: String},
    att_id: {type: Number},
    att_uid: {type: String},
    obj_att_default_value: {type: String},
    obj_att_par_editable_id: {type: Number},
    obj_att_par_editable_uid: {type: String},
    obj_att_par_interface_behavior_id: {type: Number},
    obj_att_par_interface_behavior_uid: {type: String},
    obj_att_par_status_id: {type: Number},
    obj_att_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_object_attributes');
}




