const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esPersonAttributes = sequelize.define('esPersonAttributes', {
      uid: DataTypes.STRING,
      per_id: DataTypes.INTEGER,
      per_uid: DataTypes.STRING,
      att_id: DataTypes.INTEGER,
      att_uid: DataTypes.STRING,
      per_att_value: DataTypes.STRING,
      per_att_par_status_id: DataTypes.INTEGER,
      per_att_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_person_attributes'
    });
    esPersonAttributes.associate = (models) => {
      // associations can be defined here

    };
    return esPersonAttributes;
  };
} else {
  module.exports = mongoose.model("esPersonAttributes", new Schema({
    id: {type: Number},
    uid: {type: String},
    per_id: {type: Number},
    per_uid: {type: String},
    att_id: {type: Number},
    att_uid: {type: String},
    per_att_value: {type: String},
    per_att_par_status_id: {type: Number},
    per_att_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_person_attributes');
}



