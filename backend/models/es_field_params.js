const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esViews = sequelize.define('esFieldParams', {
      uid: DataTypes.STRING,
      fie_par_status_id: DataTypes.INTEGER,
      fie_par_status_uid: DataTypes.STRING,
      fie_id: DataTypes.INTEGER,
      fie_uid: DataTypes.STRING,
      par_id: DataTypes.INTEGER,
      par_uid: DataTypes.STRING,
      fie_par_severity_id: DataTypes.INTEGER,
      fie_par_severity_uid: DataTypes.STRING,
      fie_par_languaje_id: DataTypes.INTEGER,
      fie_par_languaje_uid: DataTypes.STRING,
      fie_par_description: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_field_params'
    });
    esViews.associate = (models) => {
      // associations can be defined here

    };
    return esViews;
  };
} else {
  module.exports = mongoose.model("esFieldParams", new Schema({
    id: {type: Number},
    uid: {type: String},
    fie_par_status_id: {type: Number},
    fie_par_status_uid: {type: String},
    fie_id: {type: Number},
    fie_uid: {type: String},
    par_id: {type: Number},
    par_uid: {type: String},
    fie_par_severity_id: {type: Number},
    fie_par_severity_uid: {type: String},
    fie_par_languaje_id: {type: Number},
    fie_par_languaje_uid: {type: String},
    fie_par_description: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_field_params');
}






