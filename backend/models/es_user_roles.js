const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esUserRoles = sequelize.define('esUserRoles', {
      uid: DataTypes.STRING,
      usr_id: DataTypes.INTEGER,
      usr_uid: DataTypes.STRING,
      rol_id: DataTypes.INTEGER,
      rol_uid: DataTypes.STRING,
      usr_rol_par_status_id: DataTypes.INTEGER,
      usr_rol_par_status_uid: DataTypes.STRING,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_user_roles'
    });
    esUserRoles.associate = (models) => {
      // associations can be defined here

    };
    return esUserRoles;
  };
} else {
  module.exports = mongoose.model("esUserRoles", new Schema({
    id: {type: Number},
    uid: {type: String},
    usr_id: {type: Number},
    usr_uid: {type: String},
    rol_id: {type: Number},
    rol_uid: {type: String},
    usr_rol_par_status_id: {type: Number},
    usr_rol_par_status_uid: {type: String},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_user_roles');
}






