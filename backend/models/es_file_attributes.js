const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if(sql) {
  module.exports = (sequelize, DataTypes) => {
    const esFileAttributes = sequelize.define('esFileAttributes', {
      uid: DataTypes.STRING,
      fil_id: DataTypes.INTEGER,
      fil_uid: DataTypes.STRING,
      att_id: DataTypes.INTEGER,
      att_uid: DataTypes.STRING,
      fil_att_value: DataTypes.STRING,
      fil_att_par_status_uid: DataTypes.STRING,
      fil_att_par_status_id: DataTypes.INTEGER,
      createdById: DataTypes.INTEGER,
      createdByUid: DataTypes.STRING,
      updatedById: DataTypes.INTEGER,
      updatedByUid: DataTypes.STRING,
      dueAt: DataTypes.DATE,
    }, {
      tableName:'es_file_attributes'
    });
    esFileAttributes.associate = (models) => {
      // associations can be defined here

    };
    return esFileAttributes;
  };
} else {
  module.exports = mongoose.model("esFileAttributes", new Schema({
    id: {type: Number},
    uid: {type: String},
    fil_id: {type: Number},
    fil_uid: {type: String},
    att_id: {type: Number},
    att_uid: {type: String},
    fil_att_value: {type: String},
    fil_att_par_status_uid: {type: String},
    fil_att_par_status_id: {type: Number},
    createdById: {type: Number},
    createdByUid: {type: String},
    updatedById: {type: Number},
    updatedByUid: {type: String},
    dueAt: {type: Date},
    createdAt: {type: Date},
    updatedAt: {type: Date},
  }),'es_file_attributes');
}




