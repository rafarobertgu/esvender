const mongoose = require("mongoose");
const {Schema} = mongoose;
const sql = process.env.SQL;

'use strict';
if (sql) {
	module.exports = (sequelize, DataTypes) => {
		const esProfileComponents = sequelize.define('esProfileComponents', {
			uid: DataTypes.STRING,
			pro_id: DataTypes.INTEGER,
			pro_uid: DataTypes.STRING,
			com_id: DataTypes.INTEGER,
			com_uid: DataTypes.STRING,
			pro_com_par_status_id: DataTypes.INTEGER,
			pro_com_par_status_uid: DataTypes.STRING,
			createdById: DataTypes.INTEGER,
			createdByUid: DataTypes.STRING,
			updatedById: DataTypes.INTEGER,
			updatedByUid: DataTypes.STRING,
			dueAt: DataTypes.DATE,
		}, {
			tableName: 'es_profile_components'
		});
		esProfileComponents.associate = (models) => {
			// associations can be defined here

		};
		return esProfileComponents;
	};
} else {
	module.exports = mongoose.model("esProfileComponents", new Schema({
		id: {type: Number},
		uid: {type: String},
		pro_id: {type: Number},
		pro_uid: {type: String},
		com_id: {type: Number},
		com_uid: {type: String},
		pro_com_par_status_id: {type: Number},
		pro_com_par_status_uid: {type: String},
		createdById: {type: Number},
		createdByUid: {type: String},
		updatedById: {type: Number},
		updatedByUid: {type: String},
		dueAt: {type: Date},
		createdAt: {type: Date},
		updatedAt: {type: Date},
	}), 'es_profile_components');
}



