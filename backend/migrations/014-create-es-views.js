'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('es_views', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uid: {
        type: Sequelize.STRING,
        unique: true
      },

      vie_module_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_modules'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      vie_module_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_modules'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      vie_code: {
        type: Sequelize.STRING
      },
      vie_description: {
        type: Sequelize.STRING
      },
      vie_route: {
        type: Sequelize.STRING
      },
      vie_params: {
        type: Sequelize.STRING
      },
      vie_return_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_views'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      vie_return_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_views'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      vie_icon: {
        type: Sequelize.STRING
      },

      vie_par_status_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_params'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      vie_par_status_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_params'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      createdById: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_users'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      createdByUid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_users'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      updatedByUid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_users'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      updatedById: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_users'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      dueAt: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('es_views');
  }
};
