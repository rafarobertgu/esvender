'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('es_params', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uid: {
        type: Sequelize.STRING,
        unique: true
      },

      par_cod: {
        type: Sequelize.STRING
      },
      par_description: {
        type: Sequelize.STRING
      },
      par_abbr: {
        type: Sequelize.STRING
      },

      par_dictionry_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_dictionaries'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      par_dictionry_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_dictionaries'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      par_order: {
        type: Sequelize.INTEGER
      },

      par_status_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_params'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      par_status_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_params'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      createdById: {
        type: Sequelize.INTEGER
      },
      createdByUid: {
        type: Sequelize.STRING
      },
      updatedById: {
        type: Sequelize.INTEGER
      },
      updatedByUid: {
        type: Sequelize.STRING
      },
      dueAt: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('es_params');
  }
};
