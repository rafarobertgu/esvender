'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('es_modules', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uid: {
        type: Sequelize.STRING,
        unique: true
      },

      mod_parent_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_modules'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      mod_parent_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_modules'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      mod_code: {
        type: Sequelize.STRING,
      },
      mod_description: {
        type: Sequelize.STRING,
      },
      mod_abbr: {
        type: Sequelize.STRING,
      },
      mod_icon: {
        type: Sequelize.STRING,
      },

      mod_par_status_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_params'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      mod_par_status_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_params'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      createdById: {
        type: Sequelize.INTEGER
      },
      createdByUid: {
        type: Sequelize.STRING
      },
      updatedById: {
        type: Sequelize.INTEGER
      },
      updatedByUid: {
        type: Sequelize.STRING
      },
      dueAt: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('es_modules');
  }
};
