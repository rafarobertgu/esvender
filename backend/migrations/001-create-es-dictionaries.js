'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('es_dictionaries', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uid: {
        type: Sequelize.STRING,
        unique: true
      },

      dic_code: {
        type: Sequelize.STRING
      },
      dic_description: {
        type: Sequelize.STRING
      },

      dic_par_status_id: {
        type: Sequelize.INTEGER
      },
      dic_par_status_uid: {
        type: Sequelize.STRING
      },

      createdById: {
        type: Sequelize.INTEGER
      },
      createdByUid: {
        type: Sequelize.STRING
      },
      updatedById: {
        type: Sequelize.INTEGER
      },
      updatedByUid: {
        type: Sequelize.STRING
      },
      dueAt: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('es_dictionaries');
  }
};
