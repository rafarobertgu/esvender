'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('es_auths', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uid: {
        type: Sequelize.STRING,
        unique: true
      },

      aut_par_status_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_params'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      aut_par_status_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_params'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      usr_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_users'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      usr_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_users'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      aut_data: {
        type: Sequelize.STRING
      },

      createdById: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_users'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      createdByUid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_users'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      updatedByUid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_users'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      updatedById: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_users'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      dueAt: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('es_auths');
  }
};
