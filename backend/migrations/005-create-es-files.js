'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('es_files', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uid: {
        type: Sequelize.STRING,
        unique: true
      },

      fil_rute: {
        type: Sequelize.STRING
      },

      fil_par_type_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_params'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      fil_par_type_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_params'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      fil_par_status_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_params'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      fil_par_status_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_params'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      createdById: {
        type: Sequelize.INTEGER
      },
      createdByUid: {
        type: Sequelize.STRING
      },
      updatedById: {
        type: Sequelize.INTEGER
      },
      updatedByUid: {
        type: Sequelize.STRING
      },
      dueAt: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('es_files');
  }
};
