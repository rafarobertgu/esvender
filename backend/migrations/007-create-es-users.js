'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('es_users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uid: {
        type: Sequelize.STRING,
        unique: true
      },

      usr_id: {
        type: Sequelize.STRING,
      },
      usr_username: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      usr_password: {
        type: Sequelize.STRING,
        allowNull: false
      },
      usr_mail: {
        type: Sequelize.STRING
      },
      usr_token: {
        type: Sequelize.STRING
      },
      usr_name: {
        type: Sequelize.STRING
      },
      usr_lastname: {
        type: Sequelize.STRING
      },
      usr_par_auth_strategy_id: {
        type: Sequelize.INTEGER
      },
      usr_par_auth_strategy_uid: {
        type: Sequelize.STRING
      },
      usr_accept_terms: {
        type: Sequelize.BOOLEAN
      },

      usr_rol_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_roles'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      usr_rol_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_roles'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      usr_mod_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_modules'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      usr_mod_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_modules'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      usr_person_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_people'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      usr_person_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_people'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      usr_fil_img_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_files'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      usr_fil_img_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_files'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      usr_last_login: {
        type: Sequelize.DATE
      },
      usr_pwd_change: {
        type: Sequelize.INTEGER
      },

      usr_par_status_uid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_params'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      usr_par_status_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_params'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },

      createdById: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_users'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      createdByUid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_users'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      updatedByUid: {
        type: Sequelize.STRING,
        references: {
          model: {tableName:'es_users'},
          key: 'uid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      updatedById: {
        type: Sequelize.INTEGER,
        references: {
          model: {tableName:'es_users'},
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      dueAt: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('es_users');
  }
};
