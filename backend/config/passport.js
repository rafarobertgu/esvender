const esUserService = require('../services/esUser.service');
const LocalStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const configAuth = require('../config/auth');
const passport = require('passport');
const models = require('../models');
const bcrypt = require("bcrypt");

module.exports = function () {
	passport.serializeUser((user, done) => {
    if(user) done(null, user);
	});

	passport.deserializeUser(async (id, done) => {
		try {
			await esUserService.findOneByUsrId(id).then(async (esUser) => {
				done(null, esUser);
			})
		} catch (error) {
			throw done(error);
		}
	});

	passport.use('local-signup', new LocalStrategy({
		usernameField: 'usr_username',
		passwordField: 'usr_password',
		passReqToCallback: true
	}, (req, username, password, done) => {
		process.nextTick(async () => {
			try {
				const esUser = await esUserService.findOneByUsrUserName(username);
				if (esUser) {
					return done(null, false, req.flash('signupMessage', 'That email'));
				} else {
					const newUser = await await esUserService.addEsUser(req.body);
					return done(null, newUser);
				}
			} catch (error) {
				throw done(error);
			}
		})
	}));

	passport.use('local-login', new LocalStrategy({
		usernameField: 'usr_username',
		passwordField: 'usr_password',
		passReqToCallback: true
	}, (req, username, password, done) => {
		process.nextTick(async () => {
			try {
				const esUser = await esUserService.findOneByUsrUserName(username);
				if (!esUser)
					return done(null, false, req.flash('loginMessage', 'No user found'));
				let compare = await bcrypt.compare(password, esUser.usr_password).then((resp)=>{return resp});
				if (!compare) {
					return done(null, false, req.flash('loginMessage', 'invalid user'));
				}
				return done(null, esUser);
			} catch (error) {
				throw done(error)
			}
		})
	}));

  // passport.use(new LocalStrategy(
  //   async (username, password, done) => {
  //     try {
  //       const esUser = await esUserService.findOneByUsrUserName(username);
  //       if (!esUser)
  //         return done(null, false, req.flash('loginMessage', 'No user found'));
  //       let compare = await bcrypt.compare(password, esUser.usr_password).then((resp)=>{return resp});
  //       if (!compare) {
  //         return done(null, false, req.flash('loginMessage', 'invalid user'));
  //       }
  //       return done(null, esUser);
  //     } catch (error) {
  //       throw done(error)
  //     }
  //   }
  // ));

	passport.use(new FacebookStrategy({
		clientID: configAuth.facebookAuth.clientId,
		clientSecret: configAuth.facebookAuth.clientSecret,
		callbackURL: configAuth.facebookAuth.callbackURL
	}, (accessToken, refreshToken, profile, done) => {
		process.nextTick(async () => {
			try {
				let esUser = await esUserService.findOneByUsrId(profile.id);
				const newEsUser = {
					usr_id: profile.id,
					usr_token: accessToken,
					usr_name: profile.displayName,
					usr_mail: profile.emails ? profile.emails[0].value : '',
					usr_username: profile.emails ? profile.emails[0].value : '',
					usr_par_auth_strategy_uid: 'facebook'
				};
				if (esUser)
					esUser = await esUserService.updateEsUserByUsrId(profile.id, newEsUser);
				else {
					esUser = await esUserService.addEsUser(newEsUser);
				}
				return done(null, esUser);
			} catch (error) {
				done(error);
			}
		})
	}));
};
